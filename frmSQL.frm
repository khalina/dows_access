VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "VSFLEX7D.OCX"
Object = "{12A54AED-8C03-11D4-8E53-ED61E578151C}#1.0#0"; "ABCOOLBAR.OCX"
Begin VB.Form frmSQL 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "����������� ��������"
   ClientHeight    =   3855
   ClientLeft      =   1800
   ClientTop       =   5100
   ClientWidth     =   11175
   Icon            =   "frmSQL.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   11175
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F1 
      Height          =   735
      Left            =   0
      TabIndex        =   17
      Top             =   3120
      Width           =   11175
      Begin VB.CommandButton cmdOK 
         Caption         =   "��"
         Height          =   375
         Left            =   7440
         TabIndex        =   19
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "������"
         Height          =   375
         Left            =   9240
         TabIndex        =   18
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame F4 
      Height          =   2655
      Left            =   0
      TabIndex        =   15
      Top             =   375
      Width           =   11175
      Begin VB.CheckBox ckIns 
         Caption         =   "����� ������ ��������� � ������� ����� ���������"
         Height          =   375
         Left            =   1560
         TabIndex        =   27
         Top             =   2160
         Value           =   1  'Checked
         Width           =   4455
      End
      Begin VB.TextBox txtSQLQuery 
         Height          =   1215
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   26
         Top             =   840
         Width           =   10935
      End
      Begin VB.CommandButton cmdDel 
         Caption         =   "�������"
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Top             =   2160
         Width           =   1335
      End
      Begin VB.CommandButton cmdCondition 
         Caption         =   "�������"
         Height          =   375
         Left            =   9840
         TabIndex        =   24
         Top             =   2160
         Width           =   1215
      End
      Begin VB.CommandButton cmdOr 
         Caption         =   "���"
         Height          =   375
         Left            =   9000
         TabIndex        =   23
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdAnd 
         Caption         =   "�"
         Height          =   375
         Left            =   8400
         TabIndex        =   22
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdCS 
         Caption         =   ")"
         Height          =   375
         Left            =   7560
         TabIndex        =   21
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdOS 
         Caption         =   "("
         Height          =   375
         Left            =   6960
         TabIndex        =   20
         Top             =   2160
         Width           =   615
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblConditions 
         Height          =   560
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   10935
         _cx             =   19288
         _cy             =   988
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   16777215
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   0
         SelectionMode   =   0
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   0   'False
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Frame F3 
      Height          =   2655
      Left            =   0
      TabIndex        =   2
      Top             =   375
      Width           =   11175
      Begin VB.CommandButton cmdFieldClear 
         Caption         =   "��������"
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   2160
         Width           =   1335
      End
      Begin VB.CommandButton cmdFieldDel 
         Caption         =   "�������"
         Height          =   375
         Left            =   9960
         TabIndex        =   12
         Top             =   2160
         Width           =   1095
      End
      Begin VB.CommandButton cmdFieldAdd 
         Caption         =   "��������"
         Height          =   375
         Left            =   8880
         TabIndex        =   11
         Top             =   2160
         Width           =   1095
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblShowFields 
         Height          =   1845
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   10935
         _cx             =   19288
         _cy             =   3254
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   0
         SelectionMode   =   0
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         Begin VB.ComboBox comboSort 
            Height          =   315
            ItemData        =   "frmSQL.frx":030A
            Left            =   2160
            List            =   "frmSQL.frx":0317
            TabIndex        =   13
            Top             =   480
            Visible         =   0   'False
            Width           =   2055
         End
      End
   End
   Begin VB.Frame F2 
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   375
      Width           =   11175
      Begin VB.CommandButton tblTableDel 
         Caption         =   "<< �������"
         Height          =   375
         Left            =   3360
         TabIndex        =   7
         Top             =   720
         Width           =   1215
      End
      Begin VB.CommandButton cmdTableAdd 
         Caption         =   "�������� >>"
         Height          =   375
         Left            =   3360
         TabIndex        =   6
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdTableClear 
         Caption         =   "�����"
         Height          =   375
         Left            =   3360
         TabIndex        =   5
         Top             =   2040
         Width           =   1215
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblUsedTables 
         Height          =   2295
         Left            =   4680
         TabIndex        =   1
         Top             =   240
         Width           =   6375
         _cx             =   11245
         _cy             =   4048
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   0
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   315
         RowHeightMax    =   315
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         Begin VB.ComboBox comboLinks 
            Height          =   315
            Left            =   3720
            TabIndex        =   9
            Top             =   1200
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VSFlex7DAOCtl.VSFlexGrid tblLinks 
            Height          =   1335
            Left            =   120
            TabIndex        =   8
            Top             =   720
            Visible         =   0   'False
            Width           =   3375
            _cx             =   5953
            _cy             =   2355
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblTables 
         Height          =   2265
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   3135
         _cx             =   5530
         _cy             =   3995
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483643
         GridColorFixed  =   -2147483643
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483643
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin CoolBar.cToolbar cToolbar1 
      Left            =   1800
      Top             =   0
      _ExtentX        =   2143
      _ExtentY        =   661
   End
   Begin CoolBar.cReBar cReBar1 
      Left            =   0
      Top             =   0
      _ExtentX        =   2566
      _ExtentY        =   661
   End
   Begin VB.Label strlen 
      AutoSize        =   -1  'True
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   0
      TabIndex        =   10
      Top             =   3960
      Visible         =   0   'False
      Width           =   960
   End
End
Attribute VB_Name = "frmSQL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private BeforeCol As Long

''-----------------------
'Public Category As String
'
'Public LTable As Integer
'Public LField As Integer
'
'Public Operation As String
'
'Public RString As String
'
'Public RTable As Integer
'Public RField As Integer
''-----------------------
'
'Private Type Condition
'    Category As String
'
'    LTable As Integer
'    LField As Integer
'
'    Operation As String
'
'    RString As String
'
'    RTable As Integer
'    RField As Integer
'End Type
'Private ConditionsArray() As Condition
'Private ConditionsCount As Integer
'
'Public SelectedTable As Integer
'Public SelectedField As Integer

Private Const TForeColorP = &H8000000B
Private Const TForeColorA = &H80000008

Sub CreateToolBars()
cToolbar1.CreateToolbar 16, , True, True
cToolbar1.AddButton , , , , "������������ �������", CTBCheckGroup
cToolbar1.AddButton , , , , "����� � ����������", CTBCheckGroup
cToolbar1.AddButton , , , , "������� ������", CTBCheckGroup

cReBar1.CreateRebar Me.hWnd
cReBar1.AddBandByHwnd cToolbar1.hWnd
End Sub

Sub ExecuteSQLQuery()
Dim SQLQuery As String

Dim i As Long
Dim j As Long

Dim TableField As Variant
Dim TFCount As Integer

Dim FirstLAdded As Boolean

SQLQuery = "SELECT "

FirstLAdded = False
For i = 1 To tblShowFields.Cols - 1
    If (tblShowFields.TextMatrix(2, i) = "��") Then
        GetLinkList tblShowFields.ColData(i), TableField, TFCount
        
        If (Not FirstLAdded) Then
            If ((TablesArrey(TableField(0)).TableNameDB = "tblGroups") And (TablesArrey(TableField(0)).FieldsArrey(TableField(1))(1) = "GroupName")) Then
                SQLQuery = SQLQuery & "(Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) as FullGroupName"
            Else
                SQLQuery = SQLQuery & TablesArrey(TableField(0)).TableNameDB & "." & TablesArrey(TableField(0)).FieldsArrey(TableField(1))(1)
            End If
            FirstLAdded = True
        Else
            If ((TablesArrey(TableField(0)).TableNameDB = "tblGroups") And (TablesArrey(TableField(0)).FieldsArrey(TableField(1))(1) = "GroupName")) Then
                SQLQuery = SQLQuery & ", " & "(Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) as FullGroupName"
            Else
                SQLQuery = SQLQuery & ", " & TablesArrey(TableField(0)).TableNameDB & "." & TablesArrey(TableField(0)).FieldsArrey(TableField(1))(1)
            End If
        End If
        
    End If
Next i

SQLQuery = SQLQuery & " FROM "

For i = 0 To tblUsedTables.Cols - 1 Step 2
    SQLQuery = SQLQuery & tblUsedTables.TextMatrix(0, i + 1)
    
    If (i < tblUsedTables.Cols - 2) Then SQLQuery = SQLQuery & ", "
    If (i = tblUsedTables.Cols - 2) Then SQLQuery = SQLQuery & " "
Next i

Dim WhereAdded As Boolean

FirstLAdded = False
WhereAdded = False

For i = 0 To tblUsedTables.Cols - 1 Step 2
    For j = 1 To tblUsedTables.Rows - 1
        If (tblUsedTables.TextMatrix(j, i) = "" Or tblUsedTables.TextMatrix(j, i + 1) = "") Then Exit For
        
        If (tblUsedTables.Cell(flexcpForeColor, j, i) = TForeColorA) Then
            If (Not FirstLAdded) Then
                SQLQuery = SQLQuery & "WHERE (" & ConnectTables(tblUsedTables.TextMatrix(0, i + 1), tblUsedTables.TextMatrix(j, i + 1))
                FirstLAdded = True
                WhereAdded = True
            Else
                SQLQuery = SQLQuery & " AND " & ConnectTables(tblUsedTables.TextMatrix(0, i + 1), tblUsedTables.TextMatrix(j, i + 1))
            End If
        End If
    Next j
Next i

If (WhereAdded) Then SQLQuery = SQLQuery & ")"

If (tblConditions.Cols > 0) Then
    If (WhereAdded) Then
        SQLQuery = SQLQuery & " AND ("
    Else
        SQLQuery = SQLQuery & " WHERE ("
        WhereAdded = True
    End If
End If

Dim strCondition As String

For i = 0 To tblConditions.Cols - 1
    Select Case tblConditions.TextMatrix(1, i)
        Case "OB":
            SQLQuery = SQLQuery & " ("
            
        Case "CB":
            SQLQuery = SQLQuery & " )"
            
        Case "AND":
            SQLQuery = SQLQuery & " AND"
            
        Case "OR":
            SQLQuery = SQLQuery & " OR"
            
        Case "CONDITION":
        
            LTable = CInt(tblConditions.TextMatrix(2, i))
            LField = CInt(tblConditions.TextMatrix(3, i))
            Operation = CStr(tblConditions.TextMatrix(4, i))
            RString = CStr(tblConditions.TextMatrix(5, i))
            RTable = CInt(tblConditions.TextMatrix(6, i))
            RField = CInt(tblConditions.TextMatrix(7, i))

            strCondition = TablesArrey(LTable).TableNameDB & "." & TablesArrey(LTable).FieldsArrey(LField)(1)
            If (UCase(Operation) <> "���") Then
                strCondition = strCondition & " " & Operation & " "
            Else
                strCondition = strCondition & " LIKE "
            End If
            
            If ((RTable <> -1) And (RField <> -1)) Then
                strCondition = strCondition & TablesArrey(RTable).TableNameDB & "." & TablesArrey(RTable).FieldsArrey(RField)(1)
            Else
                Select Case TablesArrey(LTable).FieldsArrey(LField)(2)
                    Case "TEXT":
                        CheckString RString
                        strCondition = strCondition & "'" & RString & "'"
                    
                    Case "DATETIME":
                        strCondition = strCondition & "'" & RString & "'"
                        
                    Case "BYTE", "CURRENCY", "INTEGER", "LONG":
                        strCondition = strCondition & RString
                        
                End Select
            
            End If
            
            SQLQuery = SQLQuery & " " & strCondition

    End Select
Next i

If (tblConditions.Cols > 0) Then SQLQuery = SQLQuery & ")"

FirstLAdded = False
For i = 1 To tblShowFields.Cols - 1
    If (tblShowFields.TextMatrix(1, i) <> "�����") Then
        GetLinkList tblShowFields.ColData(i), TableField, TFCount
        
        If (Not FirstLAdded) Then
            SQLQuery = SQLQuery & " ORDER BY " & TablesArrey(TableField(0)).TableNameDB & "." & TablesArrey(TableField(0)).FieldsArrey(TableField(1))(1) & IIf(tblShowFields.TextMatrix(1, i) = "�� �����������", "", " DESC")
            FirstLAdded = True
        Else
            SQLQuery = SQLQuery & ", " & TablesArrey(TableField(0)).TableNameDB & "." & TablesArrey(TableField(0)).FieldsArrey(TableField(1))(1) & IIf(tblShowFields.TextMatrix(1, i) = "�� �����������", "", " DESC")
        End If
        
    End If
Next i

Dim DataArray As Variant
Dim DataCols As Integer
Dim DataRows As Long

CDB.SQLOpenTableToArrey MainDB, SQLQuery, DataArray, DataCols, DataRows

If (DataRows = 0) Then
    MsgBox "� ���� ������ ��� ��������, ��������������� ������ �������."
    Exit Sub
End If

Main.ShowMainGrid 1

Main.MainGrid.Cols = 1
Main.MainGrid.Rows = 1
Main.MainGrid.FixedCols = 1
Main.MainGrid.FixedRows = 1

Main.MainGrid.TextMatrix(0, 0) = "�"

For i = 1 To tblShowFields.Cols - 1
    If (tblShowFields.TextMatrix(2, i) = "��") Then
        Main.MainGrid.Cols = Main.MainGrid.Cols + 1
        Main.MainGrid.TextMatrix(0, Main.MainGrid.Cols - 1) = tblShowFields.TextMatrix(0, i)
    End If
Next i


For i = 0 To DataRows - 1
    Main.MainGrid.AddItem Main.MainGrid.Rows
    Main.sbMain.PanelText("2") = "�������� ����������� ������� (" & i & " �� " & (DataRows - 1) & ")"
    ProgBar Main.PProgress, ((i + 1) * 100) / DataRows
    Main.sbMain.RedrawPanel ("3")

    For j = 1 To Main.MainGrid.Cols - 1
        Main.MainGrid.TextMatrix(Main.MainGrid.Rows - 1, j) = DataArray(j - 1, i)
    Next j
Next i

ProgBar Main.PProgress, 0
Main.sbMain.RedrawPanel ("3")
Main.sbMain.PanelText("2") = "������"

ResizeColumns Main.MainGrid
Unload Me
End Sub

Function ConnectTables(Table1 As String, Table2 As String) As String
Dim LinksArray As Variant
Dim LinksCount As Integer

Dim T1ConnField As String
Dim T2ConnField As String

Dim i As Long
Dim j As Long
Dim k As Long

Dim Finded As Boolean

Finded = False
For i = 0 To SQLTablesCount - 1
    If (TablesArrey(i).TableNameDB = Table1) Then
        For j = 0 To TablesArrey(i).FieldsCount - 1
            If (TablesArrey(i).FieldsArrey(j)(4) <> "") Then
                GetLinkList CStr(TablesArrey(i).FieldsArrey(j)(4)), LinksArray, LinksCount
                For k = 0 To LinksCount - 1
                    If (LinksArray(k) = Table2) Then
                        T1ConnField = Table1 & "." & TablesArrey(i).FieldsArrey(j)(1)
                        Finded = True
                        Exit For
                    End If
                Next k
            End If
            
            If (Finded) Then Exit For
        Next j
        
        Exit For
    End If
Next i

Finded = False
For i = 0 To SQLTablesCount - 1
    If (TablesArrey(i).TableNameDB = Table2) Then
        For j = 0 To TablesArrey(i).FieldsCount - 1
            If (TablesArrey(i).FieldsArrey(j)(4) <> "") Then
                GetLinkList CStr(TablesArrey(i).FieldsArrey(j)(4)), LinksArray, LinksCount
                For k = 0 To LinksCount - 1
                    If (LinksArray(k) = Table1) Then
                        T2ConnField = Table2 & "." & TablesArrey(i).FieldsArrey(j)(1)
                        Finded = True
                        Exit For
                    End If
                Next k
            End If
            
            If (Finded) Then Exit For
        Next j
        
        Exit For
    End If
Next i

ConnectTables = T1ConnField & " = " & T2ConnField
End Function

Function CheckSQLConditions(T As VSFlexGrid) As Boolean
Dim BeforeData As String
Dim i As Long

Dim Branches As Integer

BeforeData = ""
Branches = 0

For i = 0 To T.Cols - 1
    If (T.TextMatrix(1, i) = "OB") Then Branches = Branches + 1
    If (T.TextMatrix(1, i) = "CB") Then Branches = Branches - 1
Next i

If (Branches <> 0) Then
    If (Branches > 0) Then MsgBox "��������� ����������� ������", vbOKOnly, "������"
    If (Branches < 0) Then MsgBox "��������� ����������� ������", vbOKOnly, "������"
    T.Col = 0
    CheckSQLConditions = False
    Exit Function
End If

For i = 0 To T.Cols - 1
    Select Case T.TextMatrix(1, i)
        Case "OB":
            If (BeforeData <> "") Then
                Select Case BeforeData
                    Case "CB":
                        MsgBox "������ � �������� ������", vbOKOnly, "������"
                        T.Col = i
                        CheckSQLConditions = False
                        Exit Function
                        
                    Case "CONDITION":
                        MsgBox "������ � �������� ������", vbOKOnly, "������"
                        T.Col = i
                        CheckSQLConditions = False
                        Exit Function

                End Select
            End If
            
        Case "CB":
            If (BeforeData = "") Then
                MsgBox "������ � �������� ������", vbOKOnly, "������"
                T.Col = i
                CheckSQLConditions = False
                Exit Function
            End If
            
            Select Case BeforeData
                Case "OB":
                    MsgBox "������ � �������� ������", vbOKOnly, "������"
                    T.Col = i
                    CheckSQLConditions = False
                    Exit Function
                    
                Case "AND", "OR":
                    MsgBox "������ � �������� ������", vbOKOnly, "������"
                    T.Col = i
                    CheckSQLConditions = False
                    Exit Function
                    
            End Select
            
        Case "OR", "AND":
            If (BeforeData = "") Then
                MsgBox "������ � �������� ������", vbOKOnly, "������"
                T.Col = i
                CheckSQLConditions = False
                Exit Function
            End If
        
            Select Case BeforeData
                Case "OB":
                    MsgBox "������ � �������� ������", vbOKOnly, "������"
                    T.Col = i
                    CheckSQLConditions = False
                    Exit Function
                    
                Case "AND", "OR":
                    MsgBox "������ � �������� ������", vbOKOnly, "������"
                    T.Col = i
                    CheckSQLConditions = False
                    Exit Function
                    
            End Select
            
        Case "CONDITION":
            If (BeforeData <> "") Then
                Select Case BeforeData
                    Case "CB":
                        MsgBox "������ � �������� ������", vbOKOnly, "������"
                        T.Col = i
                        CheckSQLConditions = False
                        Exit Function
                        
                    Case "CONDITION":
                        MsgBox "������ � �������� ������", vbOKOnly, "������"
                        T.Col = i
                        CheckSQLConditions = False
                        Exit Function
                        
                End Select
            End If
            
    End Select
    
    BeforeData = T.TextMatrix(1, i)
Next i

Select Case BeforeData
    Case "OB":
        MsgBox "������ � �������� ������", vbOKOnly, "������"
        T.Col = i - 1
        CheckSQLConditions = False
        Exit Function
        
    Case "AND", "OR":
        MsgBox "������ � �������� ������", vbOKOnly, "������"
        T.Col = i - 1
        CheckSQLConditions = False
        Exit Function
        
End Select

CheckSQLConditions = True

End Function

Sub FillSQLTextBox(T As VSFlexGrid, TB As TextBox)
Dim i As Long

TB.Text = ""

For i = 0 To T.Cols - 1
    TB.Text = TB.Text & T.TextMatrix(0, i) & " "
Next i
End Sub

Sub RemoveCol(T As VSFlexGrid, Col As Integer)
Dim i As Integer
Dim j As Long

If (Col < 0) Then Exit Sub
If (Col >= T.Cols) Then Exit Sub

For i = Col To T.Cols - 2
    T.ColData(i) = T.ColData(i + 1)
    For j = 0 To T.Rows - 1
        T.TextMatrix(j, i) = T.TextMatrix(j, i + 1)
    Next j
Next i

T.Cols = T.Cols - 1
End Sub

Function InsertCol(T As VSFlexGrid, ColPos As Integer) As Boolean
Dim i As Integer
Dim j As Long

If (ColPos < 0) Then Exit Function
If (ColPos > T.Cols) Then Exit Function
If (ColPos = T.Cols) Then
    T.Cols = T.Cols + 1
    Exit Function
End If

T.Cols = T.Cols + 1
For i = 1 To T.Cols - ColPos - 1
    T.ColData(T.Cols - i) = T.ColData(T.Cols - i - 1)
    T.ColData(T.Cols - i - 1) = ""
    
    For j = 0 To T.Rows - 1
        T.TextMatrix(j, T.Cols - i) = T.TextMatrix(j, T.Cols - i - 1)
        T.TextMatrix(j, T.Cols - i - 1) = ""
    Next j
Next i

End Function

Sub FillTablesArrey()

'������� �� ����� (tblConfirmList)------------------------------------------------
    TablesArrey(0).TableNameDB = "tblConfirmList"
    TablesArrey(0).TableNameRu = "������� �� �����"
    TablesArrey(0).Status = 0
    TablesArrey(0).FieldsCount = 16
    
    TablesArrey(0).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "", 0), _
        Array("����� � ���������", "IDStudent", "LONG NOT NULL", 0, "tblStudents|", 0), _
        Array("���������", "Faculty", "TEXT", 200, "", 0), _
        Array("� �������", "CNumber", "TEXT", 20, "", 0), _
        Array("���� ��������", "Date1", "DATETIME", 0, "", 0), _
        Array("������������� ��", "Date2", "DATETIME", 0, "", 0), _
        Array("����", "Course", "BYTE", 0, "", 0), _
        Array("����� ��������", "EducationForm", "TEXT", 100, "", 0), _
        Array("���� 1", "Field1", "TEXT", 100, "", 0), _
        Array("���� 2", "Field2", "TEXT", 100, "", 0), _
        Array("���� 3", "Field3", "TEXT", 100, "", 0), _
        Array("���� 4", "Field4", "TEXT", 100, "", 0), _
        Array("���� 5", "Field5", "TEXT", 100, "", 0), _
        Array("���� 6", "Field6", "TEXT", 100, "", 0), _
        Array("���� 7", "Field7", "TEXT", 100, "", 0), _
        Array("���� 8", "Field8", "TEXT", 100, "", 0))
'---------------------------------------------------------------------------------
    
'������� ��������� (tblNominalGrants)---------------------------------------------
    TablesArrey(1).TableNameDB = "tblNominalGrants"
    TablesArrey(1).TableNameRu = "������� ���������"
    TablesArrey(1).Status = 0
    TablesArrey(1).FieldsCount = 3
    
    TablesArrey(1).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblStudents|", 0), _
        Array("��� ���������", "GrantName", "TEXT", 100, "", 0), _
        Array("������ ���������", "Grant", "CURRENCY", 0, "", 0))

'---------------------------------------------------------------------------------
    
'�������� (tblStudents)-----------------------------------------------------------
    TablesArrey(2).TableNameDB = "tblStudents"
    TablesArrey(2).TableNameRu = "��������"
    TablesArrey(2).Status = 0
    TablesArrey(2).FieldsCount = 27
    
    TablesArrey(2).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblConfirmList|tblSheets|tblSheetsTemporary|tblDegreeThemes|tblStudEvents|", 0), _
        Array("����� � �������", "IDGroup", "LONG NOT NULL", 0, "tblGroups|", 0), _
        Array("����� � ��������", "IDFaculty", "LONG NOT NULL", 0, "tblFaculty|", 0), _
        Array("����� � ����������", "IDCategory", "LONG NOT NULL", 0, "tblCategories|", 0), _
        Array("����� � ������� ����������", "IDNominalGrant", "LONG NOT NULL", 0, "tblNominalGrants|", 0), _
        Array("�������", "Surname", "TEXT", 40, "", 0), Array("���", "Name", "TEXT", 40, "", 0), Array("��������", "Patronymic", "TEXT", 40, "", 0), _
        Array("�����", "Address", "TEXT", 255, "", 0), Array("����� � ��������", "KharkovAddress", "TEXT", 255, "", 0), _
        Array("���� ��������", "DateOfBorn", "DATETIME", 0, "", 0), Array("��������������", "Nationality", "TEXT", 50, "", 0), _
        Array("�����������", "Education", "TEXT", 100, "", 0), Array("�������� ���������", "MaritalStatus", "TEXT", 50, "", 0), _
        Array("����� ������", "FormOfPayment", "TEXT", 20, "", 0), Array("����������", "IsForeigner", "BYTE", 0, "", 0), _
        Array("�������� � ���������", "InformationOnTheParents", "TEXT", 255, "", 0), _
        Array("�������������", "Gratitude", "TEXT", 100, "", 0), Array("��������", "Reprimands", "TEXT", 100, "", 0), Array("�� ������� � ������ �� ��������", "CReprimands", "TEXT", 200, "", 0), _
        Array("� �������� ������", "OffsetNumber", "TEXT", 10, "", 0), Array("� ������������� ������", "LibNumber", "TEXT", 10, "", 0), Array("����������", "IsForeigner", "BYTE", 0, "", 0), _
        Array("���� ����������", "DeductionDate", "DATETIME", 0, "", 0), _
        Array("����� ������� �� ����������", "DeductionOrderNumber", "TEXT", 20, "", 0), _
        Array("������� ����������", "DeductionReason", "TEXT", 50, "", 0), _
        Array("������", "IsActive", "BYTE", 0, "", 0), _
        Array("�������������� ����������", "AddPreparing", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------
    
'�������� ������������ (tblSheets)------------------------------------------------
    TablesArrey(3).TableNameDB = "tblSheets"
    TablesArrey(3).TableNameRu = "�������� ������������"
    TablesArrey(3).Status = 0
    TablesArrey(3).FieldsCount = 4
    
    TablesArrey(3).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "", 0), _
        Array("����� � ��������������� ����������", "IDExam", "LONG NOT NULL", 0, "tblExaminations|", 0), _
        Array("����� � ���������", "IDStudent", "LONG NOT NULL", 0, "tblStudents|", 0), _
        Array("������", "Mark", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------
    
'�������������� ������������ (tblSheetsTemporary)---------------------------------
    TablesArrey(4).TableNameDB = "tblSheetsTemporary"
    TablesArrey(4).TableNameRu = "�������������� ������������"
    TablesArrey(4).Status = 0
    TablesArrey(4).FieldsCount = 4
    
    TablesArrey(4).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "", 0), _
        Array("����� � ��������������� ����������", "IDExam", "LONG NOT NULL", 0, "tblExaminations|", 0), _
        Array("����� � ���������", "IDStudent", "LONG NOT NULL", 0, "tblStudents|", 0), _
        Array("������", "Mark", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------
    
'������������� (tblLecturers)-----------------------------------------------------
    TablesArrey(5).TableNameDB = "tblLecturers"
    TablesArrey(5).TableNameRu = "�������������"
    TablesArrey(5).Status = 0
    TablesArrey(5).FieldsCount = 12
    
    TablesArrey(5).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblExaminations|tblDegreeThemes|", 0), _
        Array("����� � ��������", "IDFaculty", "LONG NOT NULL", 0, "tblFaculty|", 0), _
        Array("�������", "Surname", "TEXT", 40, "", 0), Array("���", "Name", "TEXT", 40, "", 0), Array("��������", "Patronymic", "TEXT", 40, "", 0), _
        Array("���������", "Post", "TEXT", 50, "", 0), _
        Array("������ �������", "ScientificDegree", "TEXT", 200, "", 0), _
        Array("�����", "Address", "TEXT", 255, "", 0), _
        Array("�������", "Telephone", "TEXT", 20, "", 0), _
        Array("�����������", "Education", "TEXT", 100, "", 0), _
        Array("���", "Year", "INTEGER", 0, "", 0), _
        Array("������", "IsActive", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------

'��������� ��������� (tblCategories)----------------------------------------------
    TablesArrey(6).TableNameDB = "tblCategories"
    TablesArrey(6).TableNameRu = "��������� ���������"
    TablesArrey(6).Status = 0
    TablesArrey(6).FieldsCount = 3
    
    TablesArrey(6).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblStudents|", 0), _
        Array("�������� ���������", "CategoryName", "TEXT", 100, "", 0), _
        Array("�������� (%)", "PercentExtra", "CURRENCY", 0, "", 0))
'---------------------------------------------------------------------------------
    
'��������������� ��������� (tblExaminations)--------------------------------------
    TablesArrey(7).TableNameDB = "tblExaminations"
    TablesArrey(7).TableNameRu = "��������������� ���������"
    TablesArrey(7).Status = 0
    TablesArrey(7).FieldsCount = 6
    
    TablesArrey(7).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblSheets|tblSheetsTemporary|", 0), _
        Array("����� � ������� ������", "IDEP", "LONG NOT NULL", 0, "tblEducationalPlan|", 0), _
        Array("����� � �������", "IDGroup", "LONG NOT NULL", 0, "tblGroups|", 0), _
        Array("����� � ��������������", "IDLecturer", "LONG NOT NULL", 0, "tblLecturers|", 0), _
        Array("����� � ��������", "IDFaculty", "LONG NOT NULL", 0, "tblFaculty|", 0), _
        Array("����� ���������", "SheetNumber", "TEXT", 20, "", 0))
'---------------------------------------------------------------------------------
    
'������������� (tblSpecialities)--------------------------------------------------
    TablesArrey(8).TableNameDB = "tblSpecialities"
    TablesArrey(8).TableNameRu = "�������������"
    TablesArrey(8).Status = 0
    TablesArrey(8).FieldsCount = 3
    
    TablesArrey(8).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblEducationalPlan|tblGroups|", 0), _
        Array("��� �������������", "SpecName", "TEXT", 200, "", 0), _
        Array("�������� (%)", "PercentExtra", "INTEGER", 0, "", 0))
'---------------------------------------------------------------------------------
    
'������� (tblFaculty)------------------------------------------------
    TablesArrey(9).TableNameDB = "tblFaculty"
    TablesArrey(9).TableNameRu = "�������"
    TablesArrey(9).Status = 0
    TablesArrey(9).FieldsCount = 4
    
    TablesArrey(9).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblExaminations|tblStudents|tblLecturers|", 0), _
        Array("��� �������", "FacultyName", "TEXT", 200, "", 0), _
        Array("��� ���", "SCode", "TEXT", 30, "", 0), _
        Array("������", "IsActive", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------
    
'�������� (tblSubjects)-----------------------------------------------------------
    TablesArrey(10).TableNameDB = "tblSubjects"
    TablesArrey(10).TableNameRu = "��������"
    TablesArrey(10).Status = 0
    TablesArrey(10).FieldsCount = 2
    
    TablesArrey(10).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblEducationalPlan|", 0), _
        Array("�������� ��������", "Subject", "TEXT", 50, "", 0))
'---------------------------------------------------------------------------------
    
'������� ����� (tblEducationalPlan)-----------------------------------------------
    TablesArrey(11).TableNameDB = "tblEducationalPlan"
    TablesArrey(11).TableNameRu = "������� �����"
    TablesArrey(11).Status = 0
    TablesArrey(11).FieldsCount = 9
    
    TablesArrey(11).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblExaminations|", 0), _
        Array("����� � ��������������", "IDSpec", "LONG NOT NULL", 0, "tblSpecialities|", 0), _
        Array("����� � ���������", "IDSubject", "LONG NOT NULL", 0, "tblSubjects|", 0), _
        Array("����� ��������", "Semester", "BYTE", 0, "", 0), _
        Array("��� ����������", "KindOfReport", "TEXT", 20, "", 0), _
        Array("�������������", "IsAdditional", "BYTE", 0, "", 0), _
        Array("���", "SCode", "TEXT", 30, "", 0), _
        Array("�������� �����", "NumOfHours", "LONG", 0, "", 0), _
        Array("������", "IsActive", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------
    
'������������� ������ (tblGroups)-------------------------------------------------
    TablesArrey(12).TableNameDB = "tblGroups"
    TablesArrey(12).TableNameRu = "������������� ������"
    TablesArrey(12).Status = 0
    TablesArrey(12).FieldsCount = 5
    
    TablesArrey(12).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "tblExaminations|tblStudents|", 0), _
        Array("����� � ��������������", "IDSpec", "LONG NOT NULL", 0, "tblSpecialities|", 0), _
        Array("��� ������", "GroupName", "TEXT", 20, "", 0), _
        Array("��� ������ ������", "FirstSessionYear", "LONG", 0, "", 0), _
        Array("����� ��������", "Semester", "BYTE", 0, "", 0))
'---------------------------------------------------------------------------------
    
'���� ��������� ����� (tblDegreeThemes)-------------------------------------------
    TablesArrey(13).TableNameDB = "tblDegreeThemes"
    TablesArrey(13).TableNameRu = "���� ��������� �����"
    TablesArrey(13).Status = 0
    TablesArrey(13).FieldsCount = 5
    
    TablesArrey(13).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "", 0), _
        Array("����� � ���������", "IDStudent", "LONG NOT NULL", 0, "tblStudents|", 0), _
        Array("����� � ��������������", "IDLecturer", "LONG NOT NULL", 0, "tblLecturers|", 0), _
        Array("��� ������", "WorkType", "BYTE", 0, "", 0), _
        Array("�������� ������", "NameOfWork", "TEXT", 200, "", 0))
'---------------------------------------------------------------------------------

'������� ��������� (tblStudEvents)------------------------------------------------
    TablesArrey(14).TableNameDB = "tblStudEvents"
    TablesArrey(14).TableNameRu = "������� ���������"
    TablesArrey(14).Status = 0
    TablesArrey(14).FieldsCount = 3
    
    TablesArrey(14).FieldsArrey = Array( _
        Array("�������������", "ID", "COUNTER", 0, "", 0), _
        Array("����� � ���������", "IDStudent", "LONG NOT NULL", 0, "tblStudents|", 0), _
        Array("�������� �������", "EventText", "TEXT", 255, "", 0))
'---------------------------------------------------------------------------------

End Sub

Sub RefreshMaxLinkCount(T As VSFlexGrid)
Dim LArrey As Variant
Dim LCount As Integer

Dim MaxLink As Integer
Dim MaxLinkCounter As Integer

MaxLink = 0

Dim i As Integer
Dim j As Integer
Dim k As Integer

    If (T.Cols = 0) Then Exit Sub
    If (T.Cols = 2) Then
        T.Rows = 1
        Exit Sub
    End If

    For i = 0 To T.Cols - 1 Step 2
        GetLinkList CStr(T.ColData(i + 1)), LArrey, LCount
        MaxLink = 0
        
        For j = 0 To T.Cols - 1 Step 2
            If (T.TextMatrix(0, i + 1) <> T.TextMatrix(0, j + 1)) Then
                For k = 0 To LCount - 1
                    If (T.TextMatrix(0, j + 1) = LArrey(k)) Then
                        MaxLink = MaxLink + 1
                        Exit For
                    End If
                Next k
            End If
        Next j
        
        If (MaxLinkCounter < MaxLink) Then MaxLinkCounter = MaxLink
    Next i

    T.Rows = MaxLinkCounter + 1

End Sub

Sub GetLinkCount(FullLinkString As String, ByRef Count As Integer)
Dim i As Integer

Count = 0
    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then Count = Count + 1
    Next i
    
End Sub

Sub GetLinkList(FullLinkString As String, ByRef LinkArrey As Variant, ByRef Count As Integer)
Dim i As Integer
Dim LastS As Integer
Dim ArrCounter As Integer

    Count = 0
    LastS = 0
    ArrCounter = 0

    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then Count = Count + 1
    Next i
    
    ReDim LinkArrey(Count)
    
    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then
            LinkArrey(ArrCounter) = Mid(FullLinkString, LastS + 1, i - LastS - 1)
            ArrCounter = ArrCounter + 1
            LastS = i
        End If
    Next i
    
End Sub

Sub RefreshTablesList(TSrc As VSFlexGrid, TDest As VSFlexGrid)
Dim FullDestTArrey As Variant
Dim FullDestTCount As Integer
FullDestTCount = 0

Dim FullDestArrey As Variant
Dim FullDestCount As Integer
FullDestCount = 0

Dim DestArrey As Variant
Dim DestCount As Integer

Dim SrcArrey As Variant
Dim SrcCount As Integer

Dim iFullDestT As Integer
Dim iFullDest As Integer
Dim iDest As Integer
Dim iSrc As Integer

Dim i As Long

    If (TDest.Cols = 0) Then
        LoadTablesList TSrc
        Exit Sub
    End If

    ReDim FullDestTArrey(1)
    ReDim FullDestArrey(1)
    
    Dim Finded As Boolean
    
    For iDest = 0 To TDest.Cols - 1 Step 2
        GetLinkList CStr(TDest.ColData(iDest + 1)), DestArrey, DestCount
        
        FullDestTCount = FullDestTCount + 1
        ReDim Preserve FullDestTArrey(FullDestTCount)
        FullDestTArrey(FullDestTCount - 1) = TDest.TextMatrix(0, iDest + 1)
        
        For i = 0 To DestCount
            Finded = False
            
            For iFullDest = 0 To FullDestCount - 1
                If (FullDestArrey(iFullDest) = DestArrey(i)) Then
                    Finded = True
                    Exit For
                End If
            Next iFullDest
            
            If (Not Finded) Then
                FullDestCount = FullDestCount + 1
                ReDim Preserve FullDestArrey(FullDestCount)
                
                FullDestArrey(FullDestCount - 1) = DestArrey(i)
            End If
        Next i
        
    Next iDest
    
    
    For iSrc = 0 To TSrc.Rows - 1
        Finded = False
        
        For iFullDest = 0 To FullDestCount - 1
            If (FullDestArrey(iFullDest) = TSrc.TextMatrix(iSrc, 1)) Then
                Finded = True
                Exit For
            End If
        Next iFullDest
        
        If (Finded) Then
            TSrc.RowHidden(iSrc) = False
        Else
            TSrc.RowHidden(iSrc) = True
        End If
    Next iSrc
    
    
    For iSrc = 0 To TSrc.Rows - 1
        Finded = False
        
        For iFullDestT = 0 To FullDestTCount - 1
            If (FullDestTArrey(iFullDestT) = TSrc.TextMatrix(iSrc, 1)) Then
                Finded = True
                Exit For
            End If
        Next iFullDestT
        
        If (Finded) Then TSrc.RowHidden(iSrc) = True
    Next iSrc

    For iSrc = 0 To TSrc.Rows - 1
        If (TSrc.RowHidden(iSrc) = False) Then
            TSrc.Row = iSrc
            Exit Sub
        End If
    Next iSrc
End Sub

Sub LoadTablesList(T As VSFlexGrid)
T.Rows = 0

T.AddItem "��������" & vbTab & "tblStudents"
T.RowData(T.Rows - 1) = "tblConfirmList|tblNominalGrants|tblSheets|tblSheetsTemporary|tblCategories|tblGroups|tblFaculty|tblDegreeThemes|tblStudEvents|"

T.AddItem "�������������" & vbTab & "tblLecturers"
T.RowData(T.Rows - 1) = "tblExaminations|tblFaculty|tblDegreeThemes|"

T.AddItem "�������������" & vbTab & "tblSpecialities"
T.RowData(T.Rows - 1) = "tblEducationalPlan|tblGroups|"

T.AddItem "������������� ������" & vbTab & "tblGroups"
T.RowData(T.Rows - 1) = "tblExaminations|tblSpecialities|tblStudents|"

T.AddItem "�������" & vbTab & "tblFaculty"
T.RowData(T.Rows - 1) = "tblExaminations|tblStudents|tblLecturers|"

T.AddItem "������� ���������" & vbTab & "tblNominalGrants"
T.RowData(T.Rows - 1) = "tblStudents|"

T.AddItem "��������� ���������" & vbTab & "tblCategories"
T.RowData(T.Rows - 1) = "tblStudents|"

T.AddItem "������� ���������" & vbTab & "tblStudEvents"
T.RowData(T.Rows - 1) = "tblStudents|"

T.AddItem "������� �� �����" & vbTab & "tblConfirmList"
T.RowData(T.Rows - 1) = "tblStudents|"

T.AddItem "�������� ������������" & vbTab & "tblSheets"
T.RowData(T.Rows - 1) = "tblStudents|tblExaminations|"

T.AddItem "�������������� ������������" & vbTab & "tblSheetsTemporary"
T.RowData(T.Rows - 1) = "tblStudents|tblExaminations|"

T.AddItem "��������������� ���������" & vbTab & "tblExaminations"
T.RowData(T.Rows - 1) = "tblSheets|tblSheetsTemporary|tblLecturers|tblEducationalPlan|tblFaculty|tblGroups|"

T.AddItem "������� �����" & vbTab & "tblEducationalPlan"
T.RowData(T.Rows - 1) = "tblExaminations|tblSpecialities|tblSubjects|"

T.AddItem "��������" & vbTab & "tblSubjects"
T.RowData(T.Rows - 1) = "tblEducationalPlan|"

T.AddItem "���� ��������� �����" & vbTab & "tblDegreeThemes"
T.RowData(T.Rows - 1) = "tblStudents|tblLecturers|"

End Sub

Private Sub cmdAnd_Click()
Dim ColPos As Integer

If (tblConditions.Cols = 0) Then
    tblConditions.Rows = 8
    ColPos = 0
    
    tblConditions.RowHidden(1) = True
    tblConditions.RowHidden(2) = True
    tblConditions.RowHidden(3) = True
    tblConditions.RowHidden(4) = True
    tblConditions.RowHidden(5) = True
    tblConditions.RowHidden(6) = True
    tblConditions.RowHidden(7) = True
Else
    ColPos = IIf(ckIns.Value = 0, tblConditions.Col, tblConditions.Col + 1)
End If

InsertCol tblConditions, ColPos

tblConditions.TextMatrix(0, ColPos) = "�"

tblConditions.TextMatrix(1, ColPos) = "AND"
tblConditions.TextMatrix(2, ColPos) = "-1"
tblConditions.TextMatrix(3, ColPos) = "-1"
tblConditions.TextMatrix(4, ColPos) = ""
tblConditions.TextMatrix(5, ColPos) = ""
tblConditions.TextMatrix(6, ColPos) = "-1"
tblConditions.TextMatrix(7, ColPos) = "-1"

tblConditions.Col = ColPos
tblConditions.AutoSize 0, tblConditions.Cols - 1
FillSQLTextBox tblConditions, txtSQLQuery
End Sub

Private Sub cmdCancel_Click()
Unload Me
End Sub

Private Sub cmdCondition_Click()
'-----------------------
Category = "CONDITION"
    
LTable = -1
LField = -1
    
Operation = ""
    
RString = ""
    
RTable = -1
RField = -1
'-----------------------

FCondition = True
ShowForm frmCondition, 1, Me
FCondition = False

If (Category = "") Then
    LTable = -1
    LField = -1
        
    Operation = ""
        
    RString = ""
        
    RTable = -1
    RField = -1
    Exit Sub
End If

Dim ColPos As Integer

If (tblConditions.Cols = 0) Then
    tblConditions.Rows = 8
    ColPos = 0
    
    tblConditions.RowHidden(1) = True
    tblConditions.RowHidden(2) = True
    tblConditions.RowHidden(3) = True
    tblConditions.RowHidden(4) = True
    tblConditions.RowHidden(5) = True
    tblConditions.RowHidden(6) = True
    tblConditions.RowHidden(7) = True
Else
    ColPos = IIf(ckIns.Value = 0, tblConditions.Col, tblConditions.Col + 1)
End If

InsertCol tblConditions, ColPos

Dim strCondition As String

strCondition = "[" & TablesArrey(LTable).TableNameRu & "].[" & TablesArrey(LTable).FieldsArrey(LField)(0) & "]"
strCondition = strCondition & " " & Operation & " "

If ((RTable <> -1) And (RField <> -1)) Then
    strCondition = strCondition & "[" & TablesArrey(RTable).TableNameRu & "].[" & TablesArrey(RTable).FieldsArrey(RField)(0) & "]"
Else
    Select Case TablesArrey(LTable).FieldsArrey(LField)(2)
        Case "TEXT", "DATETIME":
            strCondition = strCondition & "'" & RString & "'"
        
        Case "BYTE", "CURRENCY", "INTEGER", "LONG":
            strCondition = strCondition & RString
            
    End Select

End If

tblConditions.TextMatrix(0, ColPos) = strCondition

tblConditions.TextMatrix(1, ColPos) = Category
tblConditions.TextMatrix(2, ColPos) = LTable
tblConditions.TextMatrix(3, ColPos) = LField
tblConditions.TextMatrix(4, ColPos) = Operation
tblConditions.TextMatrix(5, ColPos) = RString
tblConditions.TextMatrix(6, ColPos) = RTable
tblConditions.TextMatrix(7, ColPos) = RField

tblConditions.Col = ColPos
tblConditions.AutoSize 0, tblConditions.Cols - 1
FillSQLTextBox tblConditions, txtSQLQuery
End Sub

Private Sub cmdCS_Click()
Dim ColPos As Integer

If (tblConditions.Cols = 0) Then
    tblConditions.Rows = 8
    ColPos = 0
    
    tblConditions.RowHidden(1) = True
    tblConditions.RowHidden(2) = True
    tblConditions.RowHidden(3) = True
    tblConditions.RowHidden(4) = True
    tblConditions.RowHidden(5) = True
    tblConditions.RowHidden(6) = True
    tblConditions.RowHidden(7) = True
Else
    ColPos = IIf(ckIns.Value = 0, tblConditions.Col, tblConditions.Col + 1)
End If

InsertCol tblConditions, ColPos

tblConditions.TextMatrix(0, ColPos) = ")"

tblConditions.TextMatrix(1, ColPos) = "CB"
tblConditions.TextMatrix(2, ColPos) = "-1"
tblConditions.TextMatrix(3, ColPos) = "-1"
tblConditions.TextMatrix(4, ColPos) = ""
tblConditions.TextMatrix(5, ColPos) = ""
tblConditions.TextMatrix(6, ColPos) = "-1"
tblConditions.TextMatrix(7, ColPos) = "-1"

tblConditions.Col = ColPos
tblConditions.AutoSize 0, tblConditions.Cols - 1
FillSQLTextBox tblConditions, txtSQLQuery
End Sub

Private Sub cmdDel_Click()
If (tblConditions.Col < 0) Then Exit Sub
If (tblConditions.Cols = 0) Then Exit Sub

RemoveCol tblConditions, tblConditions.Col

If (tblConditions.Cols = 0) Then
    tblConditions.Rows = 0
    BeforeCol = -1
Else
    If (tblConditions.Col > 0) Then tblConditions.Col = tblConditions.Col - 1
    tblConditions_RowColChange
    tblConditions.AutoSize 0, tblConditions.Cols - 1
End If

FillSQLTextBox tblConditions, txtSQLQuery
End Sub

Private Sub cmdFieldAdd_Click()
SelectedTable = -1
SelectedField = -1

FCondition = False
ShowForm frmFields, 1, Me

If (SelectedTable = -1 Or SelectedField = -1) Then Exit Sub
TablesArrey(SelectedTable).FieldsArrey(SelectedField)(5) = 1

tblShowFields.Cols = tblShowFields.Cols + 1

tblShowFields.ColData(tblShowFields.Cols - 1) = CStr(SelectedTable) & "|" & CStr(SelectedField) & "|"
tblShowFields.TextMatrix(0, tblShowFields.Cols - 1) = TablesArrey(SelectedTable).FieldsArrey(SelectedField)(0)
tblShowFields.TextMatrix(1, tblShowFields.Cols - 1) = "�� �����������"
tblShowFields.TextMatrix(2, tblShowFields.Cols - 1) = "��"

tblShowFields.Cell(flexcpAlignment, 0, tblShowFields.Cols - 1, 2, tblShowFields.Cols - 1) = flexAlignCenterCenter

ResizeColumns tblShowFields
End Sub

Private Sub cmdFieldClear_Click()
Dim TF As Variant
Dim TFC As Integer
Dim i As Integer

For i = 1 To tblShowFields.Cols - 1
    GetLinkList tblShowFields.ColData(i), TF, TFC
    TablesArrey(CInt(TF(0))).FieldsArrey(CInt(TF(1)))(5) = 0
Next i
tblShowFields.Cols = 1
End Sub

Private Sub cmdFieldDel_Click()
If (tblShowFields.Col < 1) Then Exit Sub
If (tblShowFields.Cols = 1) Then Exit Sub

Dim TF As Variant
Dim TFC As Integer

GetLinkList tblShowFields.ColData(tblShowFields.Col), TF, TFC
TablesArrey(CInt(TF(0))).FieldsArrey(CInt(TF(1)))(5) = 0

RemoveCol tblShowFields, tblShowFields.Col
End Sub

Private Sub cmdOK_Click()
If (tblShowFields.Cols = 1) Then
    MsgBox "�������� ������ ���� ���� ��� ������.", vbOKOnly, "������"
    Exit Sub
End If

If (Not CheckSQLConditions(tblConditions)) Then
    tblConditions_RowColChange
    Exit Sub
End If

ExecuteSQLQuery
End Sub

Private Sub cmdOr_Click()
Dim ColPos As Integer

If (tblConditions.Cols = 0) Then
    tblConditions.Rows = 8
    ColPos = 0
    
    tblConditions.RowHidden(1) = True
    tblConditions.RowHidden(2) = True
    tblConditions.RowHidden(3) = True
    tblConditions.RowHidden(4) = True
    tblConditions.RowHidden(5) = True
    tblConditions.RowHidden(6) = True
    tblConditions.RowHidden(7) = True
Else
    ColPos = IIf(ckIns.Value = 0, tblConditions.Col, tblConditions.Col + 1)
End If

InsertCol tblConditions, ColPos

tblConditions.TextMatrix(0, ColPos) = "���"

tblConditions.TextMatrix(1, ColPos) = "OR"
tblConditions.TextMatrix(2, ColPos) = "-1"
tblConditions.TextMatrix(3, ColPos) = "-1"
tblConditions.TextMatrix(4, ColPos) = ""
tblConditions.TextMatrix(5, ColPos) = ""
tblConditions.TextMatrix(6, ColPos) = "-1"
tblConditions.TextMatrix(7, ColPos) = "-1"

tblConditions.Col = ColPos
tblConditions.AutoSize 0, tblConditions.Cols - 1
FillSQLTextBox tblConditions, txtSQLQuery
End Sub

Private Sub cmdOS_Click()
Dim ColPos As Integer

If (tblConditions.Cols = 0) Then
    tblConditions.Rows = 8
    ColPos = 0
    
    tblConditions.RowHidden(1) = True
    tblConditions.RowHidden(2) = True
    tblConditions.RowHidden(3) = True
    tblConditions.RowHidden(4) = True
    tblConditions.RowHidden(5) = True
    tblConditions.RowHidden(6) = True
    tblConditions.RowHidden(7) = True
Else
    ColPos = IIf(ckIns.Value = 0, tblConditions.Col, tblConditions.Col + 1)
End If

InsertCol tblConditions, ColPos

tblConditions.TextMatrix(0, ColPos) = "("

tblConditions.TextMatrix(1, ColPos) = "OB"
tblConditions.TextMatrix(2, ColPos) = "-1"
tblConditions.TextMatrix(3, ColPos) = "-1"
tblConditions.TextMatrix(4, ColPos) = ""
tblConditions.TextMatrix(5, ColPos) = ""
tblConditions.TextMatrix(6, ColPos) = "-1"
tblConditions.TextMatrix(7, ColPos) = "-1"

tblConditions.Col = ColPos
tblConditions.AutoSize 0, tblConditions.Cols - 1
FillSQLTextBox tblConditions, txtSQLQuery
End Sub

Private Sub cmdTableAdd_Click()
If (comboLinks.Visible) Then Exit Sub
If (tblTables.Row < 0) Then Exit Sub
Dim i As Integer

If (tblUsedTables.Cols = 0) Then
    With tblUsedTables
        .Cols = 2
        .Rows = 1
        .FixedRows = 1
        
        .ColHidden(1) = True
    End With
    
    tblUsedTables.ColData(1) = tblTables.RowData(tblTables.Row)
    tblUsedTables.TextMatrix(0, 0) = tblTables.TextMatrix(tblTables.Row, 0)
    tblUsedTables.TextMatrix(0, 1) = tblTables.TextMatrix(tblTables.Row, 1)
    
Else
    tblUsedTables.Cols = tblUsedTables.Cols + 2
    tblUsedTables.ColHidden(tblUsedTables.Cols - 1) = True
    
    tblUsedTables.ColData(tblUsedTables.Cols - 1) = tblTables.RowData(tblTables.Row)
    tblUsedTables.TextMatrix(0, tblUsedTables.Cols - 2) = tblTables.TextMatrix(tblTables.Row, 0)
    tblUsedTables.TextMatrix(0, tblUsedTables.Cols - 1) = tblTables.TextMatrix(tblTables.Row, 1)

End If

For i = 0 To SQLTablesCount - 1
    If (TablesArrey(i).TableNameDB = tblTables.TextMatrix(tblTables.Row, 1)) Then
        TablesArrey(i).Status = 1
        Exit For
    End If
Next i

RefreshTablesList tblTables, tblUsedTables
RefreshMaxLinkCount tblUsedTables

ResizeColumns tblUsedTables
End Sub


Private Sub cmdTableClear_Click()
If (comboLinks.Visible) Then Exit Sub

Dim TF As Variant
Dim TFC As Integer
Dim i As Integer

tblUsedTables.Rows = 0
tblUsedTables.Cols = 0

For i = 1 To tblShowFields.Cols - 1
    GetLinkList tblShowFields.ColData(i), TF, TFC
    TablesArrey(CInt(TF(0))).FieldsArrey(CInt(TF(1)))(5) = 0
Next i
tblShowFields.Cols = 1

For i = 0 To SQLTablesCount - 1
    TablesArrey(i).Status = 0
Next i

RefreshTablesList tblTables, tblUsedTables
RefreshMaxLinkCount tblUsedTables
End Sub

Private Sub comboLinks_Click()
Dim CurTableName As String
Dim CurTableNameBD As String

Dim CurLinkName As String

Dim NewLinkName As String
Dim NewLinkNameBD As String

Dim i As Integer
Dim j As Integer
Dim k As Integer
    
    If (comboLinks.ListIndex < 0) Then Exit Sub
    
    CurTableName = tblUsedTables.TextMatrix(0, tblUsedTables.Col)
    CurTableNameBD = tblUsedTables.TextMatrix(0, tblUsedTables.Col + 1)
    
    CurLinkName = tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col + 1)
    
    NewLinkName = tblLinks.TextMatrix(comboLinks.ItemData(comboLinks.ListIndex), 0)
    NewLinkNameBD = tblLinks.TextMatrix(comboLinks.ItemData(comboLinks.ListIndex), 1)
    
    If (CurLinkName <> "") Then
    
        For i = 0 To tblUsedTables.Cols - 1 Step 2
        
            If (tblUsedTables.TextMatrix(0, i + 1) = CurLinkName) Then
            
                For j = 1 To tblUsedTables.Rows - 1
                    If ((tblUsedTables.TextMatrix(j, i + 1) = CurTableNameBD) And (tblUsedTables.Cell(flexcpForeColor, j, i) = TForeColorP)) Then
                        tblUsedTables.Cell(flexcpForeColor, j, i) = TForeColorA
                        
                        tblUsedTables.TextMatrix(j, i + 1) = ""
                        tblUsedTables.TextMatrix(j, i) = ""
                        '------------------------------------------------------------
                            For k = j + 1 To tblUsedTables.Rows - 1
                                tblUsedTables.Cell(flexcpForeColor, k - 1, i) = tblUsedTables.Cell(flexcpForeColor, k, i)
                                tblUsedTables.TextMatrix(k - 1, i + 1) = tblUsedTables.TextMatrix(k, i + 1)
                                tblUsedTables.TextMatrix(k - 1, i) = tblUsedTables.TextMatrix(k, i)
                            
                                tblUsedTables.Cell(flexcpForeColor, k, i) = TForeColorA
                                tblUsedTables.TextMatrix(k, i + 1) = ""
                                tblUsedTables.TextMatrix(k, i) = ""
                            Next k
                        '------------------------------------------------------------
                        Exit For
                    End If
                Next j
                
                Exit For
            End If
            
        Next i
        
    End If
    
    
    If (comboLinks.ListIndex = 0) Then
        tblUsedTables.Cell(flexcpForeColor, tblUsedTables.Row, tblUsedTables.Col) = TForeColorA
        tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col) = ""
        tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col + 1) = ""
        
        For k = tblUsedTables.Row + 1 To tblUsedTables.Rows - 1
            tblUsedTables.Cell(flexcpForeColor, k - 1, tblUsedTables.Col) = tblUsedTables.Cell(flexcpForeColor, k, tblUsedTables.Col)
            tblUsedTables.TextMatrix(k - 1, tblUsedTables.Col + 1) = tblUsedTables.TextMatrix(k, tblUsedTables.Col + 1)
            tblUsedTables.TextMatrix(k - 1, tblUsedTables.Col) = tblUsedTables.TextMatrix(k, tblUsedTables.Col)
        
            tblUsedTables.Cell(flexcpForeColor, k, tblUsedTables.Col) = TForeColorA
            tblUsedTables.TextMatrix(k, tblUsedTables.Col + 1) = ""
            tblUsedTables.TextMatrix(k, tblUsedTables.Col) = ""
        Next k
        
    Else
        tblUsedTables.Cell(flexcpForeColor, tblUsedTables.Row, tblUsedTables.Col) = TForeColorA
        tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col) = NewLinkName
        tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col + 1) = NewLinkNameBD
        
        For i = 0 To tblUsedTables.Cols - 1 Step 2
            If (tblUsedTables.TextMatrix(0, i + 1) = NewLinkNameBD) Then
            
                For j = 1 To tblUsedTables.Rows - 1
                    If ((tblUsedTables.TextMatrix(j, i) = "") And (tblUsedTables.TextMatrix(j, i + 1) = "")) Then
                        tblUsedTables.Cell(flexcpForeColor, j, i) = TForeColorP
                        
                        tblUsedTables.TextMatrix(j, i) = CurTableName
                        tblUsedTables.TextMatrix(j, i + 1) = CurTableNameBD
                        Exit For
                    End If
                    
                Next j
                
                Exit For
                
            End If
            
        Next i
        
    End If
    
ResizeColumn tblUsedTables, tblUsedTables.Col
comboLinks.Visible = False
End Sub

Private Sub comboLinks_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboLinks_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub


Private Sub comboSort_Click()
tblShowFields.Text = comboSort.Text

comboSort.Visible = False

ResizeColumns tblShowFields
End Sub

Private Sub cToolbar1_ButtonClick(ByVal lButton As Long)
Select Case lButton
    Case 0: '������������ �������
        F2.ZOrder
        
    Case 1: '����� � ����������
        F3.ZOrder
        
    Case 2: '������� ������
        F4.ZOrder
        
End Select
End Sub

Private Sub Form_Load()
    With tblTables
        .Cols = 2
        .Rows = 0
        
        .ColWidth(0) = 2860
        
        .ColHidden(1) = True
    End With

    With tblLinks
        .Cols = 3
        .Rows = 0
    End With

    With tblShowFields
        .Cols = 1
        .Rows = 3
        
        .FixedCols = 1
        .FixedRows = 1
        
        .TextMatrix(0, 0) = "�������� ���� :"
        .TextMatrix(1, 0) = "���������� :"
        .TextMatrix(2, 0) = "����� �� ����� :"
        
        .RowHeight(1) = 315
        .RowHeight(2) = 315
    End With
    
    CreateToolBars
    
    FCondition = False
    BeforeCol = -1
    
    ResizeColumns tblShowFields
    
    LoadTablesList tblTables
    FillTablesArrey
    
    F2.ZOrder
    cToolbar1.ButtonChecked(0) = True
    
    LoadSuccess = True
    
End Sub

Private Sub tblConditions_RowColChange()
If (tblConditions.Cols = 0) Then Exit Sub
If (tblConditions.Col < 0) Then Exit Sub

If (BeforeCol >= 0 And BeforeCol < tblConditions.Cols) Then
    tblConditions.Cell(flexcpBackColor, 0, BeforeCol) = tblConditions.BackColor
    tblConditions.Cell(flexcpForeColor, 0, BeforeCol) = tblConditions.ForeColor
End If

tblConditions.Cell(flexcpBackColor, 0, tblConditions.Col) = tblConditions.BackColorSel
tblConditions.Cell(flexcpForeColor, 0, tblConditions.Col) = tblConditions.ForeColorSel

BeforeCol = tblConditions.Col
End Sub

Private Sub tblShowFields_BeforeScroll(ByVal OldTopRow As Long, ByVal OldLeftCol As Long, ByVal NewTopRow As Long, ByVal NewLeftCol As Long, Cancel As Boolean)
If comboSort.Visible Then Cancel = True
End Sub

Private Sub tblShowFields_BeforeUserResize(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
If comboSort.Visible Then Cancel = True
End Sub

Private Sub tblShowFields_DblClick()
If (tblShowFields.Row = 1) Then
    comboSort.Text = tblShowFields.Text
    
    If (tblShowFields.ColWidth(tblShowFields.Col) < 1620) Then tblShowFields.ColWidth(tblShowFields.Col) = 1620 + 100
    comboSort.Left = tblShowFields.CellLeft
    comboSort.Top = tblShowFields.CellTop
    comboSort.Width = tblShowFields.CellWidth
    If ((comboSort.Left + comboSort.Width) > tblShowFields.Width) Then comboSort.Width = tblShowFields.Width - comboSort.Left - 50
    
    comboSort.Visible = True
End If

If (tblShowFields.Row = 2) Then
    tblShowFields.Text = IIf(UCase(tblShowFields.Text) = "��", "���", "��")
End If

End Sub

Private Sub tblShowFields_RowColChange()
    If (comboSort.Visible) Then
        comboSort.Visible = False
        ResizeColumns tblShowFields
    End If
    
End Sub

Private Sub tblTableDel_Click()
If (comboLinks.Visible) Then Exit Sub
End Sub

Private Sub tblUsedTables_BeforeScroll(ByVal OldTopRow As Long, ByVal OldLeftCol As Long, ByVal NewTopRow As Long, ByVal NewLeftCol As Long, Cancel As Boolean)
If comboLinks.Visible Then Cancel = True
End Sub

Private Sub tblUsedTables_BeforeUserResize(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
If comboLinks.Visible Then Cancel = True
End Sub

Private Sub tblUsedTables_DblClick()
Dim LArrey As Variant
Dim LCount As Integer

Dim TName As String

Dim i As Integer
Dim j As Integer

Dim MaxLen As Long

Dim Finded As Boolean

If (tblUsedTables.Col + 1 >= tblUsedTables.Cols) Then Exit Sub

If (tblUsedTables.Cell(flexcpForeColor, tblUsedTables.Row, tblUsedTables.Col) = TForeColorP) Then Exit Sub

    Finded = False
    For i = 1 To tblUsedTables.Row - 1
        If ((tblUsedTables.TextMatrix(i, tblUsedTables.Col) = "") And ((tblUsedTables.TextMatrix(i, tblUsedTables.Col + 1) = ""))) Then
            Finded = True
            Exit For
        End If
    Next i
    If (Finded) Then Exit Sub
    
    
    
    GetLinkList tblUsedTables.ColData(tblUsedTables.Col + 1), LArrey, LCount
    
    tblLinks.Rows = 0
    
    For i = 0 To LCount - 1
        For j = 0 To tblTables.Rows - 1
            If (tblTables.TextMatrix(j, 1) = LArrey(i)) Then
                TName = tblTables.TextMatrix(j, 0)
                Exit For
            End If
        Next j
        tblLinks.AddItem TName & vbTab & LArrey(i)
    Next i

    For i = 0 To tblLinks.Rows - 1
        Finded = False
        
        For j = 0 To tblUsedTables.Cols - 1 Step 2
            If (tblUsedTables.TextMatrix(0, j + 1) = tblLinks.TextMatrix(i, 1)) Then
                Finded = True
                Exit For
            End If
        Next j
        
        If (Not Finded) Then tblLinks.TextMatrix(i, 2) = "1"
    Next i
    
    For i = 0 To tblLinks.Rows - 1
        Finded = False
        
        For j = 1 To tblUsedTables.Rows - 1
            If ((tblUsedTables.TextMatrix(j, tblUsedTables.Col + 1) <> "") And (j <> tblUsedTables.Row)) Then
                If (tblUsedTables.TextMatrix(j, tblUsedTables.Col + 1) = tblLinks.TextMatrix(i, 1)) Then
                    Finded = True
                    Exit For
                End If
            End If
        Next j
        
        If (Finded) Then tblLinks.TextMatrix(i, 2) = "1"
    Next i
    
    tblLinks.AddItem "�����", 0
    
    
    MaxLen = 0
    comboLinks.Clear
    For i = 0 To tblLinks.Rows - 1
        If (tblLinks.TextMatrix(i, 2) <> "1") Then
            strlen.Caption = tblLinks.TextMatrix(i, 0)
            If (MaxLen < strlen.Width) Then MaxLen = strlen.Width
                    
            comboLinks.AddItem tblLinks.TextMatrix(i, 0)
            comboLinks.ItemData(comboLinks.NewIndex) = i
        End If
    Next i
    
    Dim Ind As Integer
    If (tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col + 1) = "") Then
        comboLinks.Text = "�����"
    Else
        Ind = 0
        For i = 1 To comboLinks.ListCount - 1
            If (comboLinks.List(i) = tblUsedTables.TextMatrix(tblUsedTables.Row, tblUsedTables.Col)) Then
                Ind = i
                Exit For
            End If
        Next i
        
        comboLinks.Text = comboLinks.List(Ind)
    End If
    
    If (tblUsedTables.ColWidth(tblUsedTables.Col) < MaxLen) Then tblUsedTables.ColWidth(tblUsedTables.Col) = MaxLen + 100
    comboLinks.Left = tblUsedTables.CellLeft
    comboLinks.Top = tblUsedTables.CellTop
    comboLinks.Width = tblUsedTables.CellWidth
    'comboLinks.Height = tblUsedTables.CellHeight
    If ((comboLinks.Left + comboLinks.Width) > tblUsedTables.Width) Then comboLinks.Width = tblUsedTables.Width - comboLinks.Left - 50
    
    comboLinks.Visible = True
End Sub

Private Sub tblUsedTables_RowColChange()
    comboLinks.Visible = False
End Sub

