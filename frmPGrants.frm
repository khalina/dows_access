VERSION 5.00
Begin VB.Form frmPGrants 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "���������� ���������"
   ClientHeight    =   2775
   ClientLeft      =   4695
   ClientTop       =   4440
   ClientWidth     =   5310
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2775
   ScaleWidth      =   5310
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F3 
      Height          =   2055
      Left            =   0
      TabIndex        =   37
      Top             =   0
      Width           =   5295
      Begin VB.TextBox SW 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   960
         TabIndex        =   40
         Top             =   480
         Width           =   1935
      End
      Begin VB.TextBox DebtsDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   960
         TabIndex        =   39
         Top             =   1080
         Width           =   1935
      End
      Begin VB.HScrollBar T2 
         Height          =   255
         Left            =   1800
         Max             =   9000
         TabIndex        =   38
         Top             =   1560
         Value           =   4500
         Width           =   3135
      End
      Begin VB.Label �����24 
         Caption         =   "���� ���������� ����� ���������� �������������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   840
         Width           =   4455
      End
      Begin VB.Label �����23 
         Caption         =   "������ (� ����������� ������) :"
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   240
         Width           =   2655
      End
      Begin VB.Label �����22 
         Caption         =   "����/������"
         Height          =   255
         Left            =   3240
         TabIndex        =   46
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label �����21 
         Height          =   255
         Left            =   3240
         TabIndex        =   45
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label M20 
         Caption         =   "31 ������ 2001 ����"
         Height          =   255
         Left            =   3240
         TabIndex        =   44
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label �����19 
         Caption         =   "������ ������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label �����18 
         Caption         =   "-"
         Height          =   255
         Left            =   1680
         TabIndex        =   42
         Top             =   1560
         Width           =   135
      End
      Begin VB.Label �����17 
         Caption         =   "+"
         Height          =   255
         Left            =   5040
         TabIndex        =   41
         Top             =   1560
         Width           =   135
      End
   End
   Begin VB.Frame F2 
      Height          =   735
      Left            =   0
      TabIndex        =   28
      Top             =   2040
      Width           =   5295
      Begin VB.CommandButton cmdPrint 
         Caption         =   "������"
         Height          =   375
         Left            =   1680
         TabIndex        =   30
         Top             =   240
         Width           =   1695
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "�������"
         Height          =   375
         Left            =   3480
         TabIndex        =   29
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.Frame F1 
      Height          =   2055
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin VB.HScrollBar T 
         Height          =   255
         Left            =   1800
         Max             =   9000
         TabIndex        =   31
         Top             =   1320
         Value           =   4500
         Width           =   3135
      End
      Begin VB.TextBox FIO6 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3480
         TabIndex        =   27
         Text            =   "�. �. ��������"
         Top             =   5280
         Width           =   1695
      End
      Begin VB.TextBox FIO5 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3480
         TabIndex        =   26
         Text            =   "�. �. ���������"
         Top             =   4920
         Width           =   1695
      End
      Begin VB.TextBox FIO4 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3480
         TabIndex        =   25
         Text            =   "�. �. ������"
         Top             =   4560
         Width           =   1695
      End
      Begin VB.TextBox FIO3 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3480
         TabIndex        =   24
         Text            =   "�. �. �����������"
         Top             =   4200
         Width           =   1695
      End
      Begin VB.TextBox FIO1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3480
         TabIndex        =   23
         Text            =   "�. �. ������"
         Top             =   3840
         Width           =   1695
      End
      Begin VB.TextBox FIO2 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3480
         TabIndex        =   22
         Text            =   "�. �. �����������"
         Top             =   3480
         Width           =   1695
      End
      Begin VB.TextBox ExtraO 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3960
         TabIndex        =   12
         Text            =   "15"
         Top             =   2880
         Width           =   1215
      End
      Begin VB.TextBox ExtraC 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3960
         TabIndex        =   11
         Text            =   "100"
         Top             =   2520
         Width           =   1215
      End
      Begin VB.TextBox ExtraB 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3960
         TabIndex        =   10
         Text            =   "50"
         Top             =   2160
         Width           =   1215
      End
      Begin VB.TextBox txtYear 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1920
         TabIndex        =   6
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox txtSemester 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1920
         TabIndex        =   5
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox CurDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1920
         TabIndex        =   4
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Total 
         Height          =   255
         Left            =   1920
         TabIndex        =   36
         Top             =   1680
         Width           =   2295
      End
      Begin VB.Label �����16 
         Caption         =   "����� :"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   1680
         Width           =   735
      End
      Begin VB.Label �����9 
         Caption         =   "+"
         Height          =   255
         Left            =   5040
         TabIndex        =   34
         Top             =   1320
         Width           =   135
      End
      Begin VB.Label �����8 
         Caption         =   "-"
         Height          =   255
         Left            =   1680
         TabIndex        =   33
         Top             =   1320
         Width           =   135
      End
      Begin VB.Label �����7 
         Caption         =   "������ ������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label �����15 
         Caption         =   "�������� ������������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   5280
         Width           =   2775
      End
      Begin VB.Label �����14 
         Caption         =   "��������� �������-����������� ������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   4920
         Width           =   3735
      End
      Begin VB.Label �����13 
         Caption         =   "������� ��������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   4560
         Width           =   2295
      End
      Begin VB.Label �����12 
         Caption         =   "��������� �������� ������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   4200
         Width           =   2535
      End
      Begin VB.Label �����11 
         Caption         =   "��������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   3480
         Width           =   1335
      End
      Begin VB.Label �����10 
         Caption         =   "�. �. ������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   3840
         Width           =   1215
      End
      Begin VB.Label D3 
         Height          =   255
         Left            =   3600
         TabIndex        =   15
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label D2 
         Height          =   255
         Left            =   3600
         TabIndex        =   14
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label D1 
         Height          =   255
         Left            =   3600
         TabIndex        =   13
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label �����6 
         Caption         =   "���������� �������� ��� ������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2880
         Width           =   3135
      End
      Begin VB.Label �����5 
         Caption         =   "���������� �������� ��� ������������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2520
         Width           =   3255
      End
      Begin VB.Label �����4 
         Caption         =   "���������� �������� ��� ������ ��������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   2160
         Width           =   3615
      End
      Begin VB.Label �����3 
         Caption         =   "���� :"
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   240
         Width           =   615
      End
      Begin VB.Label �����2 
         Caption         =   "������� :"
         Height          =   255
         Left            =   480
         TabIndex        =   2
         Top             =   600
         Width           =   975
      End
      Begin VB.Label �����1 
         Caption         =   "������� ��� :"
         Height          =   255
         Left            =   480
         TabIndex        =   1
         Top             =   960
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmPGrants"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Private Sub cmdCancel_Click()
'Unload Me
'End Sub
'
'Private Sub cmdPrint_Click()
'If (ChosenQuery = 3) Then
'    Main.PrintGrantsList CurDate.Text, _
'                         txtSemester.Text, _
'                         txtYear.Text, _
'                         T.Value
'
'ElseIf (ChosenQuery = 4) Then
'    Main.PrintTransmissionList SW.Text, _
'                               DebtsDate.Text, _
'                               T2.Value, True
'
'ElseIf (ChosenQuery = 5) Then
'    Main.PrintTransmissionList SW.Text, _
'                               DebtsDate.Text, _
'                               T2.Value, False
'
'End If
'End Sub
'
'Private Sub Form_Load()
'If (ChosenQuery = 3) Then
'    D1 = GlobalData
'    D2 = "������/������"
'    D3 = (Year(GlobalData) - 1) & "/" & Year(GlobalData)
'
'    'Total-------------------------------------------------------------------
'        Dim TotalSum As Currency
'        Dim strTotalSum As String
'        Dim P1 As String
'        Dim P2 As String
'
'        For i = 1 To Main.MainGrid.Rows - 1
'            If (Main.MainGrid.TextMatrix(i, 5) <> "") Then
'                If (InStr(1, Main.MainGrid.TextMatrix(i, 5), "+")) Then
'                    P1 = Left(Main.MainGrid.TextMatrix(i, 5), InStr(1, Main.MainGrid.TextMatrix(i, 5), "+") - 2)
'                    P2 = Right(Main.MainGrid.TextMatrix(i, 5), Len(Main.MainGrid.TextMatrix(i, 5)) - InStr(1, Main.MainGrid.TextMatrix(i, 5), "+") - 1)
'
'                    If (InStr(1, Main.MainGrid.TextMatrix(i, 5), "%")) Then
'                        TotalSum = TotalSum + CCur(P1) + (CCur(P1) * CCur(Left(P2, Len(P2) - 1)) / 100)
'                    Else
'                        TotalSum = TotalSum + CCur(P1) + CCur(P2)
'                    End If
'
'                Else
'                    TotalSum = TotalSum + CCur(Main.MainGrid.TextMatrix(i, 5))
'                End If
'            End If
'        Next i
'
'        strTotalSum = Format(TotalSum, ".00")
'
'        If (InStr(1, strTotalSum, ",") <> 0) Then
'            grn = Format(Left(strTotalSum, InStr(1, strTotalSum, ",") - 1), "###,###,###")
'            cop = Right(strTotalSum, Len(strTotalSum) - InStr(1, strTotalSum, ","))
'
'        ElseIf (InStr(1, strTotalSum, ".") <> 0) Then
'            grn = Format(Left(strTotalSum, InStr(1, strTotalSum, ".") - 1), "###,###,###")
'            cop = Right(strTotalSum, Len(strTotalSum) - InStr(1, strTotalSum, "."))
'
'        End If
'
'        strTotalSum = grn & " ���. " & cop & " ���."
'
'        Total.Caption = strTotalSum
'        F1.ZOrder
'
'ElseIf (ChosenQuery = 4) Then
'    M20.Caption = "31 ������ " & Year(GlobalData) & " ����"
'    F3.ZOrder
'
'End If
'LoadSuccess = True
'End Sub
'
