VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Begin VB.Form frmARSheet 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "��������������/������� ���������"
   ClientHeight    =   7260
   ClientLeft      =   1410
   ClientTop       =   1875
   ClientWidth     =   10695
   Icon            =   "frmARSheet.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   10695
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F2 
      Height          =   7215
      Left            =   3480
      TabIndex        =   3
      Top             =   0
      Width           =   7215
      Begin VB.CommandButton cmdPrintDifference 
         Height          =   375
         Left            =   3960
         Picture         =   "frmARSheet.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "��������� ��� html-����"
         Top             =   6720
         Width           =   375
      End
      Begin VB.CheckBox ckDelDublicates 
         Caption         =   "������� ������������� ������"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   6720
         Value           =   1  'Checked
         Width           =   2895
      End
      Begin VB.Frame Frame1 
         Caption         =   "������������ ��������"
         Height          =   645
         Left            =   120
         TabIndex        =   28
         Top             =   1080
         Width           =   6975
         Begin VB.CommandButton Command1 
            Height          =   375
            Left            =   6480
            Picture         =   "frmARSheet.frx":0294
            Style           =   1  'Graphical
            TabIndex        =   34
            ToolTipText     =   "��������� ��� html-����"
            Top             =   180
            Width           =   375
         End
         Begin VB.ComboBox EndSpecName 
            Height          =   315
            Left            =   1440
            TabIndex        =   30
            Top             =   240
            Width           =   2775
         End
         Begin VB.ComboBox comboGroups 
            Height          =   315
            Left            =   5040
            TabIndex        =   29
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "������������� :"
            Height          =   195
            Left            =   120
            TabIndex        =   32
            Top             =   240
            Width           =   1260
         End
         Begin VB.Label Label5 
            Caption         =   "������ :"
            Height          =   255
            Left            =   4320
            TabIndex        =   31
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.Frame FStatus 
         Caption         =   "��������� ��������"
         Height          =   975
         Left            =   120
         TabIndex        =   14
         Top             =   120
         Width           =   6975
         Begin VB.Label lblLastGroup 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   5880
            TabIndex        =   22
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label8 
            Caption         =   "������ :"
            Height          =   255
            Left            =   5160
            TabIndex        =   21
            Top             =   240
            Width           =   735
         End
         Begin VB.Label lblReason 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   960
            TabIndex        =   20
            Top             =   600
            Width           =   5895
         End
         Begin VB.Label Label6 
            Caption         =   "������� :"
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   600
            Width           =   855
         End
         Begin VB.Label lblOrder 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   3360
            TabIndex        =   18
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label label9 
            Caption         =   "� ������� :"
            Height          =   255
            Left            =   2280
            TabIndex        =   17
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblDate 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   960
            TabIndex        =   16
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label7 
            Caption         =   "���� :"
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   375
         Left            =   5880
         TabIndex        =   13
         Top             =   6720
         Width           =   1215
      End
      Begin VB.CommandButton cmdExecute 
         Caption         =   "OK"
         Enabled         =   0   'False
         Height          =   375
         Left            =   4680
         TabIndex        =   12
         Top             =   6720
         Width           =   1215
      End
      Begin VB.Frame FS3 
         Caption         =   "���������� � ���������"
         Height          =   975
         Left            =   120
         TabIndex        =   5
         Top             =   5640
         Width           =   6975
         Begin VB.Label Label4 
            Caption         =   "������������� :"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label lblTSpec 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   1560
            TabIndex        =   10
            Top             =   240
            Width           =   3255
         End
         Begin VB.Label Label3 
            Caption         =   "������ :"
            Height          =   255
            Left            =   4920
            TabIndex        =   9
            Top             =   240
            Width           =   735
         End
         Begin VB.Label lblTGroup 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   5640
            TabIndex        =   8
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "������������� :"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   600
            Width           =   1335
         End
         Begin VB.Label lblTLect 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   1560
            TabIndex        =   6
            Top             =   600
            Width           =   5295
         End
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblProgress 
         Height          =   3735
         Left            =   120
         TabIndex        =   4
         Top             =   1800
         Width           =   6975
         _cx             =   12303
         _cy             =   6588
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   3
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Frame F1 
      Height          =   7215
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   3375
      Begin VB.CommandButton Command2 
         Height          =   375
         Left            =   2880
         Picture         =   "frmARSheet.frx":03DE
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "��������� ��� html-����"
         Top             =   200
         Width           =   375
      End
      Begin VB.OptionButton O4 
         Caption         =   "�������� �����������"
         Height          =   195
         Left            =   480
         TabIndex        =   27
         Top             =   6870
         Width           =   2295
      End
      Begin VB.OptionButton O3 
         Caption         =   "��������� ����"
         Height          =   255
         Left            =   480
         TabIndex        =   26
         Top             =   6600
         Width           =   1575
      End
      Begin VB.OptionButton O2 
         Caption         =   "������������� �������"
         Height          =   255
         Left            =   480
         TabIndex        =   25
         Top             =   6360
         Width           =   2295
      End
      Begin VB.OptionButton O1 
         Caption         =   "����"
         Height          =   255
         Left            =   2160
         TabIndex        =   24
         Top             =   6120
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.TextBox txtFIO 
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   2655
      End
      Begin VB.ListBox lstFIO 
         Height          =   5520
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   3135
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "�������� ��������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   6120
         Width           =   1620
      End
   End
End
Attribute VB_Name = "frmARSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private BefData As Variant
Private ProgramSelecting As Boolean

Sub SelectGroup(IDSpec As Long, IDGroup As Long)

    Dim i As Long

    ProgramSelecting = True
    For i = 0 To EndSpecName.ListCount - 1
        If (EndSpecName.ItemData(i) = IDSpec) Then
            If (EndSpecName.ListIndex <> i) Then
                EndSpecName.ListIndex = i
            Else
                EndSpecName_Click
            End If
            Exit For
        End If
    Next i
    
    For i = 0 To comboGroups.ListCount - 1
        If (comboGroups.ItemData(i) = IDGroup) Then
            If (comboGroups.ListIndex <> i) Then
                comboGroups.ListIndex = i
            Else
                comboGroups_Click
            End If
            Exit For
        End If
    Next i
    ProgramSelecting = False
End Sub

Sub FillGroupProgressTable(IDStud As Long, IDGroup As Long, TP As VSFlexGrid)
Dim StudMarkArrey As Variant
Dim SMCols As Integer
Dim SMRows As Long

Dim i As Long

    CDB.SQLOpenTableToArrey "SELECT" & _
    " tblSubjects.Subject," & _
    " tblEducationalPlan.KindOfReport, tblEducationalPlan.Semester," & _
    " tblExaminations.ID" & _
    " FROM [tblEducationalPlan], [tblSubjects], [tblExaminations]" & _
    " WHERE (tblExaminations.IDGroup = " & IDGroup & " AND" & _
    " tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
    " tblEducationalPlan.IDSubject = tblSubjects.ID) AND" & _
    " ((NOT EXISTS(SELECT tblSheets.Mark FROM [tblSheets] WHERE tblSheets.IDStudent = " & IDStud & " AND tblSheets.IDExam = tblExaminations.ID)) OR" & _
    "  (SELECT tblSheets.Mark FROM [tblSheets] WHERE tblSheets.IDStudent = " & IDStud & " AND tblSheets.IDExam = tblExaminations.ID) IN " & IIf(FivePointMarkSystem, "(201, 202, 203, 1, 2)", "(201, 202, 203, 1, 2, 3)") & ")" & _
    " ORDER BY tblEducationalPlan.Semester, tblEducationalPlan.KindOfReport DESC", _
    StudMarkArrey, SMCols, SMRows
    
    Dim CurSemester As Byte
    CurSemester = 0
    
    Dim ExamCounter As Long
    ExamCounter = 1
    
    TP.Rows = 1
    If (SMRows > 0) Then
        For i = 0 To SMRows - 1
            If (CurSemester < CByte(StudMarkArrey(2, i))) Then
                TP.AddItem vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i))
                TP.RowData(TP.Rows - 1) = "Semester"
                TP.MergeRow(TP.Rows - 1) = True
                TP.Cell(flexcpAlignment, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = flexAlignCenterCenter

                ExamCounter = 1
                CurSemester = StudMarkArrey(2, i)
            End If
            
            TP.AddItem ExamCounter
            TP.RowData(TP.Rows - 1) = CLng(StudMarkArrey(3, i))
            TP.TextMatrix(TP.Rows - 1, 1) = CStr(StudMarkArrey(0, i))
            
            TP.TextMatrix(TP.Rows - 1, 2) = CStr(StudMarkArrey(1, i))
            TP.TextMatrix(TP.Rows - 1, 3) = ""
            TP.TextMatrix(TP.Rows - 1, 4) = "2"
            TP.TextMatrix(TP.Rows - 1, 5) = CStr(StudMarkArrey(2, i))
            TP.TextMatrix(TP.Rows - 1, 6) = "�� ���������"
            TP.TextMatrix(TP.Rows - 1, 7) = ""

            ExamCounter = ExamCounter + 1
        Next i
        
        If (TP.Rows > 1) Then TP.Cell(flexcpBackColor, 1, 3, TP.Rows - 1, 3) = RGB(200, 200, 200)
    
        UpdateProgressTable IDStud, IDGroup, TP
    End If

End Sub

Sub UpdateProgressTable(IDStud As Long, IDGroup As Long, TP As VSFlexGrid)
Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim i As Long
Dim j As Long

Dim IDExam As Long
Dim IDSubj As Long
Dim KOR As String
Dim Semester As Byte

Dim fOk As Boolean

If (lstFIO.ListCount = 0) Then Exit Sub
If (lstFIO.ListIndex < 0) Then Exit Sub

If (tblProgress.Rows < 2) Then Exit Sub

IDStud = CLng(lstFIO.ItemData(lstFIO.ListIndex))

For i = 1 To tblProgress.Rows - 1
    If ((tblProgress.RowData(i) <> "")) Then
        If (IsNumeric(tblProgress.RowData(i))) Then
            IDExam = CLng(tblProgress.RowData(i))
        
            CDB.SQLOpenTableToArrey "SELECT" & _
            " tblSubjects.ID, tblEducationalPlan.KindOfReport, tblEducationalPlan.Semester" & _
            " FROM [tblEducationalPlan], [tblSubjects], [tblExaminations]" & _
            " WHERE (tblExaminations.ID = " & IDExam & " AND" & _
            " tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
            " tblEducationalPlan.IDSubject = tblSubjects.ID)", _
            DopArrey, DopCols, DopRows
        
            IDSubj = CLng(DopArrey(0, 0))
            KOR = CStr(DopArrey(1, 0))
            Semester = CByte(DopArrey(2, 0))
        
            CDB.SQLOpenTableToArrey "SELECT" & _
            " tblSheets.ID, tblSheets.Mark, tblExaminations.ID" & _
            " FROM [tblEducationalPlan], [tblExaminations], [tblSheets]" & _
            " WHERE (tblSheets.IDStudent = " & IDStud & " AND" & _
            " tblSheets.IDExam = tblExaminations.ID AND" & _
            " tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
            " tblEducationalPlan.KindOfReport = '" & KOR & "' AND" & _
            " tblEducationalPlan.Semester = " & Semester & " AND" & _
            " tblEducationalPlan.IDSubject = " & IDSubj & ")", _
            DopArrey, DopCols, DopRows
               
            If (DopRows > 0) Then
                fOk = False
                For j = 0 To DopRows - 1
                    If (DopArrey(2, j) = IDExam) Then
                        tblProgress.TextMatrix(i, 7) = DopArrey(0, j)
                        DopArrey(0, j) = 0: DopArrey(1, j) = 0: DopArrey(2, j) = 0
                        fOk = True
                        
                        Exit For
                    End If
                Next j
                
                If ((DopRows > 2) Or ((DopRows = 2) And (Not fOk))) Then
                    tblProgress.TextMatrix(i, 3) = "������"
                    tblProgress.TextMatrix(i, 6) = ""
                ElseIf ((DopRows = 1) And fOk) Then
                    tblProgress.TextMatrix(i, 3) = ""
                    tblProgress.TextMatrix(i, 6) = "�����������"
                ElseIf ((DopRows = 2) And fOk) Then
                    Select Case CByte(IIf(j = 0, DopArrey(1, 1), DopArrey(1, 0)))
                        Case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
                            tblProgress.TextMatrix(i, 3) = CByte(IIf(j = 0, DopArrey(1, 1), DopArrey(1, 0)))
                        Case 201: tblProgress.TextMatrix(i, 3) = "�"
                        Case 202: tblProgress.TextMatrix(i, 3) = "�"
                        Case 203: tblProgress.TextMatrix(i, 3) = "�"
                    End Select
                    
                    tblProgress.RowData(i) = CLng(IIf(j = 0, DopArrey(0, 1), DopArrey(0, 0)))
                    tblProgress.TextMatrix(i, 6) = "�����������"
                    
                ElseIf ((DopRows = 1) And (Not fOk)) Then
                    Select Case CByte(DopArrey(1, 0))
                        Case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
                            tblProgress.TextMatrix(i, 3) = CByte(DopArrey(1, 0))
                        Case 201: tblProgress.TextMatrix(i, 3) = "�"
                        Case 202: tblProgress.TextMatrix(i, 3) = "�"
                        Case 203: tblProgress.TextMatrix(i, 3) = "�"
                    End Select
                    
                    tblProgress.TextMatrix(i, 7) = CLng(DopArrey(0, 0))
                End If
                
            Else
                tblProgress.TextMatrix(i, 3) = ""
                
            End If
            
        End If
    End If
Next i

End Sub


Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdExecute_Click()
'Dim DopArrey As Variant
'Dim DopCols As Integer
'Dim DopRows As Long

If (lstFIO.ListCount = 0) Then Exit Sub
If (lstFIO.ListIndex < 0) Then Exit Sub

If (comboGroups.ListCount = 0) Then Exit Sub
If (comboGroups.ListIndex < 0) Then Exit Sub

Dim IDStud As Long
Dim IDGroup As Long
Dim IDExam As Long
Dim IDSheet As Long
Dim Mark As String

Dim AR As Long

IDStud = CLng(lstFIO.ItemData(lstFIO.ListIndex))

IDGroup = comboGroups.ItemData(comboGroups.ListIndex)
AR = CDB.ExecSQLActionQuery("UPDATE [tblStudents] SET [IDGroup] = " & IDGroup & ", [IsActive] = 1, [DeductionDate] = NULL, [DeductionOrderNumber] = '', [DeductionReason] = '', [IsValidReason] = 0 WHERE [ID] = " & IDStud, 128)

'If (tblProgress.Rows < 2) Then Exit Sub

Dim BRow As Long
Dim i As Long

If (tblProgress.Row > 0) Then BRow = tblProgress.Row

For i = 1 To tblProgress.Rows - 1
    If ((tblProgress.RowData(i) <> "")) Then
        If (IsNumeric(tblProgress.RowData(i))) Then
        
            If ((tblProgress.TextMatrix(i, 6) = "��������") And (tblProgress.TextMatrix(i, 3) = "")) Then
                Mark = UCase(tblProgress.TextMatrix(i, 4))
                If (Mark = "+") Then Mark = "5"
                If (Mark = "-") Then Mark = "2"
                If (Mark = "�" Or Mark = "A") Then Mark = "201"
                If (Mark = "�") Then Mark = "202"
                If (Mark = "�" Or Mark = "H") Then Mark = "203"
                
                IDExam = CLng(tblProgress.RowData(i))
                AR = CDB.ExecSQLActionQuery("INSERT INTO tblSheets (IDStudent, IDExam, Mark) VALUES (" & IDStud & ", " & IDExam & ", " & Mark & ")", 128)
                
            ElseIf ((tblProgress.TextMatrix(i, 6) = "��������") And (tblProgress.TextMatrix(i, 3) <> "") And (tblProgress.TextMatrix(i, 7) <> "")) Then
                Mark = UCase(tblProgress.TextMatrix(i, 4))
                If (Mark = "+") Then Mark = "5"
                If (Mark = "-") Then Mark = "2"
                If (Mark = "�" Or Mark = "A") Then Mark = "201"
                If (Mark = "�") Then Mark = "202"
                If (Mark = "�" Or Mark = "H") Then Mark = "203"

                IDExam = CLng(tblProgress.RowData(i))
                AR = CDB.ExecSQLActionQuery("INSERT INTO tblSheets (IDStudent, IDExam, Mark) VALUES (" & IDStud & ", " & IDExam & ", " & Mark & ")", 128)
                
                If ((ckDelDublicates.Value = 1) And (IsNumeric(tblProgress.TextMatrix(i, 7)))) Then
                    IDSheet = CLng(tblProgress.TextMatrix(i, 7))
                    AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSheets] WHERE [ID] = " & IDSheet, 128)
                End If
                
            ElseIf ((tblProgress.TextMatrix(i, 6) = "�����������") And (tblProgress.TextMatrix(i, 3) <> "") And (tblProgress.TextMatrix(i, 7) <> "")) Then
                Mark = UCase(tblProgress.TextMatrix(i, 4))
                If (Mark = "+") Then Mark = "5"
                If (Mark = "-") Then Mark = "2"
                If (Mark = "�" Or Mark = "A") Then Mark = "201"
                If (Mark = "�") Then Mark = "202"
                If (Mark = "�" Or Mark = "H") Then Mark = "203"

                IDSheet = CLng(tblProgress.TextMatrix(i, 7))
                AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = " & Mark & " WHERE [ID] = " & IDSheet, 128)
                
                If ((ckDelDublicates.Value = 1) And (IsNumeric(tblProgress.RowData(i)))) Then
                    IDSheet = CLng(tblProgress.RowData(i))
                    AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSheets] WHERE [ID] = " & IDSheet, 128)
                End If
            
            ElseIf ((tblProgress.TextMatrix(i, 6) = "�����������") And (tblProgress.TextMatrix(i, 3) = "") And (tblProgress.TextMatrix(i, 7) <> "")) Then
                If (IsNumeric(tblProgress.TextMatrix(i, 7))) Then
                    Mark = UCase(tblProgress.TextMatrix(i, 4))
                    If (Mark = "+") Then Mark = "5"
                    If (Mark = "-") Then Mark = "2"
                    If (Mark = "�" Or Mark = "A") Then Mark = "201"
                    If (Mark = "�") Then Mark = "202"
                    If (Mark = "�" Or Mark = "H") Then Mark = "203"

                    IDSheet = CLng(tblProgress.TextMatrix(i, 7))
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = " & Mark & " WHERE [ID] = " & IDSheet, 128)
                End If
                
            End If
            
        End If
    End If
Next i

'FillGroupProgressTable IDStud, IDGroup, tblProgress
lstFIO_Click

    If ((tblProgress.Rows > 1) And (BRow <> 0)) Then
        tblProgress.Row = IIf(tblProgress.Rows > BRow, BRow, tblProgress.Rows - 1)
        tblProgress.ShowCell IIf(tblProgress.Rows > BRow, BRow, tblProgress.Rows - 1), 1
    End If

End Sub

Private Sub cmdPrintDifference_Click()
    LoadForm frmPrinting
    frmPrinting.VSPrinter1.Clear
frmPrinting.VSPrinter1.Refresh
With frmPrinting.VSPrinter1
    .Orientation = orPortrait
    
    '= orLandscape
    .StartDoc
    
    .MarginBottom = VerifyTwipsY(500)
    .MarginLeft = VerifyTwipsX(500)
    .MarginRight = VerifyTwipsX(500)
    .MarginTop = VerifyTwipsY(500)
    
    Dim a As Variant
    Dim r As Long
    Dim c As Integer
    
    Dim UN As String
    Dim FN As String
    Dim offs As String
    
    CDB.SQLOpenTableToArrey "SELECT UN,FN FROM tblOptions WHERE ID=1", a, c, r
    UN = CStr(a(0, 0))
    FN = CStr(a(1, 0))
    
    CDB.SQLOpenTableToArrey "SELECT OffsetNumber FROM tblStudents WHERE ID=" & lstFIO.ItemData(lstFIO.ListIndex), a, c, r
    offs = CStr(a(0, 0))
    Dim stud As String
 '   stud =
    stud = UCase(Left(lstFIO.List(lstFIO.ListIndex), InStr(lstFIO.List(lstFIO.ListIndex), " "))) & Right(lstFIO.List(lstFIO.ListIndex), Len(lstFIO.List(lstFIO.ListIndex)) - InStr(lstFIO.List(lstFIO.ListIndex), " "))

    .HdrFontSize = 12
    .MarginFooter = 1300

    .FontSize = 12
    .FontName = "Times New Roman Cyr"
    .FontBold = True
    .TextAlign = taCenterMiddle
    
    Main.xPrintText UN, VerifyTwipsX(1900), VerifyTwipsY(1300)
    Main.xPrintText "²��̲��� ��������� �����̲��ί в���ֲ", VerifyTwipsX(3400), VerifyTwipsY(1700)
    Main.xPrintText "��˲���� ������ � " & offs, VerifyTwipsX(3400), VerifyTwipsY(2100)
    .TextAlign = taJustBottom
    .FontBold = False
    
    Main.xPrintText "�������, ��'�, �� �������:", VerifyTwipsX(1500), VerifyTwipsY(2900)
    Main.xPrintText stud, VerifyTwipsX(5000), VerifyTwipsY(2900)
    .DrawLine VerifyTwipsX(4800), VerifyTwipsY(3200), VerifyTwipsX(10000), VerifyTwipsY(3200)
    
    .CurrentY = VerifyTwipsY(3300)
    .CurrentX = VerifyTwipsX(3300)
    .Paragraph = "���������:"
    .CurrentY = VerifyTwipsY(3300)
    .CurrentX = VerifyTwipsX(5000)
    .Paragraph = FN
    .DrawLine VerifyTwipsX(4800), VerifyTwipsY(3600), VerifyTwipsX(10000), VerifyTwipsY(3600)
    
    .CurrentY = VerifyTwipsY(3700)
    .CurrentX = VerifyTwipsX(2950)
    .Paragraph = "�������������:"
    .CurrentY = VerifyTwipsY(3700)
    .CurrentX = VerifyTwipsX(5000)
    .Paragraph = EndSpecName.text
    .DrawLine VerifyTwipsX(4800), VerifyTwipsY(4000), VerifyTwipsX(10000), VerifyTwipsY(4000)
   
    .CurrentY = VerifyTwipsY(4600)
    .CurrentX = VerifyTwipsX(600)
    
    
    .TablePenLR = 0
    .TablePenTB = 0
    .TablePen = 0

    .StartTable
    .TableCell(tcCols) = 14
    .TableCell(tcRows) = 5
    '.TableCell(tcFontSize) = 10
    
    'Dim q As Single
    'q = .PageWidth - .MarginLeft - .MarginRight - 6500
    
    .TableCell(tcColWidth, 1, 1) = VerifyTwipsX(350)
    .TableCell(tcColWidth, 1, 2) = VerifyTwipsX(2100)
    .TableCell(tcColWidth, 1, 3) = VerifyTwipsX(1000)
    .TableCell(tcColWidth, 1, 4) = VerifyTwipsX(1000)
    .TableCell(tcColWidth, 1, 5) = VerifyTwipsX(1500)
    .TableCell(tcColWidth, 1, 6) = VerifyTwipsX(350)
    .TableCell(tcColWidth, 1, 7) = VerifyTwipsX(350)
    .TableCell(tcColWidth, 1, 8) = VerifyTwipsX(350)
    .TableCell(tcColWidth, 1, 9) = VerifyTwipsX(350)
    .TableCell(tcColWidth, 1, 10) = VerifyTwipsX(600)
    .TableCell(tcColWidth, 1, 11) = VerifyTwipsX(400)
    .TableCell(tcColWidth, 1, 12) = VerifyTwipsX(1000)
    .TableCell(tcColWidth, 1, 13) = VerifyTwipsX(400)
    .TableCell(tcColWidth, 1, 14) = VerifyTwipsX(900)
  
    .FontSize = 8
    .TableCell(tcAlign) = taCenterMiddle
    '.TableCell(tcAlign, 1, 3, .TableCell(tcRows), .TableCell(tcCols)) = taCenterMiddle
    .TableCell(tcRowHeight, 1) = VerifyTwipsY(600)
    .TableCell(tcRowHeight, 2) = VerifyTwipsY(1200)
    .TableCell(tcFontSize, , 1) = 9
    .TableCell(tcFontSize, , 2) = 9
    
    .TableCell(tcRowSpan, 1, 1) = 2
    .TableCell(tcText, 1, 1) = "�"
    
    .TableCell(tcRowSpan, 1, 2) = 2
    .TableCell(tcText, 1, 2) = "����� ���������"
    
    .TableCell(tcRowSpan, 1, 3) = 2
    .TableCell(tcText, 1, 3) = "ʳ������ �����"
    
    .TableCell(tcRowSpan, 1, 4) = 2
    .TableCell(tcText, 1, 4) = "��� �������"
    .TableCell(tcRowSpan, 1, 5) = 2
    .TableCell(tcText, 1, 5) = "�.�.�. ���������"
    
    .TableCell(tcColSpan, 1, 6) = 4
    .TableCell(tcText, 1, 6) = "�������� �������� (����)"
    .TableCell(tcVertical, 2, 6) = True
    .TableCell(tcText, 2, 6) = "������ �1"
    .TableCell(tcVertical, 2, 7) = True
    .TableCell(tcText, 2, 7) = "������ �2"
    .TableCell(tcVertical, 2, 8) = True
    .TableCell(tcText, 2, 8) = "������ �3"
    .TableCell(tcVertical, 2, 9) = True
    .TableCell(tcText, 2, 9) = "������ �4"
    
    .TableCell(tcRowSpan, 1, 10) = 2
    .TableCell(tcVertical, 1, 10) = True
    .TableCell(tcText, 1, 10) = "ʳ������ ���� �� �������. ������. ��������"
    
    .TableCell(tcRowSpan, 1, 11) = 2
    .TableCell(tcVertical, 1, 11) = True
    .TableCell(tcText, 1, 11) = "�������� ������� ����"
    
    .TableCell(tcRowSpan, 1, 12) = 2
    .TableCell(tcVertical, 1, 12) = True
    .TableCell(tcText, 1, 12) = "ϳ�������� ������ �� ���.������ ��� ������ ��� ��������� �����"
    
    .TableCell(tcRowSpan, 1, 13) = 2
    .TableCell(tcVertical, 1, 13) = True
    .TableCell(tcText, 1, 13) = "������ �� ������ ECTS"
    
    .TableCell(tcRowSpan, 1, 14) = 2
    .TableCell(tcVertical, 1, 14) = True
    .TableCell(tcText, 1, 14) = "ϳ���� ���������, ���� �������� ��������� ������"
    
    Dim i As Integer
    Dim num As Integer
    Dim fgSem As Boolean
    Dim rowCnt As Integer
    rowCnt = 2
    fgSem = False
    num = 0
    For i = 1 To tblProgress.Rows - 1
        If Not IsNumeric(tblProgress.RowData(i)) Then
            If Not fgSem Then
                rowCnt = rowCnt + 1
                .TableCell(tcRows) = rowCnt
                .TableCell(tcColSpan, rowCnt, 1) = 14
                .TableCell(tcAlign, rowCnt, 1) = taCenterMiddle
                fgSem = True
            End If
                .TableCell(tcText, rowCnt, 1) = tblProgress.TextMatrix(i, 1)
        ElseIf tblProgress.TextMatrix(i, 6) = "��������" Or tblProgress.TextMatrix(i, 6) = "�����������" Then
            rowCnt = rowCnt + 1
            .TableCell(tcRows) = rowCnt
            fgSem = False

            CDB.SQLOpenTableToArrey "SELECT surname,name,patronymic, " & _
            "KindOfReport,NumOfHours, subject FROM tblLecturers, " & _
            "tblExaminations,tblEducationalPlan,tblSubjects WHERE " & _
            "idLecturer=tblLecturers.id and idep=tblEducationalPlan.id " & _
            "and idSubject=tblSubjects.id and tblExaminations.id=" & _
            tblProgress.RowData(i), a, c, r

            num = num + 1
            .TableCell(tcText, rowCnt, 1) = num
            .TableCell(tcText, rowCnt, 2) = CStr(a(5, 0))
            If Not IsNull(a(4, 0)) Then .TableCell(tcText, rowCnt, 3) = CStr(a(4, 0))
            If CStr(a(3, 0)) = "�������" Then a(3, 0) = "�����"
            If CStr(a(3, 0)) = "�����" Then a(3, 0) = "����"
            .TableCell(tcText, rowCnt, 4) = CStr(a(3, 0))
            .TableCell(tcText, rowCnt, 5) = GetFullName(CStr(a(0, 0)), CStr(a(1, 0)), CStr(a(2, 0)), False, True, False)
        End If
    Next i
    If fgSem Then .TableCell(tcRows) = .TableCell(tcRows) - 1
    .EndTable
   CDB.SQLOpenTableToArrey "SELECT [Plist],[Dlist] FROM tblOptions WHERE ID=1", a, c, r
    
    Dim SP As String, sd As String
    Dim qwe As Integer
    qwe = InStr(CStr(a(0, 0)), "|") - 1
    SP = Mid(CStr(a(0, 0)), 1, IIf(qwe > 0, qwe, Len(CStr(a(0, 0)))))
    qwe = InStr(CStr(a(1, 0)), "|") - 1
    sd = Mid(CStr(a(1, 0)), 1, IIf(qwe > 0, qwe, Len(CStr(a(1, 0)))))
    
    .FontBold = True
    .CurrentY = .CurrentY + VerifyTwipsY(500)
    a = .CurrentY
    .CurrentX = VerifyTwipsX(1000)
    .Paragraph = SP
    
    .CurrentY = a
    .CurrentX = VerifyTwipsX(5000)
    .Paragraph = sd
    .FontBold = False

.EndDoc
End With

frmPrinting.Show 1, Me

End Sub

Private Sub comboGroups_Click()
Dim IDGroup As Long
Dim IDStud As Long

If (lstFIO.ListCount = 0) Then Exit Sub
If (lstFIO.ListIndex < 0) Then Exit Sub

If (comboGroups.ListCount = 0) Then Exit Sub
If (comboGroups.ListIndex < 0) Then Exit Sub

IDStud = lstFIO.ItemData(lstFIO.ListIndex)
IDGroup = comboGroups.ItemData(comboGroups.ListIndex)

FillGroupProgressTable IDStud, IDGroup, tblProgress
End Sub

Private Sub Command1_Click()
On Error GoTo ErrHandler2

        Main.CommonDialog1.FileName = ""
        Main.CommonDialog1.InitDir = App.Path
        Main.CommonDialog1.DialogTitle = "���������� �������"
        Main.CommonDialog1.Filter = "Hyper text files (*.html)|*.html"
        Main.CommonDialog1.CancelError = True
        Main.CommonDialog1.ShowSave
        
        If (Dir(Main.CommonDialog1.FileName) <> "") Then If (MsgBox("���� " & Main.CommonDialog1.FileName & " ��� ����������." & Chr(13) & Chr(10) & "�������� ��� ?", vbYesNo, "����������") = vbNo) Then Exit Sub
        SaveHTML tblProgress, Main.CommonDialog1.FileName, lstFIO.List(lstFIO.ListIndex)
Exit Sub
ErrHandler2:
If (Err <> cdlCancel) Then MsgBox Error(Err)
End Sub

Private Sub Command2_Click()
On Error GoTo ErrHandler1

        Main.CommonDialog1.FileName = ""
        Main.CommonDialog1.InitDir = App.Path
        Main.CommonDialog1.DialogTitle = "���������� �������"
        Main.CommonDialog1.Filter = "Hyper text files (*.html)|*.html"
        Main.CommonDialog1.CancelError = True
        Main.CommonDialog1.ShowSave
        
        
        Dim LCaption As String
        If (O1.Value) Then LCaption = "����"
        If (O2.Value) Then LCaption = "������������� �������"
        If (O3.Value) Then LCaption = "��������� ����"
        If (O4.Value) Then LCaption = "�������� �����������"
    
        If (Dir(Main.CommonDialog1.FileName) <> "") Then If (MsgBox("���� " & Main.CommonDialog1.FileName & " ��� ����������." & Chr(13) & Chr(10) & "�������� ��� ?", vbYesNo, "����������") = vbNo) Then Exit Sub
        SaveHTMLList lstFIO, Main.CommonDialog1.FileName, LCaption
        
Exit Sub
ErrHandler1:
If (Err <> cdlCancel) Then MsgBox Error(Err)
End Sub

Private Sub EndSpecName_Click()
    Dim GroupArrey As Variant
    Dim GCols As Integer
    Dim GRows As Long
    
    Dim GroupName As String
    Dim i As Long
    
    If (EndSpecName.ListIndex < 0) Then Exit Sub

   
    CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester] FROM [tblGroups] WHERE [IDSpec] = " & EndSpecName.ItemData(EndSpecName.ListIndex) & " ORDER BY [Semester], [GroupName]", GroupArrey, GCols, GRows
    
    comboGroups.Clear
    If (GRows > 0) Then
    
        For i = 0 To GRows - 1
            GroupName = CStr(GroupArrey(1, i))
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupArrey(2, i)) Mod 2 = 1, (CLng(GroupArrey(2, i)) \ 2) + 1, CLng(GroupArrey(2, i)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))

            comboGroups.AddItem GroupName
            comboGroups.ItemData(comboGroups.NewIndex) = GroupArrey(0, i)
        Next i
        
        If (Not ProgramSelecting) Then comboGroups.ListIndex = 0
    End If
End Sub

Private Sub Form_Load()

ProgramSelecting = False

With tblProgress
    .Cols = 8
    .Rows = 1
    .FixedCols = 1
    .FixedRows = 1
    
    .TextMatrix(0, 0) = "�"
    .TextMatrix(0, 1) = "�������� ���������"
    .TextMatrix(0, 2) = "��� ���."
    .TextMatrix(0, 3) = "������1"
    .TextMatrix(0, 4) = "������2"
    .TextMatrix(0, 5) = "�������"
    .TextMatrix(0, 6) = "��������"
    .TextMatrix(0, 7) = "ID_Sheet"
    
    .ColWidth(0) = 500
    .ColWidth(1) = 2600
    .ColWidth(2) = 800
    .ColWidth(3) = 800
    .ColWidth(4) = 800
    .ColWidth(6) = 1180
    
    .ColHidden(5) = True
    .ColHidden(7) = True
    
    .MergeCells = flexMergeFree
    
    .ColAlignment(3) = flexAlignCenterCenter
    .ColAlignment(6) = flexAlignCenterCenter
End With

    Dim SpecArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long
    
    Dim i As Long
    
    CDB.SQLOpenTableToArrey "SELECT [ID], [SpecName] FROM [tblSpecialities]", SpecArrey, SCols, SRows
    
    If (SRows = 0) Then
        MsgBox "��� ������ � ���������� �� ���������� ������ ���� ���������������� ������� ���� ������������� !", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (SRows > 0) Then
    
        For i = 0 To SRows - 1
            EndSpecName.AddItem SpecArrey(1, i)
            EndSpecName.ItemData(EndSpecName.NewIndex) = SpecArrey(0, i)
        Next i
        
        EndSpecName.ListIndex = 0
    End If

    If (Main.ExckEditor.Value = 1) Then cmdExecute.Enabled = True

txtFIO_Change
LoadSuccess = True
End Sub

Private Sub lblCurGroup_Click()

End Sub

Private Sub lstFIO_Click()
Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim Dop2Arrey As Variant
Dim Dop2Cols As Integer
Dim Dop2Rows As Long

    lblTSpec.Caption = ""
    lblTGroup.Caption = ""
    lblTLect.Caption = ""

'EndSpecName.Enabled = False
'comboGroups.Enabled = False

If (lstFIO.ListCount = 0) Then Exit Sub
If (lstFIO.ListIndex < 0) Then Exit Sub

Dim IDSpec As Long
Dim IDGroup As Long
Dim IDStud As Long

Dim Status As String
Dim SDate As String
Dim SOrder As String
Dim SReason As String

Dim GroupName As String
Dim Semester As Byte

IDStud = lstFIO.ItemData(lstFIO.ListIndex)

CDB.SQLOpenTableToArrey "SELECT [IDGroup], [IsActive], [DeductionDate], [DeductionOrderNumber], [DeductionReason], [IsValidReason], [RestoreInfo], [TRestoreInfo] FROM [tblStudents] WHERE tblStudents.ID = " & IDStud, DopArrey, DopCols, DopRows

If (DopArrey(1, 0) = 1) Then
    IDGroup = DopArrey(0, 0)
    
    CDB.SQLOpenTableToArrey "SELECT [IDSpec] FROM [tblGroups] WHERE ID = " & IDGroup, Dop2Arrey, Dop2Cols, Dop2Rows
    IDSpec = CLng(Dop2Arrey(0, 0))
    
    Status = "��������� �������� : ���������"

    SDate = ""
    SOrder = ""
    SReason = ""
    GroupName = ""
    
    SelectGroup IDSpec, IDGroup
Else

    If ((Not IsNull(DopArrey(7, 0))) And (Not IsNull(DopArrey(6, 0)))) Then
        If ((DopArrey(7, 0) <> "") And (IsNumeric(DopArrey(6, 0)))) Then
            GroupName = CStr(DopArrey(7, 0))
            Semester = CByte(DopArrey(6, 0))
            
            CDB.SQLOpenTableToArrey "SELECT [ID], [IDSpec] FROM [tblGroups] WHERE " & _
                                    "[GroupName] = '" & GroupName & "' AND " & _
                                    "([Semester] = " & IIf(Semester \ 2 = 1, Semester, Semester - 1) & " OR " & _
                                    " [Semester] = " & IIf(Semester \ 2 = 1, Semester + 1, Semester) & ")" _
                                    , Dop2Arrey, Dop2Cols, Dop2Rows

            If (Dop2Rows > 0) Then
                IDGroup = CLng(Dop2Arrey(0, 0))
                IDSpec = CLng(Dop2Arrey(1, 0))
                SelectGroup IDSpec, IDGroup
            End If
            
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(Semester Mod 2 = 1, (Semester \ 2) + 1, Semester \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
        End If
    End If

    If (Not IsNull(DopArrey(5, 0))) Then
        If (CByte(DopArrey(5, 0)) = 0) Then Status = "��������� �������� : �������� ��������"
        If (CByte(DopArrey(5, 0)) = 1) Then Status = "��������� �������� : ������������ ������"
        If (CByte(DopArrey(5, 0)) = 2) Then Status = "��������� �������� : ��������� ����"
    Else
        Status = "��������� �������� : �������� ��������"
    End If


    If (Not IsNull(DopArrey(2, 0))) Then SDate = CDate(DopArrey(2, 0))
    If (Not IsNull(DopArrey(3, 0))) Then SOrder = CStr(DopArrey(3, 0))
    If (Not IsNull(DopArrey(4, 0))) Then SReason = CStr(DopArrey(4, 0))

'    EndSpecName.Enabled = True
'    comboGroups.Enabled = True
    
    comboGroups_Click
End If

FStatus.Caption = Status
lblDate.Caption = SDate
lblOrder.Caption = SOrder
lblReason.Caption = SReason
lblLastGroup.Caption = GroupName

End Sub

Private Sub tblProgressT_Click()

End Sub

Private Sub O1_Click()
txtFIO_Change
End Sub

Private Sub O2_Click()
txtFIO_Change
End Sub

Private Sub O3_Click()
txtFIO_Change
End Sub

Private Sub O4_Click()
txtFIO_Change
End Sub

Private Sub tblProgress_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim Mark As String

If (Col = 4) Then
    Mark = UCase(tblProgress.TextMatrix(Row, Col))
    
    If (Not CheckMark(Mark)) Then
        MsgBox "� ���� '������' ��������� ������ ��������� ������� :" & Chr(13) & Chr(10) & IIf(FivePointMarkSystem, "[�,�,1,2,3,4,5]", "[�,�,1,2,3,4,5,6,7,8,9,10,11,12]")
        tblProgress.TextMatrix(Row, Col) = BefData
        Exit Sub
    End If
End If

End Sub

Private Sub tblProgress_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefData = tblProgress.TextMatrix(Row, Col)
End Sub

Private Sub tblProgress_Click()
    Dim SheetNumber As String
    Dim SpecName As String
    Dim GroupName As String
    Dim Lecturer As String
    Dim Semester As Byte
    
    lblTSpec.Caption = ""
    lblTGroup.Caption = ""
    lblTLect.Caption = ""
        
    If ((tblProgress.TextMatrix(tblProgress.Row, 6) = "�����������" Or tblProgress.TextMatrix(tblProgress.Row, 6) = "����������") And (tblProgress.TextMatrix(tblProgress.Row, 3) <> "") And (tblProgress.TextMatrix(tblProgress.Row, 7) <> "")) Then
        lblTSpec.Caption = ""
        lblTGroup.Caption = ""
        lblTLect.Caption = ""
        Exit Sub
    End If
    
    If (tblProgress.Row < 1 Or tblProgress.Rows = 1) Then Exit Sub
    
    If (tblProgress.RowData(tblProgress.Row) = "" Or tblProgress.RowData(tblProgress.Row) = "Semester") Then Exit Sub
    
    Semester = CByte(tblProgress.TextMatrix(tblProgress.Row, 5))
    GetSheetInfo tblProgress.RowData(tblProgress.Row), _
                 Semester, SheetNumber, SpecName, GroupName, Lecturer
                 
    lblTSpec.Caption = SpecName
    lblTGroup.Caption = GroupName
    lblTLect.Caption = Lecturer
End Sub

Private Sub tblProgress_DblClick()
    If (Main.ExckEditor.Value = 1) Then
        If (tblProgress.Col = 6 And (tblProgress.text = "��������" Or tblProgress.text = "�� ���������")) Then
            tblProgress.text = IIf(tblProgress.text = "��������", "�� ���������", "��������")
            
        ElseIf (tblProgress.Col = 6 And (tblProgress.text = "�����������" Or tblProgress.text = "����������") And tblProgress.TextMatrix(tblProgress.Row, 7) <> "") Then
            tblProgress.text = IIf(tblProgress.text = "�����������", "����������", "�����������")
            
        End If
    End If
End Sub

Private Sub tblProgress_RowColChange()
    If (Main.ExckEditor.Value = 0) Then
        tblProgress.Editable = flexEDNone
        Exit Sub
    End If

    If (tblProgress.Col = 4) Then
        tblProgress.Editable = flexEDKbdMouse
    Else:
        tblProgress.Editable = flexEDNone
    End If
End Sub

Private Sub txtFIO_Change()
Dim StudName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim StudArrey As Variant
Dim SCols As Integer
Dim SRows As Long

Dim i As Long

StudName = txtFIO.text

lblTSpec.Caption = ""
lblTGroup.Caption = ""
lblTLect.Caption = ""
'EndSpecName.Enabled = False
'comboGroups.Enabled = False

FStatus.Caption = ""
lblDate.Caption = ""
lblOrder.Caption = ""
lblReason.Caption = ""
lblLastGroup.Caption = ""
tblProgress.Rows = 1

If (StudName = "") Then
    If (O1.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents]ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
    If (O2.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [IsValidReason] = 1 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
    If (O3.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [IsValidReason] = 2 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
    If (O4.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [IsValidReason] = 0 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
Else
    
    'If (InStr(1, StudName, "'") <> 0) Then Exit Sub
    
    Dim FirstS As Byte
    Dim SecondS As Byte
    
    FirstS = InStr(1, StudName, " ")
    If (FirstS) Then SecondS = InStr(InStr(1, StudName, " ") + 1, StudName, " ")
    
    If ((FirstS = 0) And (SecondS = 0)) Then
        Surname = StudName
        
        CheckString Surname
        If (O1.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] LIKE '" & Surname & "*' ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O2.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] LIKE '" & Surname & "*' AND [IsValidReason] = 1 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O3.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] LIKE '" & Surname & "*' AND [IsValidReason] = 2 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O4.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] LIKE '" & Surname & "*' AND [IsValidReason] = 0 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        
    ElseIf ((FirstS <> 0) And (SecondS = 0)) Then
        Surname = Left(StudName, FirstS - 1)
        Name = Mid(StudName, FirstS + 1, Len(StudName) - FirstS)
        
        CheckString Surname
        CheckString Name
        If (O1.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O2.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsValidReason] = 1 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O3.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsValidReason] = 2 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O4.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsValidReason] = 0 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        
    ElseIf ((FirstS <> 0) And (SecondS <> 0)) Then
        Surname = Left(StudName, FirstS - 1)
        Name = Mid(StudName, FirstS + 1, SecondS - FirstS - 1)
        Patronymic = Right(StudName, Len(StudName) - SecondS)
        
        CheckString Surname
        CheckString Name
        CheckString Patronymic
        If (O1.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O2.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsValidReason] = 1 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O3.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsValidReason] = 2 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        If (O4.Value) Then CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsValidReason] = 0 AND [IsActive] = 0 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
        
    End If
End If

lstFIO.Clear
If (SRows > 0) Then

    For i = 0 To SRows - 1
        lstFIO.AddItem GetFullName(CStr(StudArrey(1, i)), CStr(StudArrey(2, i)), CStr(StudArrey(3, i)), False, False, True)
        lstFIO.ItemData(lstFIO.NewIndex) = StudArrey(0, i)
    Next i
    
    lstFIO.ListIndex = 0
    
Else

End If


End Sub
