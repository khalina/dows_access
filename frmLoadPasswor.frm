VERSION 5.00
Begin VB.Form frmLoadPassword 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "������� ������ ��� �����"
   ClientHeight    =   1095
   ClientLeft      =   4950
   ClientTop       =   6540
   ClientWidth     =   3375
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1095
   ScaleWidth      =   3375
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3375
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "������"
         Height          =   375
         Left            =   1680
         TabIndex        =   3
         Top             =   600
         Width           =   1575
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "��"
         Default         =   -1  'True
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox txtLoadPassword 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   120
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   240
         Width           =   3135
      End
   End
End
Attribute VB_Name = "frmLoadPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
Unload Me
End Sub

Private Sub cmdOK_Click()
    If (LoadPassword = txtLoadPassword.Text) Then
        LoadPasswordChecked = True
        Unload Me
        Exit Sub
    Else
        MsgBox "��������� ������������ ����� ������!", vbOKOnly, "������"
        txtLoadPassword.SetFocus
        txtLoadPassword.SelStart = 0
        txtLoadPassword.SelLength = Len(txtLoadPassword.Text)
    End If
End Sub

Private Sub Form_Load()
    LoadSuccess = True
End Sub
