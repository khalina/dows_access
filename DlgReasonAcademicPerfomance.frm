VERSION 5.00
Begin VB.Form DlgReasonAcademicPerfomance 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "������� ������ ���������"
   ClientHeight    =   4785
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   4755
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   4755
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox ComboReason 
      Height          =   315
      ItemData        =   "DlgReasonAcademicPerfomance.frx":0000
      Left            =   120
      List            =   "DlgReasonAcademicPerfomance.frx":0002
      TabIndex        =   4
      Top             =   720
      Width           =   4455
   End
   Begin VB.CommandButton AddReason 
      Caption         =   "��������"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   4200
      Width           =   2055
   End
   Begin VB.ListBox ListReason 
      Height          =   2790
      ItemData        =   "DlgReasonAcademicPerfomance.frx":0004
      Left            =   120
      List            =   "DlgReasonAcademicPerfomance.frx":0006
      TabIndex        =   1
      Top             =   1320
      Width           =   4455
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "�������"
      Height          =   495
      Left            =   2520
      TabIndex        =   0
      Top             =   4200
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "������� ������ ��������� �� ���������"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   240
      Width           =   4455
   End
End
Attribute VB_Name = "DlgReasonAcademicPerfomance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub AddReason_Click()
    Dim Reason As String
    
    Reason = InputBox("������� ������� ������ ���������", "���������� �������")
    If (Reason <> "") Then
        ListReason.AddItem (Reason)
        ComboReason.AddItem (Reason)
        Reason = Chr(34) & Reason & Chr(34)
            
        Dim AR As Long
        AR = CDB.ExecSQLActionQuery("INSERT INTO tblReasonAcademPerf ( Reason ) VALUES (" & Reason & ")", 128)
    End If
End Sub

Private Sub ComboReason_Change()
    Dim AR As Long
    Dim xarrey As Variant
    Dim xcols As Integer
    Dim xrows As Long
    
     CDB.SQLOpenTableToArrey "SELECT tblReasonAcademPerf.ID, tblReasonAcademPerf.Reason FROM tblReasonAcademPerf " & _
        "WHERE tblReasonAcademPerf.Reason = " & Chr(34) & ComboReason.text & Chr(34), xarrey, xcols, xrows
        
    AR = CDB.ExecSQLActionQuery("UPDATE  tblOptions SET ReasonAP = " & Chr(34) & xarrey(0, 0) & Chr(34) & " WHERE ID = 1", 128)

End Sub

Private Sub Form_Load()
    Dim xarrey As Variant
    Dim xcols As Integer
    Dim xrows As Long
    Dim i As Integer
    Dim Reason As String
        
    CDB.SQLOpenTableToArrey "SELECT tblReasonAcademPerf.Reason FROM tblReasonAcademPerf " & _
        "ORDER BY tblReasonAcademPerf.Reason ", xarrey, xcols, xrows
        
    
    For i = 0 To xrows - 1
         ListReason.AddItem (xarrey(0, i))
         ComboReason.AddItem (xarrey(0, i))
    Next
    
    'CDB.SQLGetRecordCount "SELECT tblOptions.ReasonAP FROM tblOptions WHERE isNull(tblOptions.ReasonAP)", xarrey, xcols, xrows
    If (CDB.SQLGetRecordCount("SELECT tblOptions.ReasonAP FROM tblOptions WHERE isNull(tblOptions.ReasonAP)") <> 0) Then
        MsgBox "��� ������� �� ���������"
    Else
        CDB.SQLOpenTableToArrey "SELECT tblReasonAcademPerf.Reason, tblReasonAcademPerf.ID, tblOptions.ReasonAP " & _
            "FROM tblReasonAcademPerf, tblOptions WHERE tblReasonAcademPerf.ID = tblOptions.ReasonAP", xarrey, xcols, xrows
        ComboReason.text = xarrey(0, 0)
    End If
    
End Sub


Private Sub OKButton_Click()
    DlgReasonAcademicPerfomance.Hide
    Dim AR As Long
    Dim xarrey As Variant
    Dim xcols As Integer
    Dim xrows As Long
    If (ComboReason.text <> "") Then
         CDB.SQLOpenTableToArrey "SELECT tblReasonAcademPerf.ID, tblReasonAcademPerf.Reason FROM tblReasonAcademPerf " & _
            "WHERE tblReasonAcademPerf.Reason = " & Chr(34) & ComboReason.text & Chr(34), xarrey, xcols, xrows
            
        AR = CDB.ExecSQLActionQuery("UPDATE  tblOptions SET ReasonAP = " & Chr(34) & xarrey(0, 0) & Chr(34) & " WHERE ID = 1", 128)
    End If
End Sub
