Attribute VB_Name = "DBArchive"
Option Explicit

Public arc As UDBArchive.DBArchive
Public CurArcPath As String
Public ArcLoaded As Boolean

Private Function eqChkStr(ByRef str As Variant) As String
    Dim s As String
    s = ChkStr(str)
    If IsNull(str) Then eqChkStr = " IS " & s Else eqChkStr = "=" & s
End Function
Sub MoveStudentToArchive(StudentID As Long, db As UniverDB.DBClass, arc As UDBArchive.DBArchive)
    Dim a1 As Variant, a2 As Variant, a3 As Variant, a4 As Variant, a5 As Variant
    Dim c1 As Integer, c2 As Integer, c3 As Integer, c4 As Integer, c5 As Integer
    Dim r1 As Long, r2 As Long, r3 As Long, r4 As Long, r5 As Long
    
    Dim i As Long
    
    Dim idDBO As Long
    Dim IDSpec As Long
    Dim GroupName As String
    Dim idSpecialisation As Long
    Dim IDFaculty As Long
    Dim idCategory As Long
    Dim idNomGrant As Long
    Dim idStudOld As Long
    Dim idStudNew As Long
    Dim idLecturer As Long
    Dim idSubject   As Long
    Dim idEP As Long
    Dim IDExam As Long
    
    'DBOptions for Student
    db.SQLOpenTableToArrey "SELECT un,unr,unf,fn,fnr,dlist,ilist,plist,educform,fpms" & _
    " FROM tblOptions WHERE id=1", a1, c1, r1
    '
    arc.doQueryArchive "SELECT id FROM DBOptions WHERE" & _
    " un" & eqChkStr(a1(0, 0)) & " AND unr" & eqChkStr(a1(1, 0)) & " AND unf" & _
    eqChkStr(a1(2, 0)) & " AND fn" & eqChkStr(a1(3, 0)) & " AND fnr" & eqChkStr(a1(4, 0)) & _
    " AND dlist" & eqChkStr(a1(5, 0)) & " AND  ilist" & eqChkStr(a1(6, 0)) & " AND plist" & _
    eqChkStr(a1(7, 0)) & " AND educform" & eqChkStr(a1(8, 0)) & " AND fpms=" & CStr(a1(9, 0)) _
    , a2, c2, r2
     '
    If r2 = 0 Then
        arc.doUpdateArchive "INSERT INTO DBoptions " & _
        "(un,unr,unf,fn,fnr,dlist,ilist,plist,educform,fpms) VALUES " & _
        "(" & ChkStr(a1(0, 0)) & "," & ChkStr(a1(1, 0)) & "," & ChkStr(a1(2, 0)) & _
        "," & ChkStr(a1(3, 0)) & "," & ChkStr(a1(4, 0)) & "," & ChkStr(a1(5, 0)) & _
        "," & ChkStr(a1(6, 0)) & "," & ChkStr(a1(7, 0)) & "," & ChkStr(a1(8, 0)) & _
        "," & CStr(a1(9, 0)) & ")", 128
        '
        arc.doQueryArchive "SELECT id FROM DBOptions WHERE" & _
        " un" & eqChkStr(a1(0, 0)) & " AND unr" & eqChkStr(a1(1, 0)) & " AND unf" & _
        eqChkStr(a1(2, 0)) & " AND fn" & eqChkStr(a1(3, 0)) & " AND fnr" & eqChkStr(a1(4, 0)) & _
        " AND dlist" & eqChkStr(a1(5, 0)) & " AND  ilist" & eqChkStr(a1(6, 0)) & _
        " AND plist" & eqChkStr(a1(7, 0)) & " AND educform" & eqChkStr(a1(8, 0)) & _
        " AND fpms=" & CStr(a1(9, 0)), a2, c2, r2
    End If
    idDBO = a2(0, 0)
    
    'old Student info
    db.SQLOpenTableToArrey "SELECT id,idcategory,idnominalgrant,idchair,idspecialisation," & _
    "surname,name,patronymic,studphoto,schoolcity,address,kharkovaddress,dateofborn," & _
    "nationality,education,maritalstatus,formofpayment,informationontheparents,gratitude," & _
    "reprimands,'',offsetnumber,isforeigner,libnumber,deductiondate,atestatnum," & _
    "diplomnum,deductionordernumber,deductionreason,0,regnumber," & _
    "additionnumber,dasettings,idcode,studnum,entryyear,idgroup" & _
    " FROM tblStudents WHERE ID=" & CStr(StudentID), a1, c1, r1
    '
    idStudOld = a1(0, 0)
    
    '
    db.SQLOpenTableToArrey "SELECT idspec," & _
    "(Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#')))" & _
    " FROM tblGroups WHERE id=" & CStr(a1(36, 0)), a2, c2, r2
    '
    IDSpec = a2(0, 0)
    GroupName = a2(1, 0)
    
    'Speciality for Student
    db.SQLOpenTableToArrey "SELECT SpecName,SCode FROM tblSpecialities WHERE id=" & _
    CStr(IDSpec), a2, c2, r2
    '
    arc.doQueryArchive "SELECT id FROM Speciality WHERE SpecName" & eqChkStr(a2(0, 0)) & _
    " AND SCode" & eqChkStr(a2(1, 0)), a3, c3, r3
    '
    If r3 = 0 Then
        arc.doUpdateArchive "INSERT INTO Speciality (SpecName,SCode) VALUES " & _
        "(" & ChkStr(a2(0, 0)) & "," & ChkStr(a2(1, 0)) & ")", 128
        '
        arc.doQueryArchive "SELECT id FROM Speciality WHERE SpecName" & eqChkStr(a2(0, 0)) & _
        " AND SCode" & eqChkStr(a2(1, 0)), a3, c3, r3
    End If
    IDSpec = a3(0, 0)
    
    'Specialisation for Student
    db.SQLOpenTableToArrey "SELECT Specialisation FROM tblSpecialisations" & _
    " WHERE id=" & a1(4, 0), a2, c2, r2
    '
    arc.doQueryArchive "SELECT id FROM Specialisation WHERE Specialisation" & eqChkStr(a2(0, 0)) _
    , a3, c3, r3
    '
    If r3 = 0 Then
        arc.doUpdateArchive "INSERT INTO Specialisation (Specialisation) VALUES " & _
        "(" & ChkStr(a2(0, 0)) & ")", 128
        '
        arc.doQueryArchive "SELECT id FROM Specialisation WHERE Specialisation" & _
        eqChkStr(a2(0, 0)), a3, c3, r3
    End If
    idSpecialisation = a3(0, 0)
    
    'Faculty for Student
    db.SQLOpenTableToArrey "SELECT FacultyName,SCode FROM tblFaculty WHERE id=" & _
    CStr(a1(3, 0)), a2, c2, r2
    '
    arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a2(0, 0)) & _
    " AND SCode" & eqChkStr(a2(1, 0)) & "", a3, c3, r3
    '
    If r3 = 0 Then
        arc.doUpdateArchive "INSERT INTO Faculty (FacultyName,SCode) VALUES " & _
        "(" & ChkStr(a2(0, 0)) & "," & ChkStr(a2(1, 0)) & ")", 128
         '
        arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a2(0, 0)) & _
        " AND SCode" & eqChkStr(a2(1, 0)), a3, c3, r3
    End If
    IDFaculty = a3(0, 0)
    
    'Category for Student
    db.SQLOpenTableToArrey "SELECT CategoryName FROM tblCategories" & _
    " WHERE id=" & a1(1, 0), a2, c2, r2
    '
    arc.doQueryArchive "SELECT id FROM Category WHERE CategoryName" & eqChkStr(a2(0, 0)) _
    , a3, c3, r3
    '
    If r3 = 0 Then
        arc.doUpdateArchive "INSERT INTO Category (CategoryName) VALUES " & _
        "(" & ChkStr(a2(0, 0)) & ")", 128
        '
        arc.doQueryArchive "SELECT id FROM Category WHERE CategoryName" & eqChkStr(a2(0, 0)) _
        , a3, c3, r3
    End If
    idCategory = a3(0, 0)
    
    'NominalGrant for Student
    db.SQLOpenTableToArrey "SELECT GrantName FROM tblNominalGrants" & _
    " WHERE id=" & a1(2, 0), a2, c2, r2
    '
    arc.doQueryArchive "SELECT id FROM NominalGrant WHERE GrantName" & eqChkStr(a2(0, 0)) _
    , a3, c3, r3
    '
    If r3 = 0 Then
        arc.doUpdateArchive "INSERT INTO NominalGrant (GrantName) VALUES " & _
        "(" & ChkStr(a2(0, 0)) & ")", 128
        '
        arc.doQueryArchive "SELECT id FROM NominalGrant WHERE GrantName" & eqChkStr(a2(0, 0)) _
        , a3, c3, r3
    End If
    idNomGrant = a3(0, 0)
    
    'GraduationYear for Student
    Dim gradYear As String
    gradYear = Right$(Date, 4)
    
    'new Student info
    arc.doUpdateArchive "DELETE FROM Student WHERE Surname='[*** I WORKING HERE ***]'", 128
    arc.doUpdateArchive "INSERT INTO Student(idcategory,idnominalgrant,idchair," & _
    "idspecialisation,surname,isforeigner,idspec,idoptions) VALUES (" & _
    CStr(idCategory) & "," & CStr(idNomGrant) & "," & CStr(IDFaculty) & "," & _
    CStr(idSpecialisation) & ",'[*** I WORKING HERE ***]',0," & CStr(IDSpec) & "," & CStr(idDBO) & ")", 128
    '
    arc.doQueryArchive "SELECT id FROM Student WHERE Surname='[*** I WORKING HERE ***]'" _
    , a2, c2, r2
    idStudNew = a2(0, 0)
    '
    arc.doUpdateArchive "UPDATE Student SET Surname=" & ChkStr(a1(5, 0)) & ", Name=" & _
    ChkStr(a1(6, 0)) & ", Patronymic=" & ChkStr(a1(7, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET studphoto=" & ChkStr(a1(8, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET schoolcity=" & ChkStr(a1(9, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET address=" & ChkStr(a1(10, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET kharkovaddress=" & ChkStr(a1(11, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET dateofborn=" & ChkStr(a1(12, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET nationality=" & ChkStr(a1(13, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET education=" & ChkStr(a1(14, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET maritalstatus=" & ChkStr(a1(15, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET formofpayment=" & ChkStr(a1(16, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET informationontheparents=" & ChkStr(a1(17, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET gratitude=" & ChkStr(a1(18, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET reprimands=" & ChkStr(a1(19, 0)) & _
    " WHERE id=" & idStudNew, 128
'    arc.doUpdateArchive "UPDATE Student SET creprimands=" & ChkStr(a1(20, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET offsetnumber=" & ChkStr(a1(21, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET isforeigner=" & _
        IIf(IsNull(a1(22, 0)), 0, CStr(a1(22, 0))) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET libnumber=" & ChkStr(a1(23, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET deductiondate=" & _
        IIf(IsNull(a1(24, 0)), "NULL", "'" & a1(24, 0) & "'") & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET atestatnum=" & ChkStr(a1(25, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET diplomnum=" & ChkStr(a1(26, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET deductionordernumber=" & ChkStr(a1(27, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET deductionreason=" & ChkStr(a1(28, 0)) & _
    " WHERE id=" & idStudNew, 128
'    arc.doUpdateArchive "UPDATE Student SET isvalidreason=" & _
        IIf(IsNull(a1(29, 0)), "0", a1(29, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET regnumber=" & ChkStr(a1(30, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET additionnumber=" & ChkStr(a1(31, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET dasettings=" & ChkStr(a1(32, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET idcode=" & ChkStr(a1(33, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET studnum=" & ChkStr(a1(34, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET entryyear=" & ChkStr(a1(35, 0)) & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET groupname='" & GroupName & "'" & _
    " WHERE id=" & idStudNew, 128
    arc.doUpdateArchive "UPDATE Student SET graduationyear='" & gradYear & "'" & _
    " WHERE id=" & idStudNew, 128
    
    'StudEvents
    db.SQLOpenTableToArrey "SELECT EventText FROM tblStudEvents WHERE idStudent=" & idStudOld _
    , a1, c1, r1
    '
    For i = 0 To r1 - 1
        arc.doUpdateArchive "INSERT INTO StudEvent(IDStudent,EventText) VALUES (" & _
        idStudNew & "," & ChkStr(a1(0, i)) & ")", 128
    Next i
    
    'ConfirmList
    db.SQLOpenTableToArrey "SELECT faculty,cnumber,date1,date2,course,educationform," & _
    "field1,field2,field3,field4,field5,field6,field7,field8 FROM tblConfirmList" & _
    " WHERE id=" & idStudOld, a1, c1, r1
    '
    For i = 0 To r1 - 1
        arc.doUpdateArchive "INSERT INTO ConfirmList (idstudent,faculty,cnumber,date1," & _
        "date2,course,educationform,field1,field2,field3,field4,field5,field6,field7," & _
        "field8) VALUES (" & idStudNew & "," & ChkStr(a1(0, i)) & "," & ChkStr(a1(1, i)) & "," & _
        ChkStr(a1(2, i)) & "," & ChkStr(a1(3, i)) & "," & ChkStr(a1(4, i)) & "," & _
        ChkStr(a1(5, i)) & "," & ChkStr(a1(6, i)) & "," & ChkStr(a1(7, i)) & "," & _
        ChkStr(a1(8, i)) & "," & ChkStr(a1(9, i)) & "," & ChkStr(a1(10, i)) & "," & _
        ChkStr(a1(11, i)) & "," & ChkStr(a1(12, i)) & "," & ChkStr(a1(13, i)) & ")", 128
    Next i
    
    'DegreeTheme
    db.SQLOpenTableToArrey "SELECT IDLecturer,WorkType,NameOfWork,Mark,ECTS" & _
    " FROM TblDegreeThemes WHERE IDStudent=" & idStudOld, a1, c1, r1
    '
    For i = 0 To r1 - 1
        'Lecturer for DegreeTheme
        db.SQLOpenTableToArrey "SELECT IDFaculty,Surname,Name,Patronymic,Passport,Post," & _
        "ScientificDegree FROM tblLecturers WHERE ID=" & a1(0, i), a2, c2, r2
        '
        'Faculty for Lecturer
        db.SQLOpenTableToArrey "SELECT FacultyName,SCode FROM tblFaculty WHERE id=" & _
        a2(0, 0), a3, c3, r3
        '
        arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a3(0, 0)) & _
        " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        '
        If r4 = 0 Then
            arc.doUpdateArchive "INSERT INTO Faculty(FacultyName,SCode) VALUES (" & _
            ChkStr(a3(0, 0)) & "," & ChkStr(a3(1, 0)) & ")", 128
        '
            arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & _
            eqChkStr(a3(0, 0)) & " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        End If
        IDFaculty = a4(0, 0)
        '
        arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
        " AND Surname" & eqChkStr(a2(1, 0)) & " AND Name" & eqChkStr(a2(2, 0)) & _
        " AND Patronymic" & eqChkStr(a2(3, 0)) & " AND Passport" & eqChkStr(a2(4, 0)) & _
        " AND Post" & eqChkStr(a2(5, 0)) & " AND ScientificDegree" & eqChkStr(a2(6, 0)) _
        , a3, c3, r3
        '
        If r3 = 0 Then
            arc.doUpdateArchive "INSERT INTO Lecturer (idfaculty,surname,name,patronymic," & _
            "passport,post,scientificdegree) VALUES (" & IDFaculty & "," & ChkStr(a2(1, 0)) & _
            "," & ChkStr(a2(2, 0)) & "," & ChkStr(a2(3, 0)) & "," & ChkStr(a2(4, 0)) & "," & _
            ChkStr(a2(5, 0)) & "," & ChkStr(a2(6, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
            " AND Surname" & eqChkStr(a2(1, 0)) & " AND Name" & eqChkStr(a2(2, 0)) & _
            " AND Patronymic" & eqChkStr(a2(3, 0)) & " AND Passport" & eqChkStr(a2(4, 0)) & _
            " AND Post" & eqChkStr(a2(5, 0)) & " AND ScientificDegree" & eqChkStr(a2(6, 0)) _
            , a3, c3, r3
        End If
        idLecturer = a3(0, 0)
        '
        arc.doUpdateArchive "INSERT INTO DegreeTheme (IDStudent,IDLecturer,WorkType," & _
        "NameOfWork,Mark,ECTS) VALUES (" & idStudNew & "," & idLecturer & "," & a1(1, i) & _
        "," & ChkStr(a1(2, i)) & "," & a1(3, i) & "," & ChkStr(a1(4, i)) & ")", 128
    Next i
    
    'DegreeTheme2
    db.SQLOpenTableToArrey "SELECT IDLecturer,WorkType,NameOfWork,Mark,ECTS" & _
    " FROM TblDegreeThemes2 WHERE IDStudent=" & idStudOld, a1, c1, r1
    '
    For i = 0 To r1 - 1
        'Lecturer for DegreeTheme2
        db.SQLOpenTableToArrey "SELECT IDFaculty,Surname,Name,Patronymic,Passport,Post," & _
        "ScientificDegree FROM tblLecturers WHERE ID=" & a1(0, i), a2, c2, r2
        '
        'Faculty for Lecturer
        db.SQLOpenTableToArrey "SELECT FacultyName,SCode FROM tblFaculty WHERE id=" & _
        a2(0, 0), a3, c3, r3
        '
        arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a3(0, 0)) & _
        " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        '
        If r4 = 0 Then
            arc.doUpdateArchive "INSERT INTO Faculty(FacultyName,SCode) VALUES (" & _
            ChkStr(a3(0, 0)) & "," & ChkStr(a3(1, 0)) & ")", 128
        '
            arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & _
            eqChkStr(a3(0, 0)) & " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        End If
        IDFaculty = a4(0, 0)
        '
        arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
        " AND Surname" & eqChkStr(a2(1, 0)) & " AND Name" & eqChkStr(a2(2, 0)) & _
        " AND Patronymic" & eqChkStr(a2(3, 0)) & " AND Passport" & eqChkStr(a2(4, 0)) & _
        " AND Post" & eqChkStr(a2(5, 0)) & " AND ScientificDegree" & eqChkStr(a2(6, 0)) _
        , a3, c3, r3
        '
        If r3 = 0 Then
            arc.doUpdateArchive "INSERT INTO Lecturer (idfaculty,surname,name,patronymic," & _
            "passport,post,scientificdegree) VALUES (" & IDFaculty & ChkStr(a2(1, 0)) & "," & _
            ChkStr(a2(2, 0)) & "," & ChkStr(a2(3, 0)) & "," & ChkStr(a2(4, 0)) & "," & _
            ChkStr(a2(5, 0)) & "," & ChkStr(a2(6, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
            " AND Surname" & eqChkStr(a2(1, 0)) & " AND Name" & eqChkStr(a2(2, 0)) & _
            " AND Patronymic" & eqChkStr(a2(3, 0)) & " AND Passport" & eqChkStr(a2(4, 0)) & _
            " AND Post" & eqChkStr(a2(5, 0)) & " AND ScientificDegree" & eqChkStr(a2(6, 0)) _
            , a3, c3, r3
        End If
        idLecturer = a3(0, 0)
        '
        arc.doUpdateArchive "INSERT INTO DegreeTheme2 (IDStudent,IDLecturer,WorkType," & _
        "NameOfWork,Mark,ECTS) VALUES (" & idStudNew & "," & idLecturer & "," & a1(1, i) & _
        "," & ChkStr(a1(2, i)) & "," & a1(3, i) & "," & ChkStr(a1(4, i)) & ")", 128
    Next i
    
    'SpecialCourse
    db.SQLOpenTableToArrey "SELECT idSubject,idLecturer,KindOfReport,NumOfHours,Semester," & _
    "Mark,ECTS,CourseType FROM tblSpecialCourses WHERE idStudent=" & idStudOld, a1, c1, r1
    '
    For i = 0 To r1 - 1
        'Lecturer for SpecialCourse
        db.SQLOpenTableToArrey "SELECT IDFaculty,Surname,Name,Patronymic,Passport,Post," & _
        "ScientificDegree FROM tblLecturers WHERE ID=" & a1(1, i), a2, c2, r2
        '
        'Faculty for Lecturer
        db.SQLOpenTableToArrey "SELECT FacultyName,SCode FROM tblFaculty WHERE id=" & _
        a2(0, 0), a3, c3, r3
        '
        arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a3(0, 0)) & _
        " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        '
        If r4 = 0 Then
            arc.doUpdateArchive "INSERT INTO Faculty(FacultyName,SCode) VALUES (" & _
            ChkStr(a3(0, 0)) & "," & ChkStr(a3(1, 0)) & ")", 128
        '
            arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & _
            eqChkStr(a3(0, 0)) & " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        End If
        IDFaculty = a4(0, 0)
        '
        arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
        " AND Surname" & eqChkStr(a2(1, 0)) & " AND Name" & eqChkStr(a2(2, 0)) & _
        " AND Patronymic" & eqChkStr(a2(3, 0)) & " AND Passport" & eqChkStr(a2(4, 0)) & _
        " AND Post" & eqChkStr(a2(5, 0)) & " AND ScientificDegree" & eqChkStr(a2(6, 0)) _
        , a3, c3, r3
        '
        If r3 = 0 Then
            arc.doUpdateArchive "INSERT INTO Lecturer (idfaculty,surname,name,patronymic," & _
            "passport,post,scientificdegree) VALUES (" & IDFaculty & "," & ChkStr(a2(1, 0)) & _
             "," & ChkStr(a2(2, 0)) & "," & ChkStr(a2(3, 0)) & "," & ChkStr(a2(4, 0)) & _
             "," & ChkStr(a2(5, 0)) & "," & ChkStr(a2(6, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
            " AND Surname" & eqChkStr(a2(1, 0)) & " AND Name" & eqChkStr(a2(2, 0)) & _
            " AND Patronymic" & eqChkStr(a2(3, 0)) & " AND Passport" & eqChkStr(a2(4, 0)) & _
            " AND Post" & eqChkStr(a2(5, 0)) & " AND ScientificDegree" & eqChkStr(a2(6, 0)) _
            , a3, c3, r3
        End If
        idLecturer = a3(0, 0)
        '
        'Subject for SpecialCourse
        db.SQLOpenTableToArrey "SELECT Subject FROM tblSubjects WHERE id=" & a1(0, i) _
        , a2, c2, r2
        '
        arc.doQueryArchive "SELECT id FROM Subject WHERE Subject" & eqChkStr(a2(0, 0)) _
        , a3, c3, r3
        '
        If r3 = 0 Then
            arc.doUpdateArchive "INSERT INTO Subject (Subject) VALUES (" & _
            ChkStr(a2(0, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM Subject WHERE Subject" & eqChkStr(a2(0, 0)) _
            , a3, c3, r3
        End If
        idSubject = a3(0, 0)
        '
        arc.doUpdateArchive "INSERT INTO SpecialCourse (idstudent,idsubject,idlecturer," & _
        "kindofreport,numofhours,semester,mark,ects,coursetype) VALUES (" & idStudNew & _
        "," & idSubject & "," & idLecturer & "," & ChkStr(a1(2, i)) & "," & a1(3, i) & "," & _
        a1(4, i) & "," & a1(5, i) & "," & ChkStr(a1(6, i)) & "," & a1(7, i) & ")", 128
    Next i
    
    'Sheets for Student
    db.SQLOpenTableToArrey "SELECT idExam,Mark,ECTS FROM tblSheets WHERE IDStudent=" & _
    idStudOld, a1, c1, r1
    '
    For i = 0 To r1 - 1
        'Examination for Sheet
        db.SQLOpenTableToArrey "SELECT IDEP,IDLecturer,IDFaculty,SheetNumber FROM" & _
        " tblExaminations WHERE ID=" & a1(0, i), a2, c2, r2
        '
        'EP for Examination
        db.SQLOpenTableToArrey "SELECT idSpec,idSubject,Semester,KindOfReport," & _
        "IsAdditional,SCode,NumOfHours FROM tblEducationalPlan WHERE ID=" & a2(0, 0), _
        a3, c3, r3
        '
        'Speciality for EP
        db.SQLOpenTableToArrey "SELECT SpecName,SCode FROM tblSpecialities WHERE ID=" & _
        a3(0, 0), a4, c4, r4
        '
        arc.doQueryArchive "SELECT ID FROM Speciality WHERE SpecName" & eqChkStr(a4(0, 0)) & _
        " AND SCode" & eqChkStr(a4(1, 0)), a5, c5, r5
        '
        If r5 = 0 Then
            arc.doUpdateArchive "INSERT INTO Speciality (SpecName,SCode) VALUES (" & _
            ChkStr(a4(0, 0)) & "," & ChkStr(a4(1, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT ID FROM Speciality WHERE SpecName" & _
            eqChkStr(a4(0, 0)) & " AND SCode" & eqChkStr(a4(1, 0)), a5, c5, r5
        End If
        IDSpec = a5(0, 0)
        '
        'Subject for EP
        db.SQLOpenTableToArrey "SELECT Subject FROM tblSubjects WHERE ID=" & a3(1, 0) _
        , a4, c4, r4
        '
        arc.doQueryArchive "SELECT ID FROM Subject WHERE Subject" & eqChkStr(a4(0, 0)) _
        , a5, c5, r5
        '
        If r5 = 0 Then
            arc.doUpdateArchive "INSERT INTO Subject(Subject) VALUES (" & ChkStr(a4(0, 0)) & _
            ")", 128
            '
            arc.doQueryArchive "SELECT ID FROM Subject WHERE Subject" & eqChkStr(a4(0, 0)) _
            , a5, c5, r5
        End If
        idSubject = a5(0, 0)
        '
        arc.doQueryArchive "SELECT id FROM EP WHERE IDSpec=" & IDSpec & " AND IDSubject=" & _
        idSubject & " AND Semester=" & a3(2, 0) & " AND KindOfReport" & eqChkStr(a3(3, 0)) & _
        " AND IsAdditional=" & a3(4, 0) & " AND SCode" & eqChkStr(a3(5, 0)) & _
        " AND NumOfHours" & IIf(IsNull(a3(6, 0)), " is NULL", "=" & a3(6, 0)), a4, c4, r4
        '
        If r4 = 0 Then
            arc.doUpdateArchive "INSERT INTO EP (idspec,idsubject,semester,kindofreport," & _
            "isadditional,scode,numofhours) VALUES (" & IDSpec & "," & idSubject & "," & _
            a3(2, 0) & "," & ChkStr(a3(3, 0)) & "," & a3(4, 0) & "," & ChkStr(a3(5, 0)) & _
            "," & IIf(IsNull(a3(6, 0)), "NULL", a3(6, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM EP WHERE IDSpec=" & IDSpec & _
            " AND IDSubject=" & idSubject & " AND Semester=" & a3(2, 0) & _
            " AND KindOfReport" & eqChkStr(a3(3, 0)) & " AND IsAdditional=" & _
            a3(4, 0) & "AND SCode" & eqChkStr(a3(5, 0)) & " AND NumOfHours" & _
            IIf(IsNull(a3(6, 0)), " is NULL", "=" & a3(6, 0)), a4, c4, r4
        End If
        idEP = a4(0, 0)
        '
        'Lecturer for Examination
        db.SQLOpenTableToArrey "SELECT IDFaculty,Surname,Name,Patronymic,Passport,Post," & _
        "ScientificDegree FROM tblLecturers WHERE ID=" & a2(1, 0), a3, c3, r3
        '
        'Faculty for Lecturer
        db.SQLOpenTableToArrey "SELECT FacultyName,SCode FROM tblFaculty WHERE id=" & _
        a3(0, 0), a4, c4, r4
        '
        arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a4(0, 0)) & _
        " AND SCode" & eqChkStr(a4(1, 0)), a5, c5, r5
        '
        If r5 = 0 Then
            arc.doUpdateArchive "INSERT INTO Faculty(FacultyName,SCode) VALUES (" & _
            ChkStr(a4(0, 0)) & "," & ChkStr(a4(1, 0)) & ")", 128
        '
            arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & _
            eqChkStr(a4(0, 0)) & " AND SCode" & eqChkStr(a4(1, 0)), a5, c5, r5
        End If
        IDFaculty = a5(0, 0)
        '
        arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
        " AND Surname" & eqChkStr(a3(1, 0)) & " AND Name" & eqChkStr(a3(2, 0)) & _
        " AND Patronymic" & eqChkStr(a3(3, 0)) & " AND Passport" & eqChkStr(a3(4, 0)) & _
        " AND Post" & eqChkStr(a3(5, 0)) & " AND ScientificDegree" & eqChkStr(a3(6, 0)) _
        , a4, c4, r4
        '
        If r4 = 0 Then
            arc.doUpdateArchive "INSERT INTO Lecturer (idfaculty,surname,name,patronymic," & _
            "passport,post,scientificdegree) VALUES (" & IDFaculty & "," & ChkStr(a3(1, 0)) & "," & _
            ChkStr(a3(2, 0)) & "," & ChkStr(a3(3, 0)) & "," & ChkStr(a3(4, 0)) & "," & _
            ChkStr(a3(5, 0)) & "," & ChkStr(a3(6, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM Lecturer WHERE IDFaculty=" & IDFaculty & _
            " AND Surname" & eqChkStr(a3(1, 0)) & " AND Name" & eqChkStr(a3(2, 0)) & _
            " AND Patronymic" & eqChkStr(a3(3, 0)) & " AND Passport" & eqChkStr(a3(4, 0)) & _
            " AND Post" & eqChkStr(a3(5, 0)) & " AND ScientificDegree" & eqChkStr(a3(6, 0)) _
            , a4, c4, r4
        End If
        idLecturer = a4(0, 0)
        '
        'Faculty for Examination
        db.SQLOpenTableToArrey "SELECT FacultyName,SCode FROM tblFaculty WHERE id=" & _
        a2(2, 0), a3, c3, r3
        '
        arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & eqChkStr(a3(0, 0)) & _
        " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        '
        If r4 = 0 Then
            arc.doUpdateArchive "INSERT INTO Faculty(FacultyName,SCode) VALUES (" & _
            ChkStr(a3(0, 0)) & "," & ChkStr(a3(1, 0)) & ")", 128
        '
            arc.doQueryArchive "SELECT id FROM Faculty WHERE FacultyName" & _
            eqChkStr(a3(0, 0)) & " AND SCode" & eqChkStr(a3(1, 0)), a4, c4, r4
        End If
        IDFaculty = a4(0, 0)
        '
        arc.doQueryArchive "SELECT id FROM Examination WHERE IDEP=" & idEP & _
        " AND IDLecturer=" & idLecturer & " AND IDFaculty=" & IDFaculty & _
        " AND SheetNumber" & eqChkStr(a2(3, 0)), a3, c3, r3
        '
        If r3 = 0 Then
            arc.doUpdateArchive "INSERT INTO Examination (idep,idlecturer,idfaculty," & _
            "sheetnumber) VALUES (" & idEP & "," & idLecturer & "," & IDFaculty & "," & _
            ChkStr(a2(3, 0)) & ")", 128
            '
            arc.doQueryArchive "SELECT id FROM Examination WHERE IDEP=" & idEP & _
            " AND IDLecturer=" & idLecturer & " AND IDFaculty=" & IDFaculty & _
            " AND SheetNumber" & eqChkStr(a2(3, 0)), a3, c3, r3
        End If
        IDExam = a3(0, 0)
        '
        arc.doUpdateArchive "INSERT INTO Sheet(idexam,idstudent,mark,ects) VALUES (" & _
        IDExam & "," & idStudNew & "," & a1(1, i) & "," & ChkStr(a1(2, i)) & ")", 128
    Next i
    
    'Delete student from db
    db.ExecSQLActionQuery "DELETE * FROM tblSheets WHERE IDStudent = " & idStudOld, 128
    db.ExecSQLActionQuery "DELETE * FROM tblDegreeThemes WHERE IDStudent = " & idStudOld, 128
    db.ExecSQLActionQuery "DELETE * FROM tblDegreeThemes2 WHERE IDStudent = " & idStudOld, 128
    db.ExecSQLActionQuery "DELETE * FROM tblConfirmList WHERE IDStudent = " & idStudOld, 128
    db.ExecSQLActionQuery "DELETE * FROM tblStudEvents WHERE IDStudent = " & idStudOld, 128
    db.ExecSQLActionQuery "DELETE * FROM tblSpecialCourses WHERE IDStudent = " & idStudOld, 128
    db.ExecSQLActionQuery "DELETE * FROM tblStudents WHERE ID = " & idStudOld, 128

End Sub

Private Function ChkStr(ByRef TString As Variant) As String
    Dim AddString As String
    
    If IsNull(TString) Then
        ChkStr = "NULL"
        Exit Function
    End If
    
    Dim i  As Long
    For i = 1 To Len(TString)
        If (Mid(TString, i, 1) <> "'" And Mid(TString, i, 1) <> "[" And _
            Mid(TString, i, 1) <> "]") Then AddString = AddString & Mid(TString, i, 1)
        If (Mid(TString, i, 1) = "'") Then AddString = AddString & "' & Chr(39) & '"
        If (Mid(TString, i, 1) = "[") Then AddString = AddString & "[[]"
        If (Mid(TString, i, 1) = "]") Then AddString = AddString & "[]]"
    Next
    
    ChkStr = "'" & AddString & "'"
End Function

