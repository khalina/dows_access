VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Begin VB.Form frmFillGroup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "���������� ������"
   ClientHeight    =   8910
   ClientLeft      =   3660
   ClientTop       =   1605
   ClientWidth     =   6975
   Icon            =   "frmFillGroup.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8910
   ScaleWidth      =   6975
   Begin VB.Frame F2 
      Height          =   6255
      Left            =   0
      TabIndex        =   6
      Top             =   2640
      Width           =   6975
      Begin VB.ComboBox comboCountry 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmFillGroup.frx":014A
         Left            =   5280
         List            =   "frmFillGroup.frx":0154
         TabIndex        =   42
         Top             =   2400
         Width           =   1575
      End
      Begin VB.ComboBox comboCategory 
         Enabled         =   0   'False
         Height          =   315
         Left            =   2040
         TabIndex        =   40
         Top             =   2400
         Width           =   1575
      End
      Begin VB.TextBox txtIOP 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   37
         Top             =   2760
         Width           =   4815
      End
      Begin VB.TextBox txtMaritalStatus 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5280
         TabIndex        =   36
         Top             =   2040
         Width           =   1575
      End
      Begin VB.TextBox txtEducation 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   34
         Top             =   2040
         Width           =   1575
      End
      Begin VB.TextBox txtNationality 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5280
         TabIndex        =   32
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox txtDOB 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   30
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox AddressInKh 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   27
         Top             =   960
         Width           =   4815
      End
      Begin VB.TextBox Address 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   26
         Top             =   600
         Width           =   4815
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   375
         Left            =   4680
         TabIndex        =   15
         Top             =   5760
         Width           =   2175
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblStudents 
         Height          =   2055
         Left            =   120
         TabIndex        =   14
         Top             =   3600
         Width           =   6735
         _cx             =   11880
         _cy             =   3625
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   3
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
      Begin VB.CommandButton cmdAddStud 
         Caption         =   "��������"
         Enabled         =   0   'False
         Height          =   375
         Left            =   4680
         TabIndex        =   13
         Top             =   3120
         Width           =   2175
      End
      Begin VB.TextBox txtON 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   5280
         TabIndex        =   12
         Top             =   1335
         Width           =   1575
      End
      Begin VB.ComboBox comboFP 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmFillGroup.frx":0161
         Left            =   2040
         List            =   "frmFillGroup.frx":016B
         TabIndex        =   11
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox txtFIO 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         TabIndex        =   10
         Top             =   240
         Width           =   4815
      End
      Begin VB.Label �����7 
         Caption         =   "������ ��������� ������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   3360
         Width           =   2175
      End
      Begin VB.Label �����6 
         Caption         =   "���������� :"
         Height          =   255
         Left            =   3840
         TabIndex        =   41
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label lblCategory 
         Caption         =   "��������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label �����18 
         AutoSize        =   -1  'True
         Caption         =   "�������� � ��������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   38
         Top             =   2760
         Width           =   1785
      End
      Begin VB.Label �����17 
         AutoSize        =   -1  'True
         Caption         =   "���. ��������� :"
         Height          =   195
         Left            =   3840
         TabIndex        =   35
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label �����16 
         AutoSize        =   -1  'True
         Caption         =   "����������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   33
         Top             =   2040
         Width           =   1110
      End
      Begin VB.Label �����15 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   3840
         TabIndex        =   31
         Top             =   1680
         Width           =   765
      End
      Begin VB.Label �����14 
         AutoSize        =   -1  'True
         Caption         =   "���� �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   29
         Top             =   1680
         Width           =   1275
      End
      Begin VB.Label �����13 
         AutoSize        =   -1  'True
         Caption         =   "����� � �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   25
         Top             =   960
         Width           =   1470
      End
      Begin VB.Label �����12 
         AutoSize        =   -1  'True
         Caption         =   "����� :"
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   600
         Width           =   555
      End
      Begin VB.Label SCount 
         Height          =   255
         Left            =   2640
         TabIndex        =   21
         Top             =   5760
         Width           =   855
      End
      Begin VB.Label �����8 
         AutoSize        =   -1  'True
         Caption         =   "���������� ��������� � ������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   5760
         Width           =   2475
      End
      Begin VB.Label �����5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����� ������� :"
         Height          =   195
         Left            =   3840
         TabIndex        =   9
         Top             =   1335
         Width           =   1245
      End
      Begin VB.Label �����4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "����� ������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   1320
         Width           =   1245
      End
      Begin VB.Label �����3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�. �. �."
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   630
      End
   End
   Begin VB.Frame F1 
      Height          =   2775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6975
      Begin VB.TextBox txtFirstSessionYear 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2880
         TabIndex        =   45
         Top             =   2280
         Width           =   975
      End
      Begin VB.TextBox SNum 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   2880
         TabIndex        =   28
         Top             =   1800
         Width           =   975
      End
      Begin VB.CheckBox NewGroup 
         Caption         =   "�����"
         Height          =   255
         Left            =   6000
         TabIndex        =   19
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton cmdChoiseGroup 
         Caption         =   "�������"
         Height          =   375
         Left            =   4680
         TabIndex        =   5
         Top             =   2160
         Width           =   2175
      End
      Begin VB.ComboBox comboGroups 
         Height          =   315
         Left            =   2880
         TabIndex        =   4
         Top             =   720
         Width           =   2895
      End
      Begin VB.ComboBox comboSpecialities 
         Height          =   315
         Left            =   2280
         TabIndex        =   1
         Top             =   240
         Width           =   4575
      End
      Begin VB.Label Label2 
         Caption         =   $"frmFillGroup.frx":0181
         Height          =   615
         Left            =   120
         TabIndex        =   46
         Top             =   1080
         Width           =   6615
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "��� ������ ������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   44
         Top             =   2280
         Width           =   1530
      End
      Begin VB.Label �����9 
         AutoSize        =   -1  'True
         Caption         =   "����� �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   1920
         Width           =   1380
      End
      Begin VB.Label �����10 
         AutoSize        =   -1  'True
         Caption         =   "ID :"
         Height          =   195
         Left            =   7080
         TabIndex        =   22
         Top             =   270
         Width           =   255
      End
      Begin VB.Label IDG 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   7320
         TabIndex        =   18
         Top             =   750
         Width           =   855
      End
      Begin VB.Label IDS 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   7320
         TabIndex        =   17
         Top             =   270
         Width           =   855
      End
      Begin VB.Label �����11 
         AutoSize        =   -1  'True
         Caption         =   "ID :"
         Height          =   195
         Left            =   7080
         TabIndex        =   16
         Top             =   750
         Width           =   255
      End
      Begin VB.Label �����2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������� ������ ��� ���������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   750
         Width           =   2640
      End
      Begin VB.Label �����1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "�������� ������������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   270
         Width           =   2040
      End
   End
End
Attribute VB_Name = "frmFillGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private SpecArrey As Variant
Private SCols As Integer
Private SRows As Long

Private GroupArrey As Variant
Private GCols As Integer
Private GRows As Long

Private FacultyArrey As Variant
Private FCols As Integer
Private FRows As Long

Private CategoryArrey As Variant
Private CCols As Integer
Private CRows As Long

Private NominalGrantsArrey As Variant
Private NGCols As Integer
Private NGRows As Long

Private ExamsArrey As Variant
Private ExamsCols As Integer
Private ExamsRows As Long

Private CreateNewGroup As Boolean

Private SelectedGroupID As Long
Private SelectedSpecID As Long

Sub LoadGroupExams()
If (SelectedGroupID = 0) Then Exit Sub

        CDB.SQLOpenTableToArrey "SELECT" & _
        " tblExaminations.ID" & _
        " FROM tblExaminations, tblEducationalPlan, tblGroups" & _
        " WHERE tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
        " tblExaminations.IDGroup = " & SelectedGroupID & " AND" & _
        " tblGroups.ID = " & SelectedGroupID & " AND" & _
        " tblEducationalPlan.Semester >= tblGroups.Semester", _
        ExamsArrey, ExamsCols, ExamsRows
End Sub
Sub FillStudInfo2()
    Dim DopArrey As Variant
    Dim DopRows As Long
    Dim DopCols As Integer

    Dim StudArrey As Variant
    Dim Rows As Long
    Dim Cols As Integer
    Dim i As Long
    
        Main.sbMain.PanelText("2") = "�������� ������ ��������� ������ " & comboGroups.Text
        
        CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic], [FormOfPayment], [OffsetNumber] FROM [tblStudents] WHERE [IDGroup] = " & SelectedGroupID & " AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, Cols, Rows
        If (Rows > 0) Then
            tblStudents.Rows = Rows + 1
            
            For i = 0 To Rows - 1
                ProgBar Main.PProgress, ((i + 1) * 100) / Rows
                Main.sbMain.RedrawPanel ("3")
            
                If (Not IsNull(StudArrey(0, i))) Then tblStudents.TextMatrix(i + 1, 0) = i + 1
                If (Not IsNull(StudArrey(1, i)) And Not IsNull(StudArrey(2, i)) And Not IsNull(StudArrey(3, i))) Then tblStudents.TextMatrix(i + 1, 1) = GetFullName(CStr(StudArrey(1, i)), CStr(StudArrey(2, i)), CStr(StudArrey(3, i)), False, False, True)
                If (Not IsNull(StudArrey(4, i))) Then tblStudents.TextMatrix(i + 1, 2) = StudArrey(4, i)
                If (Not IsNull(StudArrey(5, i))) Then tblStudents.TextMatrix(i + 1, 3) = StudArrey(5, i)
            Next i
        End If
        
        Main.sbMain.PanelText("2") = "������"
        ProgBar Main.PProgress, 0
        Main.sbMain.RedrawPanel ("3")
    
    SCount.Caption = Rows
End Sub

Sub FillStudInfo()
    Dim DopArrey As Variant
    Dim DopRows As Long
    Dim DopCols As Integer

    Dim StudArrey As Variant
    Dim Rows As Long
    Dim Cols As Integer
    Dim i As Long
    
        Main.sbMain.PanelText("2") = "�������� ������ ��������� ������ " & comboGroups.Text
        
        CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic], [FormOfPayment], [OffsetNumber], [Address], [KharkovAddress], [DateOfBorn], [Nationality], [Education], [MaritalStatus], [InformationOnTheParents], [Gratitude], [Reprimands], [IDFaculty], [IDCategory], [IDNominalGrant] FROM [tblStudents] WHERE [IDGroup] = " & SelectedGroupID & " AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, Cols, Rows
        If (Rows > 0) Then
            tblStudents.Rows = Rows + 1
            
            For i = 0 To Rows - 1
                ProgBar Main.PProgress, ((i + 1) * 100) / Rows
                Main.sbMain.RedrawPanel ("3")
            
                If (Not IsNull(StudArrey(0, i))) Then tblStudents.TextMatrix(i + 1, 0) = i + 1
                If (Not IsNull(StudArrey(1, i)) And Not IsNull(StudArrey(2, i)) And Not IsNull(StudArrey(3, i))) Then tblStudents.TextMatrix(i + 1, 1) = GetFullName(CStr(StudArrey(1, i)), CStr(StudArrey(2, i)), CStr(StudArrey(3, i)), False, False, True)
                If (Not IsNull(StudArrey(4, i))) Then tblStudents.TextMatrix(i + 1, 2) = StudArrey(4, i)
                If (Not IsNull(StudArrey(5, i))) Then tblStudents.TextMatrix(i + 1, 3) = StudArrey(5, i)
                If (Not IsNull(StudArrey(6, i))) Then tblStudents.TextMatrix(i + 1, 4) = StudArrey(6, i)
                If (Not IsNull(StudArrey(7, i))) Then tblStudents.TextMatrix(i + 1, 5) = StudArrey(7, i)
                If (Not IsNull(StudArrey(8, i))) Then tblStudents.TextMatrix(i + 1, 6) = StudArrey(8, i)
                If (Not IsNull(StudArrey(9, i))) Then tblStudents.TextMatrix(i + 1, 7) = StudArrey(9, i)
                If (Not IsNull(StudArrey(10, i))) Then tblStudents.TextMatrix(i + 1, 8) = StudArrey(10, i)
                If (Not IsNull(StudArrey(11, i))) Then tblStudents.TextMatrix(i + 1, 9) = StudArrey(11, i)
                If (Not IsNull(StudArrey(12, i))) Then tblStudents.TextMatrix(i + 1, 10) = StudArrey(12, i)
                If (Not IsNull(StudArrey(13, i))) Then tblStudents.TextMatrix(i + 1, 11) = StudArrey(13, i)
                If (Not IsNull(StudArrey(14, i))) Then tblStudents.TextMatrix(i + 1, 12) = StudArrey(14, i)

                CDB.SQLOpenTableToArrey "SELECT [FacultyName] FROM [tblFaculty] WHERE [ID] = " & StudArrey(15, i), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblStudents.TextMatrix(i + 1, 13) = DopArrey(0, 0)

                CDB.SQLOpenTableToArrey "SELECT [CategoryName] FROM [tblCategories] WHERE [ID] = " & StudArrey(16, i), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblStudents.TextMatrix(i + 1, 14) = DopArrey(0, 0)

                CDB.SQLOpenTableToArrey "SELECT [GrantName] FROM [tblNominalGrants] WHERE [ID] = " & StudArrey(17, i), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblStudents.TextMatrix(i + 1, 15) = DopArrey(0, 0)

            Next i
        End If
        
        Main.sbMain.PanelText("2") = "������"
        ProgBar Main.PProgress, 0
        Main.sbMain.RedrawPanel ("3")
    
    SCount.Caption = Rows
End Sub

Sub AddGroupToMainTree(SpecN As String, GroupN As String, Course As Integer, GroupID As Long)
    Dim i As Long
    Dim nds As VSFlexNode
    Dim nd As VSFlexNode
    Dim fOk As Boolean
    
    fOk = False
    For i = 0 To Main.MainTree.Rows - 1
        If ((Main.MainTree.TextMatrix(i, 0) = SpecN) And (Main.MainTree.RowData(i) Like "BD_SPEC_*")) Then
            Set nd = Main.MainTree.GetNode(i)
            Set nds = Main.MainTree.GetNode(i)
            
            If (nd.Children > 0) Then
                Set nd = nd.GetNode(flexNTFirstChild)
                
                While ((Not (nd Is Nothing)) And (Not fOk))
                    If (Main.MainTree.RowData(nd.Row) = "FR_" & Course & "Course") Then
                        nd.AddNode flexNTLastChild, GroupN
                        Set nd = nd.GetNode(flexNTLastChild)
                        Main.MainTree.IsSubtotal(nd.Row) = True
                        Main.MainTree.RowData(nd.Row) = "BD_GROUP_" & GroupID
                        
                        Set nd = nd.GetNode(flexNTParent)
                        If (nd.Expanded = False) Then nd.Expanded = False
                        
                        Set nd = nd.GetNode(flexNTParent)
                        If (nd.Expanded = False) Then nd.Expanded = False
                        
                        fOk = True
                    End If
                    Set nd = nd.GetNode(flexNTNextSibling)
                Wend
            End If
            
            Set nd = nds
            If ((nd.Children = 0) Or (Not fOk)) Then
                nd.AddNode flexNTFirstChild, Course & " ����"
                Set nd = nd.GetNode(flexNTFirstChild)
                Main.MainTree.IsSubtotal(nd.Row) = True
                Main.MainTree.RowData(nd.Row) = "FR_" & Course & "Course"
            
                    nd.AddNode flexNTLastChild, GroupN
                    Set nd = nd.GetNode(flexNTLastChild)
                    Main.MainTree.IsSubtotal(nd.Row) = True
                    Main.MainTree.RowData(nd.Row) = "BD_GROUP_" & GroupID
                    
                    Set nd = nd.GetNode(flexNTParent)
                    If (nd.Expanded = False) Then nd.Expanded = False
                    
                    Set nd = nd.GetNode(flexNTParent)
                    If (nd.Expanded = False) Then nd.Expanded = False
                    
                    Set nd = nd.GetNode(flexNTParent)
                    If (nd.Expanded = False) Then nd.Expanded = False
                    
                    fOk = True
            End If


            If (fOk) Then Exit For
        End If
    Next i

End Sub


Private Sub Address_GotFocus()
Address.BackColor = ActiveFieldsColor
End Sub

Private Sub Address_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    AddressInKh.SetFocus
End If
End Sub

Private Sub Address_LostFocus()
Address.BackColor = NonActiveFieldsColor
End Sub

Private Sub AddressInKh_GotFocus()
AddressInKh.BackColor = ActiveFieldsColor
End Sub

Private Sub AddressInKh_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    comboFP.SetFocus
End If
End Sub

Private Sub AddressInKh_LostFocus()
AddressInKh.BackColor = NonActiveFieldsColor
End Sub

Private Sub cmdAddStud_Click()
Dim StudName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim FOP As String
Dim OffNum As String

Dim DOB As Date
Dim Nationality As String
Dim Education As String
Dim MaritalStatus As String
Dim IOP As String

Dim strAddress As String
Dim strAddressInKh As String

StudName = txtFIO
If (Not GetFIO(StudName, "��������", "�.�.�.", 40, 40, 40, Surname, Name, Patronymic)) Then Exit Sub
CheckString Surname
CheckString Name
CheckString Patronymic

strAddress = Address.Text
If (FindErrors(Address.Text, 255, "�����", False, True)) Then Exit Sub
CheckString strAddress

strAddressInKh = AddressInKh.Text
If (FindErrors(AddressInKh.Text, 255, "����� � ��������", False, True)) Then Exit Sub
CheckString strAddressInKh

OffNum = txtON.Text
If (FindErrors(OffNum, 10, "����� �������", True, True)) Then Exit Sub
CheckString OffNum

Nationality = txtNationality.Text
If (FindErrors(Nationality, 50, "�������", False, True)) Then Exit Sub
CheckString Nationality

Education = txtEducation.Text
If (FindErrors(Education, 100, "�����������", False, True)) Then Exit Sub
CheckString Education

MaritalStatus = txtMaritalStatus.Text
If (FindErrors(MaritalStatus, 50, "���. ���������", False, True)) Then Exit Sub
CheckString MaritalStatus

IOP = txtIOP.Text
If (FindErrors(IOP, 255, "�������� � ���������", False, True)) Then Exit Sub
CheckString IOP

If (Not IsDate(txtDOB.Text)) Then
    MsgBox "���� �������� ������� �� �����", vbOKOnly, "������"
    Exit Sub
End If
DOB = CDate(txtDOB.Text)

FOP = comboFP.Text
If (FOP = "") Then
    MsgBox "�������� ����� ������", vbOKOnly, "������"
    Exit Sub
End If


If (CDB.SQLGetRecordCount("SELECT * FROM tblStudents WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] = '" & Patronymic & "'") > 0) Then
    MsgBox "������� � ����� ������ ��� ����������", vbOKOnly, "������"
    Exit Sub
End If

If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblStudents] WHERE [OffsetNumber] = '" & OffNum & "'") > 0) Then
    MsgBox "������� � ����� ������� ������� ��� ��� ���������������", vbOKOnly, "������"
    Exit Sub
End If

Dim AR As Long
AR = CDB.ExecSQLActionQuery("INSERT INTO tblStudents(IDGroup, Surname, Name, Patronymic, Address, KharkovAddress, DateOfBorn, Nationality, Education, MaritalStatus, FormOfPayment, InformationOnTheParents, OffsetNumber, IDCategory, IsForeigner, IDNominalGrant, IsActive, IDChair, IDSpecialisation) VALUES (" & SelectedGroupID & ", '" & Surname & "', '" & Name & "', '" & Patronymic & "', '" & strAddress & "', '" & strAddressInKh & "', '" & DOB & "', '" & Nationality & "', '" & Education & "', '" & MaritalStatus & "', '" & FOP & "', '" & IOP & "', '" & OffNum & "', " & comboCategory.ItemData(comboCategory.ListIndex) & ", " & comboCountry.ListIndex & ", 1, 1, 1, 1)", 128)

If (AR > 0) Then
    Dim IDArrey As Variant
    Dim Cols As Integer
    Dim Rows As Long
    Dim IDStud As Long
    
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblStudents] WHERE [OffsetNumber] = '" & OffNum & "'", IDArrey, Cols, Rows
    If (Rows > 0) Then
    
        IDStud = CLng(IDArrey(0, 0))
        
        'tblStudents.AddItem tblStudents.Rows & vbTab & StudName & vbTab & FOP & vbTab & txtON.Text & vbTab & Address.Text & vbTab & AddressInKh.Text & vbTab & DOB & vbTab & txtNationality.Text & vbTab & txtEducation.Text & vbTab & txtMaritalStatus.Text & vbTab & txtIOP.Text & vbTab & vbTab & vbTab & "-�����-" & vbTab & comboCategory.Text & vbTab & "-�����-"
        tblStudents.AddItem tblStudents.Rows & vbTab & StudName & vbTab & FOP & vbTab & txtON.Text
        tblStudents.RowData(tblStudents.Rows - 1) = IDArrey(0, 0)
        
        SCount.Caption = tblStudents.Rows - 1
        
        txtFIO.Text = ""
        txtON.Text = ""
        Address.Text = ""
        AddressInKh.Text = ""
        txtDOB.Text = ""
        txtIOP.Text = ""
        txtMaritalStatus.Text = ""
        txtNationality.Text = ""
        txtEducation.Text = ""
        
    '------------------------------------------------------------------------------------
    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    Dim i As Long
   
    For i = 0 To ExamsRows - 1
        CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSheets] WHERE [IDStudent] = " & IDStud & " AND [IDExam] = " & ExamsArrey(0, i), DopArrey, DopCols, DopRows
        If (DopRows = 0) Then AR = CDB.ExecSQLActionQuery("INSERT INTO tblSheets (IDExam, IDStudent, Mark) VALUES (" & CLng(ExamsArrey(0, i)) & ", " & IDStud & ", 2)", 128)
    Next i
    '------------------------------------------------------------------------------------
    
    End If
End If

End Sub

Private Sub cmdAddStud_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtFIO.SetFocus
End Sub

Private Sub cmdChoiseGroup_Click()

    If (comboGroups.Text = "") Then
        MsgBox "�������� ������ ��� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (InStr(1, comboGroups.Text, "'") <> 0) Then
        MsgBox "��� ������ �� ������ ��������� ����� �������� ��� '", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (CreateNewGroup) Then
 '       If (InStr(1, comboGroups.Text, "#") = 0) Then
 '           MsgBox "��� ������ ������ ��������� ������ '#', ������������ ����� ��������", vbOKOnly, "������"
 '           Exit Sub
 '       End If
        
        If (InStr(1, comboGroups.Text, "'") <> 0) Then
            MsgBox "��� ������ �� ������ ��������� ����� �������� ��� << ' >>.", vbOKOnly, "������"
            Exit Sub
        End If
    
        If (SNum.Text = "") Then
            MsgBox "������� ����� ��������", vbOKOnly, "������"
            Exit Sub
        End If
    
        If (Not IsNumeric(SNum.Text)) Then
            MsgBox "���� '����� ��������' ������ ��������� �����", vbOKOnly, "������"
            Exit Sub
        End If
    
        If (txtFirstSessionYear.Text = "") Then
            MsgBox "������� ��� ������ ������", vbOKOnly, "������"
            Exit Sub
        End If
    
        If (Not IsNumeric(txtFirstSessionYear.Text)) Then
            MsgBox "���� '��� ������ ������' ������ ��������� �����", vbOKOnly, "������"
            Exit Sub
        End If
    
        If ((CLng(SNum.Text) > 10) Or (CLng(SNum.Text) < 1)) Then
            MsgBox "����� �������� - ����� ����� �� 1 �� 10", vbOKOnly, "������"
            Exit Sub
        End If
    
        If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblGroups] WHERE [GroupName] = '" & comboGroups.Text & "' AND ([Semester] = " & SNum.Text & " OR [Semester] = " & IIf((SNum.Text Mod 2) = 0, CByte(SNum.Text) - 1, CByte(SNum.Text) + 1) & ")") > 0) Then
            MsgBox "������ � ������ ������� ��� ����������", vbOKOnly, "������"
            Exit Sub
        End If
        
        Dim AR As Long
        AR = CDB.ExecSQLActionQuery("INSERT INTO tblGroups(IDSpec, GroupName, Semester, FirstSessionYear) VALUES (" & IDS.Caption & ", '" & comboGroups.Text & "', " & CLng(SNum.Text) & ", " & CLng(txtFirstSessionYear.Text) & ")", 128)
        
        If (AR > 0) Then
            Dim IDArrey As Variant
            Dim Cols As Integer
            Dim Rows As Long
            
            Dim GroupName As String
            GroupName = comboGroups.Text
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(SNum.Text) Mod 2 = 1, (CLng(SNum.Text) \ 2) + 1, CLng(SNum.Text) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
            
            CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblGroups] WHERE [GroupName] = '" & comboGroups.Text & "' AND [Semester] = " & CLng(SNum.Text), IDArrey, Cols, Rows
            If (Rows > 0) Then
                SelectedGroupID = CLng(IDArrey(0, 0))
                SelectedSpecID = CLng(IDS.Caption)
                
                AddGroupToMainTree comboSpecialities.Text, GroupName, IIf((CLng(SNum.Text) Mod 2) = 1, (CLng(SNum.Text) \ 2 + 1), (CLng(SNum.Text) \ 2)), SelectedGroupID
                
                FillStudInfo2
            End If
        End If
    Else
        SelectedGroupID = CLng(IDG.Caption)
        SelectedSpecID = CLng(IDS.Caption)
        LoadGroupExams
            
        FillStudInfo2
    End If
    
    comboSpecialities.Enabled = False
    comboGroups.Enabled = False
    cmdChoiseGroup.Enabled = False
    SNum.Enabled = False
    NewGroup.Enabled = False
    
    txtFIO.Enabled = True
    comboFP.Enabled = True
    txtON.Enabled = True
    cmdAddStud.Enabled = True
    tblStudents.Enabled = True
    Address.Enabled = True
    AddressInKh.Enabled = True
    txtDOB.Enabled = True
    txtIOP.Enabled = True
    txtMaritalStatus.Enabled = True
    txtNationality.Enabled = True
    txtEducation.Enabled = True
    comboCategory.Enabled = True
    comboCountry.Enabled = True
    
    txtFIO.SetFocus
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub comboCategory_GotFocus()
comboCategory.BackColor = ActiveFieldsColor
End Sub

Private Sub comboCategory_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    comboCountry.SetFocus
End If
KeyCode = 0
End Sub

Private Sub comboCategory_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboCategory_LostFocus()
comboCategory.BackColor = NonActiveFieldsColor
End Sub

Private Sub comboDep_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    cmdAddStud.SetFocus
End If
KeyCode = 0
End Sub

Private Sub comboDep_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboCountry_GotFocus()
comboCountry.BackColor = ActiveFieldsColor
End Sub

Private Sub comboCountry_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    txtIOP.SetFocus
End If
KeyCode = 0
End Sub

Private Sub comboCountry_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboCountry_LostFocus()
comboCountry.BackColor = NonActiveFieldsColor
End Sub

Private Sub comboFP_GotFocus()
comboFP.BackColor = ActiveFieldsColor
End Sub

Private Sub comboFP_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    txtON.SetFocus
End If
KeyCode = 0
End Sub

Private Sub comboFP_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboFP_LostFocus()
comboFP.BackColor = NonActiveFieldsColor
End Sub

Private Sub comboGroups_Click()
If (comboGroups.ListIndex = -1) Then Exit Sub

IDG.Caption = comboGroups.ItemData(comboGroups.ListIndex)

CDB.SQLOpenTableToArrey "SELECT [Semester], [FirstSessionYear] FROM [tblGroups] WHERE [ID] = " & IDG.Caption, GroupArrey, GCols, GRows

SNum.Text = CByte(GroupArrey(0, 0))
txtFirstSessionYear.Text = CLng(IIf(IsNull(GroupArrey(1, 0)), 0, GroupArrey(1, 0)))

CreateNewGroup = False
SNum.Enabled = False
NewGroup.Value = 0

End Sub

Private Sub comboGroups_KeyDown(KeyCode As Integer, Shift As Integer)
If (Not CreateNewGroup) Then KeyCode = 0
End Sub

Private Sub comboGroups_KeyPress(KeyAscii As Integer)
If (Not CreateNewGroup) Then KeyAscii = 0
End Sub

Private Sub comboSpecialities_Click()
    'MsgBox "Name : " & comboSpecialities.List(comboSpecialities.ListIndex) & ", ID : " & comboSpecialities.ItemData(comboSpecialities.ListIndex)
    
    If (comboSpecialities.ListIndex = -1) Then Exit Sub
    Dim i As Long
    Dim GroupName As String
    
    comboGroups.Clear
    IDS.Caption = comboSpecialities.ItemData(comboSpecialities.ListIndex)
    CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester] FROM [tblGroups] WHERE ([Semester] BETWEEN 1 AND 10) AND [IDSpec] = " & comboSpecialities.ItemData(comboSpecialities.ListIndex) & " ORDER BY [Semester]", GroupArrey, GCols, GRows
    
    If (GRows > 0) Then
    
        For i = 0 To GRows - 1
            GroupName = CStr(GroupArrey(1, i))
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupArrey(2, i)) Mod 2 = 1, (CLng(GroupArrey(2, i)) \ 2) + 1, CLng(GroupArrey(2, i)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
            
            comboGroups.AddItem GroupName
            comboGroups.ItemData(comboGroups.NewIndex) = GroupArrey(0, i)
        Next i
        
        comboGroups.ListIndex = 0
    End If
    
    If (GRows = 0) Then
        NewGroup.Value = 1
        NewGroup_Click
    End If

End Sub

Private Sub comboSpecialities_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboSpecialities_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Form_Load()
    CreateNewGroup = False
    Dim i As Long
        
    CDB.SQLOpenTableToArrey "SELECT [ID], [SpecName] FROM [tblSpecialities]", SpecArrey, SCols, SRows
    
    If (SRows = 0) Then
        MsgBox "��� ���������� ������ �� ���������� ������ ���� ���������������� ������� ���� ������������� !", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (SRows > 0) Then
    
        For i = 0 To SRows - 1
            comboSpecialities.AddItem SpecArrey(1, i)
            comboSpecialities.ItemData(comboSpecialities.NewIndex) = SpecArrey(0, i)
        Next i
        
        comboSpecialities.ListIndex = 0
    End If
    
    comboFP.ListIndex = 0
    comboCountry.ListIndex = 0

    CDB.SQLOpenTableToArrey "SELECT [ID], [CategoryName] FROM [tblCategories]", CategoryArrey, CCols, CRows
    
    comboCategory.Clear
    If (CRows > 0) Then
    
        For i = 0 To CRows - 1
            comboCategory.AddItem CategoryArrey(1, i)
            comboCategory.ItemData(comboCategory.NewIndex) = CategoryArrey(0, i)
        Next i
        
        comboCategory.ListIndex = 0
    End If


        tblStudents.Cols = 4
        tblStudents.Rows = 1
        tblStudents.FixedRows = 1
        tblStudents.FixedCols = 1
        
        tblStudents.TextMatrix(0, 0) = "ID"
        tblStudents.TextMatrix(0, 1) = "�. �. �."
        tblStudents.TextMatrix(0, 2) = "����� ������"
        tblStudents.TextMatrix(0, 3) = "� �������"
'        tblStudents.TextMatrix(0, 4) = "�����"
'        tblStudents.TextMatrix(0, 5) = "����� � ��������"
'        tblStudents.TextMatrix(0, 6) = "���� ��������"
'        tblStudents.TextMatrix(0, 7) = "�������"
'        tblStudents.TextMatrix(0, 8) = "�����������"
'        tblStudents.TextMatrix(0, 9) = "�������� ���������"
'        tblStudents.TextMatrix(0, 10) = "�������� � ���������"
'        tblStudents.TextMatrix(0, 11) = "�������������"
'        tblStudents.TextMatrix(0, 12) = "��������"
'        tblStudents.TextMatrix(0, 13) = "�������"
'        tblStudents.TextMatrix(0, 14) = "���������"
'        tblStudents.TextMatrix(0, 15) = "������� ���������"
        
        tblStudents.ColWidth(0) = 400
        tblStudents.ColWidth(1) = 3300
        tblStudents.ColWidth(2) = 1400
        tblStudents.ColWidth(3) = 1300
'        tblStudents.ColWidth(4) = 4500
'        tblStudents.ColWidth(5) = 4500
'        tblStudents.ColWidth(6) = 1500
'        tblStudents.ColWidth(7) = 1500
'        tblStudents.ColWidth(8) = 4500
'        tblStudents.ColWidth(9) = 4500
'        tblStudents.ColWidth(10) = 4500
'        tblStudents.ColWidth(11) = 4500
'        tblStudents.ColWidth(12) = 4500
'        tblStudents.ColWidth(13) = 3000
'        tblStudents.ColWidth(14) = 3000
'        tblStudents.ColWidth(15) = 3000
    LoadSuccess = True
End Sub

Private Sub NewGroup_Click()
If (NewGroup.Value = 0) Then
    If (comboGroups.ListCount = 0) Then
        MsgBox "�� ���� ������������� ��� ����� ��� ������, �������� ����� !"
        NewGroup.Value = 1
        Exit Sub
    End If

    CreateNewGroup = False
    
    SNum.Text = ""
    txtFirstSessionYear.Text = ""
    SNum.Enabled = False
    txtFirstSessionYear.Enabled = False
    comboGroups.ListIndex = 0
End If

If (NewGroup.Value = 1) Then
    CreateNewGroup = True
    
    SNum.Enabled = True
    txtFirstSessionYear.Enabled = True
    SNum.Text = "1"
    txtFirstSessionYear.Text = Year(GlobalData)
    comboGroups.Text = ""
    IDG.Caption = ""
End If
End Sub

Private Sub txtDOB_GotFocus()
txtDOB.BackColor = ActiveFieldsColor
End Sub

Private Sub txtDOB_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    txtNationality.SetFocus
End If
End Sub

Private Sub txtDOB_LostFocus()
txtDOB.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtEducation_GotFocus()
txtEducation.BackColor = ActiveFieldsColor
End Sub

Private Sub txtEducation_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    txtMaritalStatus.SetFocus
End If
End Sub

Private Sub txtEducation_LostFocus()
txtEducation.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtFIO_GotFocus()
txtFIO.BackColor = ActiveFieldsColor
End Sub

Private Sub txtFIO_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    Address.SetFocus
End If
End Sub

Private Sub txtFIO_LostFocus()
txtFIO.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtIOP_GotFocus()
txtIOP.BackColor = ActiveFieldsColor
End Sub

Private Sub txtIOP_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    cmdAddStud.SetFocus
End If
End Sub

Private Sub txtIOP_LostFocus()
txtIOP.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtMaritalStatus_GotFocus()
txtMaritalStatus.BackColor = ActiveFieldsColor
End Sub

Private Sub txtMaritalStatus_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    comboCategory.SetFocus
End If
End Sub

Private Sub txtMaritalStatus_LostFocus()
txtMaritalStatus.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtNationality_GotFocus()
txtNationality.BackColor = ActiveFieldsColor
End Sub

Private Sub txtNationality_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    txtEducation.SetFocus
End If
End Sub

Private Sub txtNationality_LostFocus()
txtNationality.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtON_GotFocus()
txtON.BackColor = ActiveFieldsColor
End Sub

Private Sub txtON_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then
    txtDOB.SetFocus
End If
End Sub

Private Sub txtON_LostFocus()
txtON.BackColor = NonActiveFieldsColor
End Sub

