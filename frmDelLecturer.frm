VERSION 5.00
Begin VB.Form frmDelLecturer 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "������� �������������"
   ClientHeight    =   1935
   ClientLeft      =   1650
   ClientTop       =   2970
   ClientWidth     =   9135
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1935
   ScaleWidth      =   9135
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F5 
      Height          =   615
      Left            =   4320
      TabIndex        =   10
      Top             =   0
      Width           =   4815
      Begin VB.Label LName 
         Height          =   255
         Left            =   840
         TabIndex        =   12
         Top             =   240
         Width           =   3855
      End
      Begin VB.Label �����9 
         AutoSize        =   -1  'True
         Caption         =   "�.�.�. :"
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   630
      End
   End
   Begin VB.Frame F4 
      Height          =   735
      Left            =   4320
      TabIndex        =   7
      Top             =   1200
      Width           =   4815
      Begin VB.CommandButton cmdSend 
         Caption         =   "�������"
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   2295
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   375
         Left            =   2400
         TabIndex        =   8
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame F2 
      Caption         =   "������"
      Height          =   615
      Left            =   4320
      TabIndex        =   4
      Top             =   600
      Width           =   4815
      Begin VB.Label StartFacultyName 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1080
         TabIndex        =   6
         Top             =   240
         Width           =   3615
      End
      Begin VB.Label �����3 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   765
      End
   End
   Begin VB.Frame F1 
      Caption         =   "�������� ������������� ��� ��������"
      Height          =   1935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4215
      Begin VB.ListBox lstFIO 
         Height          =   1035
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   3975
      End
      Begin VB.TextBox txtFIO 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   3975
      End
      Begin VB.Label �����1 
         AutoSize        =   -1  'True
         Caption         =   "������� ����� :"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1275
      End
   End
End
Attribute VB_Name = "frmDelLecturer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private SelectedLecturer As Long

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdSend_Click()

Dim LCount1 As Long
Dim LCount2 As Long

Dim AR As Long

    If (SelectedLecturer = 0) Then
        MsgBox "�������� ������������� ��� ��������", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (MsgBox("����������� �������� ������������� '" & LName.Caption & "'", vbYesNo) = vbNo) Then Exit Sub
    
    LCount1 = CDB.SQLGetRecordCount("SELECT [ID] FROM [tblExaminations] WHERE [IDLecturer] = " & SelectedLecturer)
    LCount2 = CDB.SQLGetRecordCount("SELECT [ID] FROM [tblDegreeThemes] WHERE [IDLecturer] = " & SelectedLecturer)
    
    If ((LCount1 > 0) Or (LCount2 > 0)) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblLecturers] SET [IsActive] = 0 WHERE [ID] = " & SelectedLecturer, 128)
    Else
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblLecturers] WHERE [ID] = " & SelectedLecturer, 128)
    End If
    
        Dim nd As VSFlexNode
        Dim i As Long
    
        For i = 0 To Main.MainTree.Rows - 1
            If ((Main.MainTree.TextMatrix(i, 0) = LName.Caption) And (Main.MainTree.RowData(i) Like "BD_LECTOR*")) Then
                Main.MainTree.RemoveItem (i)
                Exit For
            End If
        Next i
        
        If (AR > 0) Then MsgBox "������������� ��� ������� ������"

txtFIO_Change
End Sub

Private Sub Form_Load()
SelectedLecturer = 0

LoadSuccess = True
End Sub

Private Sub lstFIO_Click()
Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

If (lstFIO.ListIndex = -1) Then Exit Sub

Dim FacultyName As String
Dim IDFaculty As Long

LName.Caption = lstFIO.List(lstFIO.ListIndex)
SelectedLecturer = lstFIO.ItemData(lstFIO.ListIndex)

CDB.SQLOpenTableToArrey "SELECT [IDFaculty] FROM [tblLecturers] WHERE [ID] = " & SelectedLecturer, DopArrey, DopCols, DopRows
IDFaculty = CLng(DopArrey(0, 0))

CDB.SQLOpenTableToArrey "SELECT [FacultyName] FROM [tblFaculty] WHERE [ID] = " & IDFaculty, DopArrey, DopCols, DopRows
FacultyName = CStr(DopArrey(0, 0))

StartFacultyName.Caption = FacultyName
End Sub

Private Sub txtFIO_Change()
Dim LecturerName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim LecturersArrey As Variant
Dim LCols As Integer
Dim LRows As Long

Dim i As Long

LecturerName = txtFIO.Text

If (LecturerName = "") Then
    lstFIO.Clear
    LName.Caption = ""
    StartFacultyName.Caption = ""
    Exit Sub
End If

'If (InStr(1, LecturerName, "'") <> 0) Then Exit Sub

Dim FirstS As Byte
Dim SecondS As Byte

FirstS = InStr(1, LecturerName, " ")
If (FirstS) Then SecondS = InStr(InStr(1, LecturerName, " ") + 1, LecturerName, " ")

If ((FirstS = 0) And (SecondS = 0)) Then
    Surname = LecturerName
    CheckString Surname
    
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] LIKE '" & Surname & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
ElseIf ((FirstS <> 0) And (SecondS = 0)) Then
    Surname = Left(LecturerName, FirstS - 1)
    Name = Mid(LecturerName, FirstS + 1, Len(LecturerName) - FirstS)
    
    CheckString Surname
    CheckString Name
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
ElseIf ((FirstS <> 0) And (SecondS <> 0)) Then
    Surname = Left(LecturerName, FirstS - 1)
    Name = Mid(LecturerName, FirstS + 1, SecondS - FirstS - 1)
    Patronymic = Right(LecturerName, Len(LecturerName) - SecondS)
    
    CheckString Surname
    CheckString Name
    CheckString Patronymic
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
End If

lstFIO.Clear
If (LRows > 0) Then

    For i = 0 To LRows - 1
        lstFIO.AddItem GetFullName(CStr(LecturersArrey(1, i)), CStr(LecturersArrey(2, i)), CStr(LecturersArrey(3, i)), False, False, True)
        lstFIO.ItemData(lstFIO.NewIndex) = LecturersArrey(0, i)
    Next i
    
    lstFIO.ListIndex = 0
    
Else
    LName.Caption = ""
    StartFacultyName.Caption = ""
End If
End Sub
