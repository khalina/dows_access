Attribute VB_Name = "SQLModule"
Option Explicit

'-----------------------
Public Category As String
    
Public LTable As Integer
Public LField As Integer
Public LFunc As String
    
Public Operation As String
    
Public RString As String
    
Public RTable As Integer
Public RField As Integer
'-----------------------

Public Type Condition
    Category As String
    
    LTable As Integer
    LField As Integer
    
    Operation As String
    
    RString As String
    
    RTable As Integer
    RField As Integer
End Type
Public ConditionsArray() As Condition
Public ConditionsCount As Integer

Public SelectedTable As Integer
Public SelectedField As Integer

