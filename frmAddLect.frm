VERSION 5.00
Begin VB.Form frmAddLect 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "���������� ��������������"
   ClientHeight    =   6975
   ClientLeft      =   6675
   ClientTop       =   2265
   ClientWidth     =   5415
   Icon            =   "frmAddLect.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6975
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F4 
      Caption         =   "�������� �������������"
      Height          =   6975
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5415
      Begin VB.TextBox txtIDCode 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   27
         Top             =   4920
         Width           =   2055
      End
      Begin VB.TextBox txtPassport 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   25
         Top             =   4560
         Width           =   2055
      End
      Begin VB.TextBox txtDoctorWorkName 
         Height          =   645
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   23
         Top             =   3000
         Width           =   5175
      End
      Begin VB.TextBox txtCandidateWorkName 
         Height          =   645
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   21
         Top             =   2040
         Width           =   5175
      End
      Begin VB.TextBox txtName 
         Height          =   285
         Left            =   1440
         TabIndex        =   0
         Top             =   240
         Width           =   3855
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   375
         Left            =   3840
         TabIndex        =   11
         Top             =   6480
         Width           =   1455
      End
      Begin VB.CommandButton cmdAddLect 
         Caption         =   "��������"
         Height          =   375
         Left            =   2400
         TabIndex        =   10
         Top             =   6480
         Width           =   1455
      End
      Begin VB.TextBox txtPost 
         Height          =   285
         Left            =   1440
         TabIndex        =   9
         Top             =   960
         Width           =   3855
      End
      Begin VB.TextBox txtAddress 
         Height          =   285
         Left            =   1440
         TabIndex        =   8
         Top             =   5640
         Width           =   3855
      End
      Begin VB.TextBox txtTelephone 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   7
         Top             =   6000
         Width           =   2055
      End
      Begin VB.ComboBox comboFaculty 
         Height          =   315
         Left            =   1440
         TabIndex        =   6
         Top             =   600
         Width           =   3855
      End
      Begin VB.TextBox txtEducation 
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   3840
         Width           =   3855
      End
      Begin VB.TextBox txtEducYear 
         Height          =   285
         Left            =   1440
         TabIndex        =   4
         Top             =   4200
         Width           =   2055
      End
      Begin VB.TextBox txtSDegree 
         Height          =   285
         Left            =   1440
         TabIndex        =   3
         Top             =   1320
         Width           =   3855
      End
      Begin VB.TextBox txtDOB 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   2
         Top             =   5280
         Width           =   2055
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "��. ��� :"
         Height          =   195
         Left            =   120
         TabIndex        =   28
         Top             =   4920
         Width           =   705
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   26
         Top             =   4560
         Width           =   735
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "���� ���������� ������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   2760
         Width           =   2205
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "���� ������������ ������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   1800
         Width           =   2205
         WordWrap        =   -1  'True
      End
      Begin VB.Label �����1 
         AutoSize        =   -1  'True
         Caption         =   "�.�.�. :"
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   630
      End
      Begin VB.Label �����2 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   600
         Width           =   765
      End
      Begin VB.Label �����3 
         AutoSize        =   -1  'True
         Caption         =   "��������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   960
         Width           =   960
      End
      Begin VB.Label �����4 
         AutoSize        =   -1  'True
         Caption         =   "����� :"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   5640
         Width           =   555
      End
      Begin VB.Label �����5 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   6000
         Width           =   765
      End
      Begin VB.Label �����13 
         AutoSize        =   -1  'True
         Caption         =   "��� �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   3840
         Width           =   1125
      End
      Begin VB.Label �����14 
         AutoSize        =   -1  'True
         Caption         =   "� ����� ���� :"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   4200
         Width           =   1095
      End
      Begin VB.Label �����21 
         AutoSize        =   -1  'True
         Caption         =   "��. ������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   1320
         Width           =   990
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "���� �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   5280
         Width           =   1275
      End
   End
End
Attribute VB_Name = "frmAddLect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAddLect_Click()

Dim LectName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim Cafedra As Long
Dim Post As String
Dim CandidateWorkName As String
Dim DoctorWorkName As String
Dim Address As String
Dim Telephone As String
Dim Education As String
Dim EducYear As Integer
Dim SDegree As String
Dim Passport As String
Dim IDCode As String
Dim DOB As String

LectName = txtName.Text
If (Not GetFIO(LectName, "�������������", "�.�.�.", 40, 40, 40, Surname, Name, Patronymic)) Then Exit Sub
CheckString Surname
CheckString Name
CheckString Patronymic

Cafedra = comboFaculty.ItemData(comboFaculty.ListIndex)
If ((Cafedra = 0) Or (Cafedra = 1)) Then
    MsgBox "�������� �������", vbOKOnly, "������"
    Exit Sub
End If

Post = txtPost.Text
If (FindErrors(Post, 50, "���������", False, True)) Then Exit Sub
CheckString Post

SDegree = txtSDegree.Text
If (FindErrors(SDegree, 200, "������ �������", False, True)) Then Exit Sub
CheckString SDegree

CandidateWorkName = txtCandidateWorkName.Text
If (FindErrors(CandidateWorkName, 255, "���� ������������ ������", False, True)) Then Exit Sub
CheckString CandidateWorkName

DoctorWorkName = txtDoctorWorkName.Text
If (FindErrors(DoctorWorkName, 255, "���� ���������� ������", False, True)) Then Exit Sub
CheckString DoctorWorkName

Education = txtEducation.Text
If (FindErrors(Education, 100, "��� ��������", False, True)) Then Exit Sub
CheckString Education

If (txtEducYear.Text = "") Then
    EducYear = 0
Else
    If (Not IsNumeric(txtEducYear.Text)) Then
        MsgBox "���� '� ����� ����' ������ ��������� �����.", vbOKOnly, "������"
        Exit Sub
    End If
    EducYear = CInt(txtEducYear.Text)
End If

Passport = txtPassport.Text
If (FindErrors(Passport, 30, "�������", False, True)) Then Exit Sub
CheckString Passport

IDCode = txtIDCode.Text
If (FindErrors(IDCode, 30, "����������������� ���", False, True)) Then Exit Sub
CheckString IDCode

Address = txtAddress.Text
If (FindErrors(Address, 255, "�����", False, True)) Then Exit Sub
CheckString Address

Telephone = txtTelephone.Text
If (FindErrors(Telephone, 20, "�������", False, True)) Then Exit Sub
CheckString Telephone


DOB = txtDOB.Text
If (DOB <> "") Then
    If (Not IsDate(DOB)) Then
        MsgBox "���� '���� ��������' ������ ��������� ������ ���� : '12.03.2002'", vbOKOnly, "������"
        Exit Sub
    End If
End If


    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    Dim fOk As Boolean
    Dim AR As Long
    
    fOk = True
    CDB.SQLOpenTableToArrey "SELECT [ID], [IsActive] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] = '" & Patronymic & "'", DopArrey, DopCols, DopRows
    If (DopRows > 0) Then
        If (DopArrey(1, 0) = 1) Then
            MsgBox "������������� � ����� ������ ��� ����������", vbOKOnly, "������"
            Exit Sub
        End If
        If (DopArrey(1, 0) = 0) Then
            If (MsgBox("������������� � ����� ������ ��� �������� � ���� ������, �� �� �����-�� �������� ��� ������. ������������ ���������� ������������� ?", vbYesNo) = vbNo) Then Exit Sub
            AR = CDB.ExecSQLActionQuery("UPDATE [tblLecturers] SET [IsActive] = 1, [IDFaculty] = " & Cafedra & ", [Post] = '" & Post & "', [Address] = '" & Address & "', [Telephone] = '" & Telephone & "', [Education] = '" & Education & "', [Year] = " & EducYear & ", [CandidateWorkTheme] = '" & CandidateWorkName & "', [DoctorWorkTheme] = '" & DoctorWorkName & "', [IDCode] = '" & IDCode & "', [Passport] = '" & Passport & "', [DateOfBorn] = " & IIf(DOB <> "", "'" & DOB & "'", "NULL") & " WHERE [ID] = " & CLng(DopArrey(0, 0)), 128)
            fOk = False
        End If
    End If

    If (fOk) Then AR = CDB.ExecSQLActionQuery("INSERT INTO tblLecturers(Surname, Name, Patronymic, IDFaculty, Post, ScientificDegree, Address, Telephone, Education, Year, DateOfBorn, CandidateWorkTheme, DoctorWorkTheme, IDCode, Passport, IsActive) VALUES ('" & Surname & "', '" & Name & "', '" & Patronymic & "', " & Cafedra & ", '" & Post & "', '" & SDegree & "', '" & Address & "', '" & Telephone & "', '" & Education & "', " & EducYear & ", " & IIf(DOB <> "", "'" & DOB & "'", "NULL") & ", '" & CandidateWorkName & "', '" & DoctorWorkName & "', '" & IDCode & "', '" & Passport & "', 1)", 128)

If (AR > 0) Then
    Dim IDArrey As Variant
    Dim Cols As Integer
    Dim Rows As Long
    Dim i As Long
    
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM tblLecturers WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] = '" & Patronymic & "'", IDArrey, Cols, Rows
    If (Rows > 0) Then
        Dim nd As VSFlexNode
    
        For i = 0 To Main.MainTree.Rows - 1
            If ((Main.MainTree.TextMatrix(i, 0) = comboFaculty.Text) And (Main.MainTree.RowData(i) Like "BD_FACULTY*")) Then
            
                Set nd = Main.MainTree.GetNode(i)
                
                nd.AddNode flexNTLastChild, LectName
                Set nd = nd.GetNode(flexNTLastChild)
                Main.MainTree.IsSubtotal(nd.Row) = True
                Main.MainTree.RowData(nd.Row) = "BD_LECTOR_" & IDArrey(0, 0)
                
                If (nd.Expanded = False) Then nd.Expanded = False
                
                Set nd = nd.GetNode(flexNTParent)
                If (nd.Expanded = False) Then nd.Expanded = False
                
                Set nd = nd.GetNode(flexNTParent)
                If (nd.Expanded = False) Then nd.Expanded = False
                
                Exit For
            End If
        Next i
        
    End If
    
End If


txtName.Text = ""
txtPost.Text = ""
txtSDegree.Text = ""
txtCandidateWorkName.Text = ""
txtDoctorWorkName.Text = ""
txtEducation.Text = ""
txtEducYear.Text = ""
txtPassport.Text = ""
txtIDCode.Text = ""
txtDOB.Text = ""
txtAddress.Text = ""
txtTelephone.Text = ""

txtName.SetFocus
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub comboFaculty_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Form_Load()
Dim FacultyArrey As Variant
Dim FCols As Integer
Dim FRows As Long

Dim i As Long
        
    CDB.SQLOpenTableToArrey "SELECT [ID], [FacultyName] FROM [tblFaculty] WHERE [IsActive] = 1", FacultyArrey, FCols, FRows

    If (FRows = 0) Then
        MsgBox "��� ���������� ������������� �� ���������� ������ ���� ���������������� ������� ���� ������� !", vbOKOnly, "������"
        Exit Sub
    End If

    comboFaculty.Clear
    If (FRows > 0) Then

        For i = 0 To FRows - 1
            comboFaculty.AddItem FacultyArrey(1, i)
            comboFaculty.ItemData(comboFaculty.NewIndex) = FacultyArrey(0, i)
        Next i

        comboFaculty.ListIndex = 0
    End If

    LoadSuccess = True
End Sub

Private Sub txtName_GotFocus()
txtName.BackColor = ActiveFieldsColor
End Sub

Private Sub txtName_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then comboFaculty.SetFocus
End Sub

Private Sub txtName_LostFocus()
txtName.BackColor = NonActiveFieldsColor
End Sub

Private Sub comboFaculty_GotFocus()
comboFaculty.BackColor = ActiveFieldsColor
End Sub

Private Sub comboFaculty_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtPost.SetFocus
End Sub

Private Sub comboFaculty_LostFocus()
comboFaculty.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtPost_GotFocus()
txtPost.BackColor = ActiveFieldsColor
End Sub

Private Sub txtPost_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtSDegree.SetFocus
End Sub

Private Sub txtPost_LostFocus()
txtPost.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtSDegree_GotFocus()
txtSDegree.BackColor = ActiveFieldsColor
End Sub

Private Sub txtSDegree_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtCandidateWorkName.SetFocus
End Sub

Private Sub txtSDegree_LostFocus()
txtSDegree.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtCandidateWorkName_GotFocus()
txtCandidateWorkName.BackColor = ActiveFieldsColor
End Sub

Private Sub txtCandidateWorkName_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtDoctorWorkName.SetFocus
End Sub

Private Sub txtCandidateWorkName_LostFocus()
txtCandidateWorkName.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtDoctorWorkName_GotFocus()
txtDoctorWorkName.BackColor = ActiveFieldsColor
End Sub

Private Sub txtDoctorWorkName_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtEducation.SetFocus
End Sub

Private Sub txtDoctorWorkName_LostFocus()
txtDoctorWorkName.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtEducation_GotFocus()
txtEducation.BackColor = ActiveFieldsColor
End Sub

Private Sub txtEducation_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtEducYear.SetFocus
End Sub

Private Sub txtEducation_LostFocus()
txtEducation.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtEducYear_GotFocus()
txtEducYear.BackColor = ActiveFieldsColor
End Sub

Private Sub txtEducYear_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtPassport.SetFocus
End Sub

Private Sub txtEducYear_LostFocus()
txtEducYear.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtPassport_GotFocus()
txtPassport.BackColor = ActiveFieldsColor
End Sub

Private Sub txtPassport_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtIDCode.SetFocus
End Sub

Private Sub txtPassport_LostFocus()
txtPassport.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtIDCode_GotFocus()
txtIDCode.BackColor = ActiveFieldsColor
End Sub

Private Sub txtIDCode_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtDOB.SetFocus
End Sub

Private Sub txtIDCode_LostFocus()
txtIDCode.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtDOB_GotFocus()
txtDOB.BackColor = ActiveFieldsColor
End Sub

Private Sub txtDOB_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtAddress.SetFocus
End Sub

Private Sub txtDOB_LostFocus()
txtDOB.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtAddress_GotFocus()
txtAddress.BackColor = ActiveFieldsColor
End Sub

Private Sub txtAddress_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then txtTelephone.SetFocus
End Sub

Private Sub txtAddress_LostFocus()
txtAddress.BackColor = NonActiveFieldsColor
End Sub

Private Sub txtTelephone_GotFocus()
txtTelephone.BackColor = ActiveFieldsColor
End Sub

Private Sub txtTelephone_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 13) Then cmdAddLect.SetFocus
End Sub

Private Sub txtTelephone_LostFocus()
txtTelephone.BackColor = NonActiveFieldsColor
End Sub


