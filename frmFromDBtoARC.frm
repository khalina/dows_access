VERSION 5.00
Begin VB.Form frmFromDBtoARC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�������� � �����"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5865
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   5865
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSendGroup 
      Caption         =   "��������� ������"
      Height          =   375
      Left            =   1920
      TabIndex        =   7
      Top             =   3840
      Width           =   1815
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "�������"
      Height          =   375
      Left            =   3960
      TabIndex        =   6
      Top             =   3840
      Width           =   1815
   End
   Begin VB.CommandButton cmdSend 
      Caption         =   "���������"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   3840
      Width           =   1815
   End
   Begin VB.ComboBox cboGroup 
      Height          =   315
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   4215
   End
   Begin VB.ComboBox cboSpeciality 
      Height          =   315
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   120
      Width           =   4215
   End
   Begin VB.ListBox lstStudents 
      Height          =   2595
      Left            =   120
      TabIndex        =   4
      Top             =   1080
      Width           =   5655
   End
   Begin VB.Label lblGroup 
      Alignment       =   1  'Right Justify
      Caption         =   "������:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   1335
   End
   Begin VB.Label lblSpeciality 
      Alignment       =   1  'Right Justify
      Caption         =   "�������������:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "frmFromDBtoARC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MoveAll As Boolean
Private Stopped As Boolean

Private Sub cboGroup_Click()
    FillStudents
End Sub

Private Sub cboSpeciality_Click()
    FillGroups
    FillStudents
End Sub

Private Sub cmdExit_Click()
    If Stopped = False Then
        Stopped = True
        cmdExit.Enabled = False
    Else
        Unload Me
    End If
End Sub

Private Sub cmdSend_Click()
    If Not MoveAll Then
    cboSpeciality.Enabled = False
    cboGroup.Enabled = False
    lstStudents.Enabled = False
    cmdSend.Enabled = False
    cmdSendGroup.Enabled = False
    cmdExit.Enabled = False
    End If
    
    MoveStudentToArchive lstStudents.ItemData(lstStudents.ListIndex), CDB, arc
    FillStudents
    
    If Not MoveAll Then
    cmdSend.Enabled = True
    cmdSendGroup.Enabled = True
    cmdExit.Enabled = True
    cboSpeciality.Enabled = True
    cboGroup.Enabled = True
    lstStudents.Enabled = True
    End If
   
'   ����� ������ ������� � ����������� ����������...
'    If lstStudents.ListCount = 0 Then
'    If MsgBox("� ��������� ������ �� �������� �� ������ ��������. ������� �?", _
'    vbQuestion Or vbYesNo Or vbDefaultButton1) = vbYes Then
'        CDB.ExecSQLActionQuery "DELETE * FROM tblGroups WHERE id=" & _
'        cboGroup.ItemData(cboGroup.ListIndex), 128
'    End If
'    End If
End Sub

Private Sub cmdSendGroup_Click()
    MoveAll = True
'    cboSpeciality.Enabled = False
'    cboGroup.Enabled = False
'    lstStudents.Enabled = False
'    cmdSend.Enabled = False
'    cmdSendGroup.Enabled = False
    cmdExit.Caption = "����!"
    Stopped = False
    
    lstStudents.ListIndex = 0
    Do While lstStudents.ListCount > 0 And Not Stopped
        cboSpeciality.Enabled = False
        cboGroup.Enabled = False
        lstStudents.Enabled = False
        cmdSend.Enabled = False
        cmdSendGroup.Enabled = False
        cmdSend_Click
        DoEvents
    Loop
    
    Stopped = True
    cmdSend.Enabled = True
    cmdSendGroup.Enabled = True
    cmdExit.Caption = "�������"
    cmdExit.Enabled = True
    cboSpeciality.Enabled = True
    cboGroup.Enabled = True
    lstStudents.Enabled = True
    MoveAll = False
    
    Dim i As Integer
    i = cboGroup.ListIndex
    FillGroups
    cboGroup.ListIndex = i
    FillStudents
End Sub

Private Sub Form_Load()
    MoveAll = False
    Stopped = True
    
    FillSpecialities
    If (cboSpeciality.ListCount = 0) Then
        MsgBox "��� �� ����� ������������� �� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    FillGroups
    FillStudents
    
        
    CurArcPath = ""
    ArcLoaded = arc.OpenArchive _
    ("UDBArc", "", MainDBPass, "SCOMP", Main.CommonDialog1, CurArcPath)
    
    If Not ArcLoaded Then
        MsgBox "���� ������ �� ��� ������!", vbCritical, "������"
        Exit Sub
    End If
    
    LoadSuccess = True
End Sub

Private Sub FillSpecialities()
    Dim SpecArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long

    CDB.SQLOpenTableToArrey _
    "SELECT ID,SpecName FROM tblSpecialities ORDER BY [SpecName]" _
    , SpecArrey, SCols, SRows
    
    cboSpeciality.Clear
    If (SRows > 0) Then
        Dim i As Long
        For i = 0 To SRows - 1
            cboSpeciality.AddItem SpecArrey(1, i)
            cboSpeciality.ItemData(cboSpeciality.NewIndex) = SpecArrey(0, i)
        Next i
        cboSpeciality.ListIndex = 0
    End If
End Sub

Private Sub FillGroups()
    Dim GroupsArrey As Variant
    Dim GCols As Integer
    Dim GRows As Long

    Dim GroupName As String

    CDB.SQLOpenTableToArrey _
    "SELECT ID,GroupName,Semester FROM tblGroups WHERE (Semester BETWEEN 1 AND 10)" & _
    " AND IDSpec=" & cboSpeciality.ItemData(cboSpeciality.ListIndex) & _
    " ORDER BY Semester DESC", GroupsArrey, GCols, GRows
    
    cboGroup.Clear
    If (GRows > 0) Then
        Dim i As Long
        For i = 0 To GRows - 1
            GroupName = GroupsArrey(1, i)
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupsArrey(2, i)) Mod 2 = 1, (CLng(GroupsArrey(2, i)) \ 2) + 1, CLng(GroupsArrey(2, i)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))

            cboGroup.AddItem GroupName
            cboGroup.ItemData(cboGroup.NewIndex) = GroupsArrey(0, i)
        Next i
        cboGroup.ListIndex = 0
        cboGroup.Enabled = True
    Else
        cboGroup.Enabled = False
    End If
End Sub

Private Sub FillStudents()
    If cboGroup.ListCount = 0 Then
        lstStudents.Clear
        lstStudents.Enabled = False
        cmdSend.Enabled = False
        cmdSendGroup.Enabled = False
        Exit Sub
    End If
    
    Dim StudArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long
    
    CDB.SQLOpenTableToArrey "SELECT ID,Surname,Name,Patronymic FROM tblStudents" & _
    " WHERE isActive=1 AND idGroup=" & cboGroup.ItemData(cboGroup.ListIndex) & _
    " ORDER BY Surname,Name,Patronymic", StudArrey, SCols, SRows
    
    lstStudents.Clear
    If SRows > 0 Then
        Dim i As Long
        For i = 0 To SRows - 1
            lstStudents.AddItem _
            StudArrey(1, i) & " " & StudArrey(2, i) & " " & StudArrey(3, i)
            lstStudents.ItemData(lstStudents.NewIndex) = StudArrey(0, i)
        Next i
        lstStudents.ListIndex = 0
        lstStudents.Enabled = True
        cmdSend.Enabled = True
        cmdSendGroup.Enabled = True
    Else
        lstStudents.Enabled = False
        cmdSend.Enabled = False
        cmdSendGroup.Enabled = False
        Exit Sub
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If ArcLoaded Then arc.closeArchive
    ArcLoaded = False
End Sub
