VERSION 5.00
Begin VB.Form frmBDDiag 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "������ ���� ������"
   ClientHeight    =   2910
   ClientLeft      =   2250
   ClientTop       =   2820
   ClientWidth     =   6735
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2910
   ScaleWidth      =   6735
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F3 
      Height          =   735
      Left            =   2760
      TabIndex        =   13
      Top             =   2160
      Width           =   3975
      Begin VB.CommandButton cmdClose 
         Caption         =   "������"
         Height          =   375
         Left            =   2040
         TabIndex        =   15
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdDel 
         Caption         =   "�������"
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame F2 
      Height          =   2175
      Left            =   2760
      TabIndex        =   2
      Top             =   0
      Width           =   3975
      Begin VB.Label lblSpecName 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   1440
         TabIndex        =   17
         Top             =   240
         Width           =   2415
      End
      Begin VB.Label lbl2 
         AutoSize        =   -1  'True
         Caption         =   "������������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   1260
      End
      Begin VB.Label lblConfirm 
         Height          =   255
         Left            =   2160
         TabIndex        =   12
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label lblDeactive 
         Height          =   255
         Left            =   2160
         TabIndex        =   11
         Top             =   1440
         Width           =   1695
      End
      Begin VB.Label lblActive 
         Height          =   255
         Left            =   2160
         TabIndex        =   10
         Top             =   1200
         Width           =   1695
      End
      Begin VB.Label lblStudNum 
         Height          =   255
         Left            =   2160
         TabIndex        =   9
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label lblSemester 
         Height          =   255
         Left            =   2160
         TabIndex        =   8
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label �����5 
         AutoSize        =   -1  'True
         Caption         =   "����� �� ������ ������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   1800
         Width           =   1965
      End
      Begin VB.Label �����4 
         AutoSize        =   -1  'True
         Caption         =   "�������� �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label �����3 
         AutoSize        =   -1  'True
         Caption         =   "�� ��� ��������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   1440
      End
      Begin VB.Label �����2 
         AutoSize        =   -1  'True
         Caption         =   "��������� � ������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   840
         Width           =   1560
      End
      Begin VB.Label �����1 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   750
      End
   End
   Begin VB.Frame F1 
      Caption         =   "������ ���������� ��������"
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2655
      Begin VB.ListBox GroupList 
         Height          =   2595
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmBDDiag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdDel_Click()
Dim GroupArrey As Variant
Dim GroupCols As Integer
Dim GroupRows As Long

Dim StudArrey As Variant
Dim StudCols As Integer
Dim StudRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim i As Integer
Dim j As Integer

Dim IDGroup As Long
Dim AR As Long
Dim DelGroup As Boolean

For i = 0 To GroupList.ListCount - 1
    DelGroup = True
    IDGroup = GroupList.ItemData(i)
    
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblStudents] WHERE [IDGroup] = " & IDGroup, StudArrey, StudCols, StudRows
    For j = 0 To StudRows - 1
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSheets] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes2] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblConfirmList] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblStudEvents] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSpecialcourses] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblStudents] WHERE [ID] = " & CLng(StudArrey(0, j)), 128)
    Next j
    
    CDB.SQLOpenTableToArrey "SELECT [tblSheets.IDStudent] FROM [tblSheets], [tblExaminations] WHERE tblSheets.IDExam = tblExaminations.ID AND tblExaminations.IDGroup = " & IDGroup & " GROUP BY tblSheets.IDStudent", StudArrey, StudCols, StudRows
    For j = 0 To StudRows - 1
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSheets] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes2] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblConfirmList] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblStudEvents] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSpecialcourses] WHERE [IDStudent] = " & CLng(StudArrey(0, j)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblStudents] WHERE [ID] = " & CLng(StudArrey(0, j)), 128)
    Next j
    
    If (DelGroup) Then
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblExaminations] WHERE [IDGroup] = " & IDGroup, 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblGroups] WHERE [ID] = " & IDGroup, 128)
    End If
Next i

Unload Me
End Sub

Private Sub Form_Load()
LoadSuccess = True
End Sub

Private Sub GroupList_Click()

Dim GroupArrey As Variant
Dim GroupCols As Integer
Dim GroupRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim IDGroup As Long

If (GroupList.ListIndex = -1) Then Exit Sub

IDGroup = GroupList.ItemData(GroupList.ListIndex)

CDB.SQLOpenTableToArrey "SELECT [Semester], [IDSpec] FROM [tblGroups] WHERE [ID] = " & IDGroup, GroupArrey, GroupCols, GroupRows

CDB.SQLOpenTableToArrey "SELECT [SpecName] FROM [tblSpecialities] WHERE [ID] = " & CLng(GroupArrey(1, 0)), DopArrey, DopCols, DopRows
lblSpecName.Caption = CStr(DopArrey(0, 0))

lblSemester.Caption = GroupArrey(0, 0)

CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblStudents] WHERE [IDGroup] = " & IDGroup, DopArrey, DopCols, DopRows
lblStudNum.Caption = DopRows

CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblStudents] WHERE [IsActive] = 1 AND [IDGroup] = " & IDGroup, DopArrey, DopCols, DopRows
lblActive.Caption = DopRows

CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblStudents] WHERE [IsActive] = 0 AND [IDGroup] = " & IDGroup, DopArrey, DopCols, DopRows
lblDeactive.Caption = DopRows
If (DopRows = 0) Then lblConfirm.Caption = "��"
If (DopRows <> 0) Then lblConfirm.Caption = "���"

End Sub
