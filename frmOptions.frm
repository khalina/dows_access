VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Object = "{F5C0D7B0-1CEA-11D7-8C70-97E62479B906}#1.0#0"; "ABTabStrip.ocx"
Begin VB.Form frmOptions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "��������� ���������"
   ClientHeight    =   5775
   ClientLeft      =   3630
   ClientTop       =   2145
   ClientWidth     =   6240
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5775
   ScaleWidth      =   6240
   Begin ABTabStrip.TabControl OptionsTab 
      Height          =   5775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   10186
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CommandButton cmdCancel 
         Caption         =   "������"
         Height          =   375
         Left            =   4800
         TabIndex        =   60
         Top             =   5280
         Width           =   1335
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   3480
         TabIndex        =   59
         Top             =   5280
         Width           =   1335
      End
      Begin VB.Frame F5 
         Height          =   4695
         Left            =   120
         TabIndex        =   40
         Top             =   480
         Width           =   6015
         Begin VB.Frame Frame2 
            Caption         =   "����� ��������"
            Height          =   615
            Left            =   3000
            TabIndex        =   66
            Top             =   120
            Width           =   2895
            Begin VB.TextBox txtEducForm 
               Height          =   285
               Left            =   120
               TabIndex        =   67
               Top             =   240
               Width           =   2655
            End
         End
         Begin VB.Frame F2 
            Caption         =   "�������� �������� ���������"
            Height          =   2655
            Left            =   120
            TabIndex        =   49
            Top             =   840
            Width           =   5775
            Begin VB.TextBox txtUNF4 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   2760
               TabIndex        =   55
               Top             =   2235
               Width           =   2295
            End
            Begin VB.TextBox txtUNF3 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   2760
               TabIndex        =   54
               Top             =   1995
               Width           =   2295
            End
            Begin VB.TextBox txtUNF2 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   2760
               TabIndex        =   53
               Top             =   1755
               Width           =   2295
            End
            Begin VB.TextBox txtUNF1 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   2760
               TabIndex        =   52
               Top             =   1500
               Width           =   2295
            End
            Begin VB.TextBox txtUniverNameRP 
               Height          =   285
               Left            =   120
               TabIndex        =   51
               Top             =   1040
               Width           =   5535
            End
            Begin VB.TextBox txtUniverName 
               Height          =   285
               Left            =   120
               TabIndex        =   50
               Top             =   480
               Width           =   5535
            End
            Begin VB.Label �����5 
               Caption         =   "��� ��������� ������� :"
               Height          =   255
               Left            =   120
               TabIndex        =   58
               Top             =   1500
               Width           =   2055
            End
            Begin VB.Label �����4 
               Caption         =   "��� ������� (� ����������� ������) :"
               Height          =   255
               Left            =   120
               TabIndex        =   57
               Top             =   820
               Width           =   3135
            End
            Begin VB.Label �����3 
               Caption         =   "��� ���������� :"
               Height          =   255
               Left            =   120
               TabIndex        =   56
               Top             =   260
               Width           =   1575
            End
         End
         Begin VB.Frame F3 
            Caption         =   "�������� ���������� :"
            Height          =   975
            Left            =   120
            TabIndex        =   44
            Top             =   3600
            Width           =   5775
            Begin VB.TextBox txtFNR 
               Height          =   285
               Left            =   2040
               TabIndex        =   46
               Top             =   600
               Width           =   3615
            End
            Begin VB.TextBox txtFNI 
               Height          =   285
               Left            =   2040
               TabIndex        =   45
               Top             =   260
               Width           =   3615
            End
            Begin VB.Label �����7 
               Caption         =   "����������� ����� :"
               Height          =   255
               Left            =   120
               TabIndex        =   48
               Top             =   600
               Width           =   1695
            End
            Begin VB.Label �����6 
               Caption         =   "������������ ����� :"
               Height          =   255
               Left            =   120
               TabIndex        =   47
               Top             =   260
               Width           =   1815
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "������������ ������"
            Height          =   615
            Left            =   120
            TabIndex        =   41
            Top             =   120
            Width           =   2775
            Begin VB.OptionButton O2 
               Caption         =   "12 ������"
               Height          =   255
               Left            =   1440
               TabIndex        =   43
               Top             =   240
               Width           =   1095
            End
            Begin VB.OptionButton O1 
               Caption         =   "5 ������"
               Height          =   255
               Left            =   240
               TabIndex        =   42
               Top             =   240
               Value           =   -1  'True
               Width           =   1095
            End
         End
      End
      Begin VB.Frame F6 
         Height          =   4695
         Left            =   120
         TabIndex        =   15
         Top             =   480
         Width           =   6015
         Begin VB.Frame F4 
            Caption         =   "����������� ����"
            Height          =   975
            Left            =   120
            TabIndex        =   33
            Top             =   120
            Width           =   5775
            Begin VB.TextBox txtSumSession 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   3840
               TabIndex        =   36
               Top             =   480
               Width           =   1815
            End
            Begin VB.TextBox txtWinSession 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   1800
               TabIndex        =   35
               Top             =   480
               Width           =   1815
            End
            Begin VB.TextBox NewData 
               Alignment       =   2  'Center
               Height          =   285
               Left            =   120
               TabIndex        =   34
               Top             =   480
               Width           =   1455
            End
            Begin VB.Label �����2 
               Caption         =   "������ ������ ������"
               Height          =   255
               Left            =   3840
               TabIndex        =   39
               Top             =   240
               Width           =   1815
            End
            Begin VB.Label �����1 
               Caption         =   "������ ������ ������"
               Height          =   255
               Left            =   1800
               TabIndex        =   38
               Top             =   240
               Width           =   1935
            End
            Begin VB.Label �����11 
               AutoSize        =   -1  'True
               Caption         =   "�������"
               Height          =   195
               Left            =   120
               TabIndex        =   37
               Top             =   240
               Width           =   630
            End
         End
         Begin VB.Frame F7 
            Caption         =   "������"
            Height          =   1575
            Left            =   120
            TabIndex        =   21
            Top             =   1200
            Width           =   6015
            Begin VB.TextBox txtPassOld 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   3840
               PasswordChar    =   "*"
               TabIndex        =   27
               Top             =   460
               Width           =   1935
            End
            Begin VB.TextBox txtPassNew 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   3840
               PasswordChar    =   "*"
               TabIndex        =   26
               Top             =   760
               Width           =   1935
            End
            Begin VB.TextBox txtPassConfirm 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   3840
               PasswordChar    =   "*"
               TabIndex        =   25
               Top             =   1080
               Width           =   1935
            End
            Begin VB.TextBox txtPassConfirm2 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   1680
               PasswordChar    =   "*"
               TabIndex        =   24
               Top             =   1080
               Width           =   1935
            End
            Begin VB.TextBox txtPassNew2 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   1680
               PasswordChar    =   "*"
               TabIndex        =   23
               Top             =   760
               Width           =   1935
            End
            Begin VB.TextBox txtPassOld2 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   1680
               PasswordChar    =   "*"
               TabIndex        =   22
               Top             =   460
               Width           =   1935
            End
            Begin VB.Label �����8 
               Caption         =   "������� :"
               Height          =   255
               Left            =   120
               TabIndex        =   32
               Top             =   460
               Width           =   1215
            End
            Begin VB.Label �����9 
               Caption         =   "����� :"
               Height          =   255
               Left            =   120
               TabIndex        =   31
               Top             =   760
               Width           =   1215
            End
            Begin VB.Label �����10 
               Caption         =   "������������� :"
               Height          =   255
               Left            =   120
               TabIndex        =   30
               Top             =   1080
               Width           =   1335
            End
            Begin VB.Line Line1 
               X1              =   3720
               X2              =   3720
               Y1              =   200
               Y2              =   1365
            End
            Begin VB.Label Label2 
               Alignment       =   2  'Center
               BackColor       =   &H00808080&
               Caption         =   "�������������� ������"
               ForeColor       =   &H00FFFFFF&
               Height          =   230
               Left            =   3840
               TabIndex        =   28
               Top             =   200
               Width           =   1935
            End
            Begin VB.Label Label1 
               Alignment       =   2  'Center
               BackColor       =   &H00808080&
               Caption         =   "������ ���������"
               ForeColor       =   &H00FFFFFF&
               Height          =   230
               Left            =   1680
               TabIndex        =   29
               Top             =   200
               Width           =   1935
            End
         End
         Begin VB.Frame F8 
            BorderStyle     =   0  'None
            Height          =   1680
            Left            =   120
            TabIndex        =   16
            Top             =   2880
            Width           =   5775
            Begin VB.TextBox txtNewPost 
               Height          =   285
               Left            =   120
               TabIndex        =   62
               Top             =   1320
               Width           =   1815
            End
            Begin VB.ListBox lstPost 
               Height          =   1035
               Left            =   120
               TabIndex        =   61
               Top             =   240
               Width           =   1815
            End
            Begin VB.ListBox lstDeans 
               Height          =   1035
               Left            =   2040
               TabIndex        =   20
               Top             =   240
               Width           =   1815
            End
            Begin VB.TextBox txtNewDeen 
               Height          =   285
               Left            =   2040
               TabIndex        =   19
               Top             =   1320
               Width           =   1815
            End
            Begin VB.ListBox lstInspectors 
               Height          =   1035
               Left            =   3960
               TabIndex        =   18
               Top             =   240
               Width           =   1815
            End
            Begin VB.TextBox txtNewInspector 
               Height          =   285
               Left            =   3960
               TabIndex        =   17
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label Label9 
               AutoSize        =   -1  'True
               Caption         =   "���������"
               Height          =   195
               Left            =   4460
               TabIndex        =   65
               Top             =   30
               Width           =   825
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "�����"
               Height          =   195
               Left            =   2690
               TabIndex        =   64
               Top             =   30
               Width           =   495
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               Caption         =   "���������"
               Height          =   195
               Left            =   600
               TabIndex        =   63
               Top             =   30
               Width           =   870
            End
         End
      End
      Begin VB.Frame F10 
         Height          =   4695
         Left            =   120
         TabIndex        =   12
         Top             =   480
         Width           =   6015
         Begin VB.ListBox lstOptions 
            Height          =   1230
            ItemData        =   "frmOptions.frx":014A
            Left            =   120
            List            =   "frmOptions.frx":014C
            TabIndex        =   14
            Top             =   240
            Width           =   5775
         End
         Begin VSFlex7DAOCtl.VSFlexGrid VSFlexGrid1 
            Height          =   3015
            Left            =   120
            TabIndex        =   13
            Top             =   1560
            Width           =   5775
            _cx             =   10186
            _cy             =   5318
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame F11 
         Height          =   4695
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   6015
         Begin VB.TextBox txtRemoteHost 
            Height          =   285
            Left            =   1800
            TabIndex        =   7
            Top             =   240
            Width           =   4095
         End
         Begin VB.TextBox txtUser 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1800
            TabIndex        =   6
            Top             =   600
            Width           =   1815
         End
         Begin VB.TextBox txtPass 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            IMEMode         =   3  'DISABLE
            Left            =   1800
            PasswordChar    =   "*"
            TabIndex        =   5
            Top             =   960
            Width           =   1815
         End
         Begin VB.TextBox txtCatalog 
            Height          =   285
            Left            =   1800
            TabIndex        =   4
            Top             =   1320
            Width           =   4095
         End
         Begin VB.CommandButton Command1 
            Caption         =   "����������� ����������"
            Height          =   375
            Left            =   120
            TabIndex        =   3
            Top             =   1800
            Width           =   5775
         End
         Begin VB.TextBox txtFTP 
            Height          =   2295
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   2
            Top             =   2280
            Width           =   5775
         End
         Begin VB.Label Label3 
            Caption         =   "FTP ������ :"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label4 
            Caption         =   "��� ������������ :"
            Height          =   255
            Left            =   120
            TabIndex        =   10
            Top             =   600
            Width           =   1575
         End
         Begin VB.Label Label5 
            Caption         =   "������ :"
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   960
            Width           =   1095
         End
         Begin VB.Label Label6 
            Caption         =   "������� :"
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   1320
            Width           =   855
         End
      End
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim BefData As Variant
Dim qwe As Integer

Sub FillDAOptTable(OptNum)
Dim StrCount As Integer
Dim i As Long
Dim j As Long

VSFlexGrid1.Clear

Select Case OptNum
    Case 0: StrCount = 3        '������ �������� �������� ���������
    Case 1: StrCount = 2        '�������� �������
    Case 2: StrCount = 2        '��� (���) ���������
    Case 3: StrCount = 1        '���� ��������
    Case 4: StrCount = 1        '����� ��������
    Case 5: StrCount = 2        '����������� ���������� (�������������)
    Case 6: StrCount = 2        '�������������
    Case 7: StrCount = 3        '������ ��������
    Case 8: StrCount = 3        '������������� �����
    Case 9: StrCount = 3        '���������������� �����
    Case 10: StrCount = 1       '�������������� ��������� �� �����������
    Case 11: StrCount = 5       '��� �������� ���������
    Case 12: StrCount = 6       '������� �����������
    Case 13: StrCount = 5       '����������� ����������
    Case 14: StrCount = 7       '������� ��������
    Case 15: StrCount = 8       '������� ���������
    Case 16: StrCount = 8       '������� ������
    Case 17: StrCount = 3       '������������
    Case 18: StrCount = 1       '���� ����������� (��.��)
    Case 19: StrCount = 1       '���� ������� (��.��)
    Case 20: StrCount = 1       '���� ���������� ������������
    Case 21: StrCount = 1       '�����
    Case 22: StrCount = 1       '���� ������
    
End Select

VSFlexGrid1.Cols = StrCount + 1
VSFlexGrid1.Rows = 21
VSFlexGrid1.FixedRows = 1
VSFlexGrid1.ColWidth(0) = 300
VSFlexGrid1.TextMatrix(0, 0) = "�"

VSFlexGrid1.Cell(flexcpBackColor, 1, 0, 20, StrCount) = 0
VSFlexGrid1.Cell(flexcpForeColor, 1, 0, 20, StrCount) = 0

For i = 1 To StrCount
    VSFlexGrid1.TextMatrix(0, i) = "������ " & i
Next i
For i = 1 To 20
    VSFlexGrid1.TextMatrix(i, 0) = i
Next i

Dim DADataArray As Variant
Dim DACols As Integer
Dim DARows As Long

Dim FArrey As Variant
Dim FCount As Integer

CDB.SQLOpenTableToArrey "SELECT [OPT" & OptNum & "] " & _
                        "FROM [tblDiplomAdditionOpt] WHERE ID >= 1 AND ID <= 21", DADataArray, DACols, DARows
                        
If (DARows <> 21) Then
    MsgBox "��������� ���� � ���� ������! ���������� � ������������."
    Exit Sub
End If

For i = 0 To 19
    If (Not IsNull(DADataArray(0, i))) Then
        GetLinkList CStr(DADataArray(0, i)), FArrey, FCount
        
        For j = 1 To StrCount
            VSFlexGrid1.TextMatrix(i + 1, j) = IIf(FArrey(j - 1) <> " ", FArrey(j - 1), "")
        Next j
    End If
Next i

If (Not IsNull(DADataArray(0, 20))) Then
    VSFlexGrid1.Cell(flexcpBackColor, CLng(DADataArray(0, 20)), 0, CLng(DADataArray(0, 20)), StrCount) = RGB(100, 100, 100)
    VSFlexGrid1.Cell(flexcpForeColor, CLng(DADataArray(0, 20)), 0, CLng(DADataArray(0, 20)), StrCount) = RGB(255, 255, 255)
End If

VSFlexGrid1.AutoSize 1, VSFlexGrid1.Cols - 1
End Sub

Sub FilllstOptions()
lstOptions.AddItem "������ �������� �������� ���������"
lstOptions.AddItem "�������� �������"
lstOptions.AddItem "��� (���) ���������"
lstOptions.AddItem "���� ��������"
lstOptions.AddItem "����� ��������"
lstOptions.AddItem "����������� ���������� (�������������)"
lstOptions.AddItem "�������������"
lstOptions.AddItem "������ ��������"
lstOptions.AddItem "������������� �����"
lstOptions.AddItem "���������������� �����"
lstOptions.AddItem "�������������� ��������� �� �����������"
lstOptions.AddItem "��� �������� ���������"
lstOptions.AddItem "������� �����������"
lstOptions.AddItem "����������� ����������"
lstOptions.AddItem "������� ��������"
lstOptions.AddItem "������� ���������"
lstOptions.AddItem "������� ������"
lstOptions.AddItem "������������"
lstOptions.AddItem "���� ����������� (��.��.)"
lstOptions.AddItem "���� ������� (��.��.)"
lstOptions.AddItem "���� ���������� ������������"
lstOptions.AddItem "�����"
lstOptions.AddItem "���� ������"
End Sub

Sub GetLinkList(FullLinkString As String, ByRef LinkArrey As Variant, ByRef Count As Integer)
Dim i As Integer
Dim LastS As Integer
Dim ArrCounter As Integer

    Count = 0
    LastS = 0
    ArrCounter = 0

    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then Count = Count + 1
    Next i
    
    ReDim LinkArrey(Count)
    
    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then
            LinkArrey(ArrCounter) = Mid(FullLinkString, LastS + 1, i - LastS - 1)
            ArrCounter = ArrCounter + 1
            LastS = i
        End If
    Next i
    
End Sub

Sub LoadData()
Dim DataArray As Variant
Dim DataCols As Integer
Dim DataRows As Long

Dim FArrey As Variant
Dim FCount As Integer

Dim i As Long

CDB.SQLOpenTableToArrey "SELECT [UN], [UNR], [UNF], [FN], [FNR], [WSB], [SSB], [DList], [IList], [FPMS], [RemoteHost], [UserName], [Password], [Catalog], [PList], [EducForm] FROM [tblOptions] WHERE ID = 1", DataArray, DataCols, DataRows

If (Not IsNull(DataArray(9, 0))) Then O2.Value = IIf(DataArray(9, 0) = 0, True, False)

If (Not IsNull(DataArray(15, 0))) Then txtEducForm.Text = CStr(DataArray(15, 0))

If (Not IsNull(DataArray(0, 0))) Then txtUniverName.Text = CStr(DataArray(0, 0))
If (Not IsNull(DataArray(1, 0))) Then txtUniverNameRP.Text = CStr(DataArray(1, 0))

If (Not IsNull(DataArray(2, 0))) Then
    GetLinkList CStr(DataArray(2, 0)), FArrey, FCount
    
    txtUNF1.Text = FArrey(0)
    txtUNF2.Text = FArrey(1)
    txtUNF3.Text = FArrey(2)
    txtUNF4.Text = FArrey(3)
End If

If (Not IsNull(DataArray(3, 0))) Then txtFNI.Text = CStr(DataArray(3, 0))
If (Not IsNull(DataArray(4, 0))) Then txtFNR.Text = CStr(DataArray(4, 0))
If (Not IsNull(DataArray(5, 0))) Then txtWinSession.Text = CDate(DataArray(5, 0))
If (Not IsNull(DataArray(6, 0))) Then txtSumSession.Text = CDate(DataArray(6, 0))

If (Not IsNull(DataArray(14, 0))) Then
    GetLinkList CStr(DataArray(14, 0)), FArrey, FCount
    
    lstPost.Clear
    
    For i = 0 To FCount - 1
        lstPost.AddItem FArrey(i)
    Next i
End If

If (Not IsNull(DataArray(7, 0))) Then
    GetLinkList CStr(DataArray(7, 0)), FArrey, FCount
    
    lstDeans.Clear
    
    For i = 0 To FCount - 1
        lstDeans.AddItem FArrey(i)
    Next i
End If

If (Not IsNull(DataArray(8, 0))) Then
    GetLinkList CStr(DataArray(8, 0)), FArrey, FCount

    lstInspectors.Clear
    
    For i = 0 To FCount - 1
        lstInspectors.AddItem FArrey(i)
    Next i
End If

If (Not IsNull(DataArray(10, 0))) Then txtRemoteHost.Text = CStr(DataArray(10, 0))
If (Not IsNull(DataArray(11, 0))) Then txtUser.Text = CStr(DataArray(11, 0))
If (Not IsNull(DataArray(12, 0))) Then txtPass.Text = CStr(DataArray(12, 0))
If (Not IsNull(DataArray(13, 0))) Then txtCatalog.Text = CStr(DataArray(13, 0))
End Sub

Sub CreateToolBars()
OptionsTab.AddTab "����� ��������"
OptionsTab.AddTab "��������"
OptionsTab.AddTab "���������� � �������"
OptionsTab.AddTab "������ � FTP"
End Sub




Private Sub cmdCancel_Click()
Main.FTPConnection.Disconnect
Unload Me
End Sub



Private Sub cmdOK_Click()
Dim DataArray As Variant
Dim DataCols As Integer
Dim DataRows As Long

Dim i As Long

Dim UN As String
Dim UNR As String

Dim UNF1 As String
Dim UNF2 As String
Dim UNF3 As String
Dim UNF4 As String
Dim UNF As String

Dim FN As String
Dim FNR As String
Dim WSB As String
Dim SSB As String
Dim EPWD As String
Dim EPWD2 As String
Dim DList As String
Dim IList As String
Dim PList As String
Dim EducForm As String

Dim RemoteHost As String
Dim User As String
Dim Pass As String
Dim Catalog As String

Dim cCrypter As New clsCrypter

If (DOWSDemoMode) Then
    MsgBox "������ ���������������� �� �������� � DEMO-������ ���������!"
    Exit Sub
End If

If (Dir(App.Path & "\BackB.bmp") = "") Then
    MsgBox "����������� ����� ���������� ��� �����������!"
    Exit Sub
End If

If (FivePointMarkSystem And O2.Value) Then
    If (MsgBox("�� �������� ������� ���������� � 5-������� �� 12-�������, ��� ������ ����� �������� ��������������� �������. ���������� ?", vbYesNo, "DOWS") = vbNo) Then Exit Sub
End If
If ((Not FivePointMarkSystem) And O1.Value) Then
    If (MsgBox("�� �������� ������� ���������� � 12-������� �� 5-�������, ��� ������ ����� �������� ��������������� �������. ���������� ?", vbYesNo, "DOWS") = vbNo) Then Exit Sub
End If

EducForm = txtEducForm.Text
If (FindErrors(EducForm, 255, "����� ��������", False, True)) Then Exit Sub
CheckString EducForm

UN = txtUniverName.Text
If (FindErrors(UN, 255, "�������� �������� ��������� (��� ����������)", False, True)) Then Exit Sub
CheckString UN

UNR = txtUniverNameRP.Text
If (FindErrors(UNR, 255, "�������� �������� ��������� (��� ������� (� ����������� ������))", False, True)) Then Exit Sub
CheckString UNR

UNF1 = IIf(txtUNF1.Text <> "", txtUNF1.Text, " ")
If (FindErrors(UNF1, 255, "�������� �������� ��������� (��� ��������� ������� (1 ����))", False, False)) Then Exit Sub
CheckString UNF1

UNF2 = IIf(txtUNF2.Text <> "", txtUNF2.Text, " ")
If (FindErrors(UNF2, 255, "�������� �������� ��������� (��� ��������� ������� (2 ����))", False, False)) Then Exit Sub
CheckString UNF2

UNF3 = IIf(txtUNF3.Text <> "", txtUNF3.Text, " ")
If (FindErrors(UNF3, 255, "�������� �������� ��������� (��� ��������� ������� (3 ����))", False, False)) Then Exit Sub
CheckString UNF3

UNF4 = IIf(txtUNF4.Text <> "", txtUNF4.Text, " ")
If (FindErrors(UNF4, 255, "�������� �������� ��������� (��� ��������� ������� (4 ����))", False, False)) Then Exit Sub
CheckString UNF4

UNF = UNF1 & "|" & UNF2 & "|" & UNF3 & "|" & UNF4 & "|"
If (FindErrors(UNF, 255, "�������� �������� ��������� (��� ��������� �������)", False, True)) Then Exit Sub

FN = txtFNI.Text
If (FindErrors(FN, 255, "�������� ���������� (������������ �����)", False, True)) Then Exit Sub
CheckString FN

FNR = txtFNR.Text
If (FindErrors(FNR, 255, "�������� ���������� (����������� �����)", False, True)) Then Exit Sub
CheckString FNR

RemoteHost = txtRemoteHost.Text
If (FindErrors(RemoteHost, 255, "FTP ������", False, True)) Then Exit Sub
CheckString RemoteHost

User = txtUser.Text
If (FindErrors(User, 255, "��� ������������", False, True)) Then Exit Sub
CheckString User

Pass = txtPass.Text
If (FindErrors(Pass, 255, "������", False, True)) Then Exit Sub
CheckString Pass

Catalog = txtCatalog.Text
If (FindErrors(Catalog, 255, "�������", False, True)) Then Exit Sub
CheckString Catalog

If (Not IsDate(txtWinSession.Text)) Then
    MsgBox "���� '������ ������ ������' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If
WSB = txtWinSession.Text

If (Not IsDate(txtSumSession.Text)) Then
    MsgBox "���� '������ ������ ������' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If
SSB = txtSumSession.Text


EPWD = ""
If (txtPassNew.Text <> "" Or txtPassOld.Text <> "") Then
    CDB.SQLOpenTableToArrey "SELECT [EPWD] FROM [tblOptions] WHERE ID = 1", DataArray, DataCols, DataRows
    
    If (IsNull(DataArray(0, 0))) Then DataArray(0, 0) = ""
    If (txtPassOld.Text <> CStr(DataArray(0, 0))) Then
        MsgBox "��������� ������������ ����� ������� ������ !", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (txtPassNew.Text <> txtPassConfirm.Text) Then
        MsgBox "����� ������ �� ����������� !", vbOKOnly, "������"
        Exit Sub
    End If
    
    EPWD = txtPassNew.Text
End If


Dim Buffer As String
cCrypter.Path = App.Path & "\BackB.bmp"
cCrypter.EncryptString = True
cCrypter.Password = "H&13$|Si|k"

EPWD2 = ""
If (txtPassNew2.Text <> "" Or txtPassOld2.Text <> "") Then
    Buffer = cCrypter.GetData(2)
    If (("LOP_" & txtPassOld2.Text) <> cCrypter.GetData(2)) Then
        MsgBox "��������� ������������ ����� ������� ������ !", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (txtPassNew2.Text <> txtPassConfirm2.Text) Then
        MsgBox "����� ������ �� ����������� !", vbOKOnly, "������"
        Exit Sub
    End If
    
    EPWD2 = txtPassNew2.Text
End If


PList = ""
For i = 0 To lstPost.ListCount - 1
    PList = PList & lstPost.List(i) & "|"
Next i
If (FindErrors(PList, 255, "������ ����������", False, True)) Then Exit Sub
CheckString PList

DList = ""
For i = 0 To lstDeans.ListCount - 1
    DList = DList & lstDeans.List(i) & "|"
Next i
If (FindErrors(DList, 255, "������ �������", False, True)) Then Exit Sub
CheckString DList

IList = ""
For i = 0 To lstInspectors.ListCount - 1
    IList = IList & lstInspectors.List(i) & "|"
Next i
If (FindErrors(IList, 255, "������ �����������", False, True)) Then Exit Sub
CheckString IList


If (txtPassNew2.Text <> "" Or txtPassOld2.Text <> "") Then
    cCrypter.WriteData "LOP_" & EPWD2, 2
    cCrypter.Save
    
    LoadPassword = EPWD2
End If

Dim AR As Long

If (FivePointMarkSystem And O2.Value) Then
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 11 WHERE [Mark] = 5", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 8 WHERE [Mark] = 4", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 5 WHERE [Mark] = 3", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblOptions] SET [FPMS] = 0", 128)
End If
If ((Not FivePointMarkSystem) And O1.Value) Then
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 2 WHERE [Mark] = 3", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 3 WHERE [Mark] = 4 OR [Mark] = 5 OR [Mark] = 6", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 4 WHERE [Mark] = 7 OR [Mark] = 8 OR [Mark] = 9", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblSheets] SET [Mark] = 5 WHERE [Mark] =10 OR [Mark] =11 OR [Mark] = 12", 128)
    AR = CDB.ExecSQLActionQuery("UPDATE [tblOptions] SET [FPMS] = 1", 128)
End If

If (txtPassNew.Text <> "" Or txtPassOld.Text <> "") Then
    AR = CDB.ExecSQLActionQuery("UPDATE [tblOptions] SET [EducForm] = '" & EducForm & "', [UN] = '" & UN & "', [UNR] = '" & UNR & "', [UNF] = '" & UNF & "', [FN] = '" & FN & "', [FNR] = '" & FNR & "', [WSB] = '" & WSB & "', [SSB] = '" & SSB & "', [RemoteHost] = '" & RemoteHost & "', [UserName] = '" & User & "', [Password] = '" & Pass & "', [Catalog] = '" & Catalog & "', [EPWD] = '" & EPWD & "', [PList] = '" & PList & "', [DList] = '" & DList & "', [IList] = '" & IList & "' WHERE ID = 1", 128)
Else
    AR = CDB.ExecSQLActionQuery("UPDATE [tblOptions] SET [EducForm] = '" & EducForm & "', [UN] = '" & UN & "', [UNR] = '" & UNR & "', [UNF] = '" & UNF & "', [FN] = '" & FN & "', [FNR] = '" & FNR & "', [WSB] = '" & WSB & "', [SSB] = '" & SSB & "', [RemoteHost] = '" & RemoteHost & "', [UserName] = '" & User & "', [Password] = '" & Pass & "', [Catalog] = '" & Catalog & "', [PList] = '" & PList & "', [DList] = '" & DList & "', [IList] = '" & IList & "' WHERE ID = 1", 128)
End If

GlobalData = DateSerial(Year(NewData.Text), Month(NewData.Text), Day(NewData.Text))
Main.sbMain.PanelText("6") = Format$(GlobalData, "dd.mm.yyyy")

Main.FillDateVariables

If (O2.Value) Then
    FivePointMarkSystem = False
    Main.sbMain.PanelText(1) = "12"
Else
    FivePointMarkSystem = True
    Main.sbMain.PanelText(1) = "5"
End If
Main.FTPConnection.Disconnect
Unload Me
End Sub



Private Sub Command1_Click()
On Error GoTo ER1

Main.CurrentFTPAction = "CONNECTIONTEST"
txtFTP.Text = ""
Main.FTPConnection.Disconnect
Main.FTPConnection.Connect txtRemoteHost.Text, 21, txtUser.Text, txtPass.Text

Exit Sub
ER1:
MsgBox Error(Err)
End Sub

Private Sub Form_Load()
CreateToolBars
FilllstOptions

LoadData

F5.ZOrder

NewData.Text = DateSerial(Year(Date), Month(Date), Day(Date))

txtWinSession = Format$(txtWinSession, "dd.mm")
txtSumSession = Format$(txtSumSession, "dd.mm")

LoadSuccess = True
End Sub



Private Sub List1_Click()

End Sub

Private Sub List1_KeyDown(KeyCode As Integer, Shift As Integer)
End Sub

Private Sub lstDeans_DblClick()
Dim newtext As String
newtext = InputBox("��������", "��������", lstDeans.List(lstDeans.ListIndex))
If (newtext <> "") Then lstDeans.List(lstDeans.ListIndex) = newtext
End Sub

Private Sub lstDeans_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 46) Then
    If (lstDeans.ListCount = 0) Then Exit Sub
    If (lstDeans.ListIndex < 0) Then Exit Sub
    
    lstDeans.RemoveItem (lstDeans.ListIndex)
ElseIf (KeyCode = 33) Then
    If (lstDeans.ListCount = 0) Then Exit Sub
    If (lstDeans.ListIndex < 0) Then Exit Sub
    lstDeans.AddItem (lstDeans.List(0))
    lstDeans.List(0) = (lstDeans.List(lstDeans.ListIndex))
    lstDeans.RemoveItem (lstDeans.ListIndex)
End If
End Sub


Private Sub lstInspectors_DblClick()
Dim newtext As String
newtext = InputBox("��������", "��������", lstInspectors.List(lstInspectors.ListIndex))
If (newtext <> "") Then lstInspectors.List(lstInspectors.ListIndex) = newtext
End Sub

Private Sub lstInspectors_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 46) Then
    If (lstInspectors.ListCount = 0) Then Exit Sub
    If (lstInspectors.ListIndex < 0) Then Exit Sub
    
    lstInspectors.RemoveItem (lstInspectors.ListIndex)
ElseIf (KeyCode = 33) Then
    If (lstInspectors.ListCount = 0) Then Exit Sub
    If (lstInspectors.ListIndex < 0) Then Exit Sub
    lstInspectors.AddItem (lstInspectors.List(0))
    lstInspectors.List(0) = (lstInspectors.List(lstInspectors.ListIndex))
    lstInspectors.RemoveItem (lstInspectors.ListIndex)
End If
End Sub

Private Sub lstOptions_Click()
FillDAOptTable lstOptions.ListIndex
End Sub


Private Sub lstPost_DblClick()
Dim newtext As String
newtext = InputBox("��������", "��������", lstPost.List(lstPost.ListIndex))
If (newtext <> "") Then lstPost.List(lstPost.ListIndex) = newtext
End Sub

Private Sub lstPost_KeyDown(KeyCode As Integer, Shift As Integer)
If (KeyCode = 46) Then
    If (lstPost.ListCount = 0) Then Exit Sub
    If (lstPost.ListIndex < 0) Then Exit Sub
    
    lstPost.RemoveItem (lstPost.ListIndex)
ElseIf (KeyCode = 33) Then
    If (lstPost.ListCount = 0) Then Exit Sub
    If (lstPost.ListIndex < 0) Then Exit Sub
    lstPost.AddItem (lstPost.List(0))
    lstPost.List(0) = (lstPost.List(lstPost.ListIndex))
    lstPost.RemoveItem (lstPost.ListIndex)
End If
End Sub

Private Sub OptionsTab_TabClick(ByVal lTab As Long)
Select Case lTab
 Case 1     '����� ��������
    F5.ZOrder
    txtUniverName.SetFocus
    
 Case 2     '��������
    F6.ZOrder
    NewData.SetFocus
    
 Case 3     '���������� � �������
    F10.ZOrder
    
 Case 4     '������ � FTP
    F11.ZOrder
    
End Select

End Sub

Private Sub txtNewDeen_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If (txtNewDeen.Text = "") Then Exit Sub
    
    lstDeans.AddItem txtNewDeen.Text
    txtNewDeen.Text = ""
End If
End Sub

Private Sub txtNewInspector_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If (txtNewInspector.Text = "") Then Exit Sub
    
    lstInspectors.AddItem txtNewInspector.Text
    txtNewInspector.Text = ""
End If
End Sub

Private Sub txtNewPost_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If (txtNewPost.Text = "") Then Exit Sub
    
    lstPost.AddItem txtNewPost.Text
    txtNewPost.Text = ""
End If
End Sub

Private Sub VSFlexGrid1_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim OldStrData As String
Dim NewStrData As String
Dim FinStrData As String
Dim AR As Long
Dim i As Long

If (lstOptions.ListIndex < 0) Then Exit Sub
If (VSFlexGrid1.Rows < 2) Then Exit Sub

NewStrData = IIf(VSFlexGrid1.TextMatrix(Row, Col) <> "", VSFlexGrid1.TextMatrix(Row, Col), " ")
If (FindErrors(NewStrData, 255, lstOptions.List(lstOptions.ListIndex) & "(������ " & Col & ")", False, False)) Then
    VSFlexGrid1.TextMatrix(Row, Col) = BefData
    Exit Sub
End If

CheckString NewStrData

For i = 1 To Col - 1
    OldStrData = IIf(VSFlexGrid1.TextMatrix(Row, i) <> "", VSFlexGrid1.TextMatrix(Row, i), " ")
    CheckString OldStrData
    
    FinStrData = FinStrData & OldStrData & "|"
Next i

FinStrData = FinStrData & NewStrData & "|"

For i = Col + 1 To VSFlexGrid1.Cols - 1
    OldStrData = IIf(VSFlexGrid1.TextMatrix(Row, i) <> "", VSFlexGrid1.TextMatrix(Row, i), " ")
    CheckString OldStrData
    
    FinStrData = FinStrData & OldStrData & "|"
Next i

If (FindErrors(FinStrData, 255, lstOptions.List(lstOptions.ListIndex), False, True)) Then
    VSFlexGrid1.TextMatrix(Row, Col) = BefData
    Exit Sub
End If

AR = CDB.ExecSQLActionQuery("UPDATE [tblDiplomAdditionOpt] SET [OPT" & lstOptions.ListIndex & "] = '" & FinStrData & "' WHERE [ID] = " & Row, 128)

VSFlexGrid1.AutoSize 1, VSFlexGrid1.Cols - 1
End Sub

Private Sub VSFlexGrid1_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefData = VSFlexGrid1.TextMatrix(Row, Col)
End Sub

Private Sub VSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
Dim AR As Long

If (lstOptions.ListIndex < 0) Then Exit Sub
If (VSFlexGrid1.Rows < 2) Then Exit Sub

If (Button = 2) Then
    VSFlexGrid1.Cell(flexcpBackColor, 1, 0, 20, VSFlexGrid1.Cols - 1) = 0
    VSFlexGrid1.Cell(flexcpForeColor, 1, 0, 20, VSFlexGrid1.Cols - 1) = 0

    VSFlexGrid1.Cell(flexcpBackColor, VSFlexGrid1.MouseRow, 0, VSFlexGrid1.MouseRow, VSFlexGrid1.Cols - 1) = RGB(100, 100, 100)
    VSFlexGrid1.Cell(flexcpForeColor, VSFlexGrid1.MouseRow, 0, VSFlexGrid1.MouseRow, VSFlexGrid1.Cols - 1) = RGB(255, 255, 255)
    
    AR = CDB.ExecSQLActionQuery("UPDATE [tblDiplomAdditionOpt] SET [OPT" & lstOptions.ListIndex & "] = '" & VSFlexGrid1.MouseRow & "' WHERE [ID] = 21", 128)
End If
End Sub

Private Sub VSFlexGrid1_RowColChange()
If (lstOptions.ListIndex < 0) Then Exit Sub
If (VSFlexGrid1.Rows < 2) Then Exit Sub

If (VSFlexGrid1.Col = 0) Then
    VSFlexGrid1.Editable = flexEDNone
Else
    VSFlexGrid1.Editable = flexEDKbdMouse
End If
End Sub
