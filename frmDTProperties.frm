VERSION 5.00
Begin VB.Form frmDTProperties 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "��������"
   ClientHeight    =   3375
   ClientLeft      =   4290
   ClientTop       =   3630
   ClientWidth     =   5415
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3375
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F11 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
      Begin VB.CheckBox O2 
         Caption         =   "��������� ������"
         Height          =   255
         Left            =   1560
         TabIndex        =   10
         Top             =   240
         Width           =   2415
      End
      Begin VB.CheckBox O1 
         Caption         =   "���. �������"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton O3 
         Caption         =   "�����"
         Height          =   255
         Left            =   4440
         TabIndex        =   8
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtTheme 
         Enabled         =   0   'False
         Height          =   285
         Left            =   840
         TabIndex        =   5
         Top             =   600
         Width           =   4455
      End
      Begin VB.TextBox txtFIO 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   1200
         Width           =   5175
      End
      Begin VB.ListBox lstFIO 
         Enabled         =   0   'False
         Height          =   1230
         Left            =   120
         TabIndex        =   3
         Top             =   1560
         Width           =   5175
      End
      Begin VB.CommandButton cmdCancelDT 
         Caption         =   "������"
         Height          =   375
         Left            =   3840
         TabIndex        =   2
         Top             =   2880
         Width           =   1455
      End
      Begin VB.CommandButton cmdOKDT 
         Caption         =   "OK"
         Height          =   375
         Left            =   2400
         TabIndex        =   1
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Label �����26 
         Caption         =   "���� :"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   600
         Width           =   615
      End
      Begin VB.Label �����27 
         Caption         =   "������������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmDTProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim SelectedLecturer As Long
Dim FirstLecturer As Long
Dim SelectedLecturerName As String

Private Sub cmdCancelDT_Click()
Unload Me
End Sub

Private Sub cmdOKDT_Click()
    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    Dim DTheme As String

    Select Case ChosenQuery
        Case 8:
            CDB.SQLOpenTableToArrey "SELECT tblDegreeThemes.ID  FROM [tblDegreeThemes], [tblLecturers] WHERE tblLecturers.ID = tblDegreeThemes.IDLecturer AND tblDegreeThemes.IDStudent = " & Main.MainGrid.RowData(Main.MainGrid.Row), DopArrey, DopCols, DopRows
        
            If (O1.Value = 1 And O2.Value = 0) Then
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "���. �������"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = ""
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = ""
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 5) = "2"
                
                If (DopRows > 0) Then
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblDegreeThemes] SET [Mark] = 2, [WorkType] = 0, [IDLecturer] = " & FirstLecturer & ", [NameOfWork] = '' WHERE [ID] = " & DopArrey(0, 0), 128)
                Else
                    AR = CDB.ExecSQLActionQuery("INSERT INTO tblDegreeThemes(IDLecturer, IDStudent, NameOfWork, WorkType, Mark) VALUES (" & FirstLecturer & ", " & Main.MainGrid.RowData(Main.MainGrid.Row) & ", '', 0, 2)", 128)
                End If
                
            ElseIf (O1.Value = 0 And O2.Value = 1) Then
                If (SelectedLecturer = 0) Then
                    MsgBox "�������� ������������", vbOKOnly, "������"
                    Exit Sub
                End If
                
                DTheme = txtTheme.Text
                If (FindErrors(DTheme, 200, "����", True, True)) Then Exit Sub
                CheckString DTheme
                
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "������������ ������"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = DTheme
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = SelectedLecturerName
                
                If (DopRows > 0) Then
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblDegreeThemes] SET [WorkType] = 1, [IDLecturer] = " & SelectedLecturer & ", [NameOfWork] = '" & DTheme & "' WHERE [ID] = " & DopArrey(0, 0), 128)
                Else
                    AR = CDB.ExecSQLActionQuery("INSERT INTO tblDegreeThemes(IDLecturer, IDStudent, NameOfWork, WorkType, Mark) VALUES (" & SelectedLecturer & ", " & Main.MainGrid.RowData(Main.MainGrid.Row) & ", '" & DTheme & "', 1, 2)", 128)
                End If
                
            ElseIf (O1.Value = 1 And O2.Value = 1) Then
                If (SelectedLecturer = 0) Then
                    MsgBox "�������� ������������", vbOKOnly, "������"
                    Exit Sub
                End If
                
                DTheme = txtTheme.Text
                If (FindErrors(DTheme, 200, "����", True, True)) Then Exit Sub
                CheckString DTheme
                
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "��� + ��"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = DTheme
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = SelectedLecturerName
                
                If (DopRows > 0) Then
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblDegreeThemes] SET [WorkType] = 2, [IDLecturer] = " & SelectedLecturer & ", [NameOfWork] = '" & DTheme & "' WHERE [ID] = " & DopArrey(0, 0), 128)
                Else
                    AR = CDB.ExecSQLActionQuery("INSERT INTO tblDegreeThemes(IDLecturer, IDStudent, NameOfWork, WorkType, Mark) VALUES (" & SelectedLecturer & ", " & Main.MainGrid.RowData(Main.MainGrid.Row) & ", '" & DTheme & "', 2, 2)", 128)
                End If
                 
            ElseIf (O3.Value) Then
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "- ����� -"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = ""
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = ""
                
                AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes] WHERE [IDStudent] = " & Main.MainGrid.RowData(Main.MainGrid.Row), 128)
                
            End If
    
        Case 88:
            CDB.SQLOpenTableToArrey "SELECT tblDegreeThemes2.ID  FROM [tblDegreeThemes2], [tblLecturers] WHERE tblLecturers.ID = tblDegreeThemes2.IDLecturer AND tblDegreeThemes2.IDStudent = " & Main.MainGrid.RowData(Main.MainGrid.Row), DopArrey, DopCols, DopRows
        
            If (O1.Value = 1 And O2.Value = 0) Then
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "���. �������"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = ""
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = ""
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 5) = "2"
                
                If (DopRows > 0) Then
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblDegreeThemes2] SET [Mark] = 2, [WorkType] = 0, [IDLecturer] = " & FirstLecturer & ", [NameOfWork] = '' WHERE [ID] = " & DopArrey(0, 0), 128)
                Else
                    AR = CDB.ExecSQLActionQuery("INSERT INTO tblDegreeThemes2(IDLecturer, IDStudent, NameOfWork, WorkType, Mark) VALUES (" & FirstLecturer & ", " & Main.MainGrid.RowData(Main.MainGrid.Row) & ", '', 0, 2)", 128)
                End If
                
            ElseIf (O1.Value = 0 And O2.Value = 1) Then
                If (SelectedLecturer = 0) Then
                    MsgBox "�������� ������������", vbOKOnly, "������"
                    Exit Sub
                End If
                
                DTheme = txtTheme.Text
                If (FindErrors(DTheme, 200, "����", True, True)) Then Exit Sub
                CheckString DTheme
                
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "��������� ������"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = DTheme
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = SelectedLecturerName
                
                If (DopRows > 0) Then
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblDegreeThemes2] SET [WorkType] = 1, [IDLecturer] = " & SelectedLecturer & ", [NameOfWork] = '" & DTheme & "' WHERE [ID] = " & DopArrey(0, 0), 128)
                Else
                    AR = CDB.ExecSQLActionQuery("INSERT INTO tblDegreeThemes2(IDLecturer, IDStudent, NameOfWork, WorkType, Mark) VALUES (" & SelectedLecturer & ", " & Main.MainGrid.RowData(Main.MainGrid.Row) & ", '" & DTheme & "', 1, 2)", 128)
                End If
                
            ElseIf (O1.Value = 1 And O2.Value = 1) Then
                If (SelectedLecturer = 0) Then
                    MsgBox "�������� ������������", vbOKOnly, "������"
                    Exit Sub
                End If
                
                DTheme = txtTheme.Text
                If (FindErrors(DTheme, 200, "����", True, True)) Then Exit Sub
                CheckString DTheme
                
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "��� + ��"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = DTheme
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = SelectedLecturerName
                
                If (DopRows > 0) Then
                    AR = CDB.ExecSQLActionQuery("UPDATE [tblDegreeThemes2] SET [WorkType] = 2, [IDLecturer] = " & SelectedLecturer & ", [NameOfWork] = '" & DTheme & "' WHERE [ID] = " & DopArrey(0, 0), 128)
                Else
                    AR = CDB.ExecSQLActionQuery("INSERT INTO tblDegreeThemes2(IDLecturer, IDStudent, NameOfWork, WorkType, Mark) VALUES (" & SelectedLecturer & ", " & Main.MainGrid.RowData(Main.MainGrid.Row) & ", '" & DTheme & "', 2, 2)", 128)
                End If
                 
            ElseIf (O3.Value) Then
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 2) = "- ����� -"
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 3) = ""
                Main.MainGrid.TextMatrix(Main.MainGrid.Row, 4) = ""
                
                AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes2] WHERE [IDStudent] = " & Main.MainGrid.RowData(Main.MainGrid.Row), 128)
                
            End If
            
    End Select
    
    Main.MainGrid.AutoSize 0, Main.MainGrid.Cols - 1
    
Unload Me
End Sub

Private Sub Form_Load()
    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    SelectedLecturer = 0
    FirstLecturer = 0
    SelectedLecturerName = ""
    
    Select Case ChosenQuery
    
        Case 8:
            O2.Caption = "������������ ������"
            CDB.SQLOpenTableToArrey "SELECT NameOfWork, WorkType, Surname, Name, Patronymic  FROM [tblDegreeThemes], [tblLecturers] WHERE tblLecturers.ID = tblDegreeThemes.IDLecturer AND tblDegreeThemes.IDStudent = " & Main.MainGrid.RowData(Main.MainGrid.Row), DopArrey, DopCols, DopRows
            
            If (DopRows > 0) Then
                If (DopArrey(1, 0) = 0) Then
                    O1.Value = 1
                    O1_Click
                    
                ElseIf (DopArrey(1, 0) = 1) Then
                    txtTheme.Text = CStr(DopArrey(0, 0))
                    
                    txtFIO.Text = CStr(DopArrey(2, 0)) & " " & CStr(DopArrey(3, 0)) & " " & CStr(DopArrey(4, 0))
                    txtFIO_Change
                    
                    O2.Value = 1
                    O2_Click
                    
                ElseIf (DopArrey(1, 0) = 2) Then
                
                    txtTheme.Text = CStr(DopArrey(0, 0))
                    
                    txtFIO.Text = CStr(DopArrey(2, 0)) & " " & CStr(DopArrey(3, 0)) & " " & CStr(DopArrey(4, 0))
                    txtFIO_Change
                    
                    O1.Value = 1
                    O2.Value = 1
                    O2_Click
                    
                End If
                
            Else
                O3.Value = True
                O3_Click
            End If
            
            
        Case 88:
            O2.Caption = "��������� ������"
            CDB.SQLOpenTableToArrey "SELECT NameOfWork, WorkType, Surname, Name, Patronymic  FROM [tblDegreeThemes2], [tblLecturers] WHERE tblLecturers.ID = tblDegreeThemes2.IDLecturer AND tblDegreeThemes2.IDStudent = " & Main.MainGrid.RowData(Main.MainGrid.Row), DopArrey, DopCols, DopRows
            
            If (DopRows > 0) Then
                If (DopArrey(1, 0) = 0) Then
                    O1.Value = 1
                    O1_Click
                    
                ElseIf (DopArrey(1, 0) = 1) Then
                    txtTheme.Text = CStr(DopArrey(0, 0))
                    
                    txtFIO.Text = CStr(DopArrey(2, 0)) & " " & CStr(DopArrey(3, 0)) & " " & CStr(DopArrey(4, 0))
                    txtFIO_Change
                    
                    O2.Value = 1
                    O2_Click
                    
                ElseIf (DopArrey(1, 0) = 2) Then
                
                    txtTheme.Text = CStr(DopArrey(0, 0))
                    
                    txtFIO.Text = CStr(DopArrey(2, 0)) & " " & CStr(DopArrey(3, 0)) & " " & CStr(DopArrey(4, 0))
                    txtFIO_Change
                    
                    O1.Value = 1
                    O2.Value = 1
                    O2_Click
                    
                End If
                
            Else
                O3.Value = True
                O3_Click
            End If
            
    End Select
    
    
    CDB.SQLOpenTableToArrey "SELECT TOP 1 ID FROM [tblLecturers]", DopArrey, DopCols, DopRows
    
    If (DopRows > 0) Then
        FirstLecturer = DopArrey(0, 0)
    Else
        MsgBox "��������������� ������� ������ �������������.", vbOKOnly, "������"
        Exit Sub
    End If
    
    LoadSuccess = True
End Sub

Private Sub lstFIO_Click()
If (lstFIO.ListIndex = -1) Then Exit Sub
SelectedLecturer = lstFIO.ItemData(lstFIO.ListIndex)
SelectedLecturerName = lstFIO.List(lstFIO.ListIndex)

End Sub

Private Sub O1_Click()
If (O2.Value = 0 And O1.Value = 0) Then
    O3.Value = True
    O3_Click
    Exit Sub
Else
    O3.Value = False
End If
    
If (O2.Value = 1) Then
    txtTheme.Enabled = True
    txtFIO.Enabled = True
    lstFIO.Enabled = True
    
Else
    txtTheme.Enabled = False
    txtFIO.Enabled = False
    lstFIO.Enabled = False
    
End If
End Sub

Private Sub O2_Click()
If (O2.Value = 0 And O1.Value = 0) Then
    O3.Value = True
    O3_Click
    Exit Sub
Else
    O3.Value = False
End If
    
If (O2.Value = 1) Then
    txtTheme.Enabled = True
    txtFIO.Enabled = True
    lstFIO.Enabled = True
    
Else
    txtTheme.Enabled = False
    txtFIO.Enabled = False
    lstFIO.Enabled = False
    
End If
End Sub

Private Sub O3_Click()
    O1.Value = 0
    O2.Value = 0
    
    txtTheme.Enabled = False
    txtFIO.Enabled = False
    lstFIO.Enabled = False
End Sub

Private Sub txtFIO_Change()
Dim LecturerName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim LecturersArrey As Variant
Dim LCols As Integer
Dim LRows As Long

Dim i As Long

LecturerName = txtFIO.Text

SelectedLecturer = 0
SelectedLecturerName = ""

If (LecturerName = "") Then
    lstFIO.Clear
    Exit Sub
End If

'If (InStr(1, LecturerName, "'") <> 0) Then Exit Sub

Dim FirstS As Byte
Dim SecondS As Byte

FirstS = InStr(1, LecturerName, " ")
If (FirstS) Then SecondS = InStr(InStr(1, LecturerName, " ") + 1, LecturerName, " ")

If ((FirstS = 0) And (SecondS = 0)) Then
    Surname = LecturerName
    
    CheckString Surname
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] LIKE '" & Surname & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
ElseIf ((FirstS <> 0) And (SecondS = 0)) Then
    Surname = Left(LecturerName, FirstS - 1)
    Name = Mid(LecturerName, FirstS + 1, Len(LecturerName) - FirstS)
    
    CheckString Surname
    CheckString Name
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
ElseIf ((FirstS <> 0) And (SecondS <> 0)) Then
    Surname = Left(LecturerName, FirstS - 1)
    Name = Mid(LecturerName, FirstS + 1, SecondS - FirstS - 1)
    Patronymic = Right(LecturerName, Len(LecturerName) - SecondS)
    
    CheckString Surname
    CheckString Name
    CheckString Patronymic
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
End If

lstFIO.Clear
If (LRows > 0) Then

    For i = 0 To LRows - 1
        lstFIO.AddItem GetFullName(CStr(LecturersArrey(1, i)), CStr(LecturersArrey(2, i)), CStr(LecturersArrey(3, i)), False, False, True)
        lstFIO.ItemData(lstFIO.NewIndex) = LecturersArrey(0, i)
    Next i
    
    lstFIO.ListIndex = 0
    
Else
End If

End Sub
