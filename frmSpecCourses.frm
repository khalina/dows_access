VERSION 5.00
Begin VB.Form frmSpecCourses 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "���������� ���������"
   ClientHeight    =   6375
   ClientLeft      =   5220
   ClientTop       =   1545
   ClientWidth     =   4455
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6375
   ScaleWidth      =   4455
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   6375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4455
      Begin VB.OptionButton optNoMark 
         Caption         =   "��� ������"
         Height          =   255
         Left            =   2640
         TabIndex        =   16
         Top             =   5400
         Width           =   1335
      End
      Begin VB.OptionButton OptOffset 
         Caption         =   "�����"
         Height          =   255
         Left            =   2640
         TabIndex        =   15
         Top             =   5160
         Width           =   735
      End
      Begin VB.OptionButton OptExam 
         Caption         =   "�������"
         Height          =   255
         Left            =   2640
         TabIndex        =   14
         Top             =   4920
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "������"
         Height          =   375
         Left            =   3000
         TabIndex        =   13
         Top             =   5880
         Width           =   1335
      End
      Begin VB.CommandButton cmdAddExam 
         Caption         =   "��������"
         Height          =   375
         Left            =   1800
         TabIndex        =   12
         Top             =   5880
         Width           =   1215
      End
      Begin VB.TextBox txtHours 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1680
         TabIndex        =   9
         Top             =   4800
         Width           =   615
      End
      Begin VB.TextBox txtSemester 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1680
         TabIndex        =   8
         Top             =   5200
         Width           =   615
      End
      Begin VB.CheckBox NewSubject 
         Caption         =   "�����"
         Height          =   255
         Left            =   3480
         TabIndex        =   7
         Top             =   480
         Width           =   855
      End
      Begin VB.ListBox lstFIO 
         Height          =   1425
         Left            =   120
         TabIndex        =   5
         Top             =   3120
         Width           =   4215
      End
      Begin VB.TextBox txtFIO 
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   2760
         Width           =   4215
      End
      Begin VB.ListBox SubjectList 
         Height          =   1425
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   4215
      End
      Begin VB.TextBox txtSubjectName 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   3255
      End
      Begin VB.Label LF51 
         AutoSize        =   -1  'True
         Caption         =   "��� ���������� :"
         Height          =   195
         Left            =   2640
         TabIndex        =   17
         Top             =   4680
         Width           =   1260
      End
      Begin VB.Label �����5 
         AutoSize        =   -1  'True
         Caption         =   "�������� ����� :"
         Height          =   195
         Left            =   330
         TabIndex        =   11
         Top             =   4800
         Width           =   1305
      End
      Begin VB.Label LF53 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   885
         TabIndex        =   10
         Top             =   5205
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "�������� �������������:"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   2520
         Width           =   1995
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "�������� ����������:"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1725
      End
   End
End
Attribute VB_Name = "frmSpecCourses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CreateNewSubject As Boolean
Private SelectedSubject As Long
Private SelectedLecturer As Long


Private Sub cmdAddExam_Click()
    Dim SemNum As Long
    Dim KindOfRep As String
    Dim Hours As Long
    
    Dim IDArrey As Variant
    Dim IDCols As Integer
    Dim IDRows As Long
    
    Dim SCID As Long
    
    Dim AR As Long
    
    Dim IDStud As Long
    
    If (Main.tblFIO.Rows = 0) Then Exit Sub
    If (Main.tblFIO.Row = -1) Then Exit Sub
    
    IDStud = Main.tblFIO.RowData(Main.tblFIO.Row)

    If (txtSemester.Text = "") Then
        MsgBox "������� ����� ��������", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (Not IsNumeric(txtSemester.Text)) Then
        MsgBox "���� '�������' ������ ��������� �����", vbOKOnly, "������"
        Exit Sub
    End If
    
    
    If (txtHours.Text = "") Then
        MsgBox "������� ���������� ���������� �����", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (Not IsNumeric(txtHours.Text)) Then
        MsgBox "���� '�������� �����' ������ ��������� �����", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (CreateNewSubject) Then
        Dim Subj As String
        Subj = txtSubjectName.Text
        
        If (FindErrors(Subj, 200, "�������� ��������", True, True)) Then Exit Sub
        CheckString Subj

        If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblSubjects] WHERE [Subject] = '" & Subj & "'") > 0) Then
            MsgBox "������� � ����� ������ ��� ��� ���������������", vbOKOnly, "������"
            Exit Sub
        End If
        
        AR = CDB.ExecSQLActionQuery("INSERT INTO tblSubjects (Subject) VALUES ('" & Subj & "')", 128)
        
        CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSubjects] WHERE [Subject] = '" & Subj & "'", IDArrey, IDCols, IDRows
        
        SelectedSubject = CLng(IDArrey(0, 0))
    End If
    
    If (SelectedSubject = 0) Then
        MsgBox "�������� �������", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (SelectedLecturer = 0) Then
        MsgBox "�������� �������������", vbOKOnly, "������"
        Exit Sub
    End If
    
    SemNum = CLng(txtSemester.Text)
    If (OptExam.Value) Then KindOfRep = "�������"
    If (OptOffset.Value) Then KindOfRep = "�����"
    If (optNoMark.Value) Then KindOfRep = "��� ������"
    Hours = CLng(txtHours.Text)
    
    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    Dim fOk As Boolean
    
    If (Main.AddNewSpecialCourse = True) Then
        fOk = True
        CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSpecialCourses] WHERE [IDStudent] = " & IDStud & " AND [IDSubject] = " & SelectedSubject & " AND [Semester] = " & SemNum & " AND [KindOfReport] = '" & KindOfRep & "' AND [CourseType] = " & Main.SpecialCourseType, DopArrey, DopCols, DopRows
        If (DopRows > 0) Then
            MsgBox "��� ���������� � ������ ������� ��� ��� ���������������", vbOKOnly, "������"
            fOk = False
            Exit Sub
        End If
        
        If (fOk) Then AR = CDB.ExecSQLActionQuery("INSERT INTO tblSpecialCourses (IDStudent, IDSubject, IDLecturer, Semester, KindOfReport, NumOfHours, Mark, CourseType) VALUES (" & IDStud & ", " & SelectedSubject & ", " & SelectedLecturer & ", " & SemNum & ", '" & KindOfRep & "', " & Hours & ", 2, " & Main.SpecialCourseType & ")", 128)
    Else
        If (Main.SpecialCourseType = 0) Then
            If (Main.tblProgressT.Rows < 2) Then Exit Sub
            If (Main.tblProgressT.Row < 1) Then Exit Sub
            If (Main.tblProgressT.RowData(Main.tblProgressT.Row) = "") Then Exit Sub
            If (Not IsNumeric(Main.tblProgressT.RowData(Main.tblProgressT.Row))) Then Exit Sub
            If (Main.ExckEditor.Value = 0) Then Exit Sub
            SCID = CLng(Main.tblProgressT.RowData(Main.tblProgressT.Row))
            
        ElseIf (Main.SpecialCourseType = 1) Then
            If (Main.tblProgressI.Rows < 2) Then Exit Sub
            If (Main.tblProgressI.Row < 1) Then Exit Sub
            If (Main.tblProgressI.RowData(Main.tblProgressI.Row) = "") Then Exit Sub
            If (Not IsNumeric(Main.tblProgressI.RowData(Main.tblProgressI.Row))) Then Exit Sub
            If (Main.ExckEditor.Value = 0) Then Exit Sub
            SCID = CLng(Main.tblProgressI.RowData(Main.tblProgressI.Row))
        
        End If

        AR = CDB.ExecSQLActionQuery("UPDATE [tblSpecialCourses] SET [IDSubject] = " & SelectedSubject & ", [IDLecturer] = " & SelectedLecturer & ", [Semester] = " & SemNum & ", [KindOfReport] = '" & KindOfRep & "', [NumOfHours] = " & Hours & " WHERE [ID] = " & SCID, 128)
        
    End If
    
    If (Main.SpecialCourseType = 0) Then
        FillSpecCoursesTable CLng(Main.tblFIO.RowData(Main.tblFIO.Row)), Main.tblProgressT, 0
        
    ElseIf (Main.SpecialCourseType = 1) Then
        FillSpecCoursesTable CLng(Main.tblFIO.RowData(Main.tblFIO.Row)), Main.tblProgressI, 1
        
    End If
    
    Unload Me

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub Form_Load()
SelectedSubject = 0
SelectedLecturer = 0

    Dim SubjArrey As Variant
    Dim SubjCols As Integer
    Dim SubjRows As Long
    
    Dim FacultyArrey As Variant
    Dim FCols As Integer
    Dim FRows As Long
    
    Dim SCID As Long
    
    Dim i As Long

    If (Main.AddNewSpecialCourse = True) Then
        frmSpecCourses.Caption = "���������� ����������"
        cmdAddExam.Caption = "��������"
            
        CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSubjects]", SubjArrey, SubjCols, SubjRows
        If (SubjRows = 0) Then
            NewSubject.Value = 1
            NewSubject_Click
        End If
        If (SubjRows > 0) Then
            NewSubject.Value = 0
            NewSubject_Click
        End If
        
    Else
        If (Main.SpecialCourseType = 0) Then
            If (Main.tblProgressT.Rows < 2) Then Exit Sub
            If (Main.tblProgressT.Row < 1) Then Exit Sub
            If (Main.tblProgressT.RowData(Main.tblProgressT.Row) = "") Then Exit Sub
            If (Not IsNumeric(Main.tblProgressT.RowData(Main.tblProgressT.Row))) Then Exit Sub
            If (Main.ExckEditor.Value = 0) Then Exit Sub
            SCID = CLng(Main.tblProgressT.RowData(Main.tblProgressT.Row))
            
        ElseIf (Main.SpecialCourseType = 1) Then
            If (Main.tblProgressI.Rows < 2) Then Exit Sub
            If (Main.tblProgressI.Row < 1) Then Exit Sub
            If (Main.tblProgressI.RowData(Main.tblProgressI.Row) = "") Then Exit Sub
            If (Not IsNumeric(Main.tblProgressI.RowData(Main.tblProgressI.Row))) Then Exit Sub
            If (Main.ExckEditor.Value = 0) Then Exit Sub
            SCID = CLng(Main.tblProgressI.RowData(Main.tblProgressI.Row))
        
        End If
        
        frmSpecCourses.Caption = "��������������"
        cmdAddExam.Caption = "OK"
            
        CDB.SQLOpenTableToArrey "SELECT " & _
                                "[tblSubjects.Subject], " & _
                                "[tblLecturers.Surname], [tblLecturers.Name], [tblLecturers.Patronymic], " & _
                                "[tblSpecialCourses.KindOfReport], [tblSpecialCourses.Semester], [tblSpecialCourses.NumOfHours] " & _
                                "FROM [tblSpecialCourses], [tblLecturers], [tblSubjects] WHERE " & _
                                "tblSpecialCourses.IDSubject = tblSubjects.ID AND " & _
                                "tblSpecialCourses.IDLecturer = tblLecturers.ID AND " & _
                                "tblSpecialCourses.ID = " & SCID, _
                                SubjArrey, SubjCols, SubjRows
                                
        NewSubject.Value = 0
        NewSubject_Click
        
        txtSubjectName.Text = CStr(SubjArrey(0, 0))
        txtSubjectName_Change
        
        txtFIO.Text = GetFullName(CStr(SubjArrey(1, 0)), CStr(SubjArrey(2, 0)), CStr(SubjArrey(3, 0)), False, False, True)
        txtFIO_Change
        
        If (Not IsNull(SubjArrey(6, 0))) Then txtHours.Text = CStr(SubjArrey(6, 0))
        If (Not IsNull(SubjArrey(5, 0))) Then txtSemester.Text = CByte(SubjArrey(5, 0))
        
        If (CStr(SubjArrey(4, 0)) = "�������") Then OptExam.Value = True
        If (CStr(SubjArrey(4, 0)) = "�����") Then OptOffset.Value = True
        If (CStr(SubjArrey(4, 0)) = "��� ������") Then optNoMark.Value = True
        
    End If

LoadSuccess = True
End Sub

Private Sub lstFIO_Click()
If (lstFIO.ListIndex = -1) Then Exit Sub

SelectedLecturer = lstFIO.ItemData(lstFIO.ListIndex)
End Sub

Private Sub NewSubject_Click()
    Dim SubjArrey As Variant
    Dim SubjCols As Integer
    Dim SubjRows As Long

If (NewSubject.Value = 0) Then
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSubjects]", SubjArrey, SubjCols, SubjRows
    If (SubjRows = 0) Then
        MsgBox "�� ���� ���������� ��� ��������� ��� ������, �������� ����� !"
        NewSubject.Value = 1
        Exit Sub
    End If

    CreateNewSubject = False
    txtSubjectName_Change
End If

If (NewSubject.Value = 1) Then
    CreateNewSubject = True
    txtSubjectName.Text = ""
    txtSubjectName_Change
End If

End Sub

Private Sub SubjectList_Click()
If (SubjectList.ListIndex = -1) Then Exit Sub

SelectedSubject = SubjectList.ItemData(SubjectList.ListIndex)

CreateNewSubject = False
NewSubject.Value = 0
End Sub

Private Sub txtFIO_Change()
Dim LecturerName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim LecturersArrey As Variant
Dim LCols As Integer
Dim LRows As Long

Dim i As Long

LecturerName = txtFIO.Text

If (LecturerName = "") Then
    lstFIO.Clear
    SelectedLecturer = 0
    Exit Sub
End If

Dim FirstS As Byte
Dim SecondS As Byte

FirstS = InStr(1, LecturerName, " ")
If (FirstS) Then SecondS = InStr(InStr(1, LecturerName, " ") + 1, LecturerName, " ")

If ((FirstS = 0) And (SecondS = 0)) Then
    Surname = LecturerName
    
    CheckString Surname
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] LIKE '" & Surname & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
ElseIf ((FirstS <> 0) And (SecondS = 0)) Then
    Surname = Left(LecturerName, FirstS - 1)
    Name = Mid(LecturerName, FirstS + 1, Len(LecturerName) - FirstS)
    
    CheckString Surname
    CheckString Name
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
ElseIf ((FirstS <> 0) And (SecondS <> 0)) Then
    Surname = Left(LecturerName, FirstS - 1)
    Name = Mid(LecturerName, FirstS + 1, SecondS - FirstS - 1)
    Patronymic = Right(LecturerName, Len(LecturerName) - SecondS)
    
    CheckString Surname
    CheckString Name
    CheckString Patronymic
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", LecturersArrey, LCols, LRows
    
End If

lstFIO.Clear
If (LRows > 0) Then

    For i = 0 To LRows - 1
        lstFIO.AddItem GetFullName(CStr(LecturersArrey(1, i)), CStr(LecturersArrey(2, i)), CStr(LecturersArrey(3, i)), False, False, True)
        lstFIO.ItemData(lstFIO.NewIndex) = LecturersArrey(0, i)
    Next i
    
    lstFIO.ListIndex = 0
    
Else
End If

End Sub

Private Sub txtSubjectName_Change()
SelectedSubject = 0
SubjectList.Clear

If (Not CreateNewSubject) Then
    Dim SubjArrey As Variant
    Dim SubjCols As Integer
    Dim SubjRows As Long
    
    Dim i As Long
    Dim SubjName As String
    
    SubjName = txtSubjectName
    CheckString SubjName
    
    If (SubjName = "") Then Exit Sub
    '��������---------------------------------------------------------------------------------------------------------------------------------
        CDB.SQLOpenTableToArrey "SELECT [ID], [Subject] FROM [tblSubjects] WHERE [Subject] LIKE '" & SubjName & "*' ORDER BY [Subject]", SubjArrey, SubjCols, SubjRows
        
        If (SubjRows > 0) Then
            
            For i = 0 To SubjRows - 1
                SubjectList.AddItem SubjArrey(1, i)
                SubjectList.ItemData(SubjectList.NewIndex) = SubjArrey(0, i)
            Next i
            
            SubjectList.ListIndex = 0
        End If
    '-----------------------------------------------------------------------------------------------------------------------------------------
End If
End Sub
