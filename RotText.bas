Attribute VB_Name = "TextDraw"
Option Explicit


Private Type LOGFONT
  lfHeight As Long
  lfWidth As Long
  lfEscapement As Long
  lfOrientation As Long
  lfWeight As Long
  lfItalic As Byte
  lfUnderline As Byte
  lfStrikeOut As Byte
  lfCharSet As Byte
  lfOutPrecision As Byte
  lfClipPrecision As Byte
  lfQuality As Byte
  lfPitchAndFamily As Byte
' lfFaceName(LF_FACESIZE) As Byte 'THIS WAS DEFINED IN API-CHANGES MY OWN
  lfFaceName As String * 33
End Type

Private Declare Function CreateFontIndirect Lib "gdi32" Alias "CreateFontIndirectA" (lpLogFont As LOGFONT) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hdc As Long, ByVal hObject As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long


Public Sub PrintAngle(txt As String, PictBox As Object, X As Long, Y As Long, FontSize As Integer, Angle As Integer)
  On Error GoTo GetOut
  Dim F As LOGFONT, hPrevFont As Long, hFont As Long, FontName As String
  
  F.lfEscapement = 10 * Angle 'rotation angle, in tenths
  FontName = "Arial cyr" + Chr$(0) 'null terminated
  F.lfFaceName = FontName
  F.lfHeight = (FontSize * -20) / Screen.TwipsPerPixelY
  hFont = CreateFontIndirect(F)
  hPrevFont = SelectObject(PictBox.hdc, hFont)
  PictBox.CurrentX = X
  PictBox.CurrentY = Y
  PictBox.Print txt
  
'  Clean up, restore original font
  hFont = SelectObject(PictBox.hdc, hPrevFont)
  DeleteObject hFont
  
  Exit Sub
GetOut:
  Exit Sub

End Sub




