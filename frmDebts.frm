VERSION 5.00
Begin VB.Form frmDebts 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   6165
   ClientLeft      =   3060
   ClientTop       =   2055
   ClientWidth     =   6375
   Icon            =   "frmDebts.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6165
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F2 
      Height          =   2775
      Left            =   0
      TabIndex        =   11
      Top             =   3360
      Width           =   6375
      Begin VB.CheckBox ckUseLastSessionData 
         Caption         =   "��������� ���������� ������ ��������� ������"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1920
         Value           =   1  'Checked
         Width           =   4335
      End
      Begin VB.CheckBox ckNoGrant 
         Caption         =   "������� ������ ���������, �� ���������� ���������"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1440
         Width           =   6135
      End
      Begin VB.CheckBox ckCurSemester 
         Caption         =   "��������� ������ �������� ��������"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1680
         Value           =   1  'Checked
         Width           =   4455
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "�������"
         Height          =   375
         Left            =   16200
         TabIndex        =   14
         Top             =   -2040
         Width           =   1575
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "������� ������"
         Default         =   -1  'True
         Height          =   375
         Left            =   3120
         TabIndex        =   13
         Top             =   2280
         Width           =   1575
      End
      Begin VB.Label txtDescryption 
         BorderStyle     =   1  'Fixed Single
         Height          =   1095
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   6135
      End
   End
   Begin VB.Frame F1 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      Begin VB.TextBox txtMaxDebts 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3120
         MaxLength       =   3
         TabIndex        =   24
         Text            =   "200"
         Top             =   2880
         Width           =   735
      End
      Begin VB.TextBox txtMinDebts 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3120
         MaxLength       =   3
         TabIndex        =   23
         Text            =   "0"
         Top             =   2520
         Width           =   735
      End
      Begin VB.TextBox txtSemester 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   285
         Left            =   4920
         TabIndex        =   16
         Top             =   1200
         Width           =   735
      End
      Begin VB.ComboBox comboLecturer 
         Height          =   315
         Left            =   2040
         TabIndex        =   10
         Top             =   2160
         Width           =   4215
      End
      Begin VB.CheckBox ckLecturer 
         Caption         =   "������������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2160
         Width           =   1815
      End
      Begin VB.ComboBox comboDep 
         Height          =   315
         Left            =   2040
         TabIndex        =   8
         Top             =   1680
         Width           =   4215
      End
      Begin VB.CheckBox ckDep 
         Caption         =   "������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1680
         Width           =   1815
      End
      Begin VB.ComboBox comboGroups 
         Height          =   315
         Left            =   2040
         TabIndex        =   6
         Top             =   1200
         Width           =   1575
      End
      Begin VB.CheckBox ckGroup 
         Caption         =   "������ :"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   1815
      End
      Begin VB.ComboBox comboCourse 
         Height          =   315
         Left            =   2040
         TabIndex        =   4
         Top             =   240
         Width           =   1575
      End
      Begin VB.CheckBox ckCourse 
         Caption         =   "���� :"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1815
      End
      Begin VB.ComboBox comboSpec 
         Height          =   315
         Left            =   2040
         TabIndex        =   2
         Top             =   720
         Width           =   4215
      End
      Begin VB.CheckBox ckSpec 
         Caption         =   "������������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label lblMaxDebts 
         Caption         =   "������������ ���������� ������:"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   2880
         Width           =   2775
      End
      Begin VB.Label lblMinDebts 
         Caption         =   "����������� ���������� ������:"
         Height          =   255
         Left            =   360
         TabIndex        =   20
         Top             =   2520
         Width           =   2775
      End
      Begin VB.Label lblSemester 
         Caption         =   "������� :"
         Height          =   255
         Left            =   3960
         TabIndex        =   15
         Top             =   1200
         Width           =   855
      End
   End
   Begin VB.Label Label1 
      Caption         =   "���������� ���������� ������:"
      Height          =   255
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   2775
   End
End
Attribute VB_Name = "frmDebts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim MinDebts As Integer
Dim MaxDebts As Integer

Private CancelExecution As Boolean

Sub Analyzer()
    Dim DesctiptionString As String
    
    DesctiptionString = "����� ��������� ��������"
    
    If (ckCourse.Value = 1) Then
        DesctiptionString = DesctiptionString & ", ����������� �� " & comboCourse.ItemData(comboCourse.ListIndex) & " �����"
        If (ckSpec.Value = 1) Then DesctiptionString = DesctiptionString & ", ������������� '" & comboSpec.List(comboSpec.ListIndex) & "'"
        
    ElseIf (ckSpec.Value = 1) Then
        DesctiptionString = DesctiptionString & ", ����������� �� ������������� '" & comboSpec.List(comboSpec.ListIndex) & "'"
        
    ElseIf (ckGroup.Value = 1) Then
        If (comboGroups.ListCount > 0) Then DesctiptionString = DesctiptionString & ", ����������� � ������ '" & comboGroups.List(comboGroups.ListIndex) & "'"
        
    End If
    
    If (ckDep.Value = 1) Then
        DesctiptionString = DesctiptionString & ", ������� ������� �������� (������) �������������� ������� '" & comboDep.List(comboDep.ListIndex) & "'"
        
    ElseIf (ckLecturer.Value = 1) Then
        If (comboLecturer.ListCount > 0) Then DesctiptionString = DesctiptionString & ", ������� ������� �������� (������) ������������� '" & comboLecturer.List(comboLecturer.ListIndex) & "'"
        If (comboLecturer.ListCount = 0) Then
            DesctiptionString = ""
            txtDescryption.Caption = ""
            Exit Sub
        End If
        
    End If
   
    If ((ckCourse.Value = 0) And (ckSpec.Value = 0) And (ckGroup.Value = 0) And (ckLecturer.Value = 0) And (ckDep.Value = 0)) Then
        DesctiptionString = "����� ��������� ��� �������� ����������"
    End If
    
    DesctiptionString = DesctiptionString & "."
    
    If ((txtSemester.Enabled) And (txtSemester.text = "")) Then
        DesctiptionString = ""
        txtDescryption.Caption = ""
        Exit Sub
    End If
    If ((txtSemester.Enabled) And (Not IsNumeric(txtSemester.text))) Then
        DesctiptionString = ""
        txtDescryption.Caption = ""
        Exit Sub
    End If

    If (txtSemester.Enabled) Then
        If (ckCurSemester.Value = 0) Then DesctiptionString = DesctiptionString & " � �������� ��������� ������ ����� �������������� ������ " & (CByte(txtSemester.text) - 1) & " ��������."
        If (ckCurSemester.Value = 1) Then DesctiptionString = DesctiptionString & " � �������� ��������� ������ ����� �������������� ������ " & (CByte(txtSemester.text)) & " ��������."
    End If
    If (Not txtSemester.Enabled) Then
        If (ckCurSemester.Value = 0) Then DesctiptionString = DesctiptionString & " � �������� ��������� ������ ����� ������� �� ������� :  (������� ����������� ������) - 1."
        If (ckCurSemester.Value = 1) Then DesctiptionString = DesctiptionString & " � �������� ��������� ������ ����� ������� �� ������� :  (������� ����������� ������)."
    End If
    
    If MinDebts > 0 Or MaxDebts < 200 Then DesctiptionString = DesctiptionString & _
    " ����� �������� ��������, ������� �� " & MinDebts & " �� " & MaxDebts & " ������."
   
    txtDescryption.Caption = DesctiptionString
End Sub

Sub CreateDiplomes(IsRed As Boolean)
Dim Subject As String

Dim midMark As Currency
Dim TotalMark As Currency
Dim TotalMarkStr As String
Dim Rest As Currency

Dim LastSemesterMark As Byte

Dim KOR As String

Dim StudentsArray As Variant
Dim StudentsCols As Integer
Dim StudentsRows As Long

Dim SubjectsArray As Variant
Dim SCols As Integer
Dim SRows As Long

Dim SubjectsArray2 As Variant
Dim SCols2 As Integer
Dim SRows2 As Long

Dim DopArray As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim i As Long
Dim Semester As Byte

Dim IDStud As Long
Dim StudCounter As Long

Dim FiveCounter As Long
Dim FourCounter As Long
Dim ThreeCounter As Long
Dim TwoCounter As Long

Main.ShowMainGrid 1

Dim Semester1 As Integer
Dim Semester2 As Integer

Dim IDSpec As Long
Dim IDGroup As Long

Dim tmp As Integer

'����������� ������ �����//////////////////////////////////////////////////////////
    If (ckCourse.Value = 1) Then
        Semester1 = CLng(comboCourse.ItemData(comboCourse.ListIndex)) * 2
        Semester2 = Semester1 - 1
        
        If (ckSpec.Value = 1) Then
            IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                            " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblGroups.IDSpec = " & IDSpec & _
                                            " AND tblSpecialities.ID = tblGroups.IDSpec AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
        
        ElseIf (ckSpec.Value = 0) Then
            CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                            " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblSpecialities.ID = tblGroups.IDSpec AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
            
        End If
        
    ElseIf (ckSpec.Value = 1) Then
        IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
        CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                        " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblGroups.IDSpec = " & IDSpec & _
                                        " AND tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12) AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
        
    ElseIf (ckGroup.Value = 1) Then
        If (comboGroups.ListCount = 0) Then
            MsgBox "�������� ������", vbOKOnly, "������"
            Exit Sub
        End If
        If (comboGroups.ListCount > 0) Then
            IDGroup = CLng(comboGroups.ItemData(comboGroups.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName  FROM [tblStudents], [tblGroups] WHERE [tblGroups.ID] = " & IDGroup & " AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1  ORDER BY [IDSpec], [Semester], [GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
        End If
        
    End If
    
    If ((ckCourse.Value = 0) And (ckSpec.Value = 0) And (ckGroup.Value = 0)) Then
        CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                        " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12) AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
    End If
'//////////////////////////////////////////////////////////////////////////////////

    If (StudentsRows = 0) Then
        MsgBox "� ���� ������ ��� ���������, ��������������� ������ �������."
        Exit Sub
    End If

If (IsRed) Then
    Main.MainGrid.Cols = 5
    Main.MainGrid.Rows = 1
    
    Main.MainGrid.TextMatrix(0, 0) = "�"
    Main.MainGrid.TextMatrix(0, 1) = "������"
    Main.MainGrid.TextMatrix(0, 2) = "���"
    Main.MainGrid.TextMatrix(0, 3) = "5"
    Main.MainGrid.TextMatrix(0, 4) = "4"
    
    Main.MainGrid.FixedCols = 1
    Main.MainGrid.FixedRows = 1
    
Else
    Main.MainGrid.Cols = 8
    Main.MainGrid.Rows = 1
    
    Main.MainGrid.TextMatrix(0, 0) = "�"
    Main.MainGrid.TextMatrix(0, 1) = "������"
    Main.MainGrid.TextMatrix(0, 2) = "���"
    Main.MainGrid.TextMatrix(0, 3) = "5"
    Main.MainGrid.TextMatrix(0, 4) = "4"
    Main.MainGrid.TextMatrix(0, 5) = "3"
    Main.MainGrid.TextMatrix(0, 6) = "2"
    Main.MainGrid.TextMatrix(0, 7) = "������� ����"
    
    Main.MainGrid.FixedCols = 1
    Main.MainGrid.FixedRows = 1
    
End If
DoEvents

For StudCounter = 0 To StudentsRows - 1
    DoEvents
    If (CancelExecution) Then Exit For
    
    Main.sbMain.PanelText("2") = "����������� ��������... (" & (StudCounter + 1) & " �� " & StudentsRows & ")"
    ProgBar Main.PProgress, ((StudCounter + 1) * 100) / StudentsRows
    Main.sbMain.RedrawPanel ("3")

    IDStud = CLng(StudentsArray(0, StudCounter))

    FourCounter = 0
    FiveCounter = 0
    ThreeCounter = 0
    TwoCounter = 0
  
    If (StudentsArray(4, StudCounter) > 0) Then

        Semester = CByte(StudentsArray(4, StudCounter))

        KOR = "�������"
        
        If (Semester < 9) Then
            CDB.SQLOpenTableToArrey "SELECT tblSubjects.ID, COUNT(tblSubjects.ID) as TotalSemesters, AVG(tblSheets.Mark) AS MidMark " & _
                                            "FROM tblSubjects, tblEducationalPlan, tblExaminations, tblSheets " & _
                                            "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
                                            "AND (tblEducationalPlan.IDSubject = tblSubjects.ID AND tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) " & _
                                            "GROUP BY tblSubjects.ID", SubjectsArray, SCols, SRows
        Else
            CDB.SQLOpenTableToArrey "SELECT tblSubjects.ID, COUNT(tblSubjects.ID) as TotalSemesters, AVG(tblSheets.Mark) AS MidMark " & _
                                            "FROM tblSubjects, tblEducationalPlan, tblExaminations, tblSheets " & _
                                            "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
                                            "AND (tblEducationalPlan.IDSubject = tblSubjects.ID AND tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) " & _
                                            "GROUP BY tblSubjects.ID", SubjectsArray, SCols, SRows
        
        End If
                                       
        For i = 0 To SRows - 1
            If (CancelExecution) Then Exit For
            
            CDB.SQLOpenTableToArrey "SELECT [Subject] FROM [tblSubjects] WHERE [ID] = " & SubjectsArray(0, i), DopArray, DopCols, DopRows
            Subject = DopArray(0, 0)
            
            If (Semester < 9) Then
                CDB.SQLOpenTableToArrey "SELECT tblSheets.Mark " & _
                                                "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
                                                "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
                                                "AND (tblEducationalPlan.IDSubject = " & SubjectsArray(0, i) & " AND tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) " & _
                                                "AND (tblEducationalPlan.Semester = (SELECT MAX(tblEducationalPlan.Semester) FROM tblEducationalPlan, tblExaminations, tblSheets WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & ") AND (tblEducationalPlan.IDSubject = " & SubjectsArray(0, i) & " AND tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)))", DopArray, DopCols, DopRows
                                                
            Else
                CDB.SQLOpenTableToArrey "SELECT tblSheets.Mark " & _
                                                "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
                                                "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
                                                "AND (tblEducationalPlan.IDSubject = " & SubjectsArray(0, i) & " AND tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) " & _
                                                "AND (tblEducationalPlan.Semester = (SELECT MAX(tblEducationalPlan.Semester) FROM tblEducationalPlan, tblExaminations, tblSheets WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & ") AND (tblEducationalPlan.IDSubject = " & SubjectsArray(0, i) & " AND tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)))", DopArray, DopCols, DopRows
                                                
            End If
                        
            If (DopRows > 0) Then
            
                LastSemesterMark = DopArray(0, 0)
                
                midMark = CCur(SubjectsArray(2, i))
                Rest = CCur(midMark - Fix(midMark))
                
                If (Rest = 0) Then
                    TotalMark = Fix(midMark)
                    
                ElseIf (Rest = 0.5) Then
                    TotalMark = IIf(CCur(LastSemesterMark) > midMark, Fix(midMark) + 1, Fix(midMark))
                    
                ElseIf (Rest > 0.5) Then
                    TotalMark = Fix(midMark) + 1
                    
                ElseIf (Rest < 0.5) Then
                    TotalMark = Fix(midMark)
                    
                End If
            
                If (FivePointMarkSystem) Then
                    Select Case TotalMark
                        Case 2:    TwoCounter = TwoCounter + 1
                        Case 3:    ThreeCounter = ThreeCounter + 1
                        Case 4:    FourCounter = FourCounter + 1
                        Case 5:    FiveCounter = FiveCounter + 1
                    End Select
                Else
                    Select Case TotalMark
                        Case 1, 2, 3:  TwoCounter = TwoCounter + 1
                        Case 4, 5, 6:  ThreeCounter = ThreeCounter + 1
                        Case 7, 8, 9:  FourCounter = FourCounter + 1
                        Case 10, 11, 12:  FiveCounter = FiveCounter + 1
                    End Select
                End If
            
        End If
        
        Next i
        
'            If (Semester < 9) Then
'                CDB.SQLOpenTableToArrey "SELECT AVG(tblSheets.Mark) " & _
'                                        "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                        "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
'                                        "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) ", _
'                                        DopArray, DopCols, DopRows
'
'            Else
'                CDB.SQLOpenTableToArrey "SELECT AVG(tblSheets.Mark) " & _
'                                        "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                        "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
'                                        "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) ", _
'                                        DopArray, DopCols, DopRows
'
'            End If

        midMark = 0
        If ((TwoCounter > 0) Or (ThreeCounter > 0) Or (FourCounter > 0) Or (FiveCounter > 0)) Then
            midMark = (CCur(TwoCounter) * CCur(2) + CCur(ThreeCounter) * CCur(3) + CCur(FourCounter) * CCur(4) + CCur(FiveCounter) * CCur(5)) / _
                      (IIf(TwoCounter > 0, CCur(TwoCounter), CCur(0)) + IIf(ThreeCounter > 0, CCur(ThreeCounter), CCur(0)) + _
                      IIf(FourCounter > 0, CCur(FourCounter), CCur(0)) + IIf(FiveCounter > 0, CCur(FiveCounter), CCur(0)))
        End If
        
        If (IsRed) Then
            If ((FiveCounter + FourCounter) > 0 And (ThreeCounter + TwoCounter) = 0) Then
            
                tmp = 0
                If Semester < 9 Then
                    CDB.SQLOpenTableToArrey "SELECT count(*) FROM tblDegreeThemes WHERE idStudent=" & IDStud & " AND mark <> 5", DopArray, DopCols, DopRows
                    If CInt(DopArray(0, 0)) > 0 Then tmp = 1
                    CDB.SQLOpenTableToArrey "SELECT count(*) " & _
                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
                                            "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark <> 5 )" & _
                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) AND isAdditional=2 ", _
                                            DopArray, DopCols, DopRows
                    If CInt(DopArray(0, 0)) > 0 Then tmp = 1
               Else
                    CDB.SQLOpenTableToArrey "SELECT count(*) FROM tblDegreeThemes2 WHERE idStudent=" & IDStud & " AND mark <> 5", DopArray, DopCols, DopRows
                    If CInt(DopArray(0, 0)) > 0 Then tmp = 1
                    CDB.SQLOpenTableToArrey "SELECT count(*) " & _
                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
                                            "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark <> 5) " & _
                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) AND isAdditional=2 ", _
                                            DopArray, DopCols, DopRows
                    If CInt(DopArray(0, 0)) > 0 Then tmp = 1
                End If
                
                If ((CCur(FiveCounter) / CCur(FiveCounter + FourCounter)) >= 0.75 And tmp = 0) Then
                    Main.MainGrid.AddItem Main.MainGrid.Rows & vbTab & CStr(StudentsArray(5, StudCounter)) & vbTab & GetFullName(CStr(StudentsArray(1, StudCounter)), CStr(StudentsArray(2, StudCounter)), CStr(StudentsArray(3, StudCounter)), False, False, True) & vbTab & FiveCounter & vbTab & FourCounter
                End If
            End If
        Else
            Main.MainGrid.AddItem Main.MainGrid.Rows & vbTab & CStr(StudentsArray(5, StudCounter)) & vbTab & GetFullName(CStr(StudentsArray(1, StudCounter)), CStr(StudentsArray(2, StudCounter)), CStr(StudentsArray(3, StudCounter)), False, False, True) & vbTab & FiveCounter & vbTab & FourCounter & vbTab & ThreeCounter & vbTab & TwoCounter & vbTab & Format(midMark, "#.00")
        End If
        
        TotalMarkStr = "err"
        TotalMark = 0
        midMark = 0
    End If
    
Next StudCounter

ProgBar Main.PProgress, 0
Main.sbMain.RedrawPanel ("3")
Main.sbMain.PanelText("2") = "������"
Main.FullNodeData = "Query"

End Sub

Sub CreateSheetBook()
Dim Subject As String

Dim midMark As Currency
Dim TotalMark As Currency
Dim TotalMarkStr As String
Dim Rest As Currency

Dim LastSemesterMark As Byte

Dim KOR As String

Dim StudentsArray As Variant
Dim StudentsCols As Integer
Dim StudentsRows As Long

Dim SubjectsArray As Variant
Dim SCols As Integer
Dim SRows As Long

Dim SubjectsArray2 As Variant
Dim SCols2 As Integer
Dim SRows2 As Long

Dim DopArray As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim i As Long
Dim Semester As Byte

Dim IDStud As Long
Dim StudCounter As Long

Dim minSem As Long
Dim maxSem As Long

Dim ii As Integer

Dim FiveCounter As Long
Dim FourCounter As Long
Dim ThreeCounter As Long
Dim TwoCounter As Long

Dim markCnt As Long
Dim midMarkk As Double

Main.ShowMainGrid 1

Dim Semester1 As Integer
Dim Semester2 As Integer

Dim IDSpec As Long
Dim IDGroup As Long

'����������� ������ �����//////////////////////////////////////////////////////////
    If (ckCourse.Value = 1) Then
        Semester1 = CLng(comboCourse.ItemData(comboCourse.ListIndex)) * 2
        Semester2 = Semester1 - 1
        
        If (ckSpec.Value = 1) Then
            IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                            " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblGroups.IDSpec = " & IDSpec & _
                                            " AND tblSpecialities.ID = tblGroups.IDSpec AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
        
        ElseIf (ckSpec.Value = 0) Then
            CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                            " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblSpecialities.ID = tblGroups.IDSpec AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
            
        End If
        
    ElseIf (ckSpec.Value = 1) Then
        IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
        CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                        " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblGroups.IDSpec = " & IDSpec & _
                                        " AND tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12) AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
        
    ElseIf (ckGroup.Value = 1) Then
        If (comboGroups.ListCount = 0) Then
            MsgBox "�������� ������", vbOKOnly, "������"
            Exit Sub
        End If
        If (comboGroups.ListCount > 0) Then
            IDGroup = CLng(comboGroups.ItemData(comboGroups.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName  FROM [tblStudents], [tblGroups] WHERE [tblGroups.ID] = " & IDGroup & " AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1  ORDER BY [IDSpec], [Semester], [GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
        End If
        
    End If
    
    If ((ckCourse.Value = 0) And (ckSpec.Value = 0) And (ckGroup.Value = 0)) Then
        CDB.SQLOpenTableToArrey "SELECT tblStudents.ID, Surname, Name, Patronymic, tblGroups.Semester, (Left$(tblGroups.GroupName, InStr(1, tblGroups.GroupName, '#') - 1) & IIf(tblGroups.Semester Mod 2 = 1, tblGroups.Semester \ 2 + 1, tblGroups.Semester \ 2) & Right$(tblGroups.GroupName, Len(tblGroups.GroupName) - InStr(1, tblGroups.GroupName, '#'))) As FullGroupName " & _
                                        " FROM [tblStudents], [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12) AND tblStudents.IDGroup = tblGroups.ID AND tblStudents.IsActive = 1 " & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName], [tblGroups.GroupName], [Surname], [Name], [Patronymic]", StudentsArray, StudentsCols, StudentsRows
    End If
'//////////////////////////////////////////////////////////////////////////////////

    If (StudentsRows = 0) Then
        MsgBox "� ���� ������ ��� ���������, ��������������� ������ �������."
        Exit Sub
    End If

    Main.MainGrid.Cols = 8
    Main.MainGrid.Rows = 1
    
    Main.MainGrid.TextMatrix(0, 0) = "�"
    Main.MainGrid.TextMatrix(0, 1) = "������"
    Main.MainGrid.TextMatrix(0, 2) = "���"
    Main.MainGrid.TextMatrix(0, 3) = "5"
    Main.MainGrid.TextMatrix(0, 4) = "4"
    Main.MainGrid.TextMatrix(0, 5) = "3"
    Main.MainGrid.TextMatrix(0, 6) = "2"
    Main.MainGrid.TextMatrix(0, 7) = "������� ����"
    
    Main.MainGrid.FixedCols = 1
    Main.MainGrid.FixedRows = 1
    
DoEvents

For StudCounter = 0 To StudentsRows - 1
    DoEvents
    If (CancelExecution) Then Exit For
    
    Main.sbMain.PanelText("2") = "����������� ��������... (" & (StudCounter + 1) & " �� " & StudentsRows & ")"
    ProgBar Main.PProgress, ((StudCounter + 1) * 100) / StudentsRows
    Main.sbMain.RedrawPanel ("3")

    IDStud = CLng(StudentsArray(0, StudCounter))

    FourCounter = 0
    FiveCounter = 0
    ThreeCounter = 0
    TwoCounter = 0
  
    If (StudentsArray(4, StudCounter) > 0) Then

        Semester = CByte(StudentsArray(4, StudCounter))

        KOR = "�������"
        
'        '���-�� 5
'        If (Semester < 9) Then
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "5", "10, 11, 12") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        Else
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "5", "10, 11, 12") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        End If
'        If (DopRows > 0) Then FiveCounter = CLng(DopArray(0, 0))
'
'        '���-�� 4
'        If (Semester < 9) Then
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "4", "7, 8, 9") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        Else
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "4", "7, 8, 9") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        End If
'        If (DopRows > 0) Then FourCounter = CLng(DopArray(0, 0))
'
'        '���-�� 3
'        If (Semester < 9) Then
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "3", "4, 5, 6") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        Else
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "3", "4, 5, 6") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        End If
'        If (DopRows > 0) Then ThreeCounter = CLng(DopArray(0, 0))
'
'        '���-�� 2
'        If (Semester < 9) Then
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "2", "1, 2, 3") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        Else
'            CDB.SQLOpenTableToArrey "SELECT COUNT(tblSheets.Mark) " & _
'                                            "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                            "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark IN (" & IIf(FivePointMarkSystem, "2", "1, 2, 3") & ")) " & _
'                                            "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
'
'        End If
'        If (DopRows > 0) Then TwoCounter = CLng(DopArray(0, 0))
'
'
'        If (Semester < 9) Then
'            CDB.SQLOpenTableToArrey "SELECT AVG(tblSheets.Mark) " & _
'                                    "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                    "WHERE (tblEducationalPlan.Semester < 9 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
'                                    "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) ", _
'                                    DopArray, DopCols, DopRows
'
'        Else
'            CDB.SQLOpenTableToArrey "SELECT AVG(tblSheets.Mark) " & _
'                                    "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
'                                    "WHERE (tblEducationalPlan.Semester > 8 AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " AND tblSheets.Mark NOT IN (201, 202, 203)) " & _
'                                    "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID) ", _
'                                    DopArray, DopCols, DopRows
'
'        End If
'
'
'        Main.MainGrid.AddItem Main.MainGrid.Rows & vbTab & CStr(StudentsArray(5, StudCounter)) & vbTab & GetFullName(CStr(StudentsArray(1, StudCounter)), CStr(StudentsArray(2, StudCounter)), CStr(StudentsArray(3, StudCounter)), False, False, True) & vbTab & FiveCounter & vbTab & FourCounter & vbTab & ThreeCounter & vbTab & TwoCounter & vbTab & IIf(DopRows > 0, IIf(Not IsNull(DopArray(0, 0)), Format(DopArray(0, 0), "#.00"), "err"), "err")
        
        If Semester < 9 Then minSem = 1 Else minSem = 9
        maxSem = Semester
        If ckCurSemester.Value = 0 Then maxSem = maxSem - 1
        If ckUseLastSessionData = 1 Then minSem = maxSem
        
        CDB.SQLOpenTableToArrey "SELECT tblSheets.Mark " & _
                                        "FROM tblEducationalPlan, tblExaminations, tblSheets " & _
                                        "WHERE (tblEducationalPlan.Semester BETWEEN " & minSem & " AND " & maxSem & " ) " & _
                                        "AND tblEducationalPlan.KindOfReport = '" & KOR & "' AND tblSheets.IDStudent = " & IDStud & " " & _
                                        "AND (tblExaminations.IDEP = tblEducationalPlan.ID AND tblSheets.IDExam = tblExaminations.ID)", DopArray, DopCols, DopRows
        
        TwoCounter = 0
        ThreeCounter = 0
        FourCounter = 0
        FiveCounter = 0
        
        For ii = 0 To DopRows - 1
            Select Case DopArray(0, ii)
            Case 2:
                TwoCounter = TwoCounter + 1
            Case 3:
                ThreeCounter = ThreeCounter + 1
            Case 4:
                FourCounter = FourCounter + 1
            Case 5:
                FiveCounter = FiveCounter + 1
            Case Else:
            End Select
        Next ii
        
        markCnt = TwoCounter + ThreeCounter + FourCounter + FiveCounter
        If markCnt = 0 Then
            midMarkk = 0
        Else
            midMarkk = TwoCounter * 2 + ThreeCounter * 3 + FourCounter * 4 + FiveCounter * 5
            midMarkk = midMarkk / markCnt
        End If
            
        Main.MainGrid.AddItem Main.MainGrid.Rows & vbTab & CStr(StudentsArray(5, StudCounter)) & vbTab & GetFullName(CStr(StudentsArray(1, StudCounter)), CStr(StudentsArray(2, StudCounter)), CStr(StudentsArray(3, StudCounter)), False, False, True) & vbTab & FiveCounter & vbTab & FourCounter & vbTab & ThreeCounter & vbTab & TwoCounter & vbTab & IIf(midMarkk <> 0, Format(midMarkk, "#.00"), "err")
        
        TotalMarkStr = "err"
        TotalMark = 0
        midMarkk = 0
    End If
    
Next StudCounter

ProgBar Main.PProgress, 0
Main.sbMain.RedrawPanel ("3")
Main.sbMain.PanelText("2") = "������"
Main.FullNodeData = "Query"

End Sub

Sub CreateDebtsList()

Dim GroupsArrey As Variant
Dim GroupsCols As Integer
Dim GroupsRows As Long

Dim StudArrey As Variant
Dim StudCols As Integer
Dim StudRows As Long

Dim ExamsArrey As Variant
Dim ExamsCols As Integer
Dim ExamsRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim a As Variant
Dim c As Integer
Dim r As Long

Dim Semester1 As Integer
Dim Semester2 As Integer

Dim IDSpec As Long
Dim IDGroup As Long
Dim IDDep As Long
Dim IDLect As Long

Dim GroupName As String
Dim SubjectName As String
Dim SemesterNum As String
Dim KindOfReport As String
Dim FacultyName As String
Dim LecturerName As String
Dim StudentName As String

Dim GIndex As Long
Dim EIndex As Long
Dim SIndex As Long

Dim StudCounter As Long

StudCounter = 1


    If ((ckLecturer.Value = 1) And (comboLecturer.ListCount = 0)) Then
        MsgBox "�������� �������������.", vbOKOnly, "������"
        Exit Sub
    End If

'����������� ������ �����//////////////////////////////////////////////////////////
    If (ckCourse.Value = 1) Then
        Semester1 = CLng(comboCourse.ItemData(comboCourse.ListIndex)) * 2
        Semester2 = Semester1 - 1
        
        If (ckSpec.Value = 1) Then
            IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [IDSpec] = " & IDSpec & " AND( [Semester] = " & Semester1 & " OR [Semester] = " & Semester2 & ") ORDER BY [IDSpec], [Semester], [GroupName]", GroupsArrey, GroupsCols, GroupsRows
        
        ElseIf (ckSpec.Value = 0) Then
            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [Semester] = " & Semester1 & " OR [Semester] = " & Semester2 & " ORDER BY [IDSpec], [Semester], [GroupName]", GroupsArrey, GroupsCols, GroupsRows
            
        End If
        
    ElseIf (ckSpec.Value = 1) Then
        IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
        CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE ([Semester] BETWEEN 1 AND 12) AND [IDSpec] = " & IDSpec & " ORDER BY [IDSpec], [Semester], [GroupName]", GroupsArrey, GroupsCols, GroupsRows
        
    ElseIf (ckGroup.Value = 1) Then
        If (comboGroups.ListCount = 0) Then
            MsgBox "�������� ������", vbOKOnly, "������"
            Exit Sub
        End If
        If (comboGroups.ListCount > 0) Then
            IDGroup = CLng(comboGroups.ItemData(comboGroups.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [ID] = " & IDGroup & " ORDER BY [IDSpec], [Semester], [GroupName]", GroupsArrey, GroupsCols, GroupsRows
        End If
        
    End If
    
    If ((ckCourse.Value = 0) And (ckSpec.Value = 0) And (ckGroup.Value = 0)) Then
        CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE ([Semester] BETWEEN 1 AND 12) ORDER BY [IDSpec], [Semester], [GroupName]", GroupsArrey, GroupsCols, GroupsRows
    End If
'//////////////////////////////////////////////////////////////////////////////////

    If (GroupsRows = 0) Then
        MsgBox "� ���� ������ ��� ���������, ��������������� ������ �������."
        Exit Sub
    End If
    
    Main.ShowMainGrid 1
    Main.MainGrid.Cols = 10
    Main.MainGrid.Rows = 1
    Main.MainGrid.FixedCols = 1
    Main.MainGrid.FixedRows = 1
    
    Main.MainGrid.TextMatrix(0, 0) = "�"
    Main.MainGrid.TextMatrix(0, 1) = "������"
    Main.MainGrid.TextMatrix(0, 2) = "�. �. �. ��������"
    Main.MainGrid.TextMatrix(0, 3) = "���-��"
    Main.MainGrid.TextMatrix(0, 4) = "�������"
    Main.MainGrid.TextMatrix(0, 5) = "���."
    Main.MainGrid.TextMatrix(0, 6) = "���."
    Main.MainGrid.TextMatrix(0, 7) = "�������"
    Main.MainGrid.TextMatrix(0, 8) = "�������������"
    Main.MainGrid.TextMatrix(0, 9) = "�������"
    
    Dim GroupAdd As Boolean
    Dim StudAdd As Boolean
    Dim StudRow As Long
    
    Dim StudDebtsCounter As Long
    Dim DepDebtsCounter As Long
    
    Dim fOk As Boolean
    
    Dim Semester As Byte
    If (txtSemester.Enabled) Then
        If (ckCurSemester.Value = 0) Then Semester = CByte(txtSemester.text) - 1
        If (ckCurSemester.Value = 1) Then Semester = CByte(txtSemester.text)
    End If
    
    If (GroupsRows > 0) Then
        DepDebtsCounter = 0
        
        For GIndex = 0 To GroupsRows - 1
            DoEvents
            If (CancelExecution) Then Exit For
            
            GroupName = GroupsArrey(1, GIndex)
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupsArrey(2, GIndex)) Mod 2 = 1, (CLng(GroupsArrey(2, GIndex)) \ 2) + 1, CLng(GroupsArrey(2, GIndex)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
            GroupAdd = False
            
            Main.sbMain.PanelText("2") = "����������� �������� ������ " & GroupName
            ProgBar Main.PProgress, ((GIndex + 1) * 100) / GroupsRows
            Main.sbMain.RedrawPanel ("3")
            
            If (Not txtSemester.Enabled) Then
                If (ckCurSemester.Value = 0) Then Semester = CByte(GroupsArrey(2, GIndex)) - 1
                If (ckCurSemester.Value = 1) Then Semester = CByte(GroupsArrey(2, GIndex))
            End If
            
            CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [IDGroup] = " & GroupsArrey(0, GIndex) & " AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, StudCols, StudRows
            
            For SIndex = 0 To StudRows - 1
                If (CancelExecution) Then Exit For
                
                Main.sbMain.PanelText("2") = "����������� �������� ������ " & GroupName & " (" & (SIndex + 1) & " �� " & StudRows & ")"
'                ProgBar Main.PProgress, ((SIndex + 1) * 100) / StudRows
'                Main.sbMain.RedrawPanel ("3")
            
                StudentName = GetFullName(CStr(StudArrey(1, SIndex)), CStr(StudArrey(2, SIndex)), CStr(StudArrey(3, SIndex)), False, False, True)
            
                StudAdd = False
            
                If (ckDep.Value = 1) Then
                    IDDep = CLng(comboDep.ItemData(comboDep.ListIndex))
                    CDB.SQLOpenTableToArrey "SELECT" & _
                    " tblExaminations.IDLecturer, tblExaminations.IDFaculty," & _
                    " tblEducationalPlan.IDSubject, tblEducationalPlan.Semester, tblEducationalPlan.KindOfReport, tblEducationalPlan.IsAdditional" & _
                    " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
                    " WHERE tblSheets.IDStudent = " & StudArrey(0, SIndex) & " AND" & _
                    " tblExaminations.ID = tblSheets.IDExam AND" & _
                    " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
                    " tblEducationalPlan.IsAdditional = 0 AND" & _
                    " tblEducationalPlan.Semester" & IIf(ckUseLastSessionData.Value = 1, " = ", " <= ") & Semester & " AND" & _
                    " tblSheets.Mark IN " & IIf(FivePointMarkSystem, "(201, 202, 203, 1, 2)", "(201, 202, 203, 1, 2, 3)") & " AND" & _
                    " tblExaminations.IDFaculty = " & IDDep, ExamsArrey, ExamsCols, ExamsRows
                    If ExamsRows < MinDebts Or ExamsRows > MaxDebts Then GoTo endfor '''sic
                    
                    CDB.SQLOpenTableToArrey "SELECT COUNT(*)" & _
                    " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
                    " WHERE tblSheets.IDStudent = " & StudArrey(0, SIndex) & " AND" & _
                    " tblExaminations.ID = tblSheets.IDExam AND" & _
                    " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
                    " tblEducationalPlan.IsAdditional = 0 AND" & _
                    " tblEducationalPlan.Semester" & IIf(ckUseLastSessionData.Value = 1, " = ", " <= ") & Semester & " AND" & _
                    " tblSheets.Mark IN " & IIf(FivePointMarkSystem, "(201, 202, 203, 1, 2)", "(201, 202, 203, 1, 2, 3)"), _
                    a, c, r
                    
                ElseIf (ckLecturer.Value = 1) Then
                    IDLect = CLng(comboLecturer.ItemData(comboLecturer.ListIndex))
                    CDB.SQLOpenTableToArrey "SELECT" & _
                    " tblExaminations.IDLecturer, tblExaminations.IDFaculty," & _
                    " tblEducationalPlan.IDSubject, tblEducationalPlan.Semester, tblEducationalPlan.KindOfReport, tblEducationalPlan.IsAdditional" & _
                    " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
                    " WHERE tblSheets.IDStudent = " & StudArrey(0, SIndex) & " AND" & _
                    " tblExaminations.ID = tblSheets.IDExam AND" & _
                    " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
                    " tblEducationalPlan.IsAdditional = 0 AND" & _
                    " tblEducationalPlan.Semester" & IIf(ckUseLastSessionData.Value = 1, " = ", " <= ") & Semester & " AND" & _
                    " tblSheets.Mark IN " & IIf(FivePointMarkSystem, "(201, 202, 203, 1, 2)", "(201, 202, 203, 1, 2, 3)") & " AND" & _
                    " tblExaminations.IDLecturer = " & IDLect, ExamsArrey, ExamsCols, ExamsRows
                    If ExamsRows < MinDebts Or ExamsRows > MaxDebts Then GoTo endfor '''sic
                End If
                
                If ((ckDep.Value = 0) And (ckLecturer.Value = 0)) Then
                    CDB.SQLOpenTableToArrey "SELECT" & _
                    " tblExaminations.IDLecturer, tblExaminations.IDFaculty," & _
                    " tblEducationalPlan.IDSubject, tblEducationalPlan.Semester, tblEducationalPlan.KindOfReport, tblEducationalPlan.IsAdditional" & _
                    " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
                    " WHERE tblSheets.IDStudent = " & StudArrey(0, SIndex) & " AND" & _
                    " tblExaminations.ID = tblSheets.IDExam AND" & _
                    " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
                    " tblEducationalPlan.IsAdditional = 0 AND" & _
                    " tblEducationalPlan.Semester" & IIf(ckUseLastSessionData.Value = 1, " = ", " <= ") & Semester & " AND" & _
                    " tblSheets.Mark IN " & IIf(FivePointMarkSystem, "(201, 202, 203, 1, 2)", "(201, 202, 203, 1, 2, 3)"), ExamsArrey, ExamsCols, ExamsRows
                    If ExamsRows < MinDebts Or ExamsRows > MaxDebts Then GoTo endfor '''sic
                End If
                
'                If (ExamsRows > 0) Then
'                    If (Not GroupAdd) Then
'                        Main.MainGrid.AddItem vbTab & GroupName
'                        GroupAdd = True
'                    End If
'                End If
                
                StudDebtsCounter = 0
                StudRow = 0
                For EIndex = 0 To ExamsRows - 1
                    
                    fOk = True
                
                    If (fOk) Then
                        If (Not GroupAdd) Then
                            Main.MainGrid.AddItem vbTab & GroupName
                            GroupAdd = True
                        End If

                        CDB.SQLOpenTableToArrey "SELECT [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [ID] = " & ExamsArrey(0, EIndex), DopArrey, DopCols, DopRows
                        If (Not IsNull(DopArrey(0, 0))) Then LecturerName = GetFullName(CStr(DopArrey(0, 0)), CStr(DopArrey(1, 0)), CStr(DopArrey(2, 0)), False, False, True)
                        
                        CDB.SQLOpenTableToArrey "SELECT [FacultyName] FROM [tblFaculty] WHERE [ID] = " & ExamsArrey(1, EIndex), DopArrey, DopCols, DopRows
                        If (Not IsNull(DopArrey(0, 0))) Then FacultyName = DopArrey(0, 0)
                    
                        CDB.SQLOpenTableToArrey "SELECT [Subject] FROM [tblSubjects] WHERE [ID] = " & ExamsArrey(2, EIndex), DopArrey, DopCols, DopRows
                        If (Not IsNull(DopArrey(0, 0))) Then SubjectName = DopArrey(0, 0)
                        
                        SemesterNum = CStr(ExamsArrey(3, EIndex))
                        KindOfReport = CStr(ExamsArrey(4, EIndex))
                        
                        If (Not StudAdd) Then
                            If (KindOfReport = "�������") Then
                                Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & vbTab & SubjectName & vbTab & "�" & vbTab & vbTab & SemesterNum & vbTab & LecturerName & vbTab & FacultyName
                            End If
                            If (KindOfReport = "�����") Then
                                Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & vbTab & SubjectName & vbTab & vbTab & "�" & vbTab & SemesterNum & vbTab & LecturerName & vbTab & FacultyName
                            End If
                            StudCounter = StudCounter + 1
                            StudAdd = True
                            
                            StudRow = Main.MainGrid.Rows - 1
                            StudDebtsCounter = StudDebtsCounter + 1
                            DepDebtsCounter = DepDebtsCounter + 1
                            
                        ElseIf (StudAdd) Then
                            If (KindOfReport = "�������") Then
                                Main.MainGrid.AddItem vbTab & vbTab & vbTab & vbTab & SubjectName & vbTab & "�" & vbTab & vbTab & SemesterNum & vbTab & LecturerName & vbTab & FacultyName
                            End If
                            If (KindOfReport = "�����") Then
                                Main.MainGrid.AddItem vbTab & vbTab & vbTab & vbTab & SubjectName & vbTab & vbTab & "�" & vbTab & SemesterNum & vbTab & LecturerName & vbTab & FacultyName
                            End If
                            
                            StudDebtsCounter = StudDebtsCounter + 1
                            DepDebtsCounter = DepDebtsCounter + 1
                        End If
                    End If
                    
                Next EIndex
                If (StudAdd) Then
                    If ckDep.Value = 1 Then
                    Main.MainGrid.TextMatrix(StudRow, 3) = StudDebtsCounter & "(" & CStr(a(0, 0)) & ")"
                    Else
                    Main.MainGrid.TextMatrix(StudRow, 3) = StudDebtsCounter
                    End If
                End If
                
endfor: '''sic
            Next SIndex
        Next GIndex
    End If
    
    If (DepDebtsCounter > 0) Then
        Main.MainGrid.AddItem ""
        Main.MainGrid.AddItem vbTab & vbTab & "����� ����� �������������� :" & vbTab & DepDebtsCounter
    End If
    
        ProgBar Main.PProgress, 0
        Main.sbMain.RedrawPanel ("3")
        Main.sbMain.PanelText("2") = "������"
        Main.FullNodeData = "Query"
End Sub

Sub CreateGrantsList()
Dim StudentName As String

Dim GroupsArrey As Variant
Dim GroupsCols As Integer
Dim GroupsRows As Long

Dim StudArrey As Variant
Dim StudCols As Integer
Dim StudRows As Long

Dim ExamsArrey As Variant
Dim ExamsCols As Integer
Dim ExamsRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim Semester1 As Integer
Dim Semester2 As Integer

Dim IDSpec As Long
Dim IDGroup As Long

Dim GroupName As String

Dim GIndex As Long
Dim EIndex As Long
Dim SIndex As Long

Dim StudCounter As Long

Dim TotalSum As Currency

StudCounter = 1
'����������� ������ �����//////////////////////////////////////////////////////////
    If (ckCourse.Value = 1) Then
              
        Semester1 = CLng(comboCourse.ItemData(comboCourse.ListIndex)) * 2
        Semester2 = Semester1 - 1
        
        If (ckSpec.Value = 1) Then
            IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                            " FROM [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblGroups.IDSpec = " & IDSpec & _
                                            " AND tblSpecialities.ID = tblGroups.IDSpec" & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester]", GroupsArrey, GroupsCols, GroupsRows
        
        ElseIf (ckSpec.Value = 0) Then
'            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [Semester] = " & Semester1 & " OR [Semester] = " & Semester2 & " ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
            
            CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                            " FROM [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester]", GroupsArrey, GroupsCols, GroupsRows
            
        End If
        
'        If (CLng(comboCourse.ItemData(comboCourse.ListIndex)) = 1) Then
'
'        End If
        
    ElseIf (ckSpec.Value = 1) Then
        IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
'        CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [IDSpec] = " & IDSpec & " ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
        
        CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                        " FROM [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblGroups.IDSpec = " & IDSpec & _
                                        " AND tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12)" & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester]", GroupsArrey, GroupsCols, GroupsRows
        
    ElseIf (ckGroup.Value = 1) Then
        If (comboGroups.ListCount = 0) Then
            MsgBox "�������� ������", vbOKOnly, "������"
            Exit Sub
        End If
        If (comboGroups.ListCount > 0) Then
            IDGroup = CLng(comboGroups.ItemData(comboGroups.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [ID] = " & IDGroup & " ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
        End If
        
    End If
    
    If ((ckCourse.Value = 0) And (ckSpec.Value = 0) And (ckGroup.Value = 0)) Then
'        CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
        
        If (Month(GlobalData) = 6 Or Month(GlobalData) = 7) Then
             CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                        " FROM [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 8)" & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName]", GroupsArrey, GroupsCols, GroupsRows
        Else
             CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                        " FROM [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12)" & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName]", GroupsArrey, GroupsCols, GroupsRows
        End If
    End If
'//////////////////////////////////////////////////////////////////////////////////

    If (GroupsRows = 0) Then
        MsgBox "� ���� ������ ��� ���������, ��������������� ������ �������."
        Exit Sub
    End If
    
    Main.ShowMainGrid 1
    Main.MainGrid.Cols = 6
    Main.MainGrid.Rows = 1
    Main.MainGrid.FixedCols = 1
    Main.MainGrid.FixedRows = 1
    
    Main.MainGrid.TextMatrix(0, 0) = "�"
    Main.MainGrid.TextMatrix(0, 1) = "������"
    Main.MainGrid.TextMatrix(0, 2) = "�. �. �. ��������"
    Main.MainGrid.TextMatrix(0, 3) = "���������"
    Main.MainGrid.TextMatrix(0, 4) = "��. ���"
    Main.MainGrid.TextMatrix(0, 5) = "�����"
    
    Dim ErrorDC As Boolean
    Dim ErrorDesc As String
    
    Dim MidValue As Double
    Dim MidValueStr As String
    Dim Counter As Long
    Dim GrantSum As Currency
    Dim TotalGrantSum As Currency
    
    Dim CategoryExtra As Currency
    Dim CategoryName As String
    
    Dim NominalGrantName As String
    Dim NominalGrantSum As Currency
    
    Dim SpecExtra As Integer
    Dim ValueSum As Currency
    Dim MinGrant As Currency
    
    Dim GroupAdd As Boolean
    
    Dim Semester As Byte
    
    Dim fOk As Boolean
    
    If (txtSemester.Enabled) Then
        If (ckCurSemester.Value = 0) Then Semester = CByte(txtSemester.text) - 1
        If (ckCurSemester.Value = 1) Then Semester = CByte(txtSemester.text)
    End If

    If (GroupsRows > 0) Then
    
        For GIndex = 0 To GroupsRows - 1
            DoEvents
            If (CancelExecution) Then Exit For
            
            GroupName = GroupsArrey(1, GIndex)
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupsArrey(2, GIndex)) Mod 2 = 1, (CLng(GroupsArrey(2, GIndex)) \ 2) + 1, CLng(GroupsArrey(2, GIndex)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
            GroupAdd = False
            
            Main.sbMain.PanelText("2") = "����������� �������� ������ " & GroupName
            ProgBar Main.PProgress, ((GIndex + 1) * 100) / GroupsRows
            Main.sbMain.RedrawPanel ("3")
            
            If (Not txtSemester.Enabled) Then
                If (ckCurSemester.Value = 0) Then Semester = CByte(GroupsArrey(2, GIndex)) - 1
                If (ckCurSemester.Value = 1) Then Semester = CByte(GroupsArrey(2, GIndex))
            End If
            
            CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic], [IDCategory], [IDNominalGrant], [FormOfPayment] FROM [tblStudents] WHERE [IDGroup] = " & GroupsArrey(0, GIndex) & " AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, StudCols, StudRows
            
            For SIndex = 0 To StudRows - 1
                If (CancelExecution) Then Exit For
                
                Main.sbMain.PanelText("2") = "����������� �������� ������ " & GroupName & " (" & (SIndex + 1) & " �� " & StudRows & ")"
                
                StudentName = GetFullName(CStr(StudArrey(1, SIndex)), CStr(StudArrey(2, SIndex)), CStr(StudArrey(3, SIndex)), False, False, True)
                
                                
                If ((Not AreStudentHaveDebts(CLng(StudArrey(0, SIndex)), Semester, IIf(ckUseLastSessionData.Value = 1, True, False))) And (CStr(StudArrey(6, SIndex)) <> "��������")) Then
                
                        CDB.SQLOpenTableToArrey "SELECT" & _
                        " tblSheets.Mark, tblEducationalPlan.KindOfReport, tblEducationalPlan.IsAdditional" & _
                        " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
                        " WHERE tblSheets.IDStudent = " & StudArrey(0, SIndex) & " AND" & _
                        " tblExaminations.ID = tblSheets.IDExam AND" & _
                        " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
                        " tblEducationalPlan.KindOfReport = '�������' AND" & _
                        " tblEducationalPlan.IsAdditional = 0 AND" & _
                        " tblEducationalPlan.Semester = " & Semester, ExamsArrey, ExamsCols, ExamsRows
                                       
                    If (ExamsRows > 0 Or ((CLng(comboCourse.ItemData(comboCourse.ListIndex)) = 1) And Month(GlobalData) = 8) Or ((CLng(comboCourse.ItemData(comboCourse.ListIndex)) = 5) And Month(GlobalData) = 8)) Then
                        MidValue = 0
                        Counter = 0
                        
                        For EIndex = 0 To ExamsRows - 1
                            fOk = True
                            
                            '����������� ��� ������------------------------------
                            'If (ExamsArrey(1, EIndex) = "�����") Then fOk = False
                            '----------------------------------------------------
                            
                            If (fOk) Then
                                MidValue = MidValue + CDbl(CByte(ExamsArrey(0, EIndex)))
                                Counter = Counter + 1
                            End If
                        Next EIndex
                    
                        If ((CLng(comboCourse.ItemData(comboCourse.ListIndex)) = 1 Or CLng(comboCourse.ItemData(comboCourse.ListIndex)) = 5) And Month(GlobalData) = 8) Then
                            Counter = 1
                            MidValue = 4
                        End If
                        If (Counter > 0) Then
                           
                            fOk = True
                            
                            ErrorDC = False
                            ErrorDesc = ""
                            
                            NominalGrantSum = 0
                            NominalGrantName = ""
                            SpecExtra = 0
                            MinGrant = 0
                            CategoryExtra = 0
                            CategoryName = ""
                            
                            TotalGrantSum = 0
    
'������� ��������� ��������-------------------------------------------------------------------------------------
                            CDB.SQLOpenTableToArrey "SELECT [GrantName], [GrantSize] FROM [tblNominalGrants] WHERE [ID] = " & CLng(StudArrey(5, SIndex)), DopArrey, DopCols, DopRows
                            
                            If (DopRows = 0) Then
                                ErrorDC = True
                                ErrorDesc = "������ ��� ��������� ������� ���������."
                            End If
                            
                            If (DopRows > 0) Then
                                NominalGrantName = CStr(DopArrey(0, 0))
                                NominalGrantSum = CCur(DopArrey(1, 0))
                            End If
'---------------------------------------------------------------------------------------------------------------
    
'�������� �������������-----------------------------------------------------------------------------------------
                            CDB.SQLOpenTableToArrey "SELECT [PercentExtra] FROM [tblSpecialities] WHERE [ID] = " & CLng(GroupsArrey(3, GIndex)), DopArrey, DopCols, DopRows
                            SpecExtra = CInt(DopArrey(0, 0))
                            If (CLng(comboCourse.ItemData(comboCourse.ListIndex)) = 5) Then SpecExtra = 0
'---------------------------------------------------------------------------------------------------------------
    
'����������� ���������------------------------------------------------------------------------------------------
                            CDB.SQLOpenTableToArrey "SELECT MIN(GrantSize) AS MinimumGrant FROM [tblGrants]", DopArrey, DopCols, DopRows
                            If (DopRows = 0) Then
                                ErrorDC = True
                                ErrorDesc = "������ ��� ������� ��������� ����������� ���������."
                            End If
                            If (DopRows > 0) Then MinGrant = CCur(DopArrey(0, 0))
'---------------------------------------------------------------------------------------------------------------
    
'������� ��� ��������-------------------------------------------------------------------------------------------
                            MidValue = MidValue / Counter
'---------------------------------------------------------------------------------------------------------------
    
'�����, ������� ��������� ������� �� ���� ������� ���-----------------------------------------------------------
                            MidValueStr = CStr(MidValue)
                            If (InStr(1, MidValueStr, ",")) Then Mid(MidValueStr, InStr(1, MidValueStr, ",")) = "."
                            
                            CDB.SQLOpenTableToArrey "SELECT [GrantSize] FROM [tblGrants] WHERE [Min] <= " & MidValueStr & " AND [Max] >= " & MidValueStr, DopArrey, DopCols, DopRows
                            ValueSum = -1
                            If (DopRows > 0) Then ValueSum = CCur(DopArrey(0, 0))
'---------------------------------------------------------------------------------------------------------------
    
'��������� ��������---------------------------------------------------------------------------------------------
                            CDB.SQLOpenTableToArrey "SELECT [CategoryName], [PercentExtra] FROM [tblCategories] WHERE [ID] = " & CLng(StudArrey(4, SIndex)), DopArrey, DopCols, DopRows
                            If (DopRows > 0) Then
                                CategoryName = CStr(DopArrey(0, 0))
                                CategoryExtra = CCur(DopArrey(1, 0))
                            End If
'---------------------------------------------------------------------------------------------------------------
    
    Dim SE As Currency
    Dim IsStarosta As Boolean
    SE = MinGrant * CategoryExtra / 100
    IsStarosta = False
    
    Dim IsCategory As Boolean
    IsCategory = False
    
'---------------------------------------------------------------------------------------------------------------
'CLng(StudArrey(5, SIndex)) - ������� ���������
'CLng(StudArrey(4, SIndex)) - ���������
    
'���� ������� �� ������� ����������,
'�� ����������� �� � ����� ���������
'� �� ������ ������ �� ���������
                            If ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) = 1) And (ValueSum = -1)) Then
                                TotalGrantSum = 0
'---------------------------------------------------------------------------------------------------------------------------------
    
'���� ������� ������� ���������� �
'�� ����������� �� � ����� ���������,
'�� ������ ���������� ������ �� ���������
                            ElseIf ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) = 1) And (ValueSum <> -1)) Then
                                TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100)
'---------------------------------------------------------------------------------------------------------------------------------
    
'���� ������� ������� ���������� �
'�� ����������� �� � ����� ���������
                            ElseIf ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) = 1)) Then
                                TotalGrantSum = NominalGrantSum
'---------------------------------------------------------------------------------------------------------------------------------
    
'���� ������� ������� ���������� �
'����������� � ��������� �������
                            ElseIf ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) = 2)) Then
                                'TotalGrantSum = NominalGrantSum + (MinGrant * CategoryExtra / 100)
                                TotalGrantSum = NominalGrantSum
                                IsStarosta = True
'---------------------------------------------------------------------------------------------------------------------------------
    
'���� ������� �� ������� ����������, ��
'����������� � ����� �� ���������
                            ElseIf ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) <> 1)) Then
                                
                                If (CLng(StudArrey(4, SIndex)) = 2) Then    '���� ������� - ��������
                                    If (ValueSum = -1) Then TotalGrantSum = 0
                                    If (ValueSum <> -1) Then
                                        'TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100) + (MinGrant * CategoryExtra / 100)
                                        TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100)
                                        IsStarosta = True
                                    End If
                                    
                                ElseIf (CLng(StudArrey(4, SIndex)) <> 2) Then
                                    If (ValueSum = -1) Then
                                        'TotalGrantSum = MinGrant + (MinGrant * SpecExtra / 100) + ((MinGrant + (MinGrant * SpecExtra / 100)) * CategoryExtra / 100)
                                        TotalGrantSum = MinGrant + (MinGrant * SpecExtra / 100)
                                    End If
                                    If (ValueSum <> -1) Then
                                        'TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100) + ((ValueSum + (ValueSum * SpecExtra / 100)) * CategoryExtra / 100)
                                        TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100)
                                    End If
                                    
                                    IsCategory = True
                                End If
                                
'���� ������� ������� ���������� �
'����������� � ������� ��������� (�� ��������)
                            ElseIf ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) <> 2)) Then
                                ErrorDC = True
                                ErrorDesc = "���������� ����������."
                            End If
'---------------------------------------------------------------------------------------------------------------
    
                            If (ErrorDC) Then
                                If (Not GroupAdd) Then
                                    Main.MainGrid.AddItem vbTab & GroupName
                                    GroupAdd = True
                                End If
                                
                                Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & ErrorDesc & vbTab & Format(MidValue, ".00") & vbTab & "������"
                                StudCounter = StudCounter + 1
                            End If
    
                            If ((TotalGrantSum > 0) And (Not ErrorDC)) Then
                                If (Not GroupAdd) Then
                                    Main.MainGrid.AddItem vbTab & GroupName
                                    If (SpecExtra > 0) Then Main.MainGrid.RowData(Main.MainGrid.Rows - 1) = "20"
                                    GroupAdd = True
                                End If
                                
                                If (Not ErrorDC) Then
                                    If ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) = 1)) Then
                                        Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & vbTab & Format(MidValue, ".00") & vbTab & Format(TotalGrantSum, ".00")
                                        If (SpecExtra > 0) Then Main.MainGrid.RowData(Main.MainGrid.Rows - 1) = "20"
                                        TotalSum = TotalSum + CCur(Format(TotalGrantSum, ".00"))
                                    End If
                                    If ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) = 1)) Then
                                        Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & NominalGrantName & vbTab & Format(MidValue, ".00") & vbTab & Format(TotalGrantSum, ".00")
                                        If (SpecExtra > 0) Then Main.MainGrid.RowData(Main.MainGrid.Rows - 1) = "20"
                                        TotalSum = TotalSum + CCur(Format(TotalGrantSum, ".00"))
                                    End If
                                    If ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) <> 1)) Then
                                        If (IsStarosta) Then
                                            Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & CategoryName & vbTab & Format(MidValue, ".00") & vbTab & Format(TotalGrantSum, ".00") & " + " & Format(SE, ".00")
                                            TotalSum = TotalSum + CCur(Format(TotalGrantSum, ".00")) + CCur(Format(SE, ".00"))
                                        ElseIf (IsCategory) Then
                                            Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & CategoryName & vbTab & Format(MidValue, ".00") & vbTab & Format(TotalGrantSum, ".00") & IIf(CategoryExtra <> 0, " + " & CategoryExtra & "%", "")
                                            TotalSum = TotalSum + CCur(Format(TotalGrantSum, ".00")) + ((CCur(TotalGrantSum) * CCur(CategoryExtra)) / CCur(100))
                                        End If
                                        If (SpecExtra > 0) Then Main.MainGrid.RowData(Main.MainGrid.Rows - 1) = "20"
                                    End If
                                    If ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) <> 1)) Then
                                        If (IsStarosta) Then
                                            Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & NominalGrantName & ", " & CategoryName & vbTab & Format(MidValue, ".00") & vbTab & Format(TotalGrantSum, ".00") & " + " & Format(SE, ".00")
                                            TotalSum = TotalSum + CCur(Format(TotalGrantSum, ".00")) + CCur(Format(SE, ".00"))
                                        ElseIf (IsCategory) Then
                                            Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & NominalGrantName & ", " & CategoryName & vbTab & Format(MidValue, ".00") & vbTab & Format(TotalGrantSum, ".00") & " + " & CategoryExtra & "%"
                                            TotalSum = TotalSum + CCur(Format(TotalGrantSum, ".00")) + ((CCur(TotalGrantSum) * CCur(CategoryExtra)) / CCur(100))
                                        End If
                                        If (SpecExtra > 0) Then Main.MainGrid.RowData(Main.MainGrid.Rows - 1) = "20"
                                    End If
                                    
                                    StudCounter = StudCounter + 1
                                End If
                            End If
                        End If
                    End If
                End If
                          
            Next SIndex
            
            StudCounter = 1
        Next GIndex
    End If
    
        Main.MainGrid.AddItem ""
        Main.MainGrid.AddItem vbTab & vbTab & "�����" & vbTab & vbTab & vbTab & TotalSum

        ProgBar Main.PProgress, 0
        Main.sbMain.RedrawPanel ("3")
        Main.sbMain.PanelText("2") = "������"
        Main.FullNodeData = "Query"
End Sub

Sub CreateNoGrantsList()
Dim StudentName As String

Dim GroupsArrey As Variant
Dim GroupsCols As Integer
Dim GroupsRows As Long

Dim StudArrey As Variant
Dim StudCols As Integer
Dim StudRows As Long

Dim ExamsArrey As Variant
Dim ExamsCols As Integer
Dim ExamsRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim Semester1 As Integer
Dim Semester2 As Integer

Dim IDSpec As Long
Dim IDGroup As Long

Dim GroupName As String

Dim GIndex As Long
Dim EIndex As Long
Dim SIndex As Long

Dim StudCounter As Long

StudCounter = 1
'����������� ������ �����//////////////////////////////////////////////////////////
    If (ckCourse.Value = 1) Then
        Semester1 = CLng(comboCourse.ItemData(comboCourse.ListIndex)) * 2
        Semester2 = Semester1 - 1
        
        If (ckSpec.Value = 1) Then
            IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                            " FROM [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblGroups.IDSpec = " & IDSpec & _
                                            " AND tblSpecialities.ID = tblGroups.IDSpec" & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester]", GroupsArrey, GroupsCols, GroupsRows
        
        ElseIf (ckSpec.Value = 0) Then
'            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [Semester] = " & Semester1 & " OR [Semester] = " & Semester2 & " ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
            
            CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                            " FROM [tblGroups], [tblSpecialities]" & _
                                            " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                            " AND( [tblGroups.Semester] = " & Semester1 & " OR [tblGroups.Semester] = " & Semester2 & ") ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester]", GroupsArrey, GroupsCols, GroupsRows
            
        End If
        
    ElseIf (ckSpec.Value = 1) Then
        IDSpec = CLng(comboSpec.ItemData(comboSpec.ListIndex))
'        CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [IDSpec] = " & IDSpec & " ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
        
        CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                        " FROM [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblGroups.IDSpec = " & IDSpec & _
                                        " AND tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12)" & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester]", GroupsArrey, GroupsCols, GroupsRows
        
    ElseIf (ckGroup.Value = 1) Then
        If (comboGroups.ListCount = 0) Then
            MsgBox "�������� ������", vbOKOnly, "������"
            Exit Sub
        End If
        If (comboGroups.ListCount > 0) Then
            IDGroup = CLng(comboGroups.ItemData(comboGroups.ListIndex))
            CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] WHERE [ID] = " & IDGroup & " ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows
        End If
        
    End If
    
    If ((ckCourse.Value = 0) And (ckSpec.Value = 0) And (ckGroup.Value = 0)) Then
'        CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester], [IDSpec] FROM [tblGroups] ORDER BY [IDSpec], [Semester]", GroupsArrey, GroupsCols, GroupsRows

        CDB.SQLOpenTableToArrey "SELECT [tblGroups.ID], [tblGroups.GroupName], [tblGroups.Semester], [tblGroups.IDSpec]" & _
                                        " FROM [tblGroups], [tblSpecialities]" & _
                                        " WHERE tblSpecialities.ID = tblGroups.IDSpec" & _
                                        " AND (tblGroups.Semester BETWEEN 1 AND 12)" & _
                                        " ORDER BY [tblSpecialities.PercentExtra], [tblGroups.IDSpec], [tblGroups.Semester], [tblGroups.GroupName]", GroupsArrey, GroupsCols, GroupsRows
    End If
'//////////////////////////////////////////////////////////////////////////////////

    If (GroupsRows = 0) Then
        MsgBox "� ���� ������ ��� ���������, ��������������� ������ �������."
        Exit Sub
    End If
    
    Main.ShowMainGrid 1
    
    Main.MainGrid.Cols = 4
    Main.MainGrid.Rows = 1
    Main.MainGrid.FixedCols = 1
    Main.MainGrid.FixedRows = 1
    
    Main.MainGrid.TextMatrix(0, 0) = "�"
    Main.MainGrid.TextMatrix(0, 1) = "������"
    Main.MainGrid.TextMatrix(0, 2) = "�. �. �. ��������"
    Main.MainGrid.TextMatrix(0, 3) = "����� ������"
    
    Dim ErrorDC As Boolean
    Dim ErrorDesc As String
    
    Dim MidValue As Double
    Dim MidValueStr As String
    Dim Counter As Long
    Dim GrantSum As Currency
    Dim TotalGrantSum As Currency
    
    Dim CategoryExtra As Currency
    Dim CategoryName As String
    
    Dim NominalGrantName As String
    Dim NominalGrantSum As Currency
    
    Dim SpecExtra As Integer
    Dim ValueSum As Currency
    Dim MinGrant As Currency
    
    Dim GroupAdd As Boolean
    
    Dim Semester As Byte
    
    Dim fOk As Boolean
    
    If (txtSemester.Enabled) Then
        If (ckCurSemester.Value = 0) Then Semester = CByte(txtSemester.text) - 1
        If (ckCurSemester.Value = 1) Then Semester = CByte(txtSemester.text)
    End If

    If (GroupsRows > 0) Then
    
        For GIndex = 0 To GroupsRows - 1
            DoEvents
            If (CancelExecution) Then Exit For
            
            GroupName = GroupsArrey(1, GIndex)
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupsArrey(2, GIndex)) Mod 2 = 1, (CLng(GroupsArrey(2, GIndex)) \ 2) + 1, CLng(GroupsArrey(2, GIndex)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
            GroupAdd = False
            
            Main.sbMain.PanelText("2") = "����������� �������� ������ " & GroupName
            ProgBar Main.PProgress, ((GIndex + 1) * 100) / GroupsRows
            Main.sbMain.RedrawPanel ("3")
            
            If (Not txtSemester.Enabled) Then
                If (ckCurSemester.Value = 0) Then Semester = CByte(GroupsArrey(2, GIndex)) - 1
                If (ckCurSemester.Value = 1) Then Semester = CByte(GroupsArrey(2, GIndex))
            End If
            
            CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic], [IDCategory], [IDNominalGrant], [FormOfPayment] FROM [tblStudents] WHERE [IDGroup] = " & GroupsArrey(0, GIndex) & " AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, StudCols, StudRows
            
            For SIndex = 0 To StudRows - 1
                If (CancelExecution) Then Exit For
                
                Main.sbMain.PanelText("2") = "����������� �������� ������ " & GroupName & " (" & (SIndex + 1) & " �� " & StudRows & ")"
                
                StudentName = GetFullName(CStr(StudArrey(1, SIndex)), CStr(StudArrey(2, SIndex)), CStr(StudArrey(3, SIndex)), False, False, True)
                
                If ((AreStudentHaveDebts(CLng(StudArrey(0, SIndex)), Semester, IIf(ckUseLastSessionData.Value = 1, True, False))) Or (CStr(StudArrey(6, SIndex)) = "��������")) Then
                    If (Not GroupAdd) Then
                        Main.MainGrid.AddItem vbTab & GroupName
                        GroupAdd = True
                    End If
                    
                    Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & CStr(StudArrey(6, SIndex))
                    StudCounter = StudCounter + 1
                
                Else
                    CDB.SQLOpenTableToArrey "SELECT" & _
                    " tblSheets.Mark, tblEducationalPlan.KindOfReport, tblEducationalPlan.IsAdditional" & _
                    " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
                    " WHERE tblSheets.IDStudent = " & StudArrey(0, SIndex) & " AND" & _
                    " tblExaminations.ID = tblSheets.IDExam AND" & _
                    " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
                    " tblEducationalPlan.KindOfReport = '�������' AND" & _
                    " tblEducationalPlan.IsAdditional = 0 AND" & _
                    " tblEducationalPlan.Semester = " & Semester, ExamsArrey, ExamsCols, ExamsRows
                    
                    If (ExamsRows > 0) Then
                        MidValue = 0
                        Counter = 0
                        
                        For EIndex = 0 To ExamsRows - 1
                            fOk = True
                            
                            '����������� ��� ������------------------------------
                            'If (ExamsArrey(1, EIndex) = "�����") Then fOk = False
                            '----------------------------------------------------
                            
                            If (fOk) Then
                                MidValue = MidValue + CDbl(CByte(ExamsArrey(0, EIndex)))
                                Counter = Counter + 1
                            End If
                        Next EIndex
                        
                        fOk = True
                        
                        ErrorDC = False
                        ErrorDesc = ""
                        
                        NominalGrantSum = 0
                        NominalGrantName = ""
                        SpecExtra = 0
                        MinGrant = 0
                        CategoryExtra = 0
                        CategoryName = ""
                        
                        TotalGrantSum = 0

'������� ��������� ��������-------------------------------------------------------------------------------------
                        CDB.SQLOpenTableToArrey "SELECT [GrantName], [GrantSize] FROM [tblNominalGrants] WHERE [ID] = " & CLng(StudArrey(5, SIndex)), DopArrey, DopCols, DopRows
                        
                        If (DopRows = 0) Then
                            ErrorDC = True
                            ErrorDesc = "������ ��� ��������� ������� ���������."
                        End If
                        
                        If (DopRows > 0) Then
                            NominalGrantName = CStr(DopArrey(0, 0))
                            NominalGrantSum = CCur(DopArrey(1, 0))
                        End If
'---------------------------------------------------------------------------------------------------------------

'�������� �������������-----------------------------------------------------------------------------------------
                        CDB.SQLOpenTableToArrey "SELECT [PercentExtra] FROM [tblSpecialities] WHERE [ID] = " & CLng(GroupsArrey(3, GIndex)), DopArrey, DopCols, DopRows
                        SpecExtra = CInt(DopArrey(0, 0))
'---------------------------------------------------------------------------------------------------------------

'����������� ���������------------------------------------------------------------------------------------------
                        CDB.SQLOpenTableToArrey "SELECT MIN(GrantSize) AS MinimumGrant FROM [tblGrants]", DopArrey, DopCols, DopRows
                        If (DopRows = 0) Then
                            ErrorDC = True
                            ErrorDesc = "������ ��� ������� ��������� ����������� ���������."
                        End If
                        If (DopRows > 0) Then MinGrant = CCur(DopArrey(0, 0))
'---------------------------------------------------------------------------------------------------------------

'������� ��� ��������-------------------------------------------------------------------------------------------
                        If (Counter > 0) Then
                            MidValue = MidValue / Counter
                        End If
'---------------------------------------------------------------------------------------------------------------

'�����, ������� ��������� ������� �� ���� ������� ���-----------------------------------------------------------
                        MidValueStr = CStr(MidValue)
                        If (InStr(1, MidValueStr, ",")) Then Mid(MidValueStr, InStr(1, MidValueStr, ",")) = "."
                        
                        CDB.SQLOpenTableToArrey "SELECT [GrantSize] FROM [tblGrants] WHERE [Min] <= " & MidValueStr & " AND [Max] >= " & MidValueStr, DopArrey, DopCols, DopRows
                        ValueSum = -1
                        If (DopRows > 0) Then ValueSum = CCur(DopArrey(0, 0))
'---------------------------------------------------------------------------------------------------------------

'��������� ��������---------------------------------------------------------------------------------------------
                        CDB.SQLOpenTableToArrey "SELECT [CategoryName], [PercentExtra] FROM [tblCategories] WHERE [ID] = " & CLng(StudArrey(4, SIndex)), DopArrey, DopCols, DopRows
                        If (DopRows > 0) Then
                            CategoryName = CStr(DopArrey(0, 0))
                            CategoryExtra = CCur(DopArrey(1, 0))
                        End If
'---------------------------------------------------------------------------------------------------------------

Dim SE As Currency
Dim IsStarosta As Boolean
SE = MinGrant * CategoryExtra / 100
IsStarosta = False

Dim IsCategory As Boolean
IsCategory = False

'---------------------------------------------------------------------------------------------------------------
'CLng(StudArrey(5, SIndex)) - ������� ���������
'CLng(StudArrey(4, SIndex)) - ���������

'���� ������� �� ������� ����������,
'�� ����������� �� � ����� ���������
'� �� ������ ������ �� ���������
                        If ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) = 1) And (ValueSum = -1)) Then
                            TotalGrantSum = 0
'---------------------------------------------------------------------------------------------------------------------------------

'���� ������� ������� ���������� �
'�� ����������� �� � ����� ���������,
'�� ������ ���������� ������ �� ���������
                        ElseIf ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) = 1) And (ValueSum <> -1)) Then
                            TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100)
'---------------------------------------------------------------------------------------------------------------------------------

'���� ������� ������� ���������� �
'�� ����������� �� � ����� ���������
                        ElseIf ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) = 1)) Then
                            TotalGrantSum = NominalGrantSum
'---------------------------------------------------------------------------------------------------------------------------------

'���� ������� ������� ���������� �
'����������� � ��������� �������
                        ElseIf ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) = 2)) Then
                            'TotalGrantSum = NominalGrantSum + (MinGrant * CategoryExtra / 100)
                            TotalGrantSum = NominalGrantSum
                            IsStarosta = True
'---------------------------------------------------------------------------------------------------------------------------------

'���� ������� �� ������� ����������, ��
'����������� � ����� �� ���������
                        ElseIf ((CLng(StudArrey(5, SIndex)) = 1) And (CLng(StudArrey(4, SIndex)) <> 1)) Then
                            
                            If (CLng(StudArrey(4, SIndex)) = 2) Then    '���� ������� - ��������
                                If (ValueSum = -1) Then TotalGrantSum = 0
                                If (ValueSum <> -1) Then
                                    'TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100) + (MinGrant * CategoryExtra / 100)
                                    TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100)
                                    IsStarosta = True
                                End If
                                
                            ElseIf (CLng(StudArrey(4, SIndex)) <> 2) Then
                                If (ValueSum = -1) Then
                                    'TotalGrantSum = MinGrant + (MinGrant * SpecExtra / 100) + ((MinGrant + (MinGrant * SpecExtra / 100)) * CategoryExtra / 100)
                                    TotalGrantSum = MinGrant + (MinGrant * SpecExtra / 100)
                                End If
                                If (ValueSum <> -1) Then
                                    'TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100) + ((ValueSum + (ValueSum * SpecExtra / 100)) * CategoryExtra / 100)
                                    TotalGrantSum = ValueSum + (ValueSum * SpecExtra / 100)
                                End If
                                IsCategory = True
                            End If
                            
'���� ������� ������� ���������� �
'����������� � ������� ��������� (�� ��������)
                        ElseIf ((CLng(StudArrey(5, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) <> 1) And (CLng(StudArrey(4, SIndex)) <> 2)) Then
                            ErrorDC = True
                            ErrorDesc = "���������� ����������."
                        End If
'---------------------------------------------------------------------------------------------------------------

                        If (ErrorDC) Then
                            If (Not GroupAdd) Then
                                Main.MainGrid.AddItem vbTab & GroupName
                                GroupAdd = True
                            End If
                            
                            Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & " " & ErrorDesc & vbTab & CStr(StudArrey(6, SIndex))
                            StudCounter = StudCounter + 1
                        End If

                        If ((TotalGrantSum = 0) And (Not ErrorDC)) Then
                            If (Not GroupAdd) Then
                                Main.MainGrid.AddItem vbTab & GroupName
                                GroupAdd = True
                            End If
                            
                            Main.MainGrid.AddItem StudCounter & vbTab & vbTab & StudentName & vbTab & CStr(StudArrey(6, SIndex))
                            StudCounter = StudCounter + 1
                        End If
                    End If
                End If
            
            Next SIndex
            
            StudCounter = 1
        Next GIndex
    End If
    
        ProgBar Main.PProgress, 0
        Main.sbMain.RedrawPanel ("3")
        Main.sbMain.PanelText("2") = "������"
        Main.FullNodeData = "Query"
End Sub

Sub FillGroupList(IDSpec As Long)
Dim GroupsArrey As Variant
Dim GCols As Integer
Dim GRows As Long

Dim GroupName As String

    CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester] FROM [tblGroups] WHERE ([Semester] BETWEEN 1 AND 12) AND [IDSpec] = " & IDSpec & " ORDER BY [Semester]", GroupsArrey, GCols, GRows
    comboGroups.Clear
    If (GRows > 0) Then

        Dim i As Long

        For i = 0 To GRows - 1
            GroupName = GroupsArrey(1, i)
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupsArrey(2, i)) Mod 2 = 1, (CLng(GroupsArrey(2, i)) \ 2) + 1, CLng(GroupsArrey(2, i)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))

            comboGroups.AddItem GroupName
            comboGroups.ItemData(comboGroups.NewIndex) = GroupsArrey(0, i)
        Next i

        comboGroups.ListIndex = 0
    End If
End Sub

Sub FillLectList(IDDep As Long)
Dim LectArrey As Variant
Dim LCols As Integer
Dim LRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblLecturers] WHERE [IDFaculty] = " & IDDep & " ORDER BY [Surname], [Name], [Patronymic]", LectArrey, LCols, LRows
    comboLecturer.Clear
    If (LRows > 0) Then

        Dim i As Long

        For i = 0 To LRows - 1
            comboLecturer.AddItem LectArrey(1, i) & " " & LectArrey(2, i) & " " & LectArrey(3, i)
            comboLecturer.ItemData(comboLecturer.NewIndex) = LectArrey(0, i)
        Next i

        comboLecturer.ListIndex = 0
    End If
End Sub

Sub FillSpecialityList()
Dim SpecArrey As Variant
Dim SCols As Integer
Dim SRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [SpecName] FROM [tblSpecialities] ORDER BY [SpecName]", SpecArrey, SCols, SRows
    comboSpec.Clear
    If (SRows > 0) Then

        Dim i As Long

        For i = 0 To SRows - 1
            comboSpec.AddItem SpecArrey(1, i)
            comboSpec.ItemData(comboSpec.NewIndex) = SpecArrey(0, i)
        Next i

        comboSpec.ListIndex = 0
    End If
End Sub

Sub FillCourseList()
    comboCourse.AddItem "1 ����"
    comboCourse.ItemData(comboCourse.NewIndex) = 1
    
    comboCourse.AddItem "2 ����"
    comboCourse.ItemData(comboCourse.NewIndex) = 2
    
    comboCourse.AddItem "3 ����"
    comboCourse.ItemData(comboCourse.NewIndex) = 3
    
    comboCourse.AddItem "4 ����"
    comboCourse.ItemData(comboCourse.NewIndex) = 4
    
    comboCourse.AddItem "5 ����"
    comboCourse.ItemData(comboCourse.NewIndex) = 5
    
    comboCourse.AddItem "6 ����"
    comboCourse.ItemData(comboCourse.NewIndex) = 6
    
    comboCourse.ListIndex = 0
End Sub

Sub FillFacultyList()
Dim FacultyArrey As Variant
Dim FCols As Integer
Dim FRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [FacultyName] FROM [tblFaculty]", FacultyArrey, FCols, FRows
    comboDep.Clear
    If (FRows > 0) Then

        Dim i As Long

        For i = 0 To FRows - 1
            If (FacultyArrey(0, i) <> 1) Then
                comboDep.AddItem FacultyArrey(1, i)
                comboDep.ItemData(comboDep.NewIndex) = FacultyArrey(0, i)
            End If
        Next i

        comboDep.ListIndex = 0
    End If
End Sub

Private Sub ckCourse_Click()
If (ckCourse.Value = 1) Then
    ckGroup = 0
    ckGroup.Enabled = False
    comboGroups.Enabled = False
    txtSemester.Enabled = False
    
    ckSpec.Enabled = True
    comboSpec.Enabled = True
End If

If (ckCourse.Value = 0) Then
    If (ckSpec.Value = 1) Then
        ckGroup.Enabled = False
        comboGroups.Enabled = False
    End If
    If (ckSpec.Value = 0) Then
        ckGroup.Enabled = True
        comboGroups.Enabled = True
    End If
End If

Analyzer
End Sub

Private Sub ckCurSemester_Click()
    Analyzer
End Sub

Private Sub ckDep_Click()
If (ckDep.Value = 1) Then
    ckLecturer.Enabled = False
    comboLecturer.Enabled = False
End If

If (ckDep.Value = 0) Then
    ckLecturer.Enabled = True
    comboLecturer.Enabled = True
End If

Analyzer
End Sub

Private Sub ckGroup_Click()
If (ckGroup.Value = 1) Then
    ckCourse.Value = 0
    ckCourse.Enabled = False
    comboCourse.Enabled = False
    txtSemester.Enabled = True
    
    ckSpec.Value = 0
    ckSpec.Enabled = False
    comboSpec.Enabled = True
End If

If (ckGroup.Value = 0) Then
    ckCourse.Enabled = True
    comboCourse.Enabled = True
    txtSemester.Enabled = False
    
    ckSpec.Enabled = True
    comboSpec.Enabled = True
End If

Analyzer
End Sub

Private Sub ckLecturer_Click()
If (ckLecturer.Value = 1) Then
    ckDep.Value = 0
    ckDep.Enabled = False
    comboDep.Enabled = True
End If

If (ckLecturer.Value = 0) Then
    ckDep.Enabled = True
    comboDep.Enabled = True
End If

Analyzer
End Sub

Private Sub ckSpec_Click()
If (ckSpec.Value = 1) Then
    ckGroup.Enabled = False
    comboGroups.Enabled = False
    txtSemester.Enabled = False
    
    ckCourse.Enabled = True
    comboCourse.Enabled = True
End If

If (ckSpec.Value = 0) Then
    If (ckCourse.Value = 1) Then
        ckGroup.Enabled = False
        comboGroups.Enabled = False
    End If
    If (ckCourse.Value = 0) Then
        ckGroup.Enabled = True
        comboGroups.Enabled = True
    End If
End If

Analyzer
End Sub

Private Sub cmdCancel_Click()
CancelExecution = True
Unload Me
End Sub


Private Sub cmdOK_Click()

CancelExecution = False
If ((txtSemester.Enabled) And (txtSemester.text = "")) Then Exit Sub
If ((txtSemester.Enabled) And (Not IsNumeric(txtSemester.text))) Then Exit Sub

If (ChosenQuery = 1) Then   '�������������
    cmdCancel.Caption = "���� !"
    CreateDebtsList
    If (Main.MainGrid.Cols > 0) Then Main.MainGrid.AutoSize 0, Main.MainGrid.Cols - 1
    
    Unload Me
End If

If (ChosenQuery = 3) Then   '���������� ���������
    If (ckNoGrant.Value = 0) Then
        cmdCancel.Caption = "���� !"
        CreateGrantsList
        If (Main.MainGrid.Cols > 0) Then Main.MainGrid.AutoSize 0, Main.MainGrid.Cols - 1
      
         Unload Me
         'frmDebts.Hide
        
    Else
        cmdCancel.Caption = "���� !"
        CreateNoGrantsList
        Main.NoGrant = False
        If (Main.MainGrid.Cols > 0) Then Main.MainGrid.AutoSize 0, Main.MainGrid.Cols - 1
        
        Unload Me
        
    End If
End If

If (ChosenQuery = 9 Or ChosenQuery = 10) Then
    cmdCancel.Caption = "���� !"
    CreateDiplomes IIf(ChosenQuery = 9, True, False)
    If (Main.MainGrid.Cols > 0) Then Main.MainGrid.AutoSize 0, Main.MainGrid.Cols - 1
    
    Unload Me
End If

If (ChosenQuery = 11) Then
    cmdCancel.Caption = "���� !"
    CreateSheetBook
    If (Main.MainGrid.Cols > 0) Then Main.MainGrid.AutoSize 0, Main.MainGrid.Cols - 1
    
    Unload Me
End If

End Sub

Private Sub comboCourse_Click()
Analyzer
End Sub

Private Sub comboDep_Click()
If (comboDep.ListIndex < 0) Then Exit Sub
FillLectList comboDep.ItemData(comboDep.ListIndex)
Analyzer
End Sub

Private Sub comboGroups_Click()
Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

If (comboGroups.ListIndex < 0) Then Exit Sub

CDB.SQLOpenTableToArrey "SELECT [Semester] FROM [tblGroups] WHERE [ID] = " & comboGroups.ItemData(comboGroups.ListIndex), DopArrey, DopCols, DopRows
If (Not IsNull(DopArrey(0, 0))) Then txtSemester.text = CStr(DopArrey(0, 0))

Analyzer
End Sub

Private Sub comboLecturer_Click()
Analyzer
End Sub

Private Sub comboSpec_Click()
If (comboSpec.ListIndex < 0) Then Exit Sub
FillGroupList comboSpec.ItemData(comboSpec.ListIndex)
Analyzer
End Sub

Private Sub Form_Load()

ckNoGrant.Enabled = True
ckCurSemester.Enabled = True
ckUseLastSessionData.Enabled = True

MinDebts = 0: MaxDebts = 200
If (ChosenQuery = 1) Then
    Me.Caption = "�������������"
    FillCourseList
    
    FillSpecialityList
    If (comboSpec.ListCount = 0) Then
        MsgBox "��� �� ����� ������������� �� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    
    FillFacultyList
    If (comboDep.ListCount = 0) Then
        MsgBox "��� �� ����� ������� �� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    
    ckNoGrant.Enabled = False: ckNoGrant.Value = 2
    
ElseIf (ChosenQuery = 2) Then
ElseIf (ChosenQuery = 3) Then
    Me.Caption = "���������� ���������"
    FillCourseList
    
    FillSpecialityList
    If (comboSpec.ListCount = 0) Then
        MsgBox "��� �� ����� ������������� �� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    
    ckDep.Visible = False
    ckLecturer.Visible = False
    comboDep.Visible = False
    comboLecturer.Visible = False
    
    F1.Height = 1695
    F2.Top = 1680
    Me.Height = 4930

ElseIf (ChosenQuery = 4) Then

ElseIf (ChosenQuery = 9 Or ChosenQuery = 10) Then
    Me.Caption = IIf(ChosenQuery = 9, "������� �������", "����� ������ �� ��������")
    FillCourseList
    
    FillSpecialityList
    If (comboSpec.ListCount = 0) Then
        MsgBox "��� �� ����� ������������� �� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    
    ckDep.Visible = False
    ckLecturer.Visible = False
    comboDep.Visible = False
    comboLecturer.Visible = False
    ckNoGrant.Enabled = False: ckNoGrant.Value = 2
    ckCurSemester.Enabled = False: ckCurSemester.Value = 2
    ckUseLastSessionData.Enabled = False: ckUseLastSessionData.Value = 2

    F1.Height = 1695
    F2.Top = 1680
    Me.Height = 4930

ElseIf (ChosenQuery = 11) Then
    Me.Caption = "�������� ������"
    FillCourseList
    
    FillSpecialityList
    If (comboSpec.ListCount = 0) Then
        MsgBox "��� �� ����� ������������� �� ����������", vbOKOnly, "������"
        Exit Sub
    End If
    
    ckDep.Visible = False
    ckLecturer.Visible = False
    comboDep.Visible = False
    comboLecturer.Visible = False
    ckNoGrant.Enabled = False: ckNoGrant.Value = 2
    ckCurSemester.Enabled = True: ckCurSemester.Value = 1
    ckUseLastSessionData.Enabled = True: ckUseLastSessionData.Value = 0

    F1.Height = 1695
    F2.Top = 1680
    Me.Height = 4930

End If

LoadSuccess = True
End Sub

Private Sub txtMinDebts_LostFocus()
If Not IsNumeric(txtMinDebts.text) Then
    MsgBox "��� ���� ������ ��������� ��������������� �����", vbExclamation Or vbOKOnly, "DOWS"
    txtMinDebts.text = MinDebts
ElseIf CInt(txtMinDebts.text) < 0 Then
    MsgBox "��� ���� ������ ��������� ��������������� �����", vbExclamation Or vbOKOnly, "DOWS"
    txtMinDebts.text = MinDebts
ElseIf CInt(txtMinDebts.text) > MaxDebts Then
    MsgBox "����������� ���������� ������ �� ������ ���� ������ �������������", vbExclamation Or vbOKOnly, "DOWS"
    txtMinDebts.text = MinDebts
Else
    MinDebts = CInt(txtMinDebts.text)
End If
Analyzer
End Sub

Private Sub txtMaxDebts_LostFocus()
If Not IsNumeric(txtMaxDebts.text) Then
    MsgBox "��� ���� ������ ��������� ��������������� �����", vbExclamation Or vbOKOnly, "DOWS"
    txtMaxDebts.text = MaxDebts
ElseIf CInt(txtMaxDebts.text) < 0 Then
    MsgBox "��� ���� ������ ��������� ��������������� �����", vbExclamation Or vbOKOnly, "DOWS"
    txtMinDebts.text = MinDebts
ElseIf CInt(txtMaxDebts.text) < MinDebts Then
    MsgBox "������������ ���������� ������ �� ������ ���� ������ ������������", vbExclamation Or vbOKOnly, "DOWS"
    txtMaxDebts.text = MaxDebts
Else
    MaxDebts = CInt(txtMaxDebts.text)
End If
Analyzer
End Sub

Private Sub txtSemester_Change()
Analyzer
End Sub

