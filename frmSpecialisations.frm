VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Begin VB.Form frmSpecialisations 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "�������������"
   ClientHeight    =   3495
   ClientLeft      =   2880
   ClientTop       =   3045
   ClientWidth     =   8340
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3495
   ScaleWidth      =   8340
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F4 
      Height          =   3495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8295
      Begin VB.TextBox txtSpecialisationName 
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   3120
         Width           =   6375
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "��������"
         Height          =   360
         Left            =   6600
         TabIndex        =   3
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cmdDel 
         Caption         =   "�������"
         Height          =   360
         Left            =   6600
         TabIndex        =   2
         Top             =   600
         Width           =   1575
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   375
         Left            =   6600
         TabIndex        =   1
         Top             =   3000
         Width           =   1575
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblSpecialisations 
         Height          =   2745
         Left            =   120
         TabIndex        =   5
         Top             =   225
         Width           =   6375
         _cx             =   11245
         _cy             =   4842
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
End
Attribute VB_Name = "frmSpecialisations"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim BefDataSpecialisation As Variant

Sub LoadSpecialisationsList()
Dim i As Long
'�������������------------------------------------------------------------------------------------------------------------
    Dim SpecialisationsArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long

    tblSpecialisations.Rows = 1
    
    CDB.SQLOpenTableToArrey "SELECT [ID], [Specialisation] FROM [tblSpecialisations] ORDER BY [Specialisation]", SpecialisationsArrey, SCols, SRows
    
    If (SRows > 0) Then
        For i = 0 To SRows - 1
            tblSpecialisations.AddItem tblSpecialisations.Rows & vbTab & CStr(SpecialisationsArrey(1, i))
            tblSpecialisations.RowData(tblSpecialisations.Rows - 1) = SpecialisationsArrey(0, i)
        Next i
    End If
'-------------------------------------------------------------------------------------------------------------------------
End Sub


Private Sub cmdAdd_Click()
Dim SpecialisationName As String

    SpecialisationName = txtSpecialisationName.Text
    If (FindErrors(SpecialisationName, 200, "�������� ��������", True, True)) Then Exit Sub
    
    CheckString SpecialisationName
    
If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblSpecialisations] WHERE [Specialisation] = '" & SpecialisationName & "'") > 0) Then
    MsgBox "������������� � ����� ��������� ��� ����������", vbOKOnly, "������"
    Exit Sub
End If

Dim AR As Long
AR = CDB.ExecSQLActionQuery("INSERT INTO tblSpecialisations(Specialisation) VALUES ('" & SpecialisationName & "')", 128)

LoadSpecialisationsList
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdDel_Click()
Dim AR As Long

If (tblSpecialisations.Row = -1) Then Exit Sub

If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblStudents] WHERE [IDSpecialisation] = " & tblSpecialisations.RowData(tblSpecialisations.Row)) > 0) Then
    MsgBox "��� ������ �� ����� ���� �������, �.�. ������������ � ������� ���������", vbOKOnly, "������"
    Exit Sub
End If

AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSpecialisations] WHERE [ID] = " & tblSpecialisations.RowData(tblSpecialisations.Row), 128)
tblSpecialisations.RemoveItem (tblSpecialisations.Row)
EnumerateColumns tblSpecialisations, 0

End Sub

Private Sub Form_Load()
With tblSpecialisations
    .Cols = 2
    .Rows = 1
    .FixedRows = 1
    .FixedCols = 1
    
    .TextMatrix(0, 0) = "�"
    .TextMatrix(0, 1) = "�������� �������������"
    
    .ColWidth(0) = 500
    .ColWidth(1) = 5500
End With

LoadSpecialisationsList

LoadSuccess = True
End Sub

Private Sub tblSpecialisations_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim AR As Long
Dim SpecialisationName As String

    If (Col = 1) Then
    
        SpecialisationName = tblSpecialisations.TextMatrix(Row, Col)
        
        If (FindErrors(SpecialisationName, 255, "�������� �������������", True, True)) Then
            tblSpecialisations.TextMatrix(Row, Col) = BefDataSpecialisation
            Exit Sub
        End If
        
        CheckString SpecialisationName
        
        AR = CDB.ExecSQLActionQuery("UPDATE [tblSpecialisations] SET [Specialisation] = '" & SpecialisationName & "' WHERE [ID] = " & tblSpecialisations.RowData(Row), 128)
        
    End If
        
End Sub

Private Sub tblSpecialisations_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefDataSpecialisation = tblSpecialisations.TextMatrix(Row, Col)
End Sub

