VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Object = "{F5C0D7B0-1CEA-11D7-8C70-97E62479B906}#1.0#0"; "ABTabStrip.ocx"
Begin VB.Form frmViewArchive 
   Caption         =   "�������� ������"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10980
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   10980
   StartUpPosition =   3  'Windows Default
   Begin ABTabStrip.TabControl StudInfoTab 
      Height          =   7935
      Left            =   3240
      TabIndex        =   1
      Top             =   240
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   13996
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabAlign        =   2
      Begin VB.Frame T2T5 
         Caption         =   "�������� ���������� �� ��������"
         Height          =   1935
         Left            =   480
         TabIndex        =   13
         Top             =   1080
         Width           =   8895
         Begin VSFlex7DAOCtl.VSFlexGrid tblSSInfo 
            Height          =   1575
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Width           =   8655
            _cx             =   15266
            _cy             =   2778
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   0
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483633
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483633
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame T2T4 
         Caption         =   "���������� � �������"
         Height          =   2295
         Left            =   480
         TabIndex        =   23
         Top             =   1080
         Width           =   4095
         Begin VB.CommandButton cmdDASides 
            Caption         =   "������� ������� ����������"
            Height          =   320
            Left            =   120
            TabIndex        =   24
            Top             =   1920
            Width           =   2655
         End
         Begin VSFlex7DAOCtl.VSFlexGrid tblFrontSide 
            Height          =   1575
            Left            =   240
            TabIndex        =   25
            Top             =   360
            Visible         =   0   'False
            Width           =   3015
            _cx             =   5318
            _cy             =   2778
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   3
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   1
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   1
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   -1  'True
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   3
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            Begin VSFlex7DAOCtl.VSFlexGrid DASelect 
               Height          =   735
               Left            =   480
               TabIndex        =   26
               Top             =   720
               Visible         =   0   'False
               Width           =   2415
               _cx             =   4260
               _cy             =   1296
               _ConvInfo       =   -1
               Appearance      =   1
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   -2147483635
               ForeColorSel    =   -2147483634
               BackColorBkg    =   -2147483636
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   1
               HighLight       =   1
               AllowSelection  =   0   'False
               AllowBigSelection=   0   'False
               AllowUserResizing=   1
               SelectionMode   =   1
               GridLines       =   1
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   0
               Cols            =   0
               FixedRows       =   0
               FixedCols       =   0
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   0   'False
               FormatString    =   ""
               ScrollTrack     =   -1  'True
               ScrollBars      =   3
               ScrollTips      =   0   'False
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   0   'False
               AutoSizeMode    =   0
               AutoSearch      =   0
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   0
               ExplorerBar     =   0
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               DataMode        =   0
               VirtualData     =   -1  'True
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
            End
         End
         Begin VSFlex7DAOCtl.VSFlexGrid tblEducationalResults 
            Height          =   1575
            Left            =   120
            TabIndex        =   27
            Top             =   240
            Width           =   3015
            _cx             =   5318
            _cy             =   2778
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   1
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   3
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame T2T3 
         Caption         =   "�������������"
         Height          =   2295
         Left            =   600
         TabIndex        =   28
         Top             =   1320
         Width           =   3975
         Begin VSFlex7DAOCtl.VSFlexGrid tblFSInfo 
            Height          =   1575
            Left            =   240
            TabIndex        =   29
            Top             =   240
            Width           =   3015
            _cx             =   5318
            _cy             =   2778
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   0
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483639
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483633
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483633
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   300
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   -1  'True
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame T2T2 
         Caption         =   "���������"
         Height          =   5775
         Left            =   240
         TabIndex        =   30
         Top             =   600
         Width           =   6735
         Begin VSFlex7DAOCtl.VSFlexGrid tblProgressT 
            Height          =   4935
            Left            =   120
            TabIndex        =   31
            Top             =   240
            Width           =   6495
            _cx             =   11456
            _cy             =   8705
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   3
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame T2T1 
         Caption         =   "���. ������������"
         Height          =   5775
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   6735
         Begin VB.Frame FS4 
            Caption         =   "���������� � ���������"
            Height          =   975
            Left            =   120
            TabIndex        =   4
            Top             =   4680
            Width           =   6495
            Begin VB.Label �����30 
               Caption         =   "������������� :"
               Height          =   255
               Left            =   120
               TabIndex        =   5
               Top             =   240
               Width           =   1335
            End
            Begin VB.Label lblLect 
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   1560
               TabIndex        =   10
               Top             =   600
               Width           =   4815
            End
            Begin VB.Label Label2 
               Caption         =   "������������� :"
               Height          =   255
               Left            =   120
               TabIndex        =   9
               Top             =   600
               Width           =   1335
            End
            Begin VB.Label lblGroup 
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   5160
               TabIndex        =   8
               Top             =   240
               Width           =   1215
            End
            Begin VB.Label lbgrupcapt 
               Caption         =   "������ :"
               Height          =   255
               Left            =   4440
               TabIndex        =   7
               Top             =   240
               Width           =   735
            End
            Begin VB.Label lblSpec 
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Left            =   1560
               TabIndex        =   6
               Top             =   240
               Width           =   2775
            End
         End
         Begin VB.CommandButton cmdSave 
            Caption         =   "��������� ��� html - ����"
            Height          =   375
            Left            =   120
            TabIndex        =   3
            Top             =   4200
            Width           =   6495
         End
         Begin VSFlex7DAOCtl.VSFlexGrid tblProgress 
            Height          =   3855
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   6495
            _cx             =   11456
            _cy             =   6800
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   3
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            Begin VSFlex7DAOCtl.VSFlexGrid ExtblStudents2 
               Height          =   375
               Left            =   5040
               TabIndex        =   12
               Top             =   3360
               Visible         =   0   'False
               Width           =   1335
               _cx             =   2355
               _cy             =   661
               _ConvInfo       =   -1
               Appearance      =   1
               BorderStyle     =   1
               Enabled         =   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   204
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MousePointer    =   0
               BackColor       =   -2147483643
               ForeColor       =   -2147483640
               BackColorFixed  =   -2147483633
               ForeColorFixed  =   -2147483630
               BackColorSel    =   -2147483635
               ForeColorSel    =   -2147483634
               BackColorBkg    =   -2147483636
               BackColorAlternate=   -2147483643
               GridColor       =   -2147483633
               GridColorFixed  =   -2147483632
               TreeColor       =   -2147483632
               FloodColor      =   192
               SheetBorder     =   -2147483642
               FocusRect       =   1
               HighLight       =   1
               AllowSelection  =   -1  'True
               AllowBigSelection=   -1  'True
               AllowUserResizing=   0
               SelectionMode   =   0
               GridLines       =   1
               GridLinesFixed  =   2
               GridLineWidth   =   1
               Rows            =   50
               Cols            =   10
               FixedRows       =   1
               FixedCols       =   1
               RowHeightMin    =   0
               RowHeightMax    =   0
               ColWidthMin     =   0
               ColWidthMax     =   0
               ExtendLastCol   =   0   'False
               FormatString    =   ""
               ScrollTrack     =   0   'False
               ScrollBars      =   3
               ScrollTips      =   0   'False
               MergeCells      =   0
               MergeCompare    =   0
               AutoResize      =   -1  'True
               AutoSizeMode    =   0
               AutoSearch      =   0
               AutoSearchDelay =   2
               MultiTotals     =   -1  'True
               SubtotalPosition=   1
               OutlineBar      =   0
               OutlineCol      =   0
               Ellipsis        =   0
               ExplorerBar     =   0
               PicturesOver    =   0   'False
               FillStyle       =   0
               RightToLeft     =   0   'False
               PictureType     =   0
               TabBehavior     =   0
               OwnerDraw       =   0
               Editable        =   0
               ShowComboButton =   1
               WordWrap        =   0   'False
               TextStyle       =   0
               TextStyleFixed  =   0
               OleDragMode     =   0
               OleDropMode     =   0
               DataMode        =   0
               VirtualData     =   -1  'True
               ComboSearch     =   3
               AutoSizeMouse   =   -1  'True
               FrozenRows      =   0
               FrozenCols      =   0
               AllowUserFreezing=   0
               BackColorFrozen =   0
               ForeColorFrozen =   0
               WallPaperAlignment=   9
            End
         End
      End
      Begin VB.Frame T2T6 
         Caption         =   "�������"
         Height          =   2055
         Left            =   840
         TabIndex        =   17
         Top             =   1560
         Width           =   3855
         Begin VB.Frame T2T6T1 
            BorderStyle     =   0  'None
            Height          =   320
            Left            =   120
            TabIndex        =   18
            Top             =   1440
            Width           =   3615
            Begin VB.TextBox txtEvText 
               Height          =   285
               Left            =   0
               TabIndex        =   21
               Top             =   15
               Width           =   1575
            End
            Begin VB.CommandButton cmdEvAdd 
               Caption         =   "��������"
               Height          =   315
               Left            =   1680
               TabIndex        =   20
               Top             =   0
               Width           =   975
            End
            Begin VB.CommandButton cmdEvDel 
               Caption         =   "�������"
               Height          =   315
               Left            =   2640
               TabIndex        =   19
               Top             =   0
               Width           =   975
            End
         End
         Begin VSFlex7DAOCtl.VSFlexGrid tblEvents 
            Height          =   1095
            Left            =   120
            TabIndex        =   22
            Top             =   240
            Width           =   3615
            _cx             =   6376
            _cy             =   1931
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   3
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   1
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   -1  'True
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   3
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame T2T7 
         Caption         =   "����������� �����"
         Height          =   5775
         Left            =   360
         TabIndex        =   15
         Top             =   840
         Width           =   6735
         Begin VSFlex7DAOCtl.VSFlexGrid tblProgressI 
            Height          =   4935
            Left            =   120
            TabIndex        =   16
            Top             =   240
            Width           =   6495
            _cx             =   11456
            _cy             =   8705
            _ConvInfo       =   -1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   0   'False
            AllowBigSelection=   0   'False
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   0
            Cols            =   0
            FixedRows       =   0
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   ""
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            DataMode        =   0
            VirtualData     =   -1  'True
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   3
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
   End
   Begin VB.Frame FS1 
      BorderStyle     =   0  'None
      Height          =   7095
      Left            =   120
      TabIndex        =   32
      Top             =   240
      Width           =   2895
      Begin VB.OptionButton optFIO 
         Caption         =   "�� �.�.�."
         Height          =   255
         Left            =   1320
         TabIndex        =   36
         Top             =   480
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optYear 
         Caption         =   "�� ����"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   480
         Width           =   855
      End
      Begin VB.TextBox txtFIO 
         Height          =   285
         Left            =   120
         TabIndex        =   33
         Top             =   120
         Width           =   2655
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblFIO 
         Height          =   6105
         Left            =   120
         TabIndex        =   34
         Top             =   840
         Width           =   2655
         _cx             =   4683
         _cy             =   10769
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483643
         GridColorFixed  =   -2147483643
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483643
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Label B 
      BorderStyle     =   1  'Fixed Single
      Height          =   7095
      Left            =   3120
      MousePointer    =   9  'Size W E
      TabIndex        =   0
      Top             =   240
      Width           =   45
   End
End
Attribute VB_Name = "frmViewArchive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private BeforeX As Integer
Private BeforeY As Integer
Private Border As Boolean
Private Sub B_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
Border = True
End Sub

Private Sub B_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
If (Button <> 0) Then
    Dim L As Long
    Dim Point As POINTAPI
    L = GetCursorPos(Point)
    Point.x = Point.x - (Me.Left \ 15)
    
    If ((Point.x * 15) < 1900) Then Exit Sub
    If ((Point.x * 15) > Me.Width - 1900) Then Exit Sub
    
    B.Left = Point.x * 15 - B.Width
    Form_Resize
End If
End Sub

Private Sub B_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
Border = False
End Sub


Private Sub cmdDASides_Click()
    If (cmdDASides.Caption = "������� ������� ����������") Then
        tblFrontSide.ZOrder
        tblEducationalResults.Visible = False
        tblFrontSide.Visible = True
        cmdDASides.Caption = "�������� �� ������������"
    Else
        tblEducationalResults.ZOrder
        tblEducationalResults.Visible = True
        tblFrontSide.Visible = False
        cmdDASides.Caption = "������� ������� ����������"
    End If
End Sub

Private Sub Form_Load()
    CurArcPath = ""
    ArcLoaded = arc.OpenArchive _
    ("UDBArc", "", MainDBPass, "SCOMP", Main.CommonDialog1, CurArcPath)
    
    If Not ArcLoaded Then
        MsgBox "���� ������ �� ��� ������!", vbCritical, "������"
        Exit Sub
    Else
        Me.Caption = "�������� ������ " & CurArcPath
    End If

    StudInfoTab.AddTab "������ ����������"
    StudInfoTab.AddTab "������������"
    StudInfoTab.AddTab "���������"
    StudInfoTab.AddTab "����������� �����"
    StudInfoTab.AddTab "�������"
    StudInfoTab.AddTab "���������� � �������"
    
    tblProgress.Rows = 1
    tblProgressT.Rows = 1
    tblProgressI.Rows = 1
    CreateShortStudInfoTab
    CreateFullStudInfoTab
    CreateDAFrontSideTable
    SetTableSizes
    PrepareExamTable '???
    
    Form_Resize
    
    LoadSuccess = True
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    If (Me.Visible = False) Then Exit Sub
    
    If Me.WindowState = 1 Then Exit Sub
    If Me.Width < 600 * 15 Then Me.Width = BeforeX
    If Me.Width < 600 * 15 Then Exit Sub
    If Me.Height < 400 * 15 Then Me.Height = BeforeY
    If Me.Height < 400 * 15 Then Exit Sub

    FS1.Width = B.Left
    
    If (Not Border) Then
        FS1.Top = 0
        FS1.Height = Me.Height - 15 * 30
        FS1.Left = 0
    End If
    
    B.Top = FS1.Top
    B.Height = FS1.Height
    
    StudInfoTab.Top = FS1.Top
    StudInfoTab.Left = FS1.Width + B.Width + 1 * 15
    StudInfoTab.Height = FS1.Height
    StudInfoTab.Width = Me.Width - FS1.Width - B.Width - 10 * 15
    
    T2T5.Top = 0 * 15
    T2T5.Left = 0 * 15
    T2T5.Width = StudInfoTab.Width
    
    tblSSInfo.Left = 5 * 15
    tblSSInfo.Width = T2T5.Width - 10 * 15

    txtFIO.Left = 1 * 15
    txtFIO.Top = 1 * 15
    txtFIO.Width = FS1.Width - 2 * 15
    txtFIO.Height = 285
    
    optYear.Left = 1 * 15
    optYear.Top = txtFIO.Top + txtFIO.Height + 2 * 15
    optYear.Width = (FS1.Width - 2 * 15) / 2
    optYear.Height = 285
    
    optFIO.Left = 1 * 15 + optYear.Width
    optFIO.Top = txtFIO.Top + txtFIO.Height + 2 * 15
    optFIO.Width = optFIO.Width
    optFIO.Height = 285
    
    tblFIO.Left = 1 * 15
    tblFIO.Top = optYear.Top + optYear.Height + 2 * 15
    tblFIO.Width = FS1.Width - 2 * 15
    tblFIO.Height = FS1.Height - tblFIO.Top
    
    tblFIO.Cols = 1
    tblFIO.ColWidth(0) = tblFIO.Width
    
    ResizePT
    ResizeP
    ResizeFI
    ResizeEV
    ResizeDA
    ResizePI
    
    BeforeX = Me.Width
    BeforeY = Me.Height
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If ArcLoaded Then arc.closeArchive
    ArcLoaded = False
End Sub

Sub ResizePT()
'���� �����-----------------------------------------------------------------------
        T2T2.Top = T2T5.Top + T2T5.Height + 0 * 15
        T2T2.Left = 0 * 15
        T2T2.Height = StudInfoTab.Height - T2T2.Top - 23 * 15
        T2T2.Width = StudInfoTab.Width
        
        tblProgressT.Top = 0
        tblProgressT.Left = 0 * 15
        tblProgressT.Height = T2T2.Height - 2 * 15
        tblProgressT.Width = T2T2.Width
        
        SetTableSizes
'---------------------------------------------------------------------------------
End Sub

Sub ResizeP()
'���. ������������----------------------------------------------------------------
        T2T1.Top = T2T2.Top
        T2T1.Left = T2T2.Left
        T2T1.Height = T2T2.Height
        T2T1.Width = T2T2.Width
        

        tblProgress.Top = tblProgressT.Top
        tblProgress.Left = tblProgressT.Left
        tblProgress.Height = T2T2.Height - FS4.Height - cmdSave.Height - 5 * 15
        tblProgress.Width = tblProgressT.Width

        cmdSave.Top = tblProgress.Top + tblProgress.Height + 1 * 15
        cmdSave.Left = tblProgress.Left
        cmdSave.Height = 375
        cmdSave.Width = tblProgress.Width
        
        FS4.Top = tblProgress.Top + tblProgress.Height + cmdSave.Height + 2 * 15
        FS4.Left = tblProgress.Left + 1 * 15
        FS4.Height = 975
        FS4.Width = tblProgress.Width - 2 * 15
        
        lblSpec.Width = FS4.Width - lblSpec.Left - lbgrupcapt.Width - lblGroup.Width - 11 * 15
        lbgrupcapt.Left = lblSpec.Left + lblSpec.Width + 6 * 15
        lblGroup.Left = lbgrupcapt.Left + lbgrupcapt.Width + 1 * 15
        lblLect.Width = FS4.Width - Label2.Left - Label2.Width - 11 * 15
        
        SetTableSizes
'---------------------------------------------------------------------------------
End Sub

Sub ResizeFI()
        T2T3.Top = T2T2.Top
        T2T3.Left = T2T2.Left
        T2T3.Height = T2T2.Height
        T2T3.Width = T2T2.Width
        
        tblFSInfo.Left = 5 * 15
        tblFSInfo.Width = T2T3.Width - 10 * 15
        tblFSInfo.Height = T2T3.Height - tblFSInfo.Top - 3 * 15
End Sub

Sub ResizeEV()
        T2T6.Top = T2T2.Top
        T2T6.Left = T2T2.Left
        T2T6.Height = T2T2.Height
        T2T6.Width = T2T2.Width

        T2T6T1.Top = T2T6.Height - T2T6T1.Height
        T2T6T1.Left = 0
        T2T6T1.Width = T2T6.Width
        
        cmdEvDel.Left = T2T6T1.Width - cmdEvDel.Width
        cmdEvAdd.Left = cmdEvDel.Left - cmdEvAdd.Width + 15
        
        txtEvText.Left = 0
        txtEvText.Width = cmdEvAdd.Left - 15
        
        tblEvents.Top = 0
        tblEvents.Left = 0
        tblEvents.Width = T2T6.Width
        tblEvents.Height = T2T6.Height - T2T6T1.Height
        
        tblEvents.ColWidth(1) = T2T6.Width - tblEvents.ColWidth(0) - 20 * 15
End Sub

Sub ResizeDA()
        T2T4.Top = T2T2.Top
        T2T4.Left = T2T2.Left
        T2T4.Height = T2T2.Height
        T2T4.Width = T2T2.Width
        
        cmdDASides.Top = T2T4.Height - cmdDASides.Height
        cmdDASides.Left = 0
        cmdDASides.Width = T2T4.Width
        
        tblEducationalResults.Top = 0
        tblEducationalResults.Left = 0
        tblEducationalResults.Width = T2T4.Width
        tblEducationalResults.Height = T2T4.Height - cmdDASides.Height
        
        tblFrontSide.Top = 0
        tblFrontSide.Left = 0
        tblFrontSide.Width = T2T4.Width
        tblFrontSide.Height = T2T4.Height - cmdDASides.Height
End Sub

Sub ResizePI()
'���� �����-----------------------------------------------------------------------
        T2T7.Top = T2T5.Top + T2T5.Height + 0 * 15
        T2T7.Left = 0 * 15
        T2T7.Height = StudInfoTab.Height - T2T7.Top - 23 * 15
        T2T7.Width = StudInfoTab.Width
        
        tblProgressI.Top = 0
        tblProgressI.Left = 0 * 15
        tblProgressI.Height = T2T7.Height - 2 * 15
        tblProgressI.Width = T2T7.Width
        
        SetTableSizes
'---------------------------------------------------------------------------------
End Sub

Private Sub optFIO_Click()
    txtFIO.Text = ""
    txtFIO_Change
End Sub

Private Sub optYear_Click()
    txtFIO.Text = ""
    txtFIO_Change
End Sub

Private Sub StudInfoTab_TabClick(ByVal lTab As Long)
Select Case lTab
    Case 1: '������ ����������
        T2T3.ZOrder
        T2T3.Visible = True
        T2T5.Visible = True
        
        T2T2.Visible = False
        T2T1.Visible = False
        T2T4.Visible = False
        T2T6.Visible = False
        T2T7.Visible = False
        
    Case 2: '���. ������������
        If (tblFIO.Rows > 0) Then
            If (tblFIO.Row >= 0) Then
            
                FillProgressTbl CLng(tblFIO.RowData(tblFIO.Row)), tblProgress
                If (tblProgress.Rows > 1) Then
                    tblProgress.Row = tblProgress.Rows - 1
                    tblProgress.ShowCell tblProgress.Rows - 1, 1
                    '#tblProgress_Click
                End If
            End If
        End If
            
        T2T1.ZOrder
        T2T1.Visible = True
        T2T5.Visible = True
        
        T2T2.Visible = False
        T2T3.Visible = False
        T2T4.Visible = False
        T2T6.Visible = False
        T2T7.Visible = False
        
    Case 3: '���������
        If (tblFIO.Rows > 0) Then
            If (tblFIO.Row >= 0) Then
                FillSpecCoursesTbl CLng(tblFIO.RowData(tblFIO.Row)), tblProgressT, 0
                If (tblProgressT.Rows > 1) Then
                    tblProgressT.Row = 2
                    'tblProgressT_Click
                End If
            End If
        End If

        
        T2T2.ZOrder
        T2T2.Visible = True
        T2T5.Visible = True
        
        T2T1.Visible = False
        T2T3.Visible = False
        T2T4.Visible = False
        T2T6.Visible = False
        T2T7.Visible = False
        
    Case 4: '����������� �����
        If (tblFIO.Rows > 0) Then
            If (tblFIO.Row >= 0) Then
                FillSpecCoursesTbl CLng(tblFIO.RowData(tblFIO.Row)), tblProgressI, 1
                If (tblProgressI.Rows > 1) Then
                    tblProgressI.Row = 2
                    'tblProgressT_Click
                End If
            End If
        End If

        
        T2T7.ZOrder
        T2T7.Visible = True
        T2T5.Visible = True
        
        T2T1.Visible = False
        T2T3.Visible = False
        T2T4.Visible = False
        T2T6.Visible = False
        T2T2.Visible = False
        
    Case 5: '�������
        '#If (tblFIO.Rows > 0) Then If (tblFIO.Row >= 0) Then LoadStudEvents CLng(tblFIO.RowData(tblFIO.Row))

        T2T6.ZOrder
        T2T6.Visible = True
        T2T5.Visible = True
        
        T2T2.Visible = False
        T2T3.Visible = False
        T2T1.Visible = False
        T2T4.Visible = False
        T2T7.Visible = False
        
    Case 6: '���������� � �������
        If (tblFIO.Rows > 0) Then
            If (tblFIO.Row >= 0) Then
                '#CreateDiplomAddition CLng(tblFIO.RowData(tblFIO.Row))
                '#CreateSCFLDiplomAddition CLng(tblFIO.RowData(tblFIO.Row)), 0
                '#CreateSCFLDiplomAddition CLng(tblFIO.RowData(tblFIO.Row)), 1
                
                '#CreateDiplomAddition2 tblFrontSide
                ''PrintDiplomAddition2 tblFrontSide, 970, 1150
            End If
        End If
        
        T2T4.ZOrder
        T2T4.Visible = True
        T2T5.Visible = True
        
        T2T2.Visible = False
        T2T3.Visible = False
        T2T1.Visible = False
        T2T6.Visible = False
        T2T7.Visible = False
End Select
End Sub

Sub SetTableSizes()
With tblProgress
    .Cols = 10
    .FixedCols = 1
    .FixedRows = 1
    
    .TextMatrix(0, 0) = "�"
    .TextMatrix(0, 1) = "�������� ���������"
    .TextMatrix(0, 2) = "�����"
    .TextMatrix(0, 3) = "��� ���."
    .TextMatrix(0, 4) = "������"
    .TextMatrix(0, 5) = "ECTS"
    .TextMatrix(0, 6) = "�������"
    .TextMatrix(0, 7) = "�������"
    .TextMatrix(0, 8) = "IDExam"
    .TextMatrix(0, 9) = ""
    
    .ColWidth(0) = 500
    .ColWidth(3) = 1200
    .ColWidth(4) = 750
    .ColWidth(5) = 550
    .ColWidth(9) = 400
    .ColWidth(1) = .Width - .ColWidth(0) - .ColWidth(3) - .ColWidth(4) - .ColWidth(5) - .ColWidth(9) - 22 * 15
    
    .ColHidden(2) = True
    .ColHidden(6) = True
    .ColHidden(7) = True
    .ColHidden(8) = True
    .MergeCells = flexMergeFree
End With

With tblProgressT
    .Cols = 8
    .FixedCols = 1
    .FixedRows = 1
    
    .TextMatrix(0, 0) = "�"
    .TextMatrix(0, 1) = "�������� ���������"
    .TextMatrix(0, 2) = "�����"
    .TextMatrix(0, 3) = "�������������"
    .TextMatrix(0, 4) = "��� ���."
    .TextMatrix(0, 5) = "������"
    .TextMatrix(0, 6) = "�������"
    .TextMatrix(0, 7) = "�������"
    
    .ColWidth(0) = 500
    .ColWidth(3) = 2400
    .ColWidth(4) = 1200
    .ColWidth(5) = 750
    .ColWidth(1) = .Width - .ColWidth(0) - .ColWidth(3) - .ColWidth(4) - .ColWidth(5) - 22 * 15
    
    .ColHidden(2) = True
    .ColHidden(6) = True
    .ColHidden(7) = True
    .MergeCells = flexMergeFree
End With

With tblProgressI
    .Cols = 8
    .FixedCols = 1
    .FixedRows = 1
    
    .TextMatrix(0, 0) = "�"
    .TextMatrix(0, 1) = "�������� ���������"
    .TextMatrix(0, 2) = "�����"
    .TextMatrix(0, 3) = "�������������"
    .TextMatrix(0, 4) = "��� ���."
    .TextMatrix(0, 5) = "������"
    .TextMatrix(0, 6) = "�������"
    .TextMatrix(0, 7) = "�������"
    
    .ColWidth(0) = 500
    .ColWidth(3) = 2400
    .ColWidth(4) = 1200
    .ColWidth(5) = 750
    .ColWidth(1) = .Width - .ColWidth(0) - .ColWidth(3) - .ColWidth(4) - .ColWidth(5) - 22 * 15
    
    .ColHidden(2) = True
    .ColHidden(6) = True
    .ColHidden(7) = True
    .MergeCells = flexMergeFree
End With

End Sub

Sub CreateShortStudInfoTab()
    Dim RowSepHeight As Long
    Dim ColSepHeight As Long
    
    Dim BackC As Long
    Dim ForeC As Long
    Dim SepC As Long
    
    RowSepHeight = 15
    ColSepHeight = 15
    
    SepC = RGB(100, 100, 100)
''    BackC = T2T2.BackColor
''    ForeC = RGB(1, 1, 1)
    
    BackC = Main.BackColor
    ForeC = Main.Back.BackColor
    
    tblSSInfo.Height = 1630
    
    tblSSInfo.Cols = 9
    tblSSInfo.Rows = 10
    
    tblSSInfo.MergeCells = flexMergeFree
    
    tblSSInfo.RowHeight(0) = RowSepHeight
    tblSSInfo.RowHeight(2) = RowSepHeight
    tblSSInfo.RowHeight(4) = RowSepHeight
    tblSSInfo.RowHeight(7) = RowSepHeight
    tblSSInfo.RowHeight(9) = RowSepHeight
    
    tblSSInfo.RowHeight(1) = 270
    tblSSInfo.RowHeight(3) = 270
    tblSSInfo.RowHeight(5) = 270
    tblSSInfo.RowHeight(6) = 270
    tblSSInfo.RowHeight(8) = 270
    
    tblSSInfo.ColWidth(0) = 1100
    tblSSInfo.ColWidth(1) = RowSepHeight
    tblSSInfo.ColWidth(3) = 1000
    tblSSInfo.ColWidth(4) = 1000
    tblSSInfo.ColWidth(6) = 1300
    
    tblSSInfo.Cell(flexcpAlignment, 8, 0) = 4
    tblSSInfo.Cell(flexcpAlignment, 3, 3) = 4
    tblSSInfo.Cell(flexcpAlignment, 3, 5) = 4
    tblSSInfo.Cell(flexcpAlignment, 3, 7) = 4
    tblSSInfo.Cell(flexcpAlignment, 5, 3, 6, 3) = 4
    tblSSInfo.Cell(flexcpAlignment, 5, 4, 6, 4) = 4
    tblSSInfo.Cell(flexcpAlignment, 5, 5, 5, 7) = 4
    tblSSInfo.Cell(flexcpAlignment, 5, 2, 6, 2) = 4
    
    tblSSInfo.TextMatrix(1, 2) = "�.�.�."
    tblSSInfo.TextMatrix(3, 2) = "������": tblSSInfo.TextMatrix(3, 4) = "� �������": tblSSInfo.TextMatrix(3, 6) = "� ���. ������"
    tblSSInfo.TextMatrix(5, 2) = "������": tblSSInfo.TextMatrix(5, 3) = "����": tblSSInfo.TextMatrix(5, 4) = "� �������": tblSSInfo.Cell(flexcpText, 5, 5, 5, 8) = "�������"
    tblSSInfo.TextMatrix(8, 0) = "��������": tblSSInfo.TextMatrix(8, 2) = "����� � ������"
    tblSSInfo.AutoSize 2
    
    tblSSInfo.Cell(flexcpPictureAlignment, 0, 0, 6, 0) = 9
    tblSSInfo.Cell(flexcpText, 0, 0, 6, 0) = " "
    
    tblSSInfo.MergeCol(0) = True
    tblSSInfo.MergeCol(2) = True
    tblSSInfo.MergeCol(4) = True
    tblSSInfo.MergeCol(6) = True
    
    tblSSInfo.MergeRow(0) = True
    tblSSInfo.MergeRow(1) = True
    tblSSInfo.MergeRow(2) = True
    tblSSInfo.MergeRow(5) = True
    tblSSInfo.MergeRow(6) = True
    tblSSInfo.MergeRow(7) = True
    tblSSInfo.MergeRow(8) = True
    
    tblSSInfo.GridLines = flexGridNone
    
    
    tblSSInfo.Cell(flexcpBackColor, 0, 1, 8, 1) = SepC
    
    tblSSInfo.Cell(flexcpBackColor, 0, 1, 0, 8) = SepC
    tblSSInfo.Cell(flexcpBackColor, 2, 1, 2, 8) = SepC
    tblSSInfo.Cell(flexcpBackColor, 4, 1, 4, 8) = SepC
    tblSSInfo.Cell(flexcpBackColor, 7, 0, 7, 8) = SepC
    tblSSInfo.Cell(flexcpBackColor, 9, 0, 9, 8) = SepC
    
    
    tblSSInfo.Cell(flexcpBackColor, 1, 2) = BackC
    tblSSInfo.Cell(flexcpBackColor, 3, 2) = BackC: tblSSInfo.Cell(flexcpBackColor, 3, 4) = BackC: tblSSInfo.Cell(flexcpBackColor, 3, 6) = BackC
    tblSSInfo.Cell(flexcpBackColor, 5, 2) = BackC: tblSSInfo.Cell(flexcpBackColor, 5, 3) = BackC: tblSSInfo.Cell(flexcpBackColor, 5, 4) = BackC: tblSSInfo.Cell(flexcpBackColor, 5, 5, 5, 8) = BackC
    tblSSInfo.Cell(flexcpBackColor, 8, 0) = BackC: tblSSInfo.Cell(flexcpBackColor, 8, 2) = BackC
    
    tblSSInfo.Cell(flexcpForeColor, 1, 2, 0, 2) = ForeC
    tblSSInfo.Cell(flexcpForeColor, 3, 2, 2, 2) = ForeC: tblSSInfo.Cell(flexcpForeColor, 3, 4, 2, 4) = ForeC: tblSSInfo.Cell(flexcpForeColor, 3, 6, 2, 6) = ForeC
    tblSSInfo.Cell(flexcpForeColor, 5, 2) = ForeC: tblSSInfo.Cell(flexcpForeColor, 5, 3) = ForeC: tblSSInfo.Cell(flexcpForeColor, 5, 4) = ForeC: tblSSInfo.Cell(flexcpForeColor, 5, 5, 5, 8) = ForeC
    tblSSInfo.Cell(flexcpForeColor, 8, 0) = ForeC: tblSSInfo.Cell(flexcpForeColor, 8, 2) = ForeC
    
    tblSSInfo.ExtendLastCol = True
End Sub

Sub CreateFullStudInfoTab()
    Dim BackC As Long
    Dim ForeC As Long
    Dim SepC As Long
    
    BackC = Main.BackColor
    ForeC = Main.Back.BackColor
    
    tblFSInfo.Cols = 2
    tblFSInfo.Rows = 20
    
    tblFSInfo.FixedCols = 1
    
    tblFSInfo.RowHeight(0) = 300
    tblFSInfo.RowHeight(1) = 300
    tblFSInfo.RowHeight(2) = 300
    tblFSInfo.RowHeight(3) = 300
    tblFSInfo.RowHeight(4) = 300
    tblFSInfo.RowHeight(5) = 300
    tblFSInfo.RowHeight(6) = 300
    tblFSInfo.RowHeight(7) = 300
    tblFSInfo.RowHeight(8) = 300
    tblFSInfo.RowHeight(9) = 300
    tblFSInfo.RowHeight(10) = 300
    tblFSInfo.RowHeight(11) = 300
    tblFSInfo.RowHeight(12) = 300
    tblFSInfo.RowHeight(13) = 300
    tblFSInfo.RowHeight(14) = 300
    tblFSInfo.RowHeight(15) = 300
    tblFSInfo.RowHeight(16) = 300
    tblFSInfo.RowHeight(17) = 300
    tblFSInfo.RowHeight(18) = 300
    tblFSInfo.RowHeight(19) = 300
    
    tblFSInfo.ExtendLastCol = True
    
    tblFSInfo.AutoSizeMode = flexAutoSizeColWidth
    tblFSInfo.TextMatrix(0, 0) = "� �����������������"
    tblFSInfo.TextMatrix(1, 0) = "��� �����������"
    tblFSInfo.TextMatrix(2, 0) = "� ������������"
    tblFSInfo.TextMatrix(3, 0) = "� ���������"
    tblFSInfo.TextMatrix(4, 0) = "� �������"
    tblFSInfo.TextMatrix(5, 0) = "� ����������"
    tblFSInfo.TextMatrix(6, 0) = "� ���������������"
    tblFSInfo.TextMatrix(7, 0) = "�����"
    tblFSInfo.TextMatrix(8, 0) = "�������������"
    tblFSInfo.TextMatrix(9, 0) = "�������"
    tblFSInfo.TextMatrix(10, 0) = "���������"
    tblFSInfo.TextMatrix(11, 0) = "�����������"
    tblFSInfo.TextMatrix(12, 0) = "�������� ���������"
    tblFSInfo.TextMatrix(13, 0) = "�������� � ���������"
    tblFSInfo.TextMatrix(14, 0) = "������� ���������"
    tblFSInfo.TextMatrix(15, 0) = "�������������"
    tblFSInfo.TextMatrix(16, 0) = "��������"
    tblFSInfo.TextMatrix(17, 0) = "�������"
    tblFSInfo.TextMatrix(18, 0) = "�������������"
    tblFSInfo.TextMatrix(19, 0) = "��� �������"
    tblFSInfo.AutoSize 0
    
    
    tblFSInfo.AutoSizeMode = flexAutoSizeRowHeight
    
    tblFSInfo.Cell(flexcpBackColor, 0, 0, 19, 0) = BackC
    tblFSInfo.Cell(flexcpForeColor, 0, 0, 19, 0) = ForeC
    
    With tblEvents
        .Cols = 2
        .Rows = 1
        .FixedCols = 1
        .FixedRows = 1
        .TextMatrix(0, 0) = "�"
        .TextMatrix(0, 1) = "�������� �������"
        .ColWidth(0) = 500
        .ColWidth(1) = T2T6.Width - tblEvents.ColWidth(0) - 20 * 15
    End With
End Sub

Sub CreateDAFrontSideTable()
Dim BackC As Long
Dim ForeC As Long

BackC = Main.BackColor
ForeC = Main.Back.BackColor

With tblFrontSide
    .Cols = 4
    .Rows = 96
    
    .ColWidth(0) = 3000
    .ColWidth(1) = 3000
    .ColWidth(2) = 3000
    
    .ColHidden(3) = True
    
    .ExtendLastCol = True
    
    .TextMatrix(0, 0) = "� ����������"
    .TextMatrix(1, 0) = "�������": .RowData(1) = 1400
    .Cell(flexcpText, 2, 0, 3, 0) = "���, ��������": .RowData(2) = 1400: .RowData(3) = 1400
    .TextMatrix(4, 0) = "���� ��������": .RowData(4) = 1450
    .Cell(flexcpText, 5, 0, 7, 0) = "���������� �������� �� �����������": .RowData(5) = 2300
    .Cell(flexcpText, 8, 0, 10, 0) = "������ �������� �������� ���������": .RowData(8) = 2200
    .Cell(flexcpText, 11, 0, 12, 0) = "�������� �������": .RowData(11) = 1450
    .Cell(flexcpText, 13, 0, 14, 0) = "���(���) ���������": .RowData(13) = 1450
    .TextMatrix(15, 0) = "���� ��������": .RowData(15) = 1450
    .TextMatrix(16, 0) = "����� ��������": .RowData(16) = 1450
    .Cell(flexcpText, 17, 0, 18, 0) = "����������� ���������� (�������������)": .RowData(17) = 2400
    .Cell(flexcpText, 19, 0, 20, 0) = "�������������": .RowData(19) = 1450
    .Cell(flexcpText, 21, 0, 23, 0) = "������ ��������": .RowData(21) = 1450
    .Cell(flexcpText, 24, 0, 26, 0) = "������������� �����": .RowData(24) = 1450
    .Cell(flexcpText, 27, 0, 29, 0) = "���������������� �����": .RowData(27) = 1450
    .TextMatrix(30, 0) = "���� �����������": .RowData(30) = 1450
    .TextMatrix(31, 0) = "���� �������": .RowData(31) = 1450
    .TextMatrix(32, 0) = "�������������� ��������� �� �����������": .RowData(32) = 2300
    .Cell(flexcpBackColor, 0, 0, 32, 0) = BackC
    .Cell(flexcpForeColor, 0, 0, 32, 0) = ForeC
    .RowHeight(33) = 100
    .Cell(flexcpBackColor, 33, 0, 33, 2) = RGB(80, 80, 80)
    
    .Cell(flexcpText, 34, 0, 38, 0) = "��� �������� ���������": .RowData(34) = 1800
    .Cell(flexcpText, 39, 0, 44, 0) = "������� �����������": .RowData(39) = 1500
    .Cell(flexcpText, 45, 0, 49, 0) = "����������� ����������": .RowData(45) = 1500
    .Cell(flexcpText, 50, 0, 56, 0) = "������� ��������": .RowData(50) = 1800
    .Cell(flexcpText, 57, 0, 64, 0) = "������ ���������": .RowData(57) = 1500
    .Cell(flexcpText, 65, 0, 72, 0) = "������� ������": .RowData(65) = 1500
    .Cell(flexcpBackColor, 34, 0, 72, 0) = BackC
    .Cell(flexcpForeColor, 34, 0, 72, 0) = ForeC
    .RowHeight(73) = 100
    .Cell(flexcpBackColor, 73, 0, 73, 2) = RGB(80, 80, 80)
    
    .Cell(flexcpText, 74, 0, 78, 0) = "���������� ���������������� ������ (����, ����� ����������, ������)"
    .Cell(flexcpBackColor, 74, 0, 78, 0) = BackC
    .Cell(flexcpForeColor, 74, 0, 78, 0) = ForeC
    
    .Cell(flexcpText, 79, 0, 79, 2) = "���������� (��������������� ��������)"
    .Cell(flexcpBackColor, 79, 0, 79, 2) = BackC
    .Cell(flexcpForeColor, 79, 0, 79, 2) = ForeC
    .Cell(flexcpAlignment, 79, 0, 79, 2) = 4
    .MergeRow(79) = True

    .Cell(flexcpText, 80, 0, 80, 1) = "�������� ����������": .Cell(flexcpAlignment, 80, 0, 80, 1) = 4
    .TextMatrix(80, 2) = "������": .Cell(flexcpAlignment, 80, 2) = 4
    .Cell(flexcpBackColor, 80, 0, 80, 2) = BackC
    .Cell(flexcpForeColor, 80, 0, 80, 2) = ForeC
    
    
    .Cell(flexcpText, 81, 0, 81, 1) = " "
    .TextMatrix(81, 2) = "": .Cell(flexcpAlignment, 81, 2) = 4
    .Cell(flexcpText, 82, 0, 82, 1) = "  "
    .TextMatrix(82, 2) = "": .Cell(flexcpAlignment, 82, 2) = 4
    .Cell(flexcpText, 83, 0, 83, 1) = " "
    .TextMatrix(83, 2) = "": .Cell(flexcpAlignment, 83, 2) = 4
    .Cell(flexcpText, 84, 0, 84, 1) = "  "
    .TextMatrix(84, 2) = "": .Cell(flexcpAlignment, 84, 2) = 4
    .Cell(flexcpText, 85, 0, 85, 1) = " "
    .TextMatrix(85, 2) = "": .Cell(flexcpAlignment, 85, 2) = 4
    .Cell(flexcpText, 86, 0, 86, 1) = "  "
    .TextMatrix(86, 2) = "": .Cell(flexcpAlignment, 86, 2) = 4
    .Cell(flexcpText, 87, 0, 87, 1) = " "
    .TextMatrix(87, 2) = "": .Cell(flexcpAlignment, 87, 2) = 4
    .Cell(flexcpText, 88, 0, 88, 1) = "  "
    .TextMatrix(88, 2) = "": .Cell(flexcpAlignment, 88, 2) = 4

    .Cell(flexcpText, 89, 0, 91, 0) = "������������"
    
    .TextMatrix(92, 0) = "���� ���������� ������������": .Cell(flexcpText, 92, 1, 92, 2) = " "
    .MergeRow(92) = True
    
    .TextMatrix(93, 0) = "�����": .Cell(flexcpText, 93, 1, 93, 2) = " "
    .MergeRow(93) = True
    
    .TextMatrix(94, 0) = "���� ������": .Cell(flexcpText, 94, 1, 94, 2) = " "
    .MergeRow(94) = True
    
    .TextMatrix(95, 0) = "��������������� �����": .Cell(flexcpText, 95, 1, 95, 2) = " "
    .MergeRow(95) = True
    
    .Cell(flexcpAlignment, 92, 0, 95, 0) = 1
    
    .Cell(flexcpBackColor, 89, 0, 95, 0) = BackC
    .Cell(flexcpForeColor, 89, 0, 95, 0) = ForeC
   
    .RowHeight(0) = 270
    .RowHeight(1) = 270
    .RowHeight(4) = 270
    .RowHeight(15) = 270
    .RowHeight(16) = 270
    .RowHeight(30) = 270
    .RowHeight(31) = 270
    .RowHeight(32) = 270
    .RowHeight(79) = 270
    .RowHeight(92) = 270

    .AutoSize 0, 2
    .MergeCol(0) = True
End With
End Sub
'Examinatios------------------------------------------------------------
Sub PrepareExamTable()
    With ExtblStudents2
        .Cols = 7
        .Rows = 2
        .FixedRows = 1
        .FixedCols = 1
        
        .TextMatrix(0, 0) = "ID"
        .TextMatrix(0, 1) = "�. �. �."
        .TextMatrix(0, 2) = "������"
        .TextMatrix(0, 3) = "ECTS"
        .TextMatrix(0, 4) = "��������"
        .TextMatrix(0, 5) = "�� �������"
        .TextMatrix(0, 6) = ""
    End With
End Sub

Private Sub txtFIO_Change()
    Dim StudName As String
    
    Dim Surname As String
    Dim Name As String
    Dim Patronymic As String
    
    Dim StudArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long
    
    Dim i As Long
    
    StudName = txtFIO.Text
    
    If (StudName = "") Then
        tblFIO.Rows = 0
        ClearStudInfoTab
        ClearDiplomAddition tblFrontSide
        Exit Sub
    End If
    
    If optFIO.Value Then
        Dim FirstS As Byte
        Dim SecondS As Byte
        
        FirstS = InStr(1, StudName, " ")
        If (FirstS) Then SecondS = InStr(InStr(1, StudName, " ") + 1, StudName, " ")
        
        If ((FirstS = 0) And (SecondS = 0)) Then
            Surname = StudName
            
            CheckString Surname
            arc.doQueryArchive "SELECT ID, Surname, Name, Patronymic " & _
            "FROM Student WHERE Surname LIKE '" & Surname & "*' " & _
            "ORDER BY Surname, Name, Patronymic", StudArrey, SCols, SRows
        ElseIf ((FirstS <> 0) And (SecondS = 0)) Then
            Surname = Left(StudName, FirstS - 1)
            Name = Mid(StudName, FirstS + 1, Len(StudName) - FirstS)
            
            CheckString Surname
            CheckString Name
            arc.doQueryArchive "SELECT ID, Surname, Name, Patronymic FROM Student " & _
            "WHERE Surname LIKE '" & Surname & "' AND [Name] LIKE '" & Name & "*' " & _
            "ORDER BY Surname, Name, Patronymic", StudArrey, SCols, SRows
        ElseIf ((FirstS <> 0) And (SecondS <> 0)) Then
            Surname = Left(StudName, FirstS - 1)
            Name = Mid(StudName, FirstS + 1, SecondS - FirstS - 1)
            Patronymic = Right(StudName, Len(StudName) - SecondS)
            
            CheckString Surname
            CheckString Name
            CheckString Patronymic
            arc.doQueryArchive "SELECT ID, Surname, Name, Patronymic " & _
            "FROM Student WHERE Surname LIKE '" & Surname & "' AND Name LIKE '" & _
            Name & "' AND Patronymic LIKE '" & Patronymic & "*' " & _
            "ORDER BY Surname, Name, Patronymic", StudArrey, SCols, SRows
        End If
    End If
    'ELSE
    If optYear.Value Then
        If Not IsNumeric(StudName) Then
            SRows = 0
        Else
            arc.doQueryArchive "SELECT ID, Surname, Name, Patronymic " & _
            "FROM Student WHERE GraduationYear=" & StudName, StudArrey, SCols, SRows
        End If
    End If
    
    tblFIO.Rows = 0
    If (SRows > 0) Then
    
        For i = 0 To SRows - 1
            tblFIO.AddItem GetFullName(CStr(StudArrey(1, i)), CStr(StudArrey(2, i)), CStr(StudArrey(3, i)), False, False, True)
            tblFIO.RowData(tblFIO.Rows - 1) = StudArrey(0, i)
        Next i
        
        tblFIO.Row = 0
        tblFIO_Click
    Else
        ClearStudInfoTab
        ClearDiplomAddition tblFrontSide
    End If
End Sub

Sub ClearStudInfoTab()

    tblProgress.Rows = 1
    tblProgressT.Rows = 1
    tblEducationalResults.Rows = 1

    FillShortStudInfoTab "", "", _
                         "", _
                         "", _
                         "", _
                         "", _
                         "", _
                         "", _
                         "", _
                         "", _
                         ""


    tblFSInfo.TextMatrix(0, 1) = ""
    tblFSInfo.TextMatrix(1, 1) = ""
    tblFSInfo.TextMatrix(2, 1) = ""
    tblFSInfo.TextMatrix(3, 1) = ""
    tblFSInfo.TextMatrix(4, 1) = ""
    tblFSInfo.TextMatrix(5, 1) = ""
    tblFSInfo.TextMatrix(6, 1) = ""
    tblFSInfo.TextMatrix(7, 1) = ""
    tblFSInfo.TextMatrix(8, 1) = ""
    tblFSInfo.TextMatrix(9, 1) = ""
    tblFSInfo.TextMatrix(10, 1) = ""
    tblFSInfo.TextMatrix(11, 1) = ""
    tblFSInfo.TextMatrix(12, 1) = ""
    tblFSInfo.TextMatrix(13, 1) = ""
    tblFSInfo.TextMatrix(14, 1) = ""
    tblFSInfo.TextMatrix(15, 1) = ""
    tblFSInfo.TextMatrix(16, 1) = ""
    tblFSInfo.TextMatrix(17, 1) = ""
    tblFSInfo.TextMatrix(18, 1) = ""
    tblFSInfo.TextMatrix(19, 1) = ""
    tblFSInfo.AutoSize 1
    



    lblSpec.Caption = ""
    lblGroup.Caption = ""
    lblLect.Caption = ""

End Sub

Sub ClearDiplomAddition(tblDA As VSFlexGrid)
    With tblDA
        .Cell(flexcpText, 0, 1, 0, 2) = " "
        .Cell(flexcpText, 1, 1, 1, 2) = " "
        .Cell(flexcpText, 2, 1, 2, 2) = " "
        .Cell(flexcpText, 3, 1, 3, 2) = " "
        .Cell(flexcpText, 4, 1, 4, 2) = " "
        .Cell(flexcpText, 5, 1, 7, 2) = " "
        .Cell(flexcpText, 8, 1, 10, 2) = " "
        .Cell(flexcpText, 11, 1, 12, 2) = " "
        .Cell(flexcpText, 13, 1, 14, 2) = " "
        .Cell(flexcpText, 15, 1, 15, 2) = " "
        .Cell(flexcpText, 16, 1, 16, 2) = " "
        .Cell(flexcpText, 17, 1, 18, 2) = " "
        .Cell(flexcpText, 19, 1, 20, 2) = " "
        .Cell(flexcpText, 21, 1, 23, 2) = " "
        .Cell(flexcpText, 24, 1, 26, 2) = " "
        .Cell(flexcpText, 27, 1, 29, 2) = " "
        .Cell(flexcpText, 30, 1, 30, 2) = " "
        .Cell(flexcpText, 31, 1, 31, 2) = " "
        .Cell(flexcpText, 32, 1, 32, 2) = " "
    
        .Cell(flexcpText, 34, 1, 38, 2) = " "
        .Cell(flexcpText, 39, 1, 44, 2) = " "
        .Cell(flexcpText, 45, 1, 49, 2) = " "
        .Cell(flexcpText, 50, 1, 56, 2) = " "
        .Cell(flexcpText, 57, 1, 64, 2) = " "
        .Cell(flexcpText, 65, 1, 72, 2) = " "
    
        .Cell(flexcpText, 74, 1, 78, 2) = " "
    
        .Cell(flexcpText, 81, 0, 81, 1) = " "
        .TextMatrix(81, 2) = "": .Cell(flexcpAlignment, 81, 2) = 4
        .Cell(flexcpText, 82, 0, 82, 1) = "  "
        .TextMatrix(82, 2) = "": .Cell(flexcpAlignment, 82, 2) = 4
        .Cell(flexcpText, 83, 0, 83, 1) = " "
        .TextMatrix(83, 2) = "": .Cell(flexcpAlignment, 83, 2) = 4
        .Cell(flexcpText, 84, 0, 84, 1) = "  "
        .TextMatrix(84, 2) = "": .Cell(flexcpAlignment, 84, 2) = 4
        .Cell(flexcpText, 85, 0, 85, 1) = " "
        .TextMatrix(85, 2) = "": .Cell(flexcpAlignment, 85, 2) = 4
        .Cell(flexcpText, 86, 0, 86, 1) = "  "
        .TextMatrix(86, 2) = "": .Cell(flexcpAlignment, 86, 2) = 4
        .Cell(flexcpText, 87, 0, 87, 1) = " "
        .TextMatrix(87, 2) = "": .Cell(flexcpAlignment, 87, 2) = 4
        .Cell(flexcpText, 88, 0, 88, 1) = "  "
        .TextMatrix(88, 2) = "": .Cell(flexcpAlignment, 88, 2) = 4
        
        .Cell(flexcpText, 89, 1, 91, 2) = " "
        
        .Cell(flexcpText, 92, 1, 92, 2) = " "
        .Cell(flexcpText, 93, 1, 93, 2) = " "
        .Cell(flexcpText, 94, 1, 94, 2) = " "
        .Cell(flexcpText, 95, 1, 95, 2) = " "
        
        Dim i  As Long
    
        For i = 0 To .Rows - 1
            .MergeRow(i) = True
        Next i
    
        .AutoSize 0, 2
End With
End Sub

Private Sub tblFIO_Click()
    On Error GoTo ErrH
    Dim StudName As String
    Dim IDStud As Long
    Dim IDSpec As Long
    
    Dim DopArrey As Variant
    Dim DopRows As Long
    Dim DopCols As Integer
    
    Dim StudArrey As Variant
    Dim Rows As Long
    Dim Cols As Integer
    
    Dim FIO As String
    Dim FIOR As String
    Dim GroupName As String
    Dim NumZBook As String
    Dim NumRBook As String
    Dim Status As String
    Dim SDate As String
    Dim SOrder As String
    Dim SReason As String
    Dim AddressInKharkov As String
    Dim SPhoto As String
           
        ClearStudInfoTab
    
        If ((tblFIO.Row < 0) Or (tblFIO.Rows = 0)) Then Exit Sub
        
        StudName = tblFIO.TextMatrix(tblFIO.Row, 0)
        IDStud = tblFIO.RowData(tblFIO.Row)
        
     
        arc.doQueryArchive "SELECT Surname,Name,Patronymic,FormOfPayment,OffsetNumber," & _
                            "Address,KharkovAddress,DateOfBorn,Nationality,Education," & _
                            "MaritalStatus,InformationOnTheParents,Gratitude,Reprimands," & _
                            "IDCategory,DeductionDate,DeductionOrderNumber," & _
                            "DeductionReason,IDNominalGrant,IsValidReason,IsForeigner," & _
                            "CReprimands,StudPhoto,LibNumber," & _
                            "IDCode,EntryYear,AtestatNum,DiplomNum,AdditionNumber," & _
                            "RegNumber,IDChair,StudNum,IDSpecialisation,GroupName," & _
                            "GraduationYear,IDSpec,IDOptions FROM Student WHERE ID = " & _
                            IDStud, StudArrey, Cols, Rows
        If (Rows > 0) Then
                IDSpec = CLng(StudArrey(35, 0))
                GroupName = StudArrey(33, 0)
            
                arc.doQueryArchive "SELECT SpecName FROM Speciality WHERE ID = " & _
                IDSpec, DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then _
                tblFSInfo.TextMatrix(8, 1) = DopArrey(0, 0)
    
                If (Not IsNull(StudArrey(0, 0)) And Not IsNull(StudArrey(1, 0)) And Not IsNull(StudArrey(2, 0))) Then
                    FIOR = GetFullName(CStr(StudArrey(0, 0)), CStr(StudArrey(1, 0)), _
                    CStr(StudArrey(2, 0)), False, False, True) & _
                    IIf(Not IsNull(StudArrey(3, 0)), "   (" & StudArrey(3, 0) & ")", "") & _
                    IIf(StudArrey(20, 0) = 0, "", " (����������)") & _
                    IIf(Not IsNull(StudArrey(7, 0)), " (" & StudArrey(7, 0) & " �.�.)", "")
                    
                    FIO = GetFullName(CStr(StudArrey(0, 0)), CStr(StudArrey(1, 0)), _
                    CStr(StudArrey(2, 0)), False, False, True)
                End If
                
                    
                If (Not IsNull(StudArrey(24, 0))) Then tblFSInfo.TextMatrix(0, 1) = StudArrey(24, 0)
                If (Not IsNull(StudArrey(25, 0))) Then tblFSInfo.TextMatrix(1, 1) = StudArrey(25, 0)
                If (Not IsNull(StudArrey(31, 0))) Then tblFSInfo.TextMatrix(2, 1) = StudArrey(31, 0)
                If (Not IsNull(StudArrey(26, 0))) Then tblFSInfo.TextMatrix(3, 1) = StudArrey(26, 0)
                If (Not IsNull(StudArrey(27, 0))) Then tblFSInfo.TextMatrix(4, 1) = StudArrey(27, 0)
                If (Not IsNull(StudArrey(28, 0))) Then tblFSInfo.TextMatrix(5, 1) = StudArrey(28, 0)
                If (Not IsNull(StudArrey(29, 0))) Then tblFSInfo.TextMatrix(6, 1) = StudArrey(29, 0)
                If (Not IsNull(StudArrey(4, 0))) Then NumZBook = StudArrey(4, 0)
                If (Not IsNull(StudArrey(5, 0))) Then tblFSInfo.TextMatrix(7, 1) = StudArrey(5, 0)
                If (Not IsNull(StudArrey(6, 0))) Then AddressInKharkov = StudArrey(6, 0)
                If (Not IsNull(StudArrey(8, 0))) Then
                    tblFSInfo.TextMatrix(9, 1) = StudArrey(8, 0)
                    AddressInKharkov = AddressInKharkov & IIf(CStr(StudArrey(8, 0)) <> "", " ���. " & StudArrey(8, 0), "")
                End If
                If (Not IsNull(StudArrey(9, 0))) Then tblFSInfo.TextMatrix(11, 1) = StudArrey(9, 0)
                If (Not IsNull(StudArrey(10, 0))) Then tblFSInfo.TextMatrix(12, 1) = StudArrey(10, 0)
                If (Not IsNull(StudArrey(11, 0))) Then tblFSInfo.TextMatrix(13, 1) = StudArrey(11, 0)
                If (Not IsNull(StudArrey(12, 0))) Then tblFSInfo.TextMatrix(15, 1) = StudArrey(12, 0)
                If (Not IsNull(StudArrey(13, 0))) Then tblFSInfo.TextMatrix(16, 1) = StudArrey(13, 0)
                If (Not IsNull(StudArrey(21, 0))) Then tblFSInfo.TextMatrix(17, 1) = StudArrey(21, 0)
                If (Not IsNull(StudArrey(22, 0))) Then SPhoto = IIf(Dir(CStr(StudArrey(22, 0))) <> "", CStr(StudArrey(22, 0)), "")
                If (Not IsNull(StudArrey(23, 0))) Then NumRBook = CStr(StudArrey(23, 0))
                
    
                arc.doQueryArchive "SELECT CategoryName FROM Category WHERE ID=" & StudArrey(14, 0), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblFSInfo.TextMatrix(10, 1) = DopArrey(0, 0)
    
                arc.doQueryArchive "SELECT GrantName FROM NominalGrant WHERE ID=" & StudArrey(18, 0), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblFSInfo.TextMatrix(14, 1) = DopArrey(0, 0)
                
                arc.doQueryArchive "SELECT FacultyName FROM Faculty WHERE ID=" & StudArrey(30, 0), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblFSInfo.TextMatrix(17, 1) = DopArrey(0, 0)
                
                arc.doQueryArchive "SELECT Specialisation FROM Specialisation WHERE ID=" & StudArrey(32, 0), DopArrey, DopCols, DopRows
                If (Not IsNull(DopArrey(0, 0))) Then tblFSInfo.TextMatrix(18, 1) = DopArrey(0, 0)
                
                tblFSInfo.TextMatrix(19, 1) = StudArrey(34, 0)
                
                        Status = "���������"

                        SDate = ""
                        SOrder = ""
                        SReason = ""
                
                FillShortStudInfoTab FIO, FIOR, _
                                     GroupName, _
                                     NumZBook, _
                                     NumRBook, _
                                     Status, _
                                     SDate, _
                                     SOrder, _
                                     SReason, _
                                     AddressInKharkov, _
                                     SPhoto
                                     
                tblSSInfo.AutoSize 2
                
            tblFSInfo.AutoSize 1
        End If
            
        StudInfoTab_TabClick StudInfoTab.SelectedTab
    Exit Sub
ErrH:
    If Err <> 52 Then
        MsgBox Error(Err)
    Else
        Resume Next
    End If
End Sub

Private Sub tblFIO_RowColChange()
    tblFIO_Click
End Sub
Sub FillShortStudInfoTab(FIO As String, FIOR As String, _
                         GroupName As String, _
                         NumZBook As String, _
                         NumRBook As String, _
                         Status As String, _
                         SDate As String, _
                         SOrder As String, _
                         SReason As String, _
                         AddressInKharkov As String, _
                         SPhoto As String)
                         
tblSSInfo.Cell(flexcpText, 1, 3, 1, 8) = FIOR
tblSSInfo.RowData(0) = FIO
tblSSInfo.Cell(flexcpText, 3, 3) = GroupName
tblSSInfo.Cell(flexcpText, 3, 5) = NumZBook
tblSSInfo.Cell(flexcpText, 3, 7) = NumRBook
tblSSInfo.Cell(flexcpText, 6, 2) = Status
tblSSInfo.Cell(flexcpText, 6, 3) = SDate
tblSSInfo.Cell(flexcpText, 6, 4) = SOrder
tblSSInfo.Cell(flexcpText, 6, 5, 6, 8) = SReason
tblSSInfo.Cell(flexcpText, 8, 3, 8, 8) = AddressInKharkov
tblSSInfo.Cell(flexcpPicture, 0, 0, 6, 0) = LoadPicture(SPhoto)

End Sub

Private Sub tblSSInfo_Click()
    On Error GoTo ErrH
    
    Dim StudName As String
    Dim IDStud As Long
    Dim PhotoPath As String
    
    Dim AR As Long
    
    If ((tblFIO.Row < 0) Or (tblFIO.Rows = 0)) Then Exit Sub
    
    StudName = tblFIO.TextMatrix(tblFIO.Row, 0)
    IDStud = tblFIO.RowData(tblFIO.Row)
    
    If (tblSSInfo.Col = 0 And tblSSInfo.Row = 8) Then
    
        Main.CommonDialog1.InitDir = App.Path
        Main.CommonDialog1.DialogTitle = "�������� ���������� ��������"
        Main.CommonDialog1.Filter = "All images |*.bmp; *.jpg; *.gif|Bitmaps (*.bmp)|*.bmp|JPEG images (*.jpg)|*.jpg|GIF images (*.gif)|*.gif"
        Main.CommonDialog1.CancelError = True
        Main.CommonDialog1.ShowOpen
    
        PhotoPath = Main.CommonDialog1.FileName
        If (FindErrors(PhotoPath, 255, "���� � ����������", True, True)) Then Exit Sub
        
        tblSSInfo.Cell(flexcpPicture, 0, 0, 6, 0) = LoadPicture(PhotoPath)
        
        CheckString PhotoPath
        arc.doUpdateArchive "UPDATE Student SET StudPhoto = '" & PhotoPath & "' WHERE ID = " & IDStud, 128
    
    End If
    
    Exit Sub
ErrH:
    If (Err <> cdlCancel) Then MsgBox Error(Err)
End Sub

Sub FillProgressTbl(IDStud As Long, TP As VSFlexGrid)
    Dim StudMarkArrey As Variant
    Dim SMCols As Integer
    Dim SMRows As Long
    
    Dim i As Long
    
    arc.doQueryArchive "SELECT Subject.Subject, EP.KindOfReport, EP.Semester," & _
    " Sheet.Mark, Sheet.IDExam, EP.NumOfHours, Sheet.ID,Sheet.ECTS" & _
    " FROM EP, Subject, Sheet, Examination WHERE Sheet.IDStudent = " & IDStud & _
    " AND Sheet.IDExam = Examination.ID AND Examination.IDEP = EP.ID AND" & _
    " EP.IDSubject = Subject.ID ORDER BY EP.Semester, EP.KindOfReport DESC", _
    StudMarkArrey, SMCols, SMRows
    
    Dim CurSemester As Byte
    CurSemester = 0
    
    Dim ExamCounter As Long
    ExamCounter = 1
    
    TP.Rows = 1
    If (SMRows > 0) Then
        For i = 0 To SMRows - 1
            If (CurSemester < CByte(StudMarkArrey(2, i))) Then
                TP.AddItem vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i))
                TP.RowData(TP.Rows - 1) = "Semester"
                TP.MergeRow(TP.Rows - 1) = True
                TP.Cell(flexcpAlignment, TP.Rows - 1, 1, TP.Rows - 1, 9) = flexAlignCenterCenter
                
                TP.Cell(flexcpBackColor, TP.Rows - 1, 1, TP.Rows - 1, 9) = RGB(240, 240, 240)

                ExamCounter = 1
                CurSemester = StudMarkArrey(2, i)
            End If
            
            TP.AddItem ExamCounter
            TP.RowData(TP.Rows - 1) = CLng(StudMarkArrey(4, i))
            TP.TextMatrix(TP.Rows - 1, 1) = CStr(StudMarkArrey(0, i))
            TP.TextMatrix(TP.Rows - 1, 8) = CLng(StudMarkArrey(6, i))
            If (Not IsNull(StudMarkArrey(5, i))) Then TP.TextMatrix(TP.Rows - 1, 2) = CLng(StudMarkArrey(5, i))
            
            TP.TextMatrix(TP.Rows - 1, 3) = CStr(StudMarkArrey(1, i))
            TP.TextMatrix(TP.Rows - 1, 6) = CByte(StudMarkArrey(2, i))
            TP.TextMatrix(TP.Rows - 1, 5) = _
                IIf(IsNull(StudMarkArrey(7, i)), "", StudMarkArrey(7, i))
            
            If (CStr(StudMarkArrey(1, i)) = "�������") Then
                Select Case CByte(StudMarkArrey(3, i))
                    Case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
                        TP.TextMatrix(TP.Rows - 1, 4) = CByte(StudMarkArrey(3, i))
                    Case 201: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                    Case 202: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                    Case 203: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                End Select

                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:   TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 3:    TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 4:    TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 5:    TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                Else
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:  TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 4, 5, 6:  TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 7, 8, 9:  TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                End If
            End If
            
            If (CStr(StudMarkArrey(1, i)) = "�����") Then
                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                            
                        Case 1, 2:
                            TP.TextMatrix(TP.Rows - 1, 4) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                            
                        Case 3, 4, 5:
                            TP.TextMatrix(TP.Rows - 1, 4) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:        TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 3, 4, 5:     TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                Else
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                            
                        Case 1, 2, 3:
                            TP.TextMatrix(TP.Rows - 1, 4) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                            
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:
                            TP.TextMatrix(TP.Rows - 1, 4) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:                       TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                End If
            End If
            
            If (CStr(StudMarkArrey(1, i)) = "��� ������") Then
''                TP.TextMatrix(TP.Rows - 1, 5) = "��"
''                TP.TextMatrix(TP.Rows - 1, 7) = "��"
            End If

            ExamCounter = ExamCounter + 1
        Next i
    End If
    
    TP.ColHidden(5) = False
End Sub

Sub FillSpecCoursesTbl(IDStud As Long, TP As VSFlexGrid, CourseType As Byte)
Dim StudMarkArrey As Variant
Dim SMCols As Integer
Dim SMRows As Long

Dim i As Long

arc.doQueryArchive "SELECT SpecialCourse.ID, Subject.Subject," & _
" SpecialCourse.KindOfReport, SpecialCourse.Semester,SpecialCourse.Mark," & _
" SpecialCourse.NumOfHours,Lecturer.Surname, Lecturer.Name, Lecturer.Patronymic" & _
" FROM SpecialCourse, Subject,Lecturer WHERE SpecialCourse.IDStudent=" & IDStud & _
" AND SpecialCourse.CourseType=" & CourseType & _
" AND SpecialCourse.IDLecturer = Lecturer.ID AND SpecialCourse.IDSubject = Subject.ID" & _
" ORDER BY SpecialCourse.Semester, SpecialCourse.KindOfReport DESC", _
StudMarkArrey, SMCols, SMRows
    
    Dim CurSemester As Byte
    CurSemester = 0
    
    Dim ExamCounter As Long
    ExamCounter = 1
    
    TP.Rows = 1
    If (SMRows > 0) Then
        For i = 0 To SMRows - 1
            If (CurSemester < CByte(StudMarkArrey(3, i))) Then
                TP.AddItem " "
                TP.Cell(flexcpText, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = "������� " & CByte(StudMarkArrey(3, i))
                TP.RowData(TP.Rows - 1) = "Semester"
                TP.MergeRow(TP.Rows - 1) = True
                TP.Cell(flexcpAlignment, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = flexAlignCenterCenter
                
                TP.Cell(flexcpBackColor, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = RGB(240, 240, 240)
                
                ExamCounter = 1
                CurSemester = StudMarkArrey(3, i)
            End If
            
            TP.AddItem ExamCounter
            TP.RowData(TP.Rows - 1) = CLng(StudMarkArrey(0, i))
            
            If (Not IsNull(StudMarkArrey(1, i))) Then TP.TextMatrix(TP.Rows - 1, 1) = CStr(StudMarkArrey(1, i))
            If (Not IsNull(StudMarkArrey(5, i))) Then TP.TextMatrix(TP.Rows - 1, 2) = CStr(StudMarkArrey(5, i))
            If (Not IsNull(StudMarkArrey(6, i))) Then TP.TextMatrix(TP.Rows - 1, 3) = GetFullName(CStr(StudMarkArrey(6, i)), CStr(StudMarkArrey(7, i)), CStr(StudMarkArrey(8, i)), False, True, True)
            If (Not IsNull(StudMarkArrey(2, i))) Then TP.TextMatrix(TP.Rows - 1, 4) = CStr(StudMarkArrey(2, i))
            If (Not IsNull(StudMarkArrey(3, i))) Then TP.TextMatrix(TP.Rows - 1, 6) = CStr(StudMarkArrey(3, i))
            
            If (CStr(StudMarkArrey(2, i)) = "�������") Then
                Select Case CByte(StudMarkArrey(4, i))
                    Case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
                        TP.TextMatrix(TP.Rows - 1, 5) = CByte(StudMarkArrey(4, i))
                    Case 201: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                    Case 202: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                    Case 203: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                End Select

                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:   TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 3:    TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 4:    TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 5:    TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                Else
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:  TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 4, 5, 6:  TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 7, 8, 9:  TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                End If
            End If
            
            If (CStr(StudMarkArrey(2, i)) = "�����") Then
                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                            
                        Case 1, 2:
                            TP.TextMatrix(TP.Rows - 1, 5) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                            
                        Case 3, 4, 5:
                            TP.TextMatrix(TP.Rows - 1, 5) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:        TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 3, 4, 5:     TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                Else
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                            
                        Case 1, 2, 3:
                            TP.TextMatrix(TP.Rows - 1, 5) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                            
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:
                            TP.TextMatrix(TP.Rows - 1, 5) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:   TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                End If
            End If
            
            If (CStr(StudMarkArrey(2, i)) = "��� ������") Then
''                TP.TextMatrix(TP.Rows - 1, 4) = "��"
''                TP.TextMatrix(TP.Rows - 1, 6) = "��"
            End If

            ExamCounter = ExamCounter + 1
        Next i
    End If

End Sub


