VERSION 5.00
Begin VB.Form DlgChoiceReasonAP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "����� ������� ���������"
   ClientHeight    =   2550
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   3975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   3975
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox ComboReason 
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   3615
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "������"
      Height          =   375
      Left            =   2400
      TabIndex        =   1
      Top             =   1680
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "��"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "�������� �������"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   1695
   End
End
Attribute VB_Name = "DlgChoiceReasonAP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit



Private Sub CancelButton_Click()
    ComboReason.text = ""
    DlgChoiceReasonAP.Hide
End Sub

Private Sub Form_Load()
    Dim xarrey As Variant
    Dim xcols As Integer
    Dim xrows As Long
    Dim i As Integer
        
    CDB.SQLOpenTableToArrey "SELECT tblReasonAcademPerf.Reason FROM tblReasonAcademPerf " & _
        "ORDER BY tblReasonAcademPerf.Reason ", xarrey, xcols, xrows
    If xrows > 0 Then
        For i = 0 To xrows - 1
            ComboReason.AddItem (xarrey(0, i))
        Next
        ComboReason.text = ComboReason.ItemData(0)
    Else
        MsgBox "��� �� ����� �������"
    End If
        
End Sub

Private Sub OKButton_Click()
    If (ComboReason.text <> "") Then
        DlgChoiceReasonAP.Hide
    Else
        MsgBox "�������� �������"
    End If
End Sub
