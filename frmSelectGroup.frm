VERSION 5.00
Begin VB.Form frmSelectGroup 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "����� ������"
   ClientHeight    =   1590
   ClientLeft      =   3780
   ClientTop       =   3720
   ClientWidth     =   4830
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1590
   ScaleWidth      =   4830
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4815
      Begin VB.ComboBox SpecName 
         Height          =   315
         Left            =   1440
         TabIndex        =   4
         Top             =   240
         Width           =   3255
      End
      Begin VB.ComboBox GroupName 
         Height          =   315
         Left            =   1440
         TabIndex        =   3
         Top             =   600
         Width           =   3255
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "�������"
         Height          =   375
         Left            =   3480
         TabIndex        =   2
         Top             =   1080
         Width           =   1215
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   2160
         TabIndex        =   1
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label M5 
         AutoSize        =   -1  'True
         Caption         =   "������������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   1260
      End
      Begin VB.Label M6 
         AutoSize        =   -1  'True
         Caption         =   "������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmSelectGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private OKSelected As Boolean


Private Sub cmdCancel_Click()
Main.ErrorDataFromDialog = True
Main.StringDataFromDialog = ""
Main.LongDataFromDialog = 0

Unload Me
End Sub


Private Sub cmdOK_Click()
If (GroupName.ListCount = 0) Then Exit Sub
If (GroupName.ListIndex < 0) Then Exit Sub

    Dim GroupArrey As Variant
    Dim GCols As Integer
    Dim GRows As Long
    
    Dim SGroupName As String
    Dim GroupSemester As Byte
    
    Dim IDGroup As Long
    
    IDGroup = CLng(GroupName.ItemData(GroupName.ListIndex))
    
    CDB.SQLOpenTableToArrey "SELECT [GroupName], [Semester] FROM [tblGroups] WHERE [ID] = " & IDGroup, GroupArrey, GCols, GRows
    
    Main.StringDataFromDialog = CStr(GroupArrey(0, 0))
    Main.LongDataFromDialog = CByte(GroupArrey(1, 0))
    Main.ErrorDataFromDialog = False

    OKSelected = True
    Unload Me
End Sub

Private Sub Form_Load()
    Dim SpecArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long
    
    Dim i As Long
    
    OKSelected = False
    
    CDB.SQLOpenTableToArrey "SELECT [ID], [SpecName] FROM [tblSpecialities] ORDER BY [SpecName]", SpecArrey, SCols, SRows
    
    If (SRows = 0) Then
        MsgBox "��� ������ ������ �� ���������� ������ ���� ���������������� ������� ���� ������������� !", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (SRows > 0) Then
    
        For i = 0 To SRows - 1
            SpecName.AddItem SpecArrey(1, i)
            SpecName.ItemData(SpecName.NewIndex) = SpecArrey(0, i)
        Next i
        
        SpecName.ListIndex = 0
    End If

LoadSuccess = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
If (Not OKSelected) Then
    Main.ErrorDataFromDialog = True
    Main.StringDataFromDialog = ""
    Main.LongDataFromDialog = 0
End If
End Sub

Private Sub SpecName_Click()
    Dim GroupArrey As Variant
    Dim GCols As Integer
    Dim GRows As Long
    
    Dim SGroupName As String
    Dim i As Long
    
    If (SpecName.ListIndex = -1) Then Exit Sub

    GroupName.Clear
    
    CDB.SQLOpenTableToArrey "SELECT [ID], [GroupName], [Semester] FROM [tblGroups] WHERE ([Semester] BETWEEN 1 AND 10) AND [IDSpec] = " & SpecName.ItemData(SpecName.ListIndex) & " ORDER BY [Semester], [GroupName]", GroupArrey, GCols, GRows
    
    If (GRows > 0) Then
    
        For i = 0 To GRows - 1
            SGroupName = CStr(GroupArrey(1, i))
            If (InStr(1, SGroupName, "#")) Then SGroupName = Left$(SGroupName, InStr(1, SGroupName, "#") - 1) & IIf(CLng(GroupArrey(2, i)) Mod 2 = 1, (CLng(GroupArrey(2, i)) \ 2) + 1, CLng(GroupArrey(2, i)) \ 2) & Right$(SGroupName, Len(SGroupName) - InStr(1, SGroupName, "#"))

            GroupName.AddItem SGroupName
            GroupName.ItemData(GroupName.NewIndex) = GroupArrey(0, i)
        Next i
        
        GroupName.ListIndex = 0
    End If
End Sub
