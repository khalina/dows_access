Attribute VB_Name = "UniverMod"
Option Explicit

Public DOWSDemoMode As Boolean

Type TablesInfo
    TableNameDB As String
    TableNameRu As String
    Status As Byte
    
    FieldsArrey() As Variant
    FieldsCount As Integer
End Type

Public Const SQLTablesCount = 15
Public TablesArrey(SQLTablesCount) As TablesInfo
Public FCondition As Boolean

Public Type RECT
   Left As Long
   Top As Long
   Right As Long
   Bottom As Long
End Type

Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Const SRCCOPY = &HCC0020 ' (DWORD) dest = source

Public CurDBPath As String

Public FivePointMarkSystem As Boolean

Public IsMainDBOpen As Boolean
Public IsObjectsCreated As Boolean

Public CDB As UniverDB.DBClass
Public LoadSuccess As Boolean

Public DateChecked As Boolean

Public SystemQuit As Boolean

Public GlobalData As Date

Public StartWinterSessionData As Date
Public StartSummerSessionData As Date

Public ChosenQuery As Integer

Public ChosenStudentsAction As Integer

Public Const ActiveFieldsColor = &HFFC0C0
Public Const NonActiveFieldsColor = &H80000005

Private Const NormalPageWidth = 11904
Private Const NormalPageHeight = 16832

Public CurrentPageWidth As Long
Public CurrentPageHeight As Long

Sub GetLinkCount(FullLinkString As String, ByRef Count As Integer)
Dim i As Integer

Count = 0
    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then Count = Count + 1
    Next i
    
End Sub

Sub GetLinkList(FullLinkString As String, ByRef LinkArrey As Variant, ByRef Count As Integer)
Dim i As Integer
Dim LastS As Integer
Dim ArrCounter As Integer

    Count = 0
    LastS = 0
    ArrCounter = 0

    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then Count = Count + 1
    Next i
    
    ReDim LinkArrey(Count)
    
    For i = 1 To Len(FullLinkString)
        If (Mid(FullLinkString, i, 1) = "|") Then
            LinkArrey(ArrCounter) = Mid(FullLinkString, LastS + 1, i - LastS - 1)
            ArrCounter = ArrCounter + 1
            LastS = i
        End If
    Next i
    
End Sub

Function SaveTable(FilePath As String, Table As String, Fields As String, FCount As Integer) As Boolean
    Dim i As Long
    Dim j As Long
    
    Dim DataArray As Variant
    Dim DataCols As Integer
    Dim DataRows As Long
    
    Dim SQLData As String
'    Dim FN As Integer
   
    CDB.SQLOpenTableToArrey "SELECT " & Fields & " FROM " & Table, DataArray, DataCols, DataRows
    
    Open FilePath For Output As #1 Len = 1
    If (DataRows > 0) Then
        'FN = FreeFile()
            For j = 0 To DataRows - 1
                Main.sbMain.PanelText("2") = "��������������� ���� ������...(" & Table & ")"
                ProgBar Main.PProgress, ((j + 1) * 100) / DataRows
                Main.sbMain.RedrawPanel ("3")
            
                SQLData = ""
                For i = 0 To FCount - 1
                    If (Not IsNull(DataArray(i, j))) Then
                        SQLData = SQLData & CStr(DataArray(i, j)) & "~"
                    Else
                        SQLData = SQLData & "NULL" & "~"
                    End If
                Next i
                Print #1, SQLData
            Next j
    End If
    Close #1
    
    ProgBar Main.PProgress, 0
    Main.sbMain.RedrawPanel ("3")
    Main.sbMain.PanelText("2") = "������"
    
End Function

Function CheckMark(ByRef Mark As String) As Boolean
    If (Mark = "+") Then Mark = "5"
    If (Mark = "-") Then Mark = "2"
    
        Select Case Mark
            Case "H", "�", "�", "1", "2", "3", "4", "5":
            Case "6", "7", "8", "9", "10", "11", "12":
                If (FivePointMarkSystem) Then
                    CheckMark = False
                    Exit Function
                End If
            Case Else:
                CheckMark = False
                Exit Function
        End Select
        
    CheckMark = True
End Function

Function CheckLoadPassword(F As Form) As Boolean
    If (LoadPassword = "") Then
        LoadPasswordChecked = True
        CheckLoadPassword = True
    Else
        ShowForm frmLoadPassword, 1, F
        CheckLoadPassword = LoadPasswordChecked
    End If
End Function

Sub BackFill(Source As PictureBox, Dest As PictureBox)
Dim i As Long
Dim j As Long

Dim iStep As Long
Dim jStep As Long

Dim ret As Long

iStep = Source.Width
jStep = Source.Height

While i <= Dest.Width
    While j <= Dest.Height
    
        ret = BitBlt(Dest.hdc, i \ 15, j \ 15, iStep \ 15, jStep \ 15, Source.hdc, 0, 0, SRCCOPY)
        j = j + jStep
    Wend
    
    j = 0
    i = i + iStep
Wend
Dest.Refresh

End Sub

Sub FillProgressTable(IDStud As Long, TP As VSFlexGrid)
Dim StudMarkArrey As Variant
Dim SMCols As Integer
Dim SMRows As Long

Dim i As Long

CDB.SQLOpenTableToArrey "SELECT" & _
" tblSubjects.Subject," & _
" tblEducationalPlan.KindOfReport, tblEducationalPlan.Semester," & _
" tblSheets.Mark, tblSheets.IDExam, tblEducationalPlan.NumOfHours, tblSheets.ID, tblSheets.SheetsPrinted," & _
"tblSheets.ECTS" & _
" FROM [tblEducationalPlan], [tblSubjects], [tblSheets], [tblExaminations]" & _
" WHERE tblSheets.IDStudent = " & IDStud & " AND" & _
" tblSheets.IDExam = tblExaminations.ID AND" & _
" tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
" tblEducationalPlan.IDSubject = tblSubjects.ID ORDER BY tblEducationalPlan.Semester, tblEducationalPlan.KindOfReport DESC", _
StudMarkArrey, SMCols, SMRows
    
    Dim CurSemester As Byte
    CurSemester = 0
    
    Dim ExamCounter As Long
    ExamCounter = 1
    
    TP.Rows = 1
    If (SMRows > 0) Then
        For i = 0 To SMRows - 1
            If (CurSemester < CByte(StudMarkArrey(2, i))) Then
                TP.AddItem vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i)) & vbTab & "������� " & CByte(StudMarkArrey(2, i))
                TP.RowData(TP.Rows - 1) = "Semester"
                TP.MergeRow(TP.Rows - 1) = True
                TP.Cell(flexcpAlignment, TP.Rows - 1, 1, TP.Rows - 1, 9) = flexAlignCenterCenter
                
                TP.Cell(flexcpBackColor, TP.Rows - 1, 1, TP.Rows - 1, 9) = RGB(240, 240, 240)

                ExamCounter = 1
                CurSemester = StudMarkArrey(2, i)
            End If
            
            TP.AddItem ExamCounter
            TP.RowData(TP.Rows - 1) = CLng(StudMarkArrey(4, i))
            TP.TextMatrix(TP.Rows - 1, 1) = CStr(StudMarkArrey(0, i))
            TP.TextMatrix(TP.Rows - 1, 8) = CLng(StudMarkArrey(6, i))
            If (Not IsNull(StudMarkArrey(5, i))) Then TP.TextMatrix(TP.Rows - 1, 2) = CLng(StudMarkArrey(5, i))
            
            TP.TextMatrix(TP.Rows - 1, 3) = CStr(StudMarkArrey(1, i))
            TP.TextMatrix(TP.Rows - 1, 6) = CByte(StudMarkArrey(2, i))
            If (Not IsNull(StudMarkArrey(7, i))) Then
                TP.TextMatrix(TP.Rows - 1, 9) = CByte(StudMarkArrey(7, i))
            Else
                TP.TextMatrix(TP.Rows - 1, 9) = 0
            End If
            
            TP.TextMatrix(TP.Rows - 1, 5) = IIf(IsNull(StudMarkArrey(8, i)), "", StudMarkArrey(8, i))
            
            If (CStr(StudMarkArrey(1, i)) = "�������") Then
                Select Case CByte(StudMarkArrey(3, i))
                    Case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
                        TP.TextMatrix(TP.Rows - 1, 4) = CByte(StudMarkArrey(3, i))
                    Case 201: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                    Case 202: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                    Case 203: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                End Select

                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:   TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 3:    TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 4:    TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 5:    TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                Else
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:  TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 4, 5, 6:  TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 7, 8, 9:  TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                End If
            End If
            
            If (CStr(StudMarkArrey(1, i)) = "�����") Then
                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                            
                        Case 1, 2:
                            TP.TextMatrix(TP.Rows - 1, 4) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                            
                        Case 3, 4, 5:
                            TP.TextMatrix(TP.Rows - 1, 4) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:        TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 3, 4, 5:     TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                Else
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 4) = "�"
                            
                        Case 1, 2, 3:
                            TP.TextMatrix(TP.Rows - 1, 4) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                            
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:
                            TP.TextMatrix(TP.Rows - 1, 4) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 4) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(3, i))
                        Case 201:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:                       TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                End If
            End If
            
            If (CStr(StudMarkArrey(1, i)) = "��� ������") Then
'                TP.TextMatrix(TP.Rows - 1, 5) = "��"
'                TP.TextMatrix(TP.Rows - 1, 7) = "��"
            End If

            ExamCounter = ExamCounter + 1
        Next i
    End If

    
    Dim a As Variant
    Dim c As Integer
    Dim r As Long
    Dim IsNewSheet As Boolean
    If IDStud = 0 Then
        IsNewSheet = False
    Else
        CDB.SQLOpenTableToArrey "SELECT [isactive] FROM tblstudents WHERE id = " & IDStud, a, c, r
        If CByte(a(0, 0)) = 1 Then
            CDB.SQLOpenTableToArrey "SELECT [FirstSessionYear] FROM [tblGroups],[tblStudents] WHERE tblGroups.ID = IDGroup and tblStudents.ID=" & IDStud, a, c, r
            If CInt(a(0, 0)) >= 2005 Then IsNewSheet = True Else IsNewSheet = False
        Else
            CDB.SQLOpenTableToArrey "SELECT [FirstSessionYear] FROM [tblGroups],[tblStudents] WHERE tblgroups.GroupName = TRestoreInfo and tblStudents.ID=" & IDStud, a, c, r
            If r = 0 Then
            IsNewSheet = True
            Else
                If CInt(a(0, 0)) >= 2005 Then IsNewSheet = True Else IsNewSheet = False
            End If
        End If
    End If
    
    If IsNewSheet Then
        TP.ColHidden(5) = False
    Else
        TP.ColHidden(5) = True
    End If
    
End Sub

Sub FillSpecCoursesTable(IDStud As Long, TP As VSFlexGrid, CourseType As Byte)
Dim StudMarkArrey As Variant
Dim SMCols As Integer
Dim SMRows As Long

Dim i As Long

CDB.SQLOpenTableToArrey "SELECT" & _
" tblSpecialCourses.ID, tblSubjects.Subject," & _
" tblSpecialCourses.KindOfReport, tblSpecialCourses.Semester," & _
" tblSpecialCourses.Mark, tblSpecialCourses.NumOfHours," & _
" tblLecturers.Surname, tblLecturers.Name, tblLecturers.Patronymic" & _
" FROM [tblSpecialCourses], [tblSubjects], [tblLecturers]" & _
" WHERE tblSpecialCourses.IDStudent = " & IDStud & " AND " & _
" tblSpecialCourses.CourseType = " & CourseType & " AND" & _
" tblSpecialCourses.IDLecturer = tblLecturers.ID AND" & _
" tblSpecialCourses.IDSubject = tblSubjects.ID ORDER BY tblSpecialCourses.Semester, tblSpecialCourses.KindOfReport DESC", _
StudMarkArrey, SMCols, SMRows
    
    Dim CurSemester As Byte
    CurSemester = 0
    
    Dim ExamCounter As Long
    ExamCounter = 1
    
    TP.Rows = 1
    If (SMRows > 0) Then
        For i = 0 To SMRows - 1
            If (CurSemester < CByte(StudMarkArrey(3, i))) Then
                TP.AddItem " "
                TP.Cell(flexcpText, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = "������� " & CByte(StudMarkArrey(3, i))
                TP.RowData(TP.Rows - 1) = "Semester"
                TP.MergeRow(TP.Rows - 1) = True
                TP.Cell(flexcpAlignment, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = flexAlignCenterCenter
                
                TP.Cell(flexcpBackColor, TP.Rows - 1, 1, TP.Rows - 1, TP.Cols - 1) = RGB(240, 240, 240)
                
                ExamCounter = 1
                CurSemester = StudMarkArrey(3, i)
            End If
            
            TP.AddItem ExamCounter
            TP.RowData(TP.Rows - 1) = CLng(StudMarkArrey(0, i))
            
            If (Not IsNull(StudMarkArrey(1, i))) Then TP.TextMatrix(TP.Rows - 1, 1) = CStr(StudMarkArrey(1, i))
            If (Not IsNull(StudMarkArrey(5, i))) Then TP.TextMatrix(TP.Rows - 1, 2) = CStr(StudMarkArrey(5, i))
            If (Not IsNull(StudMarkArrey(6, i))) Then TP.TextMatrix(TP.Rows - 1, 3) = GetFullName(CStr(StudMarkArrey(6, i)), CStr(StudMarkArrey(7, i)), CStr(StudMarkArrey(8, i)), False, True, True)
            If (Not IsNull(StudMarkArrey(2, i))) Then TP.TextMatrix(TP.Rows - 1, 4) = CStr(StudMarkArrey(2, i))
            If (Not IsNull(StudMarkArrey(3, i))) Then TP.TextMatrix(TP.Rows - 1, 6) = CStr(StudMarkArrey(3, i))
            
            If (CStr(StudMarkArrey(2, i)) = "�������") Then
                Select Case CByte(StudMarkArrey(4, i))
                    Case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12:
                        TP.TextMatrix(TP.Rows - 1, 5) = CByte(StudMarkArrey(4, i))
                    Case 201: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                    Case 202: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                    Case 203: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                End Select

                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:   TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 3:    TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 4:    TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 5:    TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                Else
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:   TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:  TP.TextMatrix(TP.Rows - 1, 7) = "�����������"
                        Case 4, 5, 6:  TP.TextMatrix(TP.Rows - 1, 7) = "���������"
                        Case 7, 8, 9:  TP.TextMatrix(TP.Rows - 1, 7) = "�����"
                        Case 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "³�����"
                    End Select
                End If
            End If
            
            If (CStr(StudMarkArrey(2, i)) = "�����") Then
                If (FivePointMarkSystem) Then
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                            
                        Case 1, 2:
                            TP.TextMatrix(TP.Rows - 1, 5) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                            
                        Case 3, 4, 5:
                            TP.TextMatrix(TP.Rows - 1, 5) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:         TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2:        TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 3, 4, 5:     TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                Else
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 202: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                        Case 203: TP.TextMatrix(TP.Rows - 1, 5) = "�"
                            
                        Case 1, 2, 3:
                            TP.TextMatrix(TP.Rows - 1, 5) = "-": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                            
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:
                            TP.TextMatrix(TP.Rows - 1, 5) = "+": TP.Cell(flexcpFontBold, TP.Rows - 1, 5) = True
                           
                    End Select
                    
                    Select Case CByte(StudMarkArrey(4, i))
                        Case 201:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 202:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 203:       TP.TextMatrix(TP.Rows - 1, 7) = "�"
                        Case 1, 2, 3:   TP.TextMatrix(TP.Rows - 1, 7) = "������������"
                        Case 4, 5, 6, 7, 8, 9, 10, 11, 12:  TP.TextMatrix(TP.Rows - 1, 7) = "����������"
                    End Select
                    
                End If
            End If
            
            If (CStr(StudMarkArrey(2, i)) = "��� ������") Then
'                TP.TextMatrix(TP.Rows - 1, 4) = "��"
'                TP.TextMatrix(TP.Rows - 1, 6) = "��"
            End If

            ExamCounter = ExamCounter + 1
        Next i
    End If

End Sub


Sub GetSheetInfo(IDExam As Long, Semester As Byte, ByRef SheetNumber As String, ByRef specName As String, ByRef GroupName As String, ByRef Lecturer As String)
    Dim SheetInfoArrey As Variant
    Dim SICols As Integer
    Dim SIRows As Long
    
    CDB.SQLOpenTableToArrey "SELECT" & _
    " tblSpecialities.SpecName, tblGroups.GroupName," & _
    " tblLecturers.Post, tblLecturers.Surname, tblLecturers.Name, tblLecturers.Patronymic," & _
    " tblExaminations.SheetNumber, tblGroups.Semester" & _
    " FROM [tblSpecialities], [tblGroups], [tblLecturers], [tblExaminations]" & _
    " WHERE tblExaminations.ID = " & IDExam & " AND" & _
    " tblExaminations.IDLecturer = tblLecturers.ID AND" & _
    " tblExaminations.IDGroup = tblGroups.ID AND" & _
    " tblGroups.IDSpec = tblSpecialities.ID", _
    SheetInfoArrey, SICols, SIRows
    
    Semester = CByte(SheetInfoArrey(7, 0))
    specName = CStr(SheetInfoArrey(0, 0))
    GroupName = CStr(SheetInfoArrey(1, 0))
    If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(Semester Mod 2 = 1, (Semester \ 2) + 1, Semester \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))
    Lecturer = CStr(SheetInfoArrey(2, 0)) & " " & CStr(SheetInfoArrey(3, 0)) & " " & CStr(SheetInfoArrey(4, 0)) & " " & CStr(SheetInfoArrey(5, 0))
    SheetNumber = CStr(SheetInfoArrey(6, 0))
End Sub

Function SaveHTML(T As VSFlexGrid, Path As String, TCaption As String) As Boolean
Dim i As Long
Dim j As Long

Dim RowData As String

    DoEvents
    
    Open Path For Output As #1 Len = 1
    
        Print #1, "<HTML>"
        Print #1, Tab(1); "<BODY>"
        
        Print #1, Tab(1); "<P>" & TCaption & "</p>"
        
        Print #1, Tab(2); "<TABLE style='font-size: 8; font-family: MS Sans Serif' border = 1 cellspacing = 0 frame = box>"
        Main.sbMain.PanelText("2") = "����������..."
        
        For i = 0 To T.Rows - 1
        
            ProgBar Main.PProgress, ((i + 1) * 100) / T.Rows
            Main.sbMain.RedrawPanel ("3")
            
            Print #1, Tab(3); "<TR>"
            
            For j = 0 To T.Cols - 1
                
                If (T.ColHidden(j) = False) Then
                    If (i = 0) Then
                        RowData = "<TH width = " & (T.ColWidth(j) \ 15) & " style='font-weight: normal'>" & T.TextMatrix(i, j) & "</TH>"
                    Else
                        RowData = "<TH style='font-weight: normal'>" & T.TextMatrix(i, j) & "</TH>"
                    End If
                    
                    Print #1, Tab(4); RowData
                End If
                
            Next j
            
            Print #1, Tab(3); "</TR>"
        Next i
        
        Print #1, Tab(2); "</TABLE>"
        
        Print #1, Tab(1); "</BODY>"
        Print #1, "</HTML>"

    Close #1
    
    ProgBar Main.PProgress, 0
    Main.sbMain.RedrawPanel ("3")
    Main.sbMain.PanelText("2") = "������"

    SaveHTML = True
End Function

Function SaveHTMLList(L As ListBox, Path As String, LCaption As String) As Boolean
Dim i As Long
Dim j As Long

Dim RowData As String

    DoEvents
    
    Open Path For Output As #1 Len = 1
    
        Print #1, "<HTML>"
        Print #1, Tab(1); "<BODY>"
        
        Print #1, Tab(1); "<P>" & LCaption & "</p>"
        
        Print #1, Tab(2); "<TABLE style='font-size: 8; font-family: MS Sans Serif' border = 1 cellspacing = 0 frame = box>"
        Main.sbMain.PanelText("2") = "����������..."
        
        For i = 0 To L.ListCount - 1
        
            ProgBar Main.PProgress, ((i + 1) * 100) / L.ListCount
            Main.sbMain.RedrawPanel ("3")
            
            Print #1, Tab(3); "<TR><TD style='font-weight: normal'>" & L.List(i) & "</td></tr>"
        Next i
        
        Print #1, Tab(2); "</TABLE>"
        
        Print #1, Tab(1); "</BODY>"
        Print #1, "</HTML>"

    Close #1
    
    ProgBar Main.PProgress, 0
    Main.sbMain.RedrawPanel ("3")
    Main.sbMain.PanelText("2") = "������"

    SaveHTMLList = True
End Function

'Function SaveXLS(T As VSFlexGrid, Path As String) As Boolean
'    Dim DB As Database
'    Dim table As String
'    Dim Fields As String
'    Dim StrData As String
'
'    Dim i As Long
'    Dim j As Long
'
'    If Path = "" Then SaveXLS = False
'    If Dir(Path) <> "" Then SaveXLS = False
'
'    Set DB = DBEngine.OpenDatabase(Path, False, False, "Excel 5.0;")
'
'    table = "CREATE TABLE DOWS("
'    For i = 0 To T.Cols - 1
'        If (i = T.Cols - 1) Then
'            table = table & "[" & T.TextMatrix(0, i) & "] " & "TEXT (255))"
'        Else
'            table = table & "[" & T.TextMatrix(0, i) & "] " & "TEXT (255), "
'        End If
'    Next i
'    DB.Execute table
'
'    Fields = "INSERT INTO DOWS("
'    For i = 0 To T.Cols - 1
'        If (i = T.Cols - 1) Then
'            Fields = Fields & "" & T.TextMatrix(0, i) & ")"
'        Else
'            Fields = Fields & "" & T.TextMatrix(0, i) & ", "
'        End If
'    Next i
'    Fields = Fields & " VALUES ("
'
'    For i = 1 To T.Rows - 1
'        StrData = Fields
'
'        For j = 0 To T.Cols - 1
'            If (j = T.Cols - 1) Then
'                StrData = StrData & "'" & T.TextMatrix(i, j) & "')"
'            Else
'                StrData = StrData & "'" & T.TextMatrix(i, j) & "', "
'            End If
'        Next j
'
'        DB.Execute StrData, 128
'    Next i
'
'
'
'    DB.Close
'End Function

Sub ResizeColumns(VSTB As VSFlexGrid)
On Error Resume Next
VSTB.AutoSize 0, VSTB.Cols - 1

'Dim i, j As Long
'Dim MaxLen As Long
'
'For j = 0 To VSTB.Cols - 1
'    For i = 0 To VSTB.Rows - 1
'        Main.strlen.Caption = VSTB.TextMatrix(i, j)
'        If (MaxLen < Main.strlen.Width) Then MaxLen = Main.strlen.Width
'    Next i
'
'    VSTB.ColWidth(j) = IIf((MaxLen + 100) <= (VSTB.Width - 1000), (MaxLen + 100), (VSTB.Width - 1000))
'    MaxLen = 0
'Next j

End Sub

Sub ResizeColumn(VSTB As VSFlexGrid, Col As Long)
On Error Resume Next
VSTB.AutoSize Col


'Dim i As Long
'Dim MaxLen As Long
'
'    For i = 0 To VSTB.Rows - 1
'        Main.strlen.Caption = VSTB.TextMatrix(i, Col)
'        If (MaxLen < Main.strlen.Width) Then MaxLen = Main.strlen.Width
'    Next i
'
'    VSTB.ColWidth(Col) = IIf((MaxLen + 100) <= (VSTB.Width - 1000), (MaxLen + 100), (VSTB.Width - 1000))
'    MaxLen = 0
End Sub

Sub EnumerateColumns(VSTB As VSFlexGrid, Col As Long)
Dim i As Long
    For i = 1 To VSTB.Rows - 1
        VSTB.TextMatrix(i, Col) = i
    Next i
End Sub

Sub EnumerateDebtsOrMagazine(VSTB As VSFlexGrid, DM As Boolean)
Dim i As Long
Dim Counter As Long

    If (DM) Then    'Debts
        Counter = 1
        For i = 1 To VSTB.Rows - 2
            If (VSTB.TextMatrix(i, 2) <> "") Then
                VSTB.TextMatrix(i, 0) = Counter
                Counter = Counter + 1
            End If
        Next i
    Else            'Magazine
        Counter = 1
        For i = 1 To VSTB.Rows - 1
            If (VSTB.TextMatrix(i, 1) <> "") Then
                VSTB.TextMatrix(i, 0) = Counter
                Counter = Counter + 1
            End If
        Next i
    End If
End Sub

Sub EnumerateGrants(VSTB As VSFlexGrid)
Dim i As Long
Dim Counter As Long

    Counter = 1
    For i = 1 To VSTB.Rows - 1
        If (VSTB.TextMatrix(i, 2) <> "") Then
            VSTB.TextMatrix(i, 0) = Counter
            Counter = Counter + 1
        ElseIf (VSTB.TextMatrix(i, 1) <> "") Then
            Counter = 1
        End If
    Next i
End Sub

Sub EnumerateTransmission(VSTB As VSFlexGrid)
Dim i As Long
Dim Counter As Long

    Counter = 1
    For i = 1 To VSTB.Rows - 1
        If (VSTB.TextMatrix(i, 3) <> "") Then
            VSTB.TextMatrix(i, 0) = Counter
            Counter = Counter + 1
        ElseIf (VSTB.RowData(i) = "GN") Then
            Counter = 1
        End If
    Next i
End Sub

Function FindErrors(StrData As String, Datalen As Integer, Field As String, NotEmpty As Boolean, CheckLen As Boolean) As Boolean

If (NotEmpty) Then
    If (StrData = "") Then
        MsgBox "��������� ���� '" & Field & "'.", vbOKOnly, "������"
        FindErrors = True
        Exit Function
    End If
End If

'If (InStr(1, strData, "'") <> 0) Then
'    MsgBox "���� '" & Field & "' �� ������ ��������� ����� �������� ��� << ' >>.", vbOKOnly, "������"
'    FindErrors = True
'    Exit Function
'End If

If (CheckLen) Then
    If (Len(StrData) > Datalen) Then
        MsgBox "���� '" & Field & "' ����� ��������� �� ����� " & Datalen & " ��������.", vbOKOnly, "������"
        FindErrors = True
        Exit Function
    End If
End If

FindErrors = False
End Function

Sub CheckString(ByRef TString As String)
    Dim AddString As String
    Dim i  As Long
    
    For i = 1 To Len(TString)
        If (Mid(TString, i, 1) <> "'" And Mid(TString, i, 1) <> "[" And Mid(TString, i, 1) <> "]") Then AddString = AddString & Mid(TString, i, 1)
        If (Mid(TString, i, 1) = "'") Then AddString = AddString & "' & Chr(39) & '"
        If (Mid(TString, i, 1) = "[") Then AddString = AddString & "[[]"
        If (Mid(TString, i, 1) = "]") Then AddString = AddString & "[]]"
    Next
    
    TString = AddString
End Sub

Function GetFullName(Surname As String, Name As String, Patronymic As String, UcaseSurname As Boolean, Initials As Boolean, NOSign As Boolean) As String
Dim S As String
Dim N As String
Dim P As String
Dim FullName As String

    If ((Right(Surname, 1) = "!") Or (Right(Name, 1) = "!") Or (Right(Patronymic, 1) = "!")) Then
        FullName = Surname & Name & Patronymic
        If (NOSign) Then FullName = Left(FullName, Len(FullName) - 1)
    Else
        S = Surname
        If (UcaseSurname) Then S = UCase(S)
        
        N = Name
        P = Patronymic
        
        If (Initials) Then
            N = UCase(Left$(N, 1)) & ". "
            P = UCase(Left$(P, 1)) & ". "
        End If
        
        FullName = S & " " & N & " " & P
    End If

    GetFullName = FullName
End Function
Function GetFIO(FullName As String, KindOfHuman As String, Field As String, SurnameLen As Integer, NameLen As Integer, PatronymicLen As Integer, ByRef S As String, ByRef N As String, ByRef P As String) As Boolean
Dim Surname As String
Dim Name As String
Dim Patronymic As String

If (FindErrors(FullName, 120, Field, True, True)) Then
    GetFIO = False
    Exit Function
End If

If (Right$(FullName, 1) <> "!") Then
    Dim FirstS As Byte
    Dim SecondS As Byte
    
    FirstS = InStr(1, FullName, " ")
    If (FirstS) Then SecondS = InStr(InStr(1, FullName, " ") + 1, FullName, " ")
    
    If ((FirstS = 0) Or (SecondS = 0)) Then
        MsgBox Field & " " & KindOfHuman & " ������ ���� ����: '������� ������ ����������'.", vbOKOnly, "������"
        GetFIO = False
        Exit Function
    End If
    
    Surname = Left(FullName, FirstS - 1)
    Name = Mid(FullName, FirstS + 1, SecondS - FirstS - 1)
    Patronymic = Right(FullName, Len(FullName) - SecondS)
    
    If ((Surname = "") Or (Name = "") Or (Patronymic = "")) Then
        MsgBox Field & " " & KindOfHuman & " ������ ���� ����: '������� ������ ����������'.", vbOKOnly, "������"
        GetFIO = False
        Exit Function
    End If
    
    If (Len(Surname) > SurnameLen) Then
        MsgBox "������� " & KindOfHuman & " ����� �������� �� �����, ��� �� " & SurnameLen & " ��������.", vbOKOnly, "������"
        GetFIO = False
        Exit Function
    End If
    If (Len(Name) > NameLen) Then
        MsgBox "��� " & KindOfHuman & " ����� �������� �� �����, ��� �� " & NameLen & " ��������.", vbOKOnly, "������"
        GetFIO = False
        Exit Function
    End If
    If (Len(Patronymic) > PatronymicLen) Then
        MsgBox "�������� " & KindOfHuman & " ����� �������� �� �����, ��� �� " & PatronymicLen & " ��������.", vbOKOnly, "������"
        GetFIO = False
        Exit Function
    End If
    
    S = Surname
    N = Name
    P = Patronymic

Else

    If (Len(FullName) <= 40) Then
        S = FullName
        N = ""
        P = ""
    ElseIf (Len(FullName) <= 80) Then
        S = Left(FullName, 40)
        N = Mid(FullName, 41, Len(FullName) - 40)
        P = ""
    ElseIf (Len(FullName) <= 120) Then
        S = Left(FullName, 40)
        N = Mid(FullName, 41, 40)
        P = Mid(FullName, 81, Len(FullName) - 80)
    End If

End If

GetFIO = True
End Function

Function AreStudentHaveDebts(IDStud As Long, Semester As Byte, Optional UseLastSemester As Boolean = False) As Boolean
Dim StudMarkArrey As Variant
Dim SMCols As Integer
Dim SMRows As Long

Dim StudentArrey As Variant
Dim SCols As Integer
Dim SRows As Long

Dim EIndex As Long
Dim TotalDebts As Long
Dim fOk As Boolean

    CDB.SQLOpenTableToArrey "SELECT" & _
    " tblSheets.ID, tblEducationalPlan.IsAdditional" & _
    " FROM tblSheets, tblExaminations, tblEducationalPlan" & _
    " WHERE tblSheets.IDStudent = " & IDStud & " AND" & _
    " tblExaminations.ID = tblSheets.IDExam AND" & _
    " tblEducationalPlan.ID = tblExaminations.IDEP AND" & _
    " tblEducationalPlan.IsAdditional = 0 AND" & _
    " tblSheets.Mark IN " & IIf(FivePointMarkSystem, "(201, 202, 203, 1, 2)", "(201, 202, 203, 1, 2, 3)") & " AND" & _
    " tblEducationalPlan.Semester" & IIf(UseLastSemester, " = ", " <= ") & Semester, _
    StudMarkArrey, SMCols, SMRows

    TotalDebts = SMRows
    
    If (TotalDebts > 0) Then AreStudentHaveDebts = True
    If (TotalDebts = 0) Then AreStudentHaveDebts = False
    
End Function

Function VerifyTwipsX(Twip As Long) As Long
    VerifyTwipsX = CLng(CDbl(Twip) * (CDbl(CurrentPageWidth) / CDbl(NormalPageWidth)))
End Function

Function VerifyTwipsY(Twip As Long) As Long
    VerifyTwipsY = CLng(CDbl(Twip) * (CDbl(CurrentPageHeight) / CDbl(NormalPageHeight)))
End Function


Sub ProgBar(picProgBar As PictureBox, Percent As Integer)
    
    picProgBar.Cls
    
    If (Percent = 0) Then Exit Sub
    
    picProgBar.Line (picProgBar.ScaleLeft, picProgBar.ScaleTop)-((picProgBar.ScaleWidth * Percent) / 100, picProgBar.ScaleHeight), picProgBar.FillColor, BF
    
    picProgBar.CurrentX = (picProgBar.ScaleWidth / 2) - picProgBar.TextWidth(CStr(Percent) & "%") / 2
    picProgBar.CurrentY = (picProgBar.ScaleHeight / 2) - picProgBar.TextHeight(CStr(Percent) & "%") / 2
    If (picProgBar.FillColor <> picProgBar.ForeColor) Then picProgBar.Print CStr(Percent) & "%"
    
    picProgBar.Refresh
    'DoEvents
    
End Sub


Sub ShowForm(NForm As Form, Modal As Integer, frmParent As Form)
    LoadSuccess = False
    Load NForm
    
    If (Not LoadSuccess) Then
        Unload NForm
        Exit Sub
    End If

    NForm.Left = (Screen.Width / 2) - (NForm.Width / 2)
    NForm.Top = (Screen.Height / 2) - (NForm.Height / 2)
    
    NForm.Show Modal, frmParent
    
End Sub

Sub LoadForm(NForm As Form)
    LoadSuccess = False
    Load NForm
    
    If (Not LoadSuccess) Then
        Unload NForm
        Exit Sub
    End If

    NForm.Left = (Screen.Width / 2) - (NForm.Width / 2)
    NForm.Top = (Screen.Height / 2) - (NForm.Height / 2)
End Sub

Sub ShowFrame(NFrame As Frame)
Dim i, i2 As Integer
NFrame.ZOrder
NFrame.Left = 0
NFrame.Top = -NFrame.Height
NFrame.Top = 0

'For i = NFrame.Top To 0
' NFrame.Top = i
'   Sleep (1)
'Next i
End Sub

Sub HideFrame(NFrame As Frame)
Dim i, i2 As Integer
NFrame.Left = 0
NFrame.Top = 0
NFrame.Top = -NFrame.Height

'For i = 0 To NFrame.Height
' NFrame.Top = NFrame.Top - 1
'   Sleep (1)
'Next i
End Sub

