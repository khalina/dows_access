VERSION 5.00
Begin VB.Form frmAddEditExams 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "���������� ����������"
   ClientHeight    =   4815
   ClientLeft      =   7035
   ClientTop       =   2325
   ClientWidth     =   4935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4815
   ScaleWidth      =   4935
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F2 
      Height          =   4815
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4935
      Begin VB.CheckBox NewSubject 
         Caption         =   "�����"
         Height          =   255
         Left            =   3960
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "������"
         Height          =   375
         Left            =   3480
         TabIndex        =   12
         Top             =   4320
         Width           =   1335
      End
      Begin VB.OptionButton OptExam 
         Caption         =   "�������"
         Height          =   255
         Left            =   3480
         TabIndex        =   11
         Top             =   3480
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton OptOffset 
         Caption         =   "�����"
         Height          =   255
         Left            =   3480
         TabIndex        =   10
         Top             =   3720
         Width           =   735
      End
      Begin VB.CommandButton cmdAddExam 
         Caption         =   "��������"
         Height          =   375
         Left            =   2280
         TabIndex        =   9
         Top             =   4320
         Width           =   1215
      End
      Begin VB.TextBox txtSemester 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   8
         Top             =   4440
         Width           =   615
      End
      Begin VB.TextBox txtSubjectName 
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3735
      End
      Begin VB.ListBox SubjectList 
         Height          =   1620
         Left            =   120
         TabIndex        =   7
         Top             =   600
         Width           =   4695
      End
      Begin VB.ComboBox comboAddE2 
         Height          =   315
         ItemData        =   "frmAddEditExams.frx":0000
         Left            =   1440
         List            =   "frmAddEditExams.frx":000D
         TabIndex        =   6
         Top             =   3320
         Width           =   1695
      End
      Begin VB.TextBox txtCode 
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   3720
         Width           =   1695
      End
      Begin VB.TextBox txtHours 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   4
         Top             =   4080
         Width           =   615
      End
      Begin VB.OptionButton optNoMark 
         Caption         =   "��� ������"
         Height          =   255
         Left            =   3480
         TabIndex        =   3
         Top             =   3960
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.ComboBox comboFaculty 
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Top             =   2880
         Width           =   4695
      End
      Begin VB.Label LF51 
         AutoSize        =   -1  'True
         Caption         =   "��� ���������� :"
         Height          =   195
         Left            =   3480
         TabIndex        =   21
         Top             =   3240
         Width           =   1260
      End
      Begin VB.Label LF53 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   645
         TabIndex        =   20
         Top             =   4440
         Width           =   750
      End
      Begin VB.Label �����1 
         Caption         =   "���������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label lblSubj 
         Height          =   375
         Left            =   1560
         TabIndex        =   18
         Top             =   2280
         Width           =   3255
      End
      Begin VB.Label �����3 
         Caption         =   "������������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   3320
         Width           =   1335
      End
      Begin VB.Label �����4 
         Caption         =   "��� (���) :"
         Height          =   255
         Left            =   555
         TabIndex        =   16
         Top             =   3720
         Width           =   855
      End
      Begin VB.Label �����5 
         AutoSize        =   -1  'True
         Caption         =   "�������� ����� :"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   4080
         Width           =   1305
      End
      Begin VB.Label �����6 
         Caption         =   "������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2640
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmAddEditExams"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CreateNewSubject As Boolean
Private SelectedSubject As Long

Private Sub cmdAddExam_Click()

    Dim SubjName As String
    
    Dim SemNum As Long
    Dim KindOfRep As String
    Dim Code As String
    Dim Hours As Long
    
    Dim IDArrey As Variant
    Dim IDCols As Integer
    Dim IDRows As Long
    
    Dim AR As Long
    
    '''???
    If CreateNewSubject Then
        SubjName = txtSubjectName.text
    Else
        SubjName = SubjectList.text
    End If
    
    If (txtSemester.text = "") Then
        MsgBox "������� ����� ��������", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (Not IsNumeric(txtSemester.text)) Then
        MsgBox "���� '��������' ������ ��������� �����", vbOKOnly, "������"
        Exit Sub
    End If
    
    
    If (txtHours.text = "") Then
        If Not (SubjName Like "*�������*" Or SubjName Like "*�����*") Then
            MsgBox "������� ���������� ���������� �����", vbOKOnly, "������"
            Exit Sub
        Else
            txtHours.text = "0"
        End If
    End If
    
    If (Not IsNumeric(txtHours.text)) Then
        MsgBox "���� '�������� �����' ������ ��������� �����", vbOKOnly, "������"
        Exit Sub
    End If
    
    If (CreateNewSubject) Then
        Dim Subj As String
        Subj = txtSubjectName.text
        
        If (FindErrors(Subj, 200, "�������� ��������", True, True)) Then Exit Sub
        CheckString Subj

        If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblSubjects] WHERE [Subject] = '" & Subj & "'") > 0) Then
            MsgBox "������� � ����� ������ ��� ��� ���������������", vbOKOnly, "������"
            Exit Sub
        End If
        
        AR = CDB.ExecSQLActionQuery("INSERT INTO tblSubjects (Subject) VALUES ('" & Subj & "')", 128)
        
        CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSubjects] WHERE [Subject] = '" & Subj & "'", IDArrey, IDCols, IDRows
        
        SelectedSubject = CLng(IDArrey(0, 0))
    End If
    
    If (SelectedSubject = 0) Then
        MsgBox "�������� �������", vbOKOnly, "������"
        Exit Sub
    End If
    
    SemNum = CLng(txtSemester.text)
    If (OptExam.Value) Then KindOfRep = "�������"
    If (OptOffset.Value) Then KindOfRep = "�����"
    If (optNoMark.Value) Then KindOfRep = "��� ������"
    Hours = CLng(txtHours.text)
    
    If (FindErrors(txtCode.text, 30, "��� (���)", False, True)) Then Exit Sub
    Code = txtCode.text
    CheckString Code
    
    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    Dim fOk As Boolean
    
    If (frmEducationalPlan.EPAddNewStatus = True) Then
        fOk = True
        CDB.SQLOpenTableToArrey "SELECT [ID], [IsActive] FROM [tblEducationalPlan] WHERE [IDSpec] = " & frmEducationalPlan.SelectedSpeciality & " AND [IDSubject] = " & SelectedSubject & " AND [Semester] = " & SemNum & " AND [KindOfReport] = '" & KindOfRep & "' AND [SCode] = '" & Code & "' AND [NumOfHours] = " & IIf(Hours = 0, "Null", Hours), DopArrey, DopCols, DopRows
        If (DopRows > 0) Then
            If (DopArrey(1, 0) = 1) Then
                MsgBox "��� ���������� � ������ ������� ��� ��� ���������������", vbOKOnly, "������"
                Exit Sub
            End If
            If (DopArrey(1, 0) = 0) Then
                AR = CDB.ExecSQLActionQuery("UPDATE [tblEducationalPlan] SET [IsActive] = 1 WHERE [ID] = " & CLng(DopArrey(0, 0)), 128)
                fOk = False
            End If
        End If
        
        If (fOk) Then AR = CDB.ExecSQLActionQuery("INSERT INTO tblEducationalPlan (IDSpec, IDSubject, Semester, KindOfReport, IsActive, IsAdditional, SCode, NumOfHours) VALUES (" & frmEducationalPlan.SelectedSpeciality & ", " & SelectedSubject & ", " & SemNum & ", '" & KindOfRep & "', 1, " & comboAddE2.ListIndex & ", '" & Code & "', " & IIf(Hours = 0, "Null", Hours) & ")", 128)
        
    Else
        AR = CDB.ExecSQLActionQuery("UPDATE [tblEducationalPlan] SET [IDSubject] = " & SelectedSubject & ", [Semester] = " & SemNum & ", [KindOfReport] = '" & KindOfRep & "', [IsAdditional] = " & comboAddE2.ListIndex & ", [SCode] = '" & Code & "', [NumOfHours] = " & IIf(Hours = 0, "Null", Hours) & " WHERE [ID] = " & frmEducationalPlan.SelectedEPSubject, 128)
        
    End If
    
    frmEducationalPlan.LoadEducationalPlan frmEducationalPlan.SelectedSpeciality
    
    Unload Me
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub comboAddE2_Change()
If (comboAddE2.ListIndex = 1) Then
    optNoMark.Visible = True
Else
    optNoMark.Visible = False
    If (optNoMark.Value = True) Then OptExam.Value = True
End If
End Sub

Private Sub Form_Load()
    Dim SubjArrey As Variant
    Dim SubjCols As Integer
    Dim SubjRows As Long
    
    Dim FacultyArrey As Variant
    Dim FCols As Integer
    Dim FRows As Long
    
    SelectedSubject = 0
    
    Dim i As Long

    If (frmEducationalPlan.EPAddNewStatus = True) Then
        frmAddEditExams.Caption = "���������� ����������"
        cmdAddExam.Caption = "��������"
            
        CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSubjects]", SubjArrey, SubjCols, SubjRows
        If (SubjRows = 0) Then
            NewSubject.Value = 1
            NewSubject_Click
        End If
        If (SubjRows > 0) Then
            NewSubject.Value = 0
            NewSubject_Click
        End If
        frmAddEditExams.comboAddE2.ListIndex = 0
        
    Else
        frmAddEditExams.Caption = "�������������� ����������"
        cmdAddExam.Caption = "OK"
            
        CDB.SQLOpenTableToArrey "SELECT [Subject], [KindOfReport], [Semester], [IsAdditional], [SCode], [NumOfHours] FROM [tblEducationalPlan], [tblSubjects] WHERE tblEducationalPlan.IDSubject = tblSubjects.ID AND tblEducationalPlan.ID = " & frmEducationalPlan.SelectedEPSubject, SubjArrey, SubjCols, SubjRows
        NewSubject.Value = 0
        NewSubject_Click
        
        txtSubjectName.text = CStr(SubjArrey(0, 0))
        txtSubjectName_Change
        
        If (Not IsNull(SubjArrey(4, 0))) Then txtCode.text = CStr(SubjArrey(4, 0))
        If (Not IsNull(SubjArrey(5, 0))) Then txtHours.text = CStr(SubjArrey(5, 0))
        If (Not IsNull(SubjArrey(2, 0))) Then txtSemester.text = CByte(SubjArrey(2, 0))
        
        If (CStr(SubjArrey(1, 0)) = "�������") Then OptExam.Value = True
        If (CStr(SubjArrey(1, 0)) = "�����") Then OptOffset.Value = True
        If (CStr(SubjArrey(1, 0)) = "��� ������") Then optNoMark.Value = True

        comboAddE2.ListIndex = SubjArrey(3, 0)
        comboAddE2_Change
        
    End If
    
        CDB.SQLOpenTableToArrey "SELECT [ID], [FacultyName], [SCode] FROM [tblFaculty] WHERE [IsActive] = 1", FacultyArrey, FCols, FRows

        comboFaculty.Clear
        If (FRows > 0) Then

            For i = 0 To FRows - 1
                If (IIf(IsNull(FacultyArrey(2, i)), "", FacultyArrey(2, i)) = "") Then
                    comboFaculty.AddItem FacultyArrey(1, i)
                Else
                    comboFaculty.AddItem FacultyArrey(1, i) & " (" & IIf(IsNull(FacultyArrey(2, i)), "", FacultyArrey(2, i)) & ")"
                End If
                
                comboFaculty.ItemData(comboFaculty.NewIndex) = FacultyArrey(0, i)
            Next i
            
            comboFaculty.ListIndex = 0
        End If

LoadSuccess = True
End Sub

Private Sub NewSubject_Click()
    Dim SubjArrey As Variant
    Dim SubjCols As Integer
    Dim SubjRows As Long

If (NewSubject.Value = 0) Then
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblSubjects]", SubjArrey, SubjCols, SubjRows
    If (SubjRows = 0) Then
        MsgBox "�� ���� ���������� ��� ��������� ��� ������, �������� ����� !"
        NewSubject.Value = 1
        Exit Sub
    End If

    CreateNewSubject = False
    txtSubjectName_Change
End If

If (NewSubject.Value = 1) Then
    CreateNewSubject = True
    txtSubjectName.text = ""
    txtSubjectName_Change
End If
End Sub

Private Sub SubjectList_Click()
If (SubjectList.ListIndex = -1) Then Exit Sub

SelectedSubject = SubjectList.ItemData(SubjectList.ListIndex)
lblSubj.Caption = SubjectList.List(SubjectList.ListIndex)

CreateNewSubject = False
NewSubject.Value = 0
End Sub

Private Sub txtSubjectName_Change()
SelectedSubject = 0
lblSubj.Caption = ""
SubjectList.Clear

If (Not CreateNewSubject) Then
    Dim SubjArrey As Variant
    Dim SubjCols As Integer
    Dim SubjRows As Long
    
    Dim i As Long
    Dim SubjName As String
    
    SubjName = txtSubjectName
    CheckString SubjName
    
    If (SubjName = "") Then Exit Sub
'    If (InStr(1, SubjName, "'") <> 0) Then Exit Sub

    '��������---------------------------------------------------------------------------------------------------------------------------------
        CDB.SQLOpenTableToArrey "SELECT [ID], [Subject] FROM [tblSubjects] WHERE [Subject] LIKE '" & SubjName & "*' ORDER BY [Subject]", SubjArrey, SubjCols, SubjRows
        
        If (SubjRows > 0) Then
            
            For i = 0 To SubjRows - 1
                SubjectList.AddItem SubjArrey(1, i)
                SubjectList.ItemData(SubjectList.NewIndex) = SubjArrey(0, i)
            Next i
            
            SubjectList.ListIndex = 0
        End If
    '-----------------------------------------------------------------------------------------------------------------------------------------
End If
End Sub
