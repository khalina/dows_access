VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Begin VB.Form frmGrants 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "�������"
   ClientHeight    =   4350
   ClientLeft      =   7125
   ClientTop       =   1455
   ClientWidth     =   7215
   Icon            =   "frmGrants.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4350
   ScaleWidth      =   7215
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F5 
      Height          =   855
      Left            =   0
      TabIndex        =   21
      Top             =   3480
      Width           =   7215
      Begin VB.OptionButton O4 
         Caption         =   "������� ���������"
         Height          =   255
         Left            =   2760
         TabIndex        =   26
         Top             =   480
         Width           =   1935
      End
      Begin VB.OptionButton O3 
         Caption         =   "��������� ���������"
         Height          =   255
         Left            =   2760
         TabIndex        =   25
         Top             =   240
         Width           =   1935
      End
      Begin VB.OptionButton O2 
         Caption         =   "�������� ��������������"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   480
         Width           =   2415
      End
      Begin VB.OptionButton O1 
         Caption         =   "������� ���������"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   495
         Left            =   5280
         TabIndex        =   22
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame F4 
      Caption         =   "������� ���������"
      Height          =   3495
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   7215
      Begin VB.TextBox txtGrantName 
         Height          =   285
         Left            =   120
         TabIndex        =   19
         Top             =   3120
         Width           =   3370
      End
      Begin VB.TextBox txtGrant 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3500
         TabIndex        =   18
         Top             =   3120
         Width           =   1700
      End
      Begin VB.CommandButton cmdAddNG 
         Caption         =   "��������"
         Height          =   360
         Left            =   5520
         TabIndex        =   17
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cmdDelNG 
         Caption         =   "�������"
         Height          =   360
         Left            =   5520
         TabIndex        =   16
         Top             =   600
         Width           =   1575
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblNominalGrants 
         Height          =   2745
         Left            =   120
         TabIndex        =   20
         Top             =   225
         Width           =   5295
         _cx             =   9340
         _cy             =   4842
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   0   'False
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Frame F3 
      Caption         =   "�������� ��������������"
      Height          =   3495
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   7215
      Begin VSFlex7DAOCtl.VSFlexGrid tblSpecExtra 
         Height          =   3195
         Left            =   120
         TabIndex        =   5
         Top             =   225
         Width           =   6975
         _cx             =   12303
         _cy             =   5636
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   0   'False
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Frame F2 
      Caption         =   "��������� ���������"
      Height          =   3495
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   7215
      Begin VB.CommandButton cmdDelCat 
         Caption         =   "�������"
         Height          =   360
         Left            =   5520
         TabIndex        =   14
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox txtExtra 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   4000
         TabIndex        =   8
         Top             =   3120
         Width           =   1180
      End
      Begin VB.TextBox txtNameCat 
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Top             =   3120
         Width           =   3880
      End
      Begin VB.CommandButton cmdAddCat 
         Caption         =   "��������"
         Height          =   360
         Left            =   5520
         TabIndex        =   6
         Top             =   240
         Width           =   1575
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblCategories 
         Height          =   2745
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   5295
         _cx             =   9340
         _cy             =   4842
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   0   'False
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Frame F1 
      Caption         =   "������� ���������"
      Height          =   3495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      Begin VB.CommandButton cmdDelI 
         Caption         =   "�������"
         Height          =   360
         Left            =   5520
         TabIndex        =   13
         Top             =   600
         Width           =   1575
      End
      Begin VB.CommandButton cmdAddI 
         Caption         =   "��������"
         Height          =   360
         Left            =   5520
         TabIndex        =   12
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox txtSum 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3540
         TabIndex        =   11
         Top             =   3120
         Width           =   1650
      End
      Begin VB.TextBox txtL 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1840
         TabIndex        =   10
         Top             =   3120
         Width           =   1700
      End
      Begin VB.TextBox txtS 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   120
         TabIndex        =   9
         Top             =   3120
         Width           =   1720
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblGrants 
         Height          =   2745
         Left            =   120
         TabIndex        =   1
         Top             =   225
         Width           =   5295
         _cx             =   9340
         _cy             =   4842
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   0   'False
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
End
Attribute VB_Name = "frmGrants"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private BefDataGrants As Variant
Private BefDataCategory As Variant
Private BefDataSpeciality As Variant
Private BefDataNominalGrant As Variant

Private comboValueGrantID As Long

Private Sub cmdAddCat_Click()
Dim CategoryName As String
Dim PercentExtra As Integer

CategoryName = txtNameCat.Text

If (CategoryName = "") Then
    MsgBox "��������� ���� '�������� ���������'", vbOKOnly, "������"
    Exit Sub
End If

'If (InStr(1, CategoryName, "'") <> 0) Then
'    MsgBox "�������� ��������� �� ������ ��������� ����� �������� ��� '", vbOKOnly, "������"
'    Exit Sub
'End If
CheckString CategoryName

If (Not IsNumeric(txtExtra.Text)) Then
    MsgBox "���� '��������' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If

PercentExtra = CCur(txtExtra.Text)

If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblCategories] WHERE [CategoryName] = '" & CategoryName & "'") > 0) Then
    MsgBox "��������� � ����� ������ ��� ����������", vbOKOnly, "������"
    Exit Sub
End If

Dim AR As Long

AR = CDB.ExecSQLActionQuery("INSERT INTO tblCategories(CategoryName, PercentExtra) VALUES ('" & CategoryName & "', " & PercentExtra & ")", 128)

If (AR > 0) Then

    Dim IDArrey As Variant
    Dim Cols As Integer
    Dim Rows As Long
    
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblCategories] WHERE [CategoryName] = '" & CategoryName & "' AND [PercentExtra] = " & PercentExtra, IDArrey, Cols, Rows
    If (Rows > 0) Then
        tblCategories.Rows = tblCategories.Rows + 1
        
        tblCategories.RowData(tblCategories.Rows - 1) = IDArrey(0, 0)
        tblCategories.TextMatrix(tblCategories.Rows - 1, 0) = txtNameCat.Text
        tblCategories.TextMatrix(tblCategories.Rows - 1, 1) = PercentExtra
    End If
    
End If
End Sub

Private Sub cmdAddI_Click()
Dim Min As String
Dim Max As String
Dim Grant As String

If (Not IsNumeric(txtS.Text)) Then
    MsgBox "���� '������ �������' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If

If (Not IsNumeric(txtL.Text)) Then
    MsgBox "���� '������� �������' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If

If (Not IsNumeric(txtSum.Text)) Then
    MsgBox "���� '�����' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If

Min = txtS.Text
Max = txtL.Text
Grant = txtSum.Text

If (Min > Max) Then
    MsgBox "������ ������� ������ ���� ������ ���� ������ ������� �������!", vbOKOnly, "������"
    Exit Sub
End If

If (InStr(1, Min, ",")) Then Mid(Min, InStr(1, Min, ",")) = "."
If (InStr(1, Max, ",")) Then Mid(Max, InStr(1, Max, ",")) = "."
If (InStr(1, Grant, ",")) Then Mid(Grant, InStr(1, Grant, ",")) = "."


If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblGrants] WHERE [Min] <= " & Min & " AND [Max] >= " & Min) > 0) Then
    MsgBox "������ ������� ������ � ��� ������������ ��������", vbOKOnly, "������"
    Exit Sub
End If


If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblGrants] WHERE [Min] <= " & Max & " AND [Max] >= " & Max) > 0) Then
    MsgBox "������� ������� ������ � ��� ������������ ��������", vbOKOnly, "������"
    Exit Sub
End If

If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblGrants] WHERE [Min] >= " & Min & " AND [Max] <= " & Max) > 0) Then
    MsgBox "��������� �������� �������� � ���� ��� ������������ ��������", vbOKOnly, "������"
    Exit Sub
End If

Dim AR As Long
AR = CDB.ExecSQLActionQuery("INSERT INTO tblGrants(Min, Max, GrantSize) VALUES (" & Min & ", " & Max & ", " & Grant & ")", 128)

If (AR > 0) Then

    Dim IDArrey As Variant
    Dim Cols As Integer
    Dim Rows As Long
    
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblGrants] WHERE [Min] = " & Min & " AND [Max] = " & Max & " AND [GrantSize] = " & Grant, IDArrey, Cols, Rows
    If (Rows > 0) Then
        tblGrants.Rows = tblGrants.Rows + 1
        
        tblGrants.RowData(tblGrants.Rows - 1) = IDArrey(0, 0)
        tblGrants.TextMatrix(tblGrants.Rows - 1, 0) = txtS.Text
        tblGrants.TextMatrix(tblGrants.Rows - 1, 1) = txtL.Text
        tblGrants.TextMatrix(tblGrants.Rows - 1, 2) = Format(txtSum.Text, "#######.00")
    End If
    
End If
End Sub

Private Sub cmdAddNG_Click()
Dim GrantName As String
Dim Grant As String

If (txtGrantName.Text = "") Then
    MsgBox "������� ��� ���������", vbOKOnly, "������"
    Exit Sub
End If
GrantName = txtGrantName.Text
CheckString GrantName

'If (InStr(1, GrantName, "'") <> 0) Then
'    MsgBox "��� ��������� �� ������ ��������� ����� �������� ��� '", vbOKOnly, "������"
'    Exit Sub
'End If

If (Not IsNumeric(txtGrant.Text)) Then
    MsgBox "���� '�����' ������ ��������� �����", vbOKOnly, "������"
    Exit Sub
End If

Grant = txtGrant.Text
If (InStr(1, Grant, ",")) Then Mid(Grant, InStr(1, Grant, ",")) = "."


If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblNominalGrants] WHERE [GrantName] = '" & GrantName & "'") > 0) Then
    MsgBox "��������� � ����� ������ ��� ����������", vbOKOnly, "������"
    Exit Sub
End If

Dim AR As Long
AR = CDB.ExecSQLActionQuery("INSERT INTO tblNominalGrants(GrantName, GrantSize) VALUES ('" & GrantName & "', " & Grant & ")", 128)

If (AR > 0) Then

    Dim IDArrey As Variant
    Dim Cols As Integer
    Dim Rows As Long
    
    CDB.SQLOpenTableToArrey "SELECT [ID] FROM [tblNominalGrants] WHERE [GrantName] = '" & GrantName & "'", IDArrey, Cols, Rows
    If (Rows > 0) Then
        tblNominalGrants.Rows = tblNominalGrants.Rows + 1
        
        tblNominalGrants.RowData(tblNominalGrants.Rows - 1) = IDArrey(0, 0)
        tblNominalGrants.TextMatrix(tblNominalGrants.Rows - 1, 0) = txtGrantName.Text
        tblNominalGrants.TextMatrix(tblNominalGrants.Rows - 1, 1) = Format(txtGrant.Text, "#######.00")
    End If
    
End If
End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdDelCat_Click()
Dim AR As Long

If (tblCategories.Row = -1) Then Exit Sub

If ((tblCategories.RowData(tblCategories.Row) = 1) Or (tblCategories.RowData(tblCategories.Row) = 2)) Then
    MsgBox "��� ������ �� ����� ���� �������", vbOKOnly, "������"
    Exit Sub
End If


If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblStudents] WHERE [IDCategory] = " & tblCategories.RowData(tblCategories.Row)) > 0) Then
    MsgBox "��� ������ �� ����� ���� �������, �.�. ������������ � ������� ���������", vbOKOnly, "������"
    Exit Sub
End If

AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblCategories] WHERE [ID] = " & tblCategories.RowData(tblCategories.Row), 128)
tblCategories.RemoveItem (tblCategories.Row)
End Sub

Private Sub cmdDelI_Click()
Dim AR As Long

If (tblGrants.Row = -1) Then Exit Sub

AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblGrants] WHERE [ID] = " & tblGrants.RowData(tblGrants.Row), 128)
tblGrants.RemoveItem (tblGrants.Row)

End Sub

Private Sub cmdDelNG_Click()
Dim AR As Long

If (tblNominalGrants.Row = -1) Then Exit Sub

If (tblNominalGrants.RowData(tblNominalGrants.Row) = 1) Then
    MsgBox "��� ������ �� ����� ���� �������", vbOKOnly, "������"
    Exit Sub
End If


If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblStudents] WHERE [IDNominalGrant] = " & tblNominalGrants.RowData(tblNominalGrants.Row)) > 0) Then
    MsgBox "��� ������ �� ����� ���� �������, �.�. ������������ � ������� ���������", vbOKOnly, "������"
    Exit Sub
End If

AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblNominalGrants] WHERE [ID] = " & tblNominalGrants.RowData(tblNominalGrants.Row), 128)
tblNominalGrants.RemoveItem (tblNominalGrants.Row)
End Sub


Private Sub Form_Load()
tblGrants.Cols = 3
tblGrants.Rows = 1
tblGrants.FixedRows = 1

tblGrants.TextMatrix(0, 0) = "������ �������"
tblGrants.TextMatrix(0, 1) = "������� �������"
tblGrants.TextMatrix(0, 2) = "�����"

tblGrants.ColWidth(0) = 1700
tblGrants.ColWidth(1) = 1700
tblGrants.ColWidth(2) = 1600

'///////////////////////////////////////////////////////

tblNominalGrants.Cols = 2
tblNominalGrants.Rows = 1
tblNominalGrants.FixedRows = 1

tblNominalGrants.TextMatrix(0, 0) = "��� ���������"
tblNominalGrants.TextMatrix(0, 1) = "�����"

tblNominalGrants.ColWidth(0) = 3350
tblNominalGrants.ColWidth(1) = 1650

'///////////////////////////////////////////////////////

tblCategories.Cols = 2
tblCategories.Rows = 1
tblCategories.FixedRows = 1

tblCategories.TextMatrix(0, 0) = "�������� ���������"
tblCategories.TextMatrix(0, 1) = "�������� (%)"

tblCategories.ColWidth(0) = 3850
tblCategories.ColWidth(1) = 1140

'///////////////////////////////////////////////////////

tblSpecExtra.Cols = 2
tblSpecExtra.Rows = 1
tblSpecExtra.FixedRows = 1

tblSpecExtra.TextMatrix(0, 0) = "�������� �������������"
tblSpecExtra.TextMatrix(0, 1) = "��������"

tblSpecExtra.ColWidth(0) = 5200
tblSpecExtra.ColWidth(1) = 1450

Dim i As Long

'������� ���������--------------------------------------------------------------------------------------------------------
    Dim GrantsArrey As Variant
    Dim GCols As Integer
    Dim GRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [Min], [Max], [GrantSize] FROM [tblGrants]", GrantsArrey, GCols, GRows
    
    If (GRows > 0) Then
        tblGrants.Rows = 1 + GRows
   
        For i = 0 To GRows - 1
            tblGrants.RowData(i + 1) = GrantsArrey(0, i)
            tblGrants.TextMatrix(i + 1, 0) = GrantsArrey(1, i)
            tblGrants.TextMatrix(i + 1, 1) = GrantsArrey(2, i)
            tblGrants.TextMatrix(i + 1, 2) = Format(GrantsArrey(3, i), "#######.00")
        Next i
    End If
'-------------------------------------------------------------------------------------------------------------------------
'������� ���������--------------------------------------------------------------------------------------------------------
    Dim NominalGrantsArrey As Variant
    Dim NGCols As Integer
    Dim NGRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [GrantName], [GrantSize] FROM [tblNominalGrants]", NominalGrantsArrey, NGCols, NGRows
    
    If (NGRows > 0) Then
        tblNominalGrants.Rows = 1 + NGRows
   
        For i = 0 To NGRows - 1
            tblNominalGrants.RowData(i + 1) = NominalGrantsArrey(0, i)
            tblNominalGrants.TextMatrix(i + 1, 0) = NominalGrantsArrey(1, i)
            tblNominalGrants.TextMatrix(i + 1, 1) = Format(NominalGrantsArrey(2, i), "#######.00")
        Next i
    End If
'-------------------------------------------------------------------------------------------------------------------------
'��������� ���������------------------------------------------------------------------------------------------------------
    Dim CategoryArrey As Variant
    Dim CCols As Integer
    Dim CRows As Long

    Dim DopArrey As Variant
    Dim DopCols As Integer
    Dim DopRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [CategoryName], [PercentExtra] FROM [tblCategories]", CategoryArrey, CCols, CRows
    
    If (CRows > 0) Then
        tblCategories.Rows = 1 + CRows
   
        For i = 0 To CRows - 1
            tblCategories.RowData(i + 1) = CategoryArrey(0, i)
            tblCategories.TextMatrix(i + 1, 0) = CategoryArrey(1, i)
            tblCategories.TextMatrix(i + 1, 1) = CategoryArrey(2, i)
        Next i
    End If
'-------------------------------------------------------------------------------------------------------------------------
'�������� ��������������--------------------------------------------------------------------------------------------------
    Dim SpecExtraArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long

    CDB.SQLOpenTableToArrey "SELECT [ID], [SpecName], [PercentExtra] FROM [tblSpecialities]", SpecExtraArrey, SCols, SRows
    
    If (SRows > 0) Then
        tblSpecExtra.Rows = 1 + SRows
   
        For i = 0 To SRows - 1
            tblSpecExtra.RowData(i + 1) = SpecExtraArrey(0, i)
            tblSpecExtra.TextMatrix(i + 1, 0) = SpecExtraArrey(1, i)
            tblSpecExtra.TextMatrix(i + 1, 1) = SpecExtraArrey(2, i)
        Next i
    End If
'-------------------------------------------------------------------------------------------------------------------------

O1.Value = True
O1_Click
LoadSuccess = True
End Sub

Private Sub O1_Click()
F1.ZOrder
End Sub

Private Sub O2_Click()
F3.ZOrder
End Sub

Private Sub O3_Click()
F2.ZOrder
End Sub

Private Sub O4_Click()
F4.ZOrder
End Sub

Private Sub tblCategories_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim AR As Long
Dim CategoryName As String
Dim PercentExtra As Integer

    If (Col = 0) Then
    
        CategoryName = tblCategories.TextMatrix(Row, Col)
        
        If (CategoryName = "") Then
            MsgBox "������� �������� ���������", vbOKOnly, "������"
            tblCategories.TextMatrix(Row, Col) = BefDataCategory
            Exit Sub
        End If

'        If (InStr(1, CategoryName, "'") <> 0) Then
'            MsgBox "��� ���� �� ������ ��������� ����� �������� ��� '", vbOKOnly, "������"
'            tblCategories.TextMatrix(Row, Col) = BefDataCategory
'            Exit Sub
'        End If
        CheckString CategoryName
        
        AR = CDB.ExecSQLActionQuery("UPDATE [tblCategories] SET [CategoryName] = '" & CategoryName & "' WHERE [ID] = " & tblCategories.RowData(Row), 128)
        
    ElseIf (Col = 1) Then
    
        If (Not IsNumeric(tblCategories.TextMatrix(Row, Col))) Then
            MsgBox "��� ���� ������ ��������� ����� �����", vbOKOnly, "������"
            tblCategories.TextMatrix(Row, Col) = BefDataCategory
            Exit Sub
        End If
        
        PercentExtra = CInt(tblCategories.TextMatrix(Row, Col))
                
        AR = CDB.ExecSQLActionQuery("UPDATE [tblCategories] SET [PercentExtra] = " & PercentExtra & " WHERE [ID] = " & tblCategories.RowData(Row), 128)
        tblCategories.TextMatrix(Row, Col) = PercentExtra
        
    End If
    
End Sub

Private Sub tblCategories_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefDataCategory = tblCategories.TextMatrix(Row, Col)
End Sub

Private Sub tblGrants_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim AR As Long
Dim GrantData As String
    If (tblGrants.Row = -1) Then Exit Sub

    If (Not IsNumeric(tblGrants.TextMatrix(Row, Col))) Then
        MsgBox "��� ���� ������ ��������� �����", vbOKOnly, "������"
        tblGrants.TextMatrix(Row, Col) = BefDataGrants
        Exit Sub
    End If
    
    GrantData = tblGrants.TextMatrix(Row, Col)
    tblGrants.TextMatrix(Row, Col) = Format(tblGrants.TextMatrix(Row, Col), "#######.00")
    If (InStr(1, GrantData, ",")) Then Mid(GrantData, InStr(1, GrantData, ",")) = "."
    
    If (Col = 0) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblGrants] SET [Min] = " & GrantData & " WHERE [ID] = " & tblGrants.RowData(Row), 128)
    ElseIf (Col = 1) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblGrants] SET [Max] = " & GrantData & " WHERE [ID] = " & tblGrants.RowData(Row), 128)
    ElseIf (Col = 2) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblGrants] SET [GrantSize] = " & GrantData & " WHERE [ID] = " & tblGrants.RowData(Row), 128)
    End If
       
End Sub

Private Sub tblGrants_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefDataGrants = tblGrants.TextMatrix(Row, Col)
End Sub

Private Sub tblNominalGrants_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim AR As Long
Dim GrantName As String
Dim GrantSum As String

    If (Col = 0) Then
    
        GrantName = tblNominalGrants.TextMatrix(Row, Col)
        
        If (GrantName = "") Then
            MsgBox "������� �������� ���������", vbOKOnly, "������"
            tblNominalGrants.TextMatrix(Row, Col) = BefDataNominalGrant
            Exit Sub
        End If

'        If (InStr(1, GrantName, "'") <> 0) Then
'            MsgBox "��� ���� �� ������ ��������� ����� �������� ��� '", vbOKOnly, "������"
'            tblNominalGrants.TextMatrix(Row, Col) = BefDataNominalGrant
'            Exit Sub
'        End If
        CheckString GrantName
        
        AR = CDB.ExecSQLActionQuery("UPDATE [tblNominalGrants] SET [GrantName] = '" & GrantName & "' WHERE [ID] = " & tblNominalGrants.RowData(Row), 128)
        
    ElseIf (Col = 1) Then
    
        If (Not IsNumeric(tblNominalGrants.TextMatrix(Row, Col))) Then
            MsgBox "��� ���� ������ ��������� �����", vbOKOnly, "������"
            tblNominalGrants.TextMatrix(Row, Col) = BefDataNominalGrant
            Exit Sub
        End If
        
        GrantSum = tblNominalGrants.TextMatrix(Row, Col)
        tblNominalGrants.TextMatrix(Row, Col) = Format(tblNominalGrants.TextMatrix(Row, Col), "#######.00")
        If (InStr(1, GrantSum, ",")) Then Mid(GrantSum, InStr(1, GrantSum, ",")) = "."
        
        AR = CDB.ExecSQLActionQuery("UPDATE [tblNominalGrants] SET [GrantSize] = " & GrantSum & " WHERE [ID] = " & tblNominalGrants.RowData(Row), 128)
    End If
End Sub

Private Sub tblNominalGrants_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefDataNominalGrant = tblNominalGrants.TextMatrix(Row, Col)
End Sub

Private Sub tblSpecExtra_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim AR As Long
Dim CategoryName As String
Dim PercentExtra As Integer
    
    If (Col <> 1) Then
        tblSpecExtra.TextMatrix(Row, Col) = BefDataSpeciality
        Exit Sub
    End If
    
    If (Col = 1) Then
    
        If (Not IsNumeric(tblSpecExtra.TextMatrix(Row, Col))) Then
            MsgBox "��� ���� ������ ��������� ����� �����", vbOKOnly, "������"
            tblSpecExtra.TextMatrix(Row, Col) = BefDataSpeciality
            Exit Sub
        End If
        
        PercentExtra = CInt(tblSpecExtra.TextMatrix(Row, Col))
                
        AR = CDB.ExecSQLActionQuery("UPDATE [tblSpecialities] SET [PercentExtra] = " & PercentExtra & " WHERE [ID] = " & tblSpecExtra.RowData(Row), 128)
        tblSpecExtra.TextMatrix(Row, Col) = PercentExtra
        
    End If

End Sub

Private Sub tblSpecExtra_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefDataSpeciality = tblSpecExtra.TextMatrix(Row, Col)
End Sub

Private Sub VSFlexGrid1_Click()

End Sub
