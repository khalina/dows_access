VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "VSFLEX7D.OCX"
Begin VB.Form frmSheet 
   BorderStyle     =   1  '������������ �������������
   Caption         =   "����������� ���������"
   ClientHeight    =   7725
   ClientLeft      =   6270
   ClientTop       =   1770
   ClientWidth     =   6615
   Icon            =   "frmSheet.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7725
   ScaleWidth      =   6615
   Begin VB.Frame F2 
      Height          =   6255
      Left            =   0
      TabIndex        =   4
      Top             =   1440
      Width           =   6615
      Begin VB.CheckBox ckFC 
         Caption         =   "������������ ����� ���������"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   5160
         Width           =   5655
      End
      Begin VB.CheckBox ckDebts 
         Caption         =   "��������� ������ ��� ��������� �������(�����) ���������"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   5400
         Width           =   6375
      End
      Begin VB.TextBox txtSheetNum 
         Alignment       =   2  '������������ �� ������
         Height          =   285
         Left            =   5040
         TabIndex        =   20
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox txtKOR 
         Height          =   285
         Left            =   2160
         TabIndex        =   15
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox txtLecturer 
         Height          =   285
         Left            =   2160
         TabIndex        =   14
         Top             =   960
         Width           =   4335
      End
      Begin VB.TextBox txtSubject 
         Height          =   285
         Left            =   2160
         TabIndex        =   13
         Top             =   600
         Width           =   4335
      End
      Begin VB.CheckBox ckOnlyRGStud 
         Caption         =   "�������� ������ ��� ���������, ������� ������ � ��������� ������"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1440
         Value           =   1  '�����������
         Width           =   6375
      End
      Begin VB.CommandButton CreateSheet 
         Caption         =   "������� ���������"
         Height          =   375
         Left            =   3600
         TabIndex        =   10
         Top             =   5760
         Width           =   1695
      End
      Begin VB.CheckBox LMark 
         Caption         =   "������ �������"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   5640
         Width           =   1935
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "�������"
         Height          =   375
         Left            =   5400
         TabIndex        =   6
         Top             =   5760
         Width           =   1095
      End
      Begin VSFlex7DAOCtl.VSFlexGrid tblStudents 
         Height          =   3375
         Left            =   120
         TabIndex        =   5
         Top             =   1725
         Width           =   6375
         _cx             =   11245
         _cy             =   5953
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   0
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   -1  'True
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         Begin VB.ComboBox comboColEdit 
            Height          =   315
            ItemData        =   "frmSheet.frx":014A
            Left            =   1440
            List            =   "frmSheet.frx":0154
            TabIndex        =   11
            Top             =   1080
            Visible         =   0   'False
            Width           =   1695
         End
      End
      Begin VB.Label �����8 
         Caption         =   "����� :"
         Height          =   255
         Left            =   4320
         TabIndex        =   19
         Top             =   240
         Width           =   735
      End
      Begin VB.Label �����3 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   600
         Width           =   765
      End
      Begin VB.Label �����4 
         AutoSize        =   -1  'True
         Caption         =   "�. �. �. ������������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   960
         Width           =   1920
      End
      Begin VB.Label �����6 
         AutoSize        =   -1  'True
         Caption         =   "��� ���������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   1260
      End
   End
   Begin VB.Frame F1 
      Height          =   1455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      Begin VB.ComboBox comboSemNum 
         Height          =   315
         ItemData        =   "frmSheet.frx":0161
         Left            =   5040
         List            =   "frmSheet.frx":0183
         TabIndex        =   23
         Top             =   600
         Width           =   1455
      End
      Begin VB.ComboBox comboGroups 
         Height          =   315
         Left            =   2160
         TabIndex        =   22
         Top             =   600
         Width           =   1575
      End
      Begin VB.ComboBox comboSpec 
         Height          =   315
         Left            =   2160
         TabIndex        =   21
         Top             =   240
         Width           =   4335
      End
      Begin VB.ComboBox comboSheets 
         Height          =   315
         Left            =   2160
         TabIndex        =   1
         Top             =   960
         Width           =   4335
      End
      Begin VB.Label �����7 
         AutoSize        =   -1  'True
         Caption         =   "������������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1260
      End
      Begin VB.Label �����5 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   4200
         TabIndex        =   7
         Top             =   600
         Width           =   750
      End
      Begin VB.Label �����2 
         AutoSize        =   -1  'True
         Caption         =   "������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   615
      End
      Begin VB.Label �����1 
         AutoSize        =   -1  'True
         Caption         =   "��������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   960
         Width           =   930
      End
   End
End
Attribute VB_Name = "frmSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private BefRow As Long
Private BefCol As Long
Private BefData As Variant

Private SelectedSpeciality As Long
Private SelectedGroup As Long
Private SelectedSemester As Long

Sub FillSheetsList()
Dim SheetArrey As Variant
Dim SheetCols As Integer
Dim SheetRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim Subject As String
Dim KOR As String
Dim SheetNumber As String

Dim i As Long
comboSheets.Clear

    If ((SelectedGroup = 0) Or (SelectedSpeciality = 0)) Then Exit Sub
    
    CDB.SQLOpenTableToArrey MainDB, "SELECT" & _
    " tblExaminations.ID, tblExaminations.SheetNumber," & _
    " tblEducationalPlan.IDSubject, tblEducationalPlan.KindOfReport" & _
    " FROM tblExaminations, tblEducationalPlan" & _
    " WHERE tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
    " tblExaminations.IDGroup = " & SelectedGroup & " AND" & _
    " tblEducationalPlan.IDSpec = " & SelectedSpeciality & " AND" & _
    " tblEducationalPlan.Semester = " & SelectedSemester, SheetArrey, SheetCols, SheetRows

    If (SheetRows > 0) Then
    
        For i = 0 To SheetRows - 1
            CDB.SQLOpenTableToArrey MainDB, "SELECT [Subject] FROM [tblSubjects] WHERE [ID] = " & SheetArrey(2, i), DopArrey, DopCols, DopRows
            Subject = CStr(DopArrey(0, 0))
            
            KOR = SheetArrey(3, i)
            SheetNumber = SheetArrey(1, i)
            
            comboSheets.AddItem SheetNumber & " (" & Subject & " - " & KOR & ")"
            comboSheets.ItemData(comboSheets.NewIndex) = SheetArrey(0, i)
        Next i
        
        comboSheets.ListIndex = 0
    End If

End Sub

Sub MarkInWords(Check As Boolean)
Dim i As Long
Dim Mark As Long
Dim SMark As String

If (Not Check) Then
    For i = 0 To tblStudents.Rows - 1
        SMark = tblStudents.TextMatrix(i, 2)
        If (SMark = "����.") Then
            tblStudents.TextMatrix(i, 2) = 2
        ElseIf (SMark = "�����.") Then
            tblStudents.TextMatrix(i, 2) = 3
        ElseIf (SMark = "���.") Then
            tblStudents.TextMatrix(i, 2) = 4
        ElseIf (SMark = "���.") Then
            tblStudents.TextMatrix(i, 2) = 5
        End If
    Next i
End If

If (Check) Then
    For i = 0 To tblStudents.Rows - 1
        If (IsNumeric(tblStudents.TextMatrix(i, 2))) Then
            Mark = tblStudents.TextMatrix(i, 2)
            If (Mark < 3) Then
                tblStudents.TextMatrix(i, 2) = "����."
            ElseIf (Mark = 3) Then
                tblStudents.TextMatrix(i, 2) = "�����."
            ElseIf (Mark = 4) Then
                tblStudents.TextMatrix(i, 2) = "���."
            ElseIf (Mark = 5) Then
                tblStudents.TextMatrix(i, 2) = "���."
            End If
        End If
    Next i
End If

End Sub

Sub FillSheetInfo(IDSheet As Long)

Dim StudArrey As Variant
Dim SCols As Integer
Dim SRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim Semester As Byte

CDB.SQLOpenTableToArrey MainDB, "SELECT [Semester] FROM [tblGroups] WHERE [ID] = " & SelectedGroup, DopArrey, DopCols, DopRows
Semester = CByte(DopArrey(0, 0))

    CDB.SQLOpenTableToArrey MainDB, "SELECT" & _
    " tblSheets.ID, tblSheets.IDStudent, tblSheets.Mark," & _
    " tblStudents.Surname, tblStudents.Name, tblStudents.Patronymic, tblStudents.IDGroup" & _
    " FROM tblSheets, tblStudents" & _
    " WHERE tblSheets.IDExam = " & IDSheet & " AND" & _
    " tblStudents.IsActive = 1 AND" & _
    " tblStudents.ID = tblSheets.IDStudent ORDER BY " & _
    " tblStudents.Surname, tblStudents.Name, tblStudents.Patronymic", _
    StudArrey, SCols, SRows

Dim StudName As String
Dim i As Long
Dim Debts As String

Main.sbMain.PanelText("2") = "�������� ������ ��������� ��������� � " & txtSheetNum.Text

tblStudents.Rows = 1
If (SRows > 0) Then

    For i = 0 To SRows - 1
        ProgBar Main.PProgress, ((i + 1) * 100) / SRows
        Main.sbMain.RedrawPanel ("3")
    
        If ((ckOnlyRGStud.Value = 0) Or (CLng(StudArrey(6, i)) = SelectedGroup)) Then
            If (AreStudentHaveDebts(CLng(StudArrey(1, i)), Semester)) Then
                Debts = "��"
            Else
                Debts = "���"
            End If
                        
            tblStudents.AddItem tblStudents.Rows & vbTab & UCase(CStr(StudArrey(3, i))) & " " & StudArrey(4, i) & " " & StudArrey(5, i) & vbTab & Chr(CByte(StudArrey(2, i))) & vbTab & Debts & vbTab & "���"
            tblStudents.RowData(tblStudents.Rows - 1) = CLng(StudArrey(0, i))
        End If
    Next i
    
End If

Main.sbMain.PanelText("2") = "������"
ProgBar Main.PProgress, 0
Main.sbMain.RedrawPanel ("3")

'tblStudents.Rows = 50
'For i = 3 To 30
'    tblStudents.TextMatrix(i, 1) = i
'Next i
End Sub

Private Sub ckFC_Click()
If (ckFC.Value = 1) Then
    tblStudents.TextMatrix(0, 4) = "������"
ElseIf (ckFC.Value = 0) Then
    tblStudents.TextMatrix(0, 4) = "�� �������"
End If
End Sub

Private Sub ckOnlyRGStud_Click()
comboSheets_Click
End Sub

Private Sub cmdCancel_Click()
Unload Me
End Sub


Private Sub comboColEdit_Click()
tblStudents.Text = comboColEdit.Text
comboColEdit.Visible = False
End Sub

Private Sub comboColEdit_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboColEdit_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboGroups_Click()
    If (comboGroups.ListIndex < 0) Then
        SelectedGroup = 0
        Exit Sub
    End If
    
    tblStudents.Rows = 1
    SelectedGroup = comboGroups.ItemData(comboGroups.ListIndex)
    FillSheetsList
End Sub

Private Sub comboGroups_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboGroups_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboSemNum_Click()
    If (comboSemNum.ListIndex < 0) Then
        SelectedSemester = 0
        Exit Sub
    End If
    
    tblStudents.Rows = 1
    SelectedSemester = CLng(comboSemNum.Text)
    FillSheetsList
    
End Sub

Private Sub comboSemNum_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboSemNum_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboSheets_Click()
Dim SheetArrey As Variant
Dim SheetCols As Integer
Dim SheetRows As Long

Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

Dim IDSheet As Long
If (comboSheets.ListIndex < 0) Then Exit Sub

tblStudents.Rows = 1
IDSheet = comboSheets.ItemData(comboSheets.ListIndex)

    CDB.SQLOpenTableToArrey MainDB, "SELECT" & _
    " tblExaminations.IDLecturer, tblExaminations.SheetNumber," & _
    " tblEducationalPlan.IDSubject, tblEducationalPlan.KindOfReport" & _
    " FROM tblExaminations, tblEducationalPlan" & _
    " WHERE tblExaminations.IDEP = tblEducationalPlan.ID AND" & _
    " tblExaminations.ID = " & IDSheet, SheetArrey, SheetCols, SheetRows

Dim Subject As String
Dim Lecturer As String
Dim KOR As String
Dim SheetNumber As String

If (SheetRows > 0) Then
    SheetNumber = CStr(SheetArrey(1, 0))
    KOR = CStr(SheetArrey(3, 0))

    CDB.SQLOpenTableToArrey MainDB, "SELECT [Subject] FROM [tblSubjects] WHERE [ID] = " & SheetArrey(2, 0), DopArrey, DopCols, DopRows
    Subject = CStr(DopArrey(0, 0))

    CDB.SQLOpenTableToArrey MainDB, "SELECT [Surname], [Name], [Patronymic], [Post] FROM [tblLecturers] WHERE [ID] = " & SheetArrey(0, 0), DopArrey, DopCols, DopRows
    Lecturer = CStr(DopArrey(3, 0)) & "  " & CStr(DopArrey(0, 0) & " " & DopArrey(1, 0) & " " & DopArrey(2, 0))


    txtSubject.Text = Subject
    txtLecturer.Text = Lecturer
    txtKOR.Text = KOR
    txtSheetNum.Text = SheetNumber
End If

tblStudents.Rows = 1
FillSheetInfo IDSheet
MarkInWords LMark.Value
End Sub

Private Sub comboSheets_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboSheets_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboSpec_Click()
    Dim GroupArrey As Variant
    Dim GCols As Integer
    Dim GRows As Long

    Dim i As Long
    
    If (comboSpec.ListIndex < 0) Then
        SelectedSpeciality = 0
        Exit Sub
    End If
    
    tblStudents.Rows = 1
    SelectedSpeciality = comboSpec.ItemData(comboSpec.ListIndex)

'������-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    Dim GroupName As String
    
    CDB.SQLOpenTableToArrey MainDB, "SELECT [ID], [GroupName], [Semester] FROM [tblGroups] WHERE ([Semester] BETWEEN 1 AND 10) AND [IDSpec] = " & SelectedSpeciality & " ORDER BY [Semester], [GroupName]", GroupArrey, GCols, GRows
    
    comboGroups.Clear
    If (GRows > 0) Then
    
        For i = 0 To GRows - 1
            GroupName = CStr(GroupArrey(1, i))
            If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(GroupArrey(2, i)) Mod 2 = 1, (CLng(GroupArrey(2, i)) \ 2) + 1, CLng(GroupArrey(2, i)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))

            comboGroups.AddItem GroupName
            comboGroups.ItemData(comboGroups.NewIndex) = GroupArrey(0, i)
        Next i
        
        comboGroups.ListIndex = 0
    End If
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
End Sub

Private Sub comboSpec_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboSpec_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub CreateSheet_Click()
If (ckDebts.Value = 0) Then
    Main.PrintSheetUkr "����������� ������������ ����������� ��. �. �. ��������", _
               "��������-������������", _
               comboGroups.Text, _
               IIf(CInt(comboSemNum.Text) Mod 2 = 1, (CInt(comboSemNum.Text) \ 2) + 1, CInt(comboSemNum.Text) \ 2), _
               txtSheetNum.Text, _
               txtSubject, _
               txtLecturer, _
               txtKOR, _
               tblStudents, _
               400, 400, _
               False, _
               CByte(comboSemNum.Text), _
               Year(GlobalData), _
               IIf(ckFC.Value = 1, True, False)
Else
    Main.PrintDebtsSheetUkr "����������� ������������ ����������� ��. �. �. ��������", _
               "��������-������������", _
               comboGroups.Text, _
               IIf(CInt(comboSemNum.Text) Mod 2 = 1, (CInt(comboSemNum.Text) \ 2) + 1, CInt(comboSemNum.Text) \ 2), _
               txtSheetNum.Text, _
               txtSubject, _
               txtLecturer, _
               txtKOR, _
               tblStudents, _
               400, 400, _
               True, _
               CByte(comboSemNum.Text), _
               Year(GlobalData), _
               IIf(ckFC.Value = 1, True, False)
End If
'Unload Me
End Sub

Private Sub Form_Load()
    Dim SpecArrey As Variant
    Dim SCols As Integer
    Dim SRows As Long

    Dim i As Long
    
SelectedSpeciality = 0
SelectedGroup = 0
SelectedSemester = 0
    
        tblStudents.Cols = 5
        tblStudents.Rows = 1
        tblStudents.FixedRows = 1
        tblStudents.FixedCols = 1
        
        tblStudents.TextMatrix(0, 0) = "ID"
        tblStudents.TextMatrix(0, 1) = "�. �. �."
        tblStudents.TextMatrix(0, 2) = "������"
        tblStudents.TextMatrix(0, 3) = "��������"
        tblStudents.TextMatrix(0, 4) = "�� �������"
        
        tblStudents.ColWidth(0) = 450
        tblStudents.ColWidth(1) = 2950
        tblStudents.ColWidth(2) = 800
        tblStudents.ColWidth(3) = 840
        tblStudents.ColWidth(4) = 1030
    
    comboSemNum.ListIndex = 0
    
    comboSpec.Clear
    '�������������----------------------------------------------------------------------------------------------------------------------------
        CDB.SQLOpenTableToArrey MainDB, "SELECT [ID], [SpecName] FROM [tblSpecialities]", SpecArrey, SCols, SRows
        
        If (SRows = 0) Then
            MsgBox "��� ����������� ��������� �� ���������� ������ ���� ���������������� ������� ���� ������������� !", vbOKOnly, "������"
            Exit Sub
        End If
        
        If (SRows > 0) Then
            
            comboSpec.Clear
            For i = 0 To SRows - 1
                comboSpec.AddItem SpecArrey(1, i)
                comboSpec.ItemData(comboSpec.NewIndex) = SpecArrey(0, i)
            Next i
            
            comboSpec.ListIndex = 0
        End If
    '-----------------------------------------------------------------------------------------------------------------------------------------


LoadSuccess = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If (tblStudents.Rows > 1) Then
    tblStudents.Row = 1
    tblStudents.Col = 1
End If
End Sub

Private Sub LMark_Click()
MarkInWords LMark.Value
End Sub

Private Sub tblStudents_AfterEdit(ByVal Row As Long, ByVal Col As Long)
Dim AR As Long
Dim Mark As String

    If (Col = 2) Then
        Mark = UCase(tblStudents.TextMatrix(Row, Col))
        
        If (Not (Mark Like ("[�,�,1,2,3,4,5]"))) Then
            MsgBox "� ���� '������' ��������� ������ ��������� ������� :" & Chr(13) & Chr(10) & "�, �, 1, 2, 3, 4, 5."
            tblStudents.TextMatrix(Row, Col) = BefData
            Exit Sub
        End If
        
        AR = CDB.ExecSQLActionQuery(MainDB, "UPDATE [tblSheets] SET [Mark] = " & Asc(Mark) & " WHERE [ID] = " & CLng(tblStudents.RowData(tblStudents.Row)), dbFailOnError)
        
    ElseIf (Col = 1) Then
        MsgBox "� ���� ���� ������ �� ����� ���� ��������", vbOKOnly, "������"
        tblStudents.TextMatrix(Row, Col) = BefData
    End If
End Sub

Private Sub tblStudents_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
BefCol = Col
BefRow = Row
BefData = tblStudents.TextMatrix(Row, Col)
End Sub

Private Sub tblStudents_BeforeScroll(ByVal OldTopRow As Long, ByVal OldLeftCol As Long, ByVal NewTopRow As Long, ByVal NewLeftCol As Long, Cancel As Boolean)
    If comboColEdit.Visible Then comboColEdit.Visible = False
End Sub

Private Sub tblStudents_BeforeUserResize(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If comboColEdit.Visible Then comboColEdit.Visible = False
End Sub

Private Sub tblStudents_Click()
tblStudents_RowColChange
End Sub

Private Sub tblStudents_RowColChange()
Dim SelectedBool As String
Dim i As Integer

    If (tblStudents.Col = 3 Or tblStudents.Col = 4) Then
    
        SelectedBool = tblStudents.Text
        
        For i = 0 To comboColEdit.ListCount - 1
            If (comboColEdit.List(i) = SelectedBool) Then
                comboColEdit.ListIndex = i
                Exit For
            End If
        Next i
        
        comboColEdit.Left = tblStudents.CellLeft
        comboColEdit.Top = tblStudents.CellTop
        comboColEdit.Width = tblStudents.CellWidth
        'comboColEdit.Height = tblStudents.CellHeight
        comboColEdit.Visible = True
    End If
    
    If (tblStudents.Col <> 3 And tblStudents.Col <> 4) Then comboColEdit.Visible = False
End Sub

Private Sub ��������1_Click()

End Sub
