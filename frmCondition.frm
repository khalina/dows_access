VERSION 5.00
Begin VB.Form frmCondition 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "������������ �������"
   ClientHeight    =   2070
   ClientLeft      =   2370
   ClientTop       =   2475
   ClientWidth     =   9015
   Icon            =   "frmCondition.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   9015
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F2 
      Height          =   735
      Left            =   0
      TabIndex        =   6
      Top             =   1320
      Width           =   9015
      Begin VB.CommandButton cmdCancel 
         Caption         =   "������"
         Height          =   375
         Left            =   7200
         TabIndex        =   8
         Top             =   240
         Width           =   1695
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   5520
         TabIndex        =   7
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.Frame F1 
      Height          =   1335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9015
      Begin VB.ComboBox comboFunction 
         Height          =   315
         ItemData        =   "frmCondition.frx":030A
         Left            =   120
         List            =   "frmCondition.frx":031A
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   480
         Width           =   1095
      End
      Begin VB.CheckBox ckIsField 
         Caption         =   "���������� � ������������ �����"
         Height          =   255
         Left            =   5400
         TabIndex        =   5
         Top             =   840
         Width           =   3135
      End
      Begin VB.CommandButton cmdChoise 
         Caption         =   "������� ����"
         Height          =   350
         Left            =   1440
         TabIndex        =   4
         Top             =   840
         Width           =   2415
      End
      Begin VB.TextBox RArg 
         Height          =   285
         Left            =   5040
         TabIndex        =   3
         Top             =   480
         Width           =   3855
      End
      Begin VB.ComboBox comboOperation 
         Height          =   315
         ItemData        =   "frmCondition.frx":0337
         Left            =   4080
         List            =   "frmCondition.frx":0350
         TabIndex        =   2
         Top             =   465
         Width           =   855
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   195
         Width           =   780
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "("
         Height          =   195
         Left            =   1270
         TabIndex        =   13
         Top             =   480
         Width           =   45
      End
      Begin VB.Label Label1 
         Caption         =   ")"
         Height          =   255
         Left            =   3940
         TabIndex        =   12
         Top             =   480
         Width           =   135
      End
      Begin VB.Label �����2 
         Caption         =   "�������� :"
         Height          =   255
         Left            =   5040
         TabIndex        =   10
         Top             =   200
         Width           =   975
      End
      Begin VB.Label �����1 
         Caption         =   "���� ��� ��������� :"
         Height          =   255
         Left            =   1440
         TabIndex        =   9
         Top             =   195
         Width           =   1935
      End
      Begin VB.Label LArg 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1440
         TabIndex        =   1
         Top             =   480
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-----------------------
Private LTable As Integer
Private LField As Integer
Private LFunc As String
    
Private Operation As String
    
Private RString As String
    
Private RTable As Integer
Private RField As Integer
'-----------------------

Public SelectedTable As Integer
Public SelectedField As Integer

Private Sub ckIsField_Click()
If (ckIsField.Value = 1) Then
    If (UCase(Operation) = "���") Then
        RTable = -1
        RField = -1
        ckIsField.Value = 0
        ckIsField_Click
        Exit Sub
    End If
    
    RArg.Text = ""
    RArg.Enabled = False

    RTable = -1
    RField = -1
    
    SelectedTable = -1
    SelectedField = -1
    
    ShowForm frmFields, 1, Me

    If (SelectedTable = -1 Or SelectedField = -1) Then
        RTable = -1
        RField = -1
        ckIsField.Value = 0
        ckIsField_Click
        Exit Sub
    End If
    
    RTable = SelectedTable
    RField = SelectedField
    
    RString = ""
    RArg.Text = "[" & TablesArrey(RTable).TableNameRu & "].[" & TablesArrey(RTable).FieldsArrey(RField)(0) & "]"
    
ElseIf (ckIsField.Value = 0) Then
    RArg.Text = ""
    RArg.Enabled = True
    RTable = -1
    RField = -1
    
End If
End Sub

Private Sub cmdCancel_Click()
SQLModule.Category = ""
Unload Me
End Sub

Private Sub cmdChoise_Click()
LTable = -1
LField = -1

SelectedTable = -1
SelectedField = -1

ShowForm frmFields, 1, Me

If (SelectedTable = -1 Or SelectedField = -1) Then
    LTable = -1
    LField = -1
    Exit Sub
End If

LTable = SelectedTable
LField = SelectedField
LArg.Caption = "[" & TablesArrey(LTable).TableNameRu & "].[" & TablesArrey(LTable).FieldsArrey(LField)(0) & "]"
End Sub

Private Sub cmdOK_Click()
Dim LType As String
Dim RType As String

Dim ErrTypes As Boolean

Dim StrData As String

If (LTable < 0 Or LField < 0) Then
    MsgBox "�������� ���� ��� ���������", vbOKOnly, "������"
    Exit Sub
End If
LType = TablesArrey(LTable).FieldsArrey(LField)(2)

If (LType <> "TEXT" And UCase(Operation) = "���") Then
    MsgBox "��������� �������� �������� ������ � ������ ��� ��������� ���� 'TEXT'", vbOKOnly, "������"
    Exit Sub
End If

If (LFunc <> "" And LType <> "DATETIME") Then LFunc = ""

ErrTypes = False
If (ckIsField.Value = 1) Then
    If (RTable < 0 Or RField < 0) Then
        MsgBox "�������� ���� ��������", vbOKOnly, "������"
        Exit Sub
    End If
    RType = TablesArrey(RTable).FieldsArrey(RField)(2)
    
    Select Case LType
        Case "TEXT":
            If (RType <> "TEXT") Then ErrTypes = True
            
        Case "DATETIME":
            If (RType <> "DATETIME") Then ErrTypes = True
        
        Case "BYTE", "CURRENCY", "INTEGER", "LONG":
            If (RType <> "BYTE" And RType <> "CURRENCY" And RType <> "INTEGER" And RType <> "LONG") Then ErrTypes = True
            
    End Select
    
    If (ErrTypes) Then
        MsgBox "��������� ���� �� ����� ���� ��������, ��-�� ��������������� ����� ������.", vbOKOnly, "������"
        Exit Sub
    End If
    
ElseIf (ckIsField.Value = 0) Then
    Select Case LType
        Case "TEXT":
            StrData = RArg.Text
            CheckString StrData
            StrData = StrData
            
        Case "DATETIME":
            If ((Not IsDate(RArg.Text)) And (LFunc = "")) Then
                MsgBox "���� �������� ������ ��������� ����.", vbOKOnly, "������"
                Exit Sub
            End If
            StrData = RArg.Text
            CheckString StrData
            StrData = StrData
        
        Case "BYTE", "CURRENCY", "INTEGER", "LONG":
            If (Not IsNumeric(RArg.Text)) Then
                MsgBox "���� �������� ������ ��������� �����.", vbOKOnly, "������"
                Exit Sub
            End If
            StrData = RArg.Text
            
    End Select
  
End If

SQLModule.LFunc = LFunc
SQLModule.LTable = LTable
SQLModule.LField = LField
SQLModule.Operation = Operation
SQLModule.RString = StrData
SQLModule.RTable = RTable
SQLModule.RField = RField

Unload Me
End Sub


Private Sub comboFunction_Click()
LFunc = comboFunction.List(comboFunction.ListIndex)
If (LFunc = "�����") Then LFunc = ""
End Sub

Private Sub comboOperation_Click()
Operation = comboOperation.Text
If (UCase(Operation) = "���") Then
    RTable = -1
    RField = -1
    ckIsField.Value = 0
    ckIsField_Click
    Exit Sub
End If

End Sub

Private Sub comboOperation_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboOperation_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Form_Load()
LTable = -1
LField = -1
LFunc = ""

Operation = ""
    
RString = ""
    
RTable = -1
RField = -1

comboOperation.ListIndex = 0
LoadSuccess = True
End Sub

