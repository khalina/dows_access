VERSION 5.00
Begin VB.Form frmWEBServise 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "���������� ������� �� �� �������"
   ClientHeight    =   4335
   ClientLeft      =   4440
   ClientTop       =   2730
   ClientWidth     =   5070
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   5070
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   4335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5055
      Begin VB.PictureBox PProgress 
         AutoRedraw      =   -1  'True
         FillColor       =   &H00FF8080&
         Height          =   255
         Left            =   3000
         ScaleHeight     =   195
         ScaleWidth      =   1875
         TabIndex        =   12
         Top             =   1320
         Width           =   1935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "�������"
         Height          =   375
         Left            =   3000
         TabIndex        =   11
         Top             =   630
         Width           =   1935
      End
      Begin VB.CommandButton cmdStart 
         Caption         =   "��������"
         Height          =   375
         Left            =   3000
         TabIndex        =   10
         Top             =   240
         Width           =   1935
      End
      Begin VB.TextBox txtFTP 
         Height          =   2535
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   9
         Top             =   1680
         Width           =   4815
      End
      Begin VB.Label L4 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   2160
         TabIndex        =   8
         Top             =   1320
         Width           =   495
      End
      Begin VB.Label L3 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   2160
         TabIndex        =   7
         Top             =   960
         Width           =   495
      End
      Begin VB.Label L2 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   2160
         TabIndex        =   6
         Top             =   600
         Width           =   495
      End
      Begin VB.Label L1 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   2160
         TabIndex        =   5
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label4 
         Caption         =   "��������� �����"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "����� ��������"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "���������� � ��������"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "��������� ��"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmWEBServise"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ZipFile As String
Public ZipCatalog As String

Private Step1 As Boolean
Public Step2 As Boolean
Public Step3 As Boolean
Public Step4 As Boolean

Private RemoteHost As String
Private User As String
Private Pass As String
Public Catalog As String

Private WithEvents m_cZ As cZip
Attribute m_cZ.VB_VarHelpID = -1

Private Function PackFile() As Boolean
Dim sFile As String
Dim dFile As String

'sFile = CurDBPath
dFile = App.Path & "\" & ZipCatalog & ZipFile

'If (Dir(sFile) = "") Then
'    MsgBox "Source file not found!"
'    PackFile = False
'    Exit Function
'End If

If (Dir$(dFile) <> "") Then Kill dFile

L1.Caption = "...": DoEvents

   With m_cZ
      .ZipFile = dFile
      .Encrypt = False
      .AddComment = False
      .BasePath = App.Path & "\" & ZipCatalog
      .ClearFileSpecs
      .AddFileSpec "*.tbu"
      .StoreFolderNames = True
      .RecurseSubDirs = True
      .Zip
      
      If (.Success) Then
            L1.Caption = "OK"
            PackFile = True
      Else
            L1.Caption = "Failed"
            PackFile = False
      End If
   End With


'      With m_cZ
'         .Encrypt = False
'         .AddComment = False
'         .ZipFile = dFile
'         .StoreFolderNames = False
'         .RecurseSubDirs = False
'         .ClearFileSpecs
'         .AddFileSpec sFile
'         .Zip
'
'         If (.Success) Then
'            L1.Caption = "OK"
'            PackFile = True
'         Else
'            L1.Caption = "Failed"
'            PackFile = False
'         End If
'
'      End With
End Function

Private Function SendFile() As Boolean
Dim CArray As Variant
Dim CCols As Integer
Dim CRows As Long

On Error GoTo ER1
CDB.SQLOpenTableToArrey "SELECT [RemoteHost], [UserName], [Password], [Catalog] FROM [tblOptions] WHERE ID = 1", CArray, CCols, CRows

If (Not IsNull(CArray(0, 0))) Then RemoteHost = CStr(CArray(0, 0))
If (Not IsNull(CArray(1, 0))) Then User = CStr(CArray(1, 0))
If (Not IsNull(CArray(2, 0))) Then Pass = CStr(CArray(2, 0))
If (Not IsNull(CArray(3, 0))) Then Catalog = CStr(CArray(3, 0))

L2.Caption = "...": DoEvents
Main.CurrentFTPAction = "UPDATINGDB"
Main.FTPConnection.Disconnect
Main.FTPConnection.Connect RemoteHost, 21, User, Pass

SendFile = True

Exit Function
ER1:
MsgBox Error(Err)
SendFile = False
End Function

Private Sub cmdStart_Click()
On Error GoTo errH

MkDir App.Path & "\" & Left$(ZipCatalog, Len(ZipCatalog) - 1)

txtFTP.Text = ""

SaveTable App.Path & "\" & ZipCatalog & "tbloptions.tbu", "tblOptions", "ID,UN,UNR,UNF,FN,FNR,WSB,SSB,EPWD,FPMS,DList,IList,RemoteHost,UserName,Password,Catalog", 16
SaveTable App.Path & "\" & ZipCatalog & "tbldiplomadditionopt.tbu", "tblDiplomAdditionOpt", "ID , OPT0, Opt1, Opt2, Opt3, Opt4, OPT5, OPT6, OPT7, OPT8, OPT9, OPT10, OPT11, OPT12, OPT13, OPT14, OPT15, OPT16, OPT17, OPT18, OPT19, OPT20, OPT21, OPT22", 24
SaveTable App.Path & "\" & ZipCatalog & "tblgrants.tbu", "tblGrants", "ID , Min, Max, GrantSize", 4
SaveTable App.Path & "\" & ZipCatalog & "tblnominalgrants.tbu", "tblNominalGrants", "ID , GrantName, GrantSize", 3
SaveTable App.Path & "\" & ZipCatalog & "tblcategories.tbu", "tblCategories", "ID , CategoryName, PercentExtra", 3
SaveTable App.Path & "\" & ZipCatalog & "tblspecialities.tbu", "tblSpecialities", "ID,SpecName,scode,PercentExtra", 4
SaveTable App.Path & "\" & ZipCatalog & "tblfaculty.tbu", "tblFaculty", "ID , FacultyName, scode, IsActive", 4
SaveTable App.Path & "\" & ZipCatalog & "tblgroups.tbu", "tblGroups", "ID , IDSpec, GroupName, Semester, FirstSessionYear", 5
SaveTable App.Path & "\" & ZipCatalog & "tblstudents.tbu", "tblStudents", "ID,IDGroup,IDCategory,IDNominalGrant,IDChair,Surname,Name,Patronymic,StudPhoto,Address,KharkovAddress,DateOfBorn,Nationality,Education,SchoolCity,MaritalStatus,FormOfPayment,InformationOnTheParents,Gratitude,Reprimands,CReprimands,OffsetNumber,IsForeigner,LibNumber,DeductionDate,AtestatNum,DiplomNum,DeductionOrderNumber,RestoreInfo,TRestoreInfo,DeductionReason,IsValidReason,RegNumber,AdditionNumber,DASettings,IsActive,IDCode,EntryYear,StudNum,WebPass", 40
SaveTable App.Path & "\" & ZipCatalog & "tblstudevents.tbu", "tblStudEvents", "ID,IDStudent,EventText", 3
SaveTable App.Path & "\" & ZipCatalog & "tblsubjects.tbu", "tblSubjects", "ID,Subject", 2
SaveTable App.Path & "\" & ZipCatalog & "tbllecturers.tbu", "tblLecturers", "ID,IDFaculty,Surname,Name,Patronymic,IDCode,Passport,Post,ScientificDegree,Address,Telephone,Education,DateOfBorn,DoctorWorkTheme,CandidateWorkTheme,Year,IsActive,WebPass", 18
SaveTable App.Path & "\" & ZipCatalog & "tbldegreethemes.tbu", "tblDegreeThemes", "ID,IDStudent,IDLecturer,WorkType,NameOfWork,Mark", 6
SaveTable App.Path & "\" & ZipCatalog & "tbleducationalplan.tbu", "tblEducationalPlan", "ID,IDSpec,IDSubject,Semester,KindOfReport,IsAdditional,SCode,NumOfHours,IsActive", 9
SaveTable App.Path & "\" & ZipCatalog & "tblexaminations.tbu", "tblExaminations", "ID,IDEP,IDGroup,IDLecturer,IDFaculty,SheetNumber", 6
SaveTable App.Path & "\" & ZipCatalog & "tblsheets.tbu", "tblSheets", "ID,IDExam,IDStudent,Mark,SheetsPrinted", 5
SaveTable App.Path & "\" & ZipCatalog & "tblspecialcourses.tbu", "tblSpecialCourses", "ID,IDStudent,IDSubject,IDLecturer,KindOfReport,NumOfHours,Semester,Mark,CourseType", 9


If (Not Step1) Then Step1 = PackFile

txtFTP.Text = txtFTP.Text & Chr(13) & Chr(10)
txtFTP.SelStart = Len(txtFTP.Text) - 1

If (Step1) Then Step2 = SendFile
Exit Sub


errH:
If (Err <> 75) Then
    MsgBox Error(Err)
    Exit Sub
Else
    Resume Next
End If
End Sub

Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Form_Load()
Set m_cZ = New cZip

ZipCatalog = "TEMP\"
ZipFile = "dbupdate.zip"

Step1 = False
Step2 = False
Step3 = False
Step4 = False

LoadSuccess = True
End Sub

Private Sub m_cZ_Progress(ByVal lCount As Long, ByVal sMsg As String)
    txtFTP.Text = txtFTP.Text & sMsg
    txtFTP.SelStart = Len(txtFTP.Text) - 1
End Sub
