Attribute VB_Name = "Declares"
Option Explicit

Public Type POINTAPI
        x As Long
        Y As Long
End Type

Public MainDBPass As String
Public LoadPassword As String
Public LoadPasswordChecked As Boolean
Public CEAMCS As Long
Public MainDBNameCS As String
Public DBCountOfStarts As String

Public Const SND_SYNC = &H0         '  play synchronously (default)
Public Declare Function sndPlaySound Lib "winmm.dll" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long

Public Declare Function GetDiskFreeSpaceEx Lib "kernel32" Alias "GetDiskFreeSpaceExA" (ByVal lpRootPathName As String, lpFreeBytesAvailableToCaller As Currency, lpTotalNumberOfBytes As Currency, lpTotalNumberOfFreeBytes As Currency) As Long
Public Const DRIVE_FIXED = 3

Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Const SW_SHOWNORMAL = 1

Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Function GetVolumeInformation Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long) As Long

Function CheckDB(DBName As String) As Boolean

Dim DataArray As Variant
Dim DataCols As Integer
Dim DataRows As Long

Dim AR As Long

Dim DBCS As String
Dim cCrypter As New clsCrypter
    
    If (Dir(App.Path & "\BackB.bmp") = "") Then
        CheckDB = False
        Exit Function
    End If

    If (Dir(App.Path & "\dows.pst") = "") Then
        CheckDB = False
        Exit Function
    End If

    If (UCase(DBName) <> UCase(MainDBNameCS)) Then
        CheckDB = True
        Exit Function
    End If

    cCrypter.Path = App.Path & "\BackB.bmp"
    cCrypter.EncryptString = True
    cCrypter.Password = "H&13$|Si|k"


    CDB.SQLOpenTableToArrey "SELECT [STAR] FROM [tblOptions] WHERE ID = 1", DataArray, DataCols, DataRows
    
    If (Not IsNull(DataArray(0, 0))) Then
        DBCS = CStr(DataArray(0, 0))
    Else
        DBCS = "0"
    End If
    
    If (CStr(DBCountOfStarts) <> DBCS) Then
        Open App.Path & "\Doc.cll" For Append As #1 Len = 1
        Print #1, Now
        Close #1
        
        Main.Visible = False
        sndPlaySound App.Path & "\dows.pst", SND_SYNC
        CheckDB = False
        Main.Visible = True
    Else
        Randomize
        DBCountOfStarts = Int((1000 * Rnd) + 1)
        
        AR = CDB.ExecSQLActionQuery("UPDATE [tblOptions] SET [STAR] = " & DBCountOfStarts & " WHERE ID = 1", 128)
        cCrypter.WriteData "DCS_" & DBCountOfStarts, 4
        cCrypter.Save
        
        CheckDB = True
    End If

End Function

Function StartQuery() As Boolean
    Dim CurPath As String
    Dim cCrypter As New clsCrypter
    
    If (Dir(App.Path & "\BackB.bmp") = "") Then
        StartQuery = False
        Exit Function
    End If

    CurPath = Left$(App.Path, 3)
    
    Dim VolumeLabel As String
    Dim SerialNumber As Long
    Dim ComponentLength As Long
    Dim FileSystem As Long
    Dim FileSystemName As String
    
    Dim FreeCallerBytes As Currency
    Dim TotalBytes As Currency
    Dim TotalFreeBytes As Currency
    
    Dim strTotalBytes As String

    Dim fOk As Long
    
    VolumeLabel = String$(256, 0)
    FileSystemName = String$(256, 0)
    
    fOk = GetVolumeInformation(CurPath, VolumeLabel, 255, SerialNumber, _
            ComponentLength, FileSystem, FileSystemName, 255)
            
    fOk = GetDiskFreeSpaceEx(CurPath, FreeCallerBytes, TotalBytes, TotalFreeBytes)
    
    strTotalBytes = CStr(TotalBytes)
    If (InStr(1, strTotalBytes, ",")) Then strTotalBytes = Left(strTotalBytes, InStr(1, strTotalBytes, ",") - 1) & Right(strTotalBytes, Len(strTotalBytes) - InStr(1, strTotalBytes, ","))
    If (InStr(1, strTotalBytes, ".")) Then strTotalBytes = Left(strTotalBytes, InStr(1, strTotalBytes, ".") - 1) & Right(strTotalBytes, Len(strTotalBytes) - InStr(1, strTotalBytes, "."))
    
    Dim MaxCountOfStarts As String
    Dim SerialAndTotal As String
    Dim Buffer As String

    cCrypter.Path = App.Path & "\BackB.bmp"
    cCrypter.EncryptString = True
    cCrypter.Password = "H&13$|Si|k"

    MainDBPass = cCrypter.GetData(1)
    LoadPassword = cCrypter.GetData(2)
    MaxCountOfStarts = cCrypter.GetData(3)
    DBCountOfStarts = cCrypter.GetData(4)
    MainDBNameCS = cCrypter.GetData(5)
    
    If ((Len(MainDBPass) < 4) Or _
        (Len(LoadPassword) < 4) Or _
        (Len(MaxCountOfStarts) < 4) Or _
        (Len(MainDBNameCS) < 4) Or _
        (Len(DBCountOfStarts) < 4)) Then
        
        StartQuery = False
        Exit Function
    End If

    If ((Left$(MainDBPass, 4) <> "DBP_") Or _
        (Left$(LoadPassword, 4) <> "LOP_") Or _
        (Left$(MainDBNameCS, 4) <> "MDB_") Or _
        (Left$(DBCountOfStarts, 4) <> "DCS_") Or _
        (Left$(MaxCountOfStarts, 4) <> "MCS_")) Then
            StartQuery = False
            Exit Function
    End If
    
    MainDBPass = Right$(MainDBPass, Len(MainDBPass) - 4)
    LoadPassword = Right$(LoadPassword, Len(LoadPassword) - 4)
    DBCountOfStarts = Right$(DBCountOfStarts, Len(DBCountOfStarts) - 4)
    MainDBNameCS = Right$(MainDBNameCS, Len(MainDBNameCS) - 4)
    
If (Not DOWSDemoMode) Then
    MaxCountOfStarts = Right$(MaxCountOfStarts, Len(MaxCountOfStarts) - 4)
    CEAMCS = MaxCountOfStarts
    If (MaxCountOfStarts <> "-1") Then
        If (CLng(MaxCountOfStarts) <= 0) Then
            StartQuery = False
            Exit Function
        Else
            cCrypter.WriteData "MCS_" & CStr(CLng(MaxCountOfStarts) - 1), 3
            CEAMCS = CLng(MaxCountOfStarts) - 1
            cCrypter.Save
        End If
    End If

    Dim DrivesVerified As Boolean
    Dim DVCounter As Long

    DrivesVerified = False
    DVCounter = 7

'    SerialAndTotal = cCrypter.GetData(6)
'    While (Len(SerialAndTotal) > 5)
'        If (Left$(SerialAndTotal, 4) <> "S&T_") Then
'            DrivesVerified = False
'            SerialAndTotal = ""
'        ElseIf (SerialAndTotal = "S&T_" & CStr(SerialNumber) & "_" & strTotalBytes) Then
'            DrivesVerified = True
'            SerialAndTotal = ""
'        Else
'            SerialAndTotal = cCrypter.GetData(DVCounter)
'            DVCounter = DVCounter + 1
'        End If
'    Wend
    DrivesVerified = True
    '''MainDBPass = "09yqDsoLLw71P" ''';)
    
    If (DrivesVerified) Then
        StartQuery = True
    Else
        StartQuery = False
    End If

Else
    StartQuery = True
    
End If

End Function

