VERSION 5.00
Begin VB.Form frmDelStudent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�������� �������� ��������"
   ClientHeight    =   4935
   ClientLeft      =   2415
   ClientTop       =   2235
   ClientWidth     =   9135
   Icon            =   "frmDelStudent.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   9135
   Begin VB.Frame F6 
      Height          =   2655
      Left            =   4320
      TabIndex        =   19
      Top             =   1560
      Width           =   4815
      Begin VB.CheckBox Check1 
         Caption         =   "������������ ������������ �������"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   2280
         Value           =   1  'Checked
         Width           =   3855
      End
      Begin VB.CommandButton Command1 
         Caption         =   "���������"
         Height          =   315
         Left            =   3650
         TabIndex        =   30
         Top             =   1800
         Width           =   1050
      End
      Begin VB.ComboBox comboReason 
         Height          =   315
         Left            =   1080
         TabIndex        =   29
         ToolTipText     =   "��������: Shift+Del"
         Top             =   1800
         Width           =   2535
      End
      Begin VB.OptionButton Opt4 
         Caption         =   "��������� ����"
         Height          =   255
         Left            =   2160
         TabIndex        =   28
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox txtOrderNumber 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3000
         TabIndex        =   27
         Top             =   1440
         Width           =   1695
      End
      Begin VB.TextBox txtDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   3000
         TabIndex        =   26
         Top             =   1080
         Width           =   1695
      End
      Begin VB.OptionButton Opt3 
         Caption         =   "������������ ������"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   720
         Width           =   2055
      End
      Begin VB.OptionButton Opt2 
         Caption         =   "��������� �������� � ������� ��� ���������� � ���"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   480
         Width           =   4575
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "��������� �������� � ����������� ���� ��� ������"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Value           =   -1  'True
         Width           =   4575
      End
      Begin VB.Label �����8 
         Caption         =   "������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1800
         Width           =   1455
      End
      Begin VB.Label �����6 
         Caption         =   "����� � ���� ������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1440
         Width           =   2655
      End
      Begin VB.Label �����5 
         Caption         =   "���� ��������� �������� :"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1080
         Width           =   2175
      End
   End
   Begin VB.Frame F1 
      Caption         =   "�������� �������� ��� ����������"
      Height          =   4935
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   4215
      Begin VB.TextBox txtFIO 
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   440
         Width           =   3975
      End
      Begin VB.ListBox lstFIO 
         Height          =   3570
         Left            =   120
         TabIndex        =   17
         Top             =   750
         Width           =   3975
      End
      Begin VB.Label �����1 
         AutoSize        =   -1  'True
         Caption         =   "������� ����� :"
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   220
         Width           =   1275
      End
   End
   Begin VB.Frame F2 
      Height          =   975
      Left            =   4320
      TabIndex        =   7
      Top             =   600
      Width           =   4815
      Begin VB.Label �����2 
         AutoSize        =   -1  'True
         Caption         =   "������������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1260
      End
      Begin VB.Label StartSpecName 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   14
         Top             =   240
         Width           =   3255
      End
      Begin VB.Label �����3 
         AutoSize        =   -1  'True
         Caption         =   "������ :"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   615
      End
      Begin VB.Label StartGroupName 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   12
         Top             =   600
         Width           =   3255
      End
      Begin VB.Label �����4 
         AutoSize        =   -1  'True
         Caption         =   "ID :"
         Height          =   195
         Left            =   4920
         TabIndex        =   11
         Top             =   240
         Width           =   255
      End
      Begin VB.Label SIDS 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   5280
         TabIndex        =   10
         Top             =   240
         Width           =   495
      End
      Begin VB.Label �����7 
         AutoSize        =   -1  'True
         Caption         =   "ID :"
         Height          =   195
         Left            =   4920
         TabIndex        =   9
         Top             =   600
         Width           =   255
      End
      Begin VB.Label SIDG 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   5280
         TabIndex        =   8
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame F4 
      Height          =   735
      Left            =   4320
      TabIndex        =   4
      Top             =   4200
      Width           =   4815
      Begin VB.CommandButton cmdClose 
         Caption         =   "�������"
         Height          =   375
         Left            =   2400
         TabIndex        =   6
         Top             =   240
         Width           =   2295
      End
      Begin VB.CommandButton cmdDel 
         Caption         =   "�������� ��������"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame F5 
      Height          =   615
      Left            =   4320
      TabIndex        =   1
      Top             =   0
      Width           =   4815
      Begin VB.Label �����9 
         AutoSize        =   -1  'True
         Caption         =   "�.�.�. �������� :"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1350
      End
      Begin VB.Label SName 
         Height          =   255
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   3135
      End
   End
End
Attribute VB_Name = "frmDelStudent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Sub LoadReasons()
    Dim RArrey As Variant
    Dim RCols As Integer
    Dim RRows As Long
    
    Dim i As Long
         
    ComboReason.Clear
    '������� ����������-----------------------------------------------------------------------------------------------------------------------
        CDB.SQLOpenTableToArrey "SELECT [ID], [Reazon] FROM [tblReazonsText] ORDER BY [ID] DESC", RArrey, RCols, RRows
        
        If (RRows > 0) Then
            
            For i = 0 To RRows - 1
                ComboReason.AddItem RArrey(1, i)
                ComboReason.ItemData(ComboReason.NewIndex) = RArrey(0, i)
            Next i
            
            ComboReason.ListIndex = 0
        End If
    '-----------------------------------------------------------------------------------------------------------------------------------------
End Sub

Private Sub cmdDel_Click()
If ((SName = "") Or (lstFIO.ListCount = 0) Or (lstFIO.ListIndex < 0)) Then
    MsgBox "�������� �������� ��� ����������", vbOKOnly, "������"
    Exit Sub
End If


    Dim DopArray As Variant
    Dim DopCols As Integer
    Dim DopRows As Long
    
    Dim AR As Long
    Dim OrderNumber As String
    Dim Reason As String
    
    Dim IDStud As Long
    
    Dim GroupName As String
    Dim Semester As Byte
    
    IDStud = CLng(lstFIO.ItemData(lstFIO.ListIndex))
    
    If (Opt1.Value Or Opt3.Value Or Opt4.Value) Then
        If (Not IsDate(txtDate.text)) Then
            MsgBox "���� ��������� �������� ������� �� �����", vbOKOnly, "������"
            Exit Sub
        End If

        OrderNumber = txtOrderNumber.text
        If (FindErrors(txtOrderNumber.text, 40, "����� �������", False, True)) Then Exit Sub
        CheckString OrderNumber
        
        Reason = ComboReason.text
        If (FindErrors(ComboReason.text, 60, "�������", False, True)) Then Exit Sub
        CheckString Reason
    End If
    
    
    If (MsgBox("����������� ������������ �������� �������� '" & SName & "'", vbYesNo) = vbNo) Then Exit Sub

    CDB.SQLOpenTableToArrey "SELECT tblGroups.GroupName, tblGroups.Semester FROM [tblStudents], [tblGroups] WHERE tblStudents.IDGroup = tblGroups.ID AND tblStudents.ID = " & IDStud, DopArray, DopCols, DopRows
    GroupName = CStr(DopArray(0, 0))
    Semester = CByte(DopArray(1, 0))

    If (Opt1.Value) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblStudents] SET [IDGroup] = NULL, [IsActive] = 0, [DeductionDate] = DateValue('" & txtDate.text & "'), [DeductionOrderNumber] = '" & OrderNumber & "', [DeductionReason] = '" & Reason & "', [IsValidReason] = 0, [RestoreInfo] = " & Semester & ", [TRestoreInfo] = '" & GroupName & "' WHERE [ID] = " & IDStud, 128)
        '������������ ������ �������
        FormAcademicTranscript IDStud, txtDate.text, OrderNumber, Reason
        
        If (AR > 0) Then MsgBox "������� ��� ������� ��������."
        txtFIO_Change
        Exit Sub
    End If
    
    If (Opt2.Value) Then
        '������������ ������ �������
        FormAcademicTranscript IDStud, txtDate.text, OrderNumber, Reason
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSheets] WHERE [IDStudent] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes] WHERE [IDStudent] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblDegreeThemes2] WHERE [IDStudent] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblConfirmList] WHERE [IDStudent] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblStudEvents] WHERE [IDStudent] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblSpecialCourses] WHERE [IDStudent] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        AR = CDB.ExecSQLActionQuery("DELETE * FROM [tblStudents] WHERE [ID] = " & CLng(lstFIO.ItemData(lstFIO.ListIndex)), 128)
        If (AR > 0) Then MsgBox "������� ��� ������� ��������."
        txtFIO_Change
        Exit Sub
    End If
    
    If (Opt3.Value) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblStudents] SET [IDGroup] = NULL, [IsActive] = 0, [DeductionDate] = DateValue('" & txtDate.text & "'), [DeductionOrderNumber] = '" & OrderNumber & "', [DeductionReason] = '" & Reason & "', [IsValidReason] = 1, [RestoreInfo] = " & Semester & ", [TRestoreInfo] = '" & GroupName & "' WHERE [ID] = " & IDStud, 128)
        If (AR > 0) Then MsgBox "�������� �������� ���� ������� ��������������."
        txtFIO_Change
        Exit Sub
    End If
    
    If (Opt4.Value) Then
        AR = CDB.ExecSQLActionQuery("UPDATE [tblStudents] SET [IDGroup] = NULL, [IsActive] = 0, [DeductionDate] = DateValue('" & txtDate.text & "'), [DeductionOrderNumber] = '" & OrderNumber & "', [DeductionReason] = '" & Reason & "', [IsValidReason] = 2, [RestoreInfo] = " & Semester & ", [TRestoreInfo] = '" & GroupName & "' WHERE [ID] = " & IDStud, 128)
        If (AR > 0) Then MsgBox "�������� �������� ���� ������� ��������������."
        txtFIO_Change
        Exit Sub
    End If
    
End Sub



Private Sub comboCourse_KeyDown(KeyCode As Integer, Shift As Integer)
KeyCode = 0
End Sub

Private Sub comboCourse_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub comboReason_KeyDown(KeyCode As Integer, Shift As Integer)
Dim AR As Long
Dim Reason As String

If (Shift = 1 And KeyCode = 46) Then
    Reason = ComboReason.text
    CheckString Reason
    
    AR = CDB.ExecSQLActionQuery("DELETE * FROM tblReazonsText WHERE Reazon = '" & Reason & "'", 128)
    LoadReasons
End If

End Sub

Private Sub Command1_Click()
Dim AR As Long
Dim Reason As String
Reason = ComboReason.text
CheckString Reason

If (CDB.SQLGetRecordCount("SELECT [ID] FROM [tblReazonsText] WHERE [Reazon] = '" & Reason & "'") > 0) Then
    MsgBox "����� ������� ��� ����������", vbOKOnly, "������"
    Exit Sub
End If

AR = CDB.ExecSQLActionQuery("INSERT INTO tblReazonsText(Reazon) VALUES ('" & Reason & "')", 128)

LoadReasons
End Sub

Private Sub Form_Load()
Opt1_Click

LoadSuccess = True
txtDate.text = Main.sbMain.PanelText("6")

LoadReasons

If (Main.ExckEditor.Value = 1) Then cmdDel.Enabled = True
End Sub

Private Sub lstFIO_Click()
Dim DopArrey As Variant
Dim DopCols As Integer
Dim DopRows As Long

SName = ""

StartSpecName = ""
SIDS = ""

StartGroupName = ""
StartGroupName.Tag = ""
SIDG = ""

If (lstFIO.ListIndex = -1) Then Exit Sub

Dim SpecName As String
Dim IDSpec As Long
Dim GroupName As String
Dim Semester As Byte
Dim IDGroup As Long
Dim IDStud As Long

SName = lstFIO.List(lstFIO.ListIndex)

IDStud = lstFIO.ItemData(lstFIO.ListIndex)

CDB.SQLOpenTableToArrey "SELECT [IDGroup] FROM [tblStudents] WHERE [ID] = " & IDStud, DopArrey, DopCols, DopRows
IDGroup = DopArrey(0, 0)

CDB.SQLOpenTableToArrey "SELECT [IDSpec], [GroupName], [Semester] FROM [tblGroups] WHERE [ID] = " & IDGroup, DopArrey, DopCols, DopRows
IDSpec = DopArrey(0, 0)
GroupName = DopArrey(1, 0)
Semester = CByte(DopArrey(2, 0))
If (InStr(1, GroupName, "#")) Then GroupName = Left$(GroupName, InStr(1, GroupName, "#") - 1) & IIf(CLng(DopArrey(2, 0)) Mod 2 = 1, (CLng(DopArrey(2, 0)) \ 2) + 1, CLng(DopArrey(2, 0)) \ 2) & Right$(GroupName, Len(GroupName) - InStr(1, GroupName, "#"))

CDB.SQLOpenTableToArrey "SELECT [SpecName] FROM [tblSpecialities] WHERE [ID] = " & IDSpec, DopArrey, DopCols, DopRows
SpecName = DopArrey(0, 0)

StartSpecName = SpecName
SIDS = IDSpec

StartGroupName = GroupName
StartGroupName.Tag = Semester
SIDG = IDGroup

If (Opt3.Value) Then Opt3_Click
If (Opt4.Value) Then Opt4_Click
End Sub


Private Sub Opt1_Click()
txtDate.Enabled = True
txtDate.BackColor = &H80000005

txtOrderNumber.Enabled = True
txtOrderNumber.BackColor = &H80000005

ComboReason.Enabled = True
ComboReason.BackColor = &H80000005

End Sub

Private Sub Opt2_Click()
txtDate.Enabled = False
txtDate.BackColor = &H8000000B

txtOrderNumber.Enabled = False
txtOrderNumber.BackColor = &H8000000B

ComboReason.Enabled = False
ComboReason.BackColor = &H8000000B
End Sub

Private Sub Opt3_Click()
txtDate.Enabled = True
txtDate.BackColor = &H80000005

txtOrderNumber.Enabled = True
txtOrderNumber.BackColor = &H80000005

ComboReason.Enabled = True
ComboReason.BackColor = &H80000005
End Sub

Private Sub Opt4_Click()
txtDate.Enabled = True
txtDate.BackColor = &H80000005

txtOrderNumber.Enabled = True
txtOrderNumber.BackColor = &H80000005

ComboReason.Enabled = True
ComboReason.BackColor = &H80000005
End Sub

Private Sub txtFIO_Change()
Dim StudName As String

Dim Surname As String
Dim Name As String
Dim Patronymic As String

Dim StudArrey As Variant
Dim SCols As Integer
Dim SRows As Long

Dim i As Long

SName = ""

StartSpecName = ""
SIDS = ""

StartGroupName = ""
StartGroupName.Tag = ""
SIDG = ""

txtDate.text = Main.sbMain.PanelText("6")
txtOrderNumber.text = ""
ComboReason.text = ""

StudName = txtFIO.text

If (StudName = "") Then
    lstFIO.Clear
    Exit Sub
End If

'If (InStr(1, StudName, "'") <> 0) Then Exit Sub

Dim FirstS As Byte
Dim SecondS As Byte

FirstS = InStr(1, StudName, " ")
If (FirstS) Then SecondS = InStr(InStr(1, StudName, " ") + 1, StudName, " ")

If ((FirstS = 0) And (SecondS = 0)) Then
    Surname = StudName
    
    CheckString Surname
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] LIKE '" & Surname & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
    
ElseIf ((FirstS <> 0) And (SecondS = 0)) Then
    Surname = Left(StudName, FirstS - 1)
    Name = Mid(StudName, FirstS + 1, Len(StudName) - FirstS)
    
    CheckString Surname
    CheckString Name
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] LIKE '" & Name & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
    
ElseIf ((FirstS <> 0) And (SecondS <> 0)) Then
    Surname = Left(StudName, FirstS - 1)
    Name = Mid(StudName, FirstS + 1, SecondS - FirstS - 1)
    Patronymic = Right(StudName, Len(StudName) - SecondS)
    
    CheckString Surname
    CheckString Name
    CheckString Patronymic
    CDB.SQLOpenTableToArrey "SELECT [ID], [Surname], [Name], [Patronymic] FROM [tblStudents] WHERE [Surname] = '" & Surname & "' AND [Name] = '" & Name & "' AND [Patronymic] LIKE '" & Patronymic & "*' AND [IsActive] = 1 ORDER BY [Surname], [Name], [Patronymic]", StudArrey, SCols, SRows
    
End If

lstFIO.Clear
If (SRows > 0) Then

    For i = 0 To SRows - 1
        lstFIO.AddItem GetFullName(CStr(StudArrey(1, i)), CStr(StudArrey(2, i)), CStr(StudArrey(3, i)), False, False, True)
        lstFIO.ItemData(lstFIO.NewIndex) = StudArrey(0, i)
    Next i
    
    lstFIO.ListIndex = 0
    
Else
    SName.Caption = ""
    StartSpecName.Caption = ""
    SIDS.Caption = ""
    StartGroupName.Caption = ""
    SIDG.Caption = ""
End If

End Sub


Sub FormAcademicTranscript(IDStudent As Long, DateDeduct As String, OrderNumber As String, Reason As String)

Dim StudArrey As Variant

Dim SCols As Integer
Dim SRows As Long

Dim dx As Long
Dim dy As Long

dx = VerifyTwipsX(100)
dy = VerifyTwipsY(400)
Dim i As Long
Dim RowCounter As Long
Dim ColCounter As Long

RowCounter = 3
ColCounter = 1

Dim TotalCountOfHours As Long
TotalCountOfHours = 0

LoadForm frmPrinting

dx = VerifyTwipsX(dx)
dy = VerifyTwipsY(dy)

frmPrinting.VSPrinter1.Clear
frmPrinting.VSPrinter1.Refresh
With frmPrinting.VSPrinter1

    .Orientation = orLandscape

    .MarginBottom = VerifyTwipsY(200)
    .MarginLeft = dx
    .MarginRight = VerifyTwipsX(200)
    .MarginTop = dy

    .StartDoc

    .CurrentY = dy
    .CurrentX = dx
    
    .TablePenLR = 0
    .TablePenTB = 0

    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(900) + dy
    .CurrentX = VerifyTwipsX(11000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "̲Ͳ�������� ��²�� � ����� �������"
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(1200) + dy
    .CurrentX = VerifyTwipsX(8500) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "���ʲ������ ��ֲ�������� �Ͳ�������� ����� �. �. ����ǲ��"
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(1800) + dy
    .CurrentX = VerifyTwipsX(11000) + dx
    .FontSize = 16
    .FontBold = False
    .Paragraph = "�����̲��� ��²��� � _______"
     .FontBold = False
    
        CDB.SQLOpenTableToArrey "SELECT [ID], [EntryYear],[Surname],[Name],[Patronymic] FROM [tblStudents] WHERE [ID] = " & IDStudent, StudArrey, SCols, SRows

    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(2500) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "������ " & StudArrey(2, 0) & " " & StudArrey(3, 0) & " " & StudArrey(4, 0)
    .DrawLine VerifyTwipsX(9900) + dx, VerifyTwipsY(2800) + dy, VerifyTwipsX(16500) + dx, VerifyTwipsY(2800) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(2900) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "��� ��, �� ��/���� ��������(����) "
    
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(3300) + dy
    .CurrentX = VerifyTwipsX(14000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "� 1 ������� " & StudArrey(1, 0) & "  ����"
    .DrawLine VerifyTwipsX(14100) + dx, VerifyTwipsY(3600) + dy, VerifyTwipsX(16000) + dx, VerifyTwipsY(3600) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(3700) + dy
    .CurrentX = VerifyTwipsX(14000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "�� " & txtDate.text & " ����"
     .DrawLine VerifyTwipsX(14200) + dx, VerifyTwipsY(4000) + dy, VerifyTwipsX(16000) + dx, VerifyTwipsY(4000) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(4100) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "� ����������� ������������� ����������� ����� �. �. �������"
    .DrawLine VerifyTwipsX(9100) + dx, VerifyTwipsY(4400) + dy, VerifyTwipsX(16500) + dx, VerifyTwipsY(4400) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(4500) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "�� ��������� ��������-�������������"
    .DrawLine VerifyTwipsX(10500) + dx, VerifyTwipsY(4800) + dy, VerifyTwipsX(16500) + dx, VerifyTwipsY(4800) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(4900) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "����� �������� �����"
    .DrawLine VerifyTwipsX(10800) + dx, VerifyTwipsY(5200) + dy, VerifyTwipsX(16500) + dx, VerifyTwipsY(5200) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(5500) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "ϳ� ��� �������� " & StudArrey(2, 0) & " " & Left(StudArrey(3, 0), 1) & ". " & Left(StudArrey(4, 0), 1) & "."
    .DrawLine VerifyTwipsX(10900) + dx, VerifyTwipsY(5800) + dy, VerifyTwipsX(16500) + dx, VerifyTwipsY(5800) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(5900) + dy
    .CurrentX = VerifyTwipsX(9000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "������(��) ��� ��������� � �����(��) ����� �� ������"
    
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(900) + dy
    .CurrentX = VerifyTwipsX(700) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "³����������(�) " & ComboReason.text
    .DrawLine VerifyTwipsX(2400) + dx, VerifyTwipsY(1200) + dy, VerifyTwipsX(8000) + dx, VerifyTwipsY(1200) + dy

    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(1300) + dy
    .CurrentX = VerifyTwipsX(700) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "�� ������� " & txtOrderNumber.text
    .DrawLine VerifyTwipsX(2000) + dx, VerifyTwipsY(1600) + dy, VerifyTwipsX(8000) + dx, VerifyTwipsY(1600) + dy

    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(3000) + dy
    .CurrentX = VerifyTwipsX(4000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "������ _______________________"
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(3500) + dy
    .CurrentX = VerifyTwipsX(4000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "����� ________________________"
   
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(5000) + dy
    .CurrentX = VerifyTwipsX(4000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = Main.sbMain.PanelText("6")
    .DrawLine VerifyTwipsX(4000) + dx, VerifyTwipsY(5400) + dy, VerifyTwipsX(8000) + dx, VerifyTwipsY(5400) + dy
    
    .TextAlign = taLeftMiddle
    .CurrentY = VerifyTwipsY(5500) + dy
    .CurrentX = VerifyTwipsX(4000) + dx
    .FontSize = 12
    .FontBold = False
    .Paragraph = "������������ ����� ____________"

    .NewPage
    
  CDB.SQLOpenTableToArrey "SELECT tblSubjects.Subject, tblSheets.Mark, tblEducationalPlan.Semester, tblEducationalPlan.NumOfHours " & _
" FROM tblSubjects, tblStudents, tblEducationalPlan, tblExaminations , tblSheets " & _
" Where tblStudents.ID = " & IDStudent & " AND tblSheets.IDStudent = tblStudents.ID  AND tblExaminations.ID = tblSheets.IDExam AND " & _
" tblEducationalPlan.ID = tblExaminations.IDEP AND tblSubjects.ID = tblEducationalPlan.IDSubject " & _
" ORDER BY tblEducationalPlan.Semester", StudArrey, SCols, SRows

    .MarginBottom = VerifyTwipsY(500)
    .MarginLeft = VerifyTwipsX(500)
    .MarginRight = VerifyTwipsX(500)
    .MarginTop = VerifyTwipsX(500)
    
    .CurrentY = VerifyTwipsY(900) + dy
    .CurrentX = VerifyTwipsX(1700) + dx
    
    .StartTable
   ' .Align = taCenterMiddle
    .TableCell(tcCols) = 7
    If (SRows \ 2 > 38) Then
        .TableCell(tcRows) = SRows \ 2
    Else
        .TableCell(tcRows) = 38
    End If
    .TableCell(tcColWidth, 1, 1) = VerifyTwipsX(5000)
    .TableCell(tcColWidth, 1, 2) = VerifyTwipsX(1000)
    .TableCell(tcColWidth, 1, 3) = VerifyTwipsX(820)
     .TableCell(tcColWidth, 1, 4) = VerifyTwipsX(1500)
    ' .TableCell(tcColBorder, 1, 4, SRows, 4) = tbNone
    .TableCell(tcColWidth, 1, 5) = VerifyTwipsX(5000)
    .TableCell(tcColWidth, 1, 6) = VerifyTwipsX(1000)
    .TableCell(tcColWidth, 1, 7) = VerifyTwipsX(820)
    
    
    .TableCell(tcText, 1, 1) = "����� ��������� (�����)"
    .TableCell(tcText, 1, 2) = "���������� ���"
    .TableCell(tcText, 1, 3) = "������"
 .TableCell(tcText, 1, 5) = "����� ��������� (�����)"
    .TableCell(tcText, 1, 6) = "���������� ���"
    .TableCell(tcText, 1, 7) = "������"
   
 
    Dim tabCol As Integer
    Dim j As Integer
    j = 1
    tabCol = 0
   For i = 1 To SRows
   
        If (i = 38) Then
            tabCol = 4
            j = 1
        End If
            
        If (i = 75) Then
            tabCol = 0
            j = 39
        End If
         If (i = 112) Then
            tabCol = 4
            j = 39
        End If
   
        .TableCell(tcRowHeight, j) = VerifyTwipsY(275)
        .TableCell(tcFontSize, j) = 10
        .TableCell(tcText, j + 1, tabCol + 1) = StudArrey(0, i - 1)
        If (IsNull(StudArrey(3, i - 1))) Then
         .TableCell(tcText, j + 1, tabCol + 2) = ""
        Else
           .TableCell(tcText, j + 1, tabCol + 2) = StudArrey(3, i - 1)
        End If
        
        .TableCell(tcText, j + 1, tabCol + 3) = StudArrey(1, i - 1)
        j = j + 1
    Next i
    
    .TableCell(tcText, 1, 1) = "����� ��������� (�����)"
    .TableCell(tcText, 1, 2) = "���������� ���"
    .TableCell(tcText, 1, 3) = "������"
    .TableCell(tcText, 1, 5) = "����� ��������� (�����)"
    .TableCell(tcText, 1, 6) = "���������� ���"
    .TableCell(tcText, 1, 7) = "������"
    
    .TableCell(tcText, 38, 1) = "����� ��������� (�����)"
    .TableCell(tcText, 38, 2) = "���������� ���"
    .TableCell(tcText, 38, 3) = "������"
    .TableCell(tcText, 38, 5) = "����� ��������� (�����)"
    .TableCell(tcText, 38, 6) = "���������� ���"
    .TableCell(tcText, 38, 7) = "������"
    
    .EndTable
    

    .EndDoc

End With

frmPrinting.Show 1, Me
                         
End Sub


