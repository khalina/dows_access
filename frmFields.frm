VERSION 5.00
Object = "{D76D7130-4A96-11D3-BD95-D296DC2DD072}#1.0#0"; "Vsflex7d.ocx"
Begin VB.Form frmFields 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "����� ����"
   ClientHeight    =   5295
   ClientLeft      =   5100
   ClientTop       =   1845
   ClientWidth     =   9495
   Icon            =   "frmFields.frx":0000
   LinkTopic       =   "�����1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5295
   ScaleWidth      =   9495
   ShowInTaskbar   =   0   'False
   Begin VB.Frame F3 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   4560
      Width           =   9495
      Begin VB.CommandButton cmdCancel 
         Caption         =   "������"
         Height          =   375
         Left            =   7560
         TabIndex        =   4
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "��"
         Height          =   375
         Left            =   5760
         TabIndex        =   3
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame F2 
      Caption         =   "������ �����"
      Height          =   4575
      Left            =   3480
      TabIndex        =   1
      Top             =   0
      Width           =   6015
      Begin VSFlex7DAOCtl.VSFlexGrid tblFields 
         Height          =   4215
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   5775
         _cx             =   10186
         _cy             =   7435
         _ConvInfo       =   -1
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   -2147483635
         ForeColorSel    =   -2147483634
         BackColorBkg    =   -2147483636
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   1
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   1
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   0
         Cols            =   0
         FixedRows       =   0
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   0   'False
         FormatString    =   ""
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   0   'False
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   0
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   0
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         DataMode        =   0
         VirtualData     =   -1  'True
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   3
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
      End
   End
   Begin VB.Frame F1 
      Caption         =   "������ ������"
      Height          =   4575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3375
      Begin VB.ListBox lstTables 
         Height          =   4155
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   3135
      End
   End
End
Attribute VB_Name = "frmFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdCancel_Click()
    SQLModule.SelectedTable = -1
    SQLModule.SelectedField = -1

    Unload Me
End Sub

Private Sub cmdOK_Click()
If (tblFields.Rows = 1) Then Exit Sub
If (tblFields.Row < 1) Then Exit Sub

If (Not FCondition) Then
    SQLModule.SelectedTable = lstTables.ItemData(lstTables.ListIndex)
    SQLModule.SelectedField = tblFields.RowData(tblFields.Row)
Else
    frmCondition.SelectedTable = lstTables.ItemData(lstTables.ListIndex)
    frmCondition.SelectedField = tblFields.RowData(tblFields.Row)
End If
Unload Me
End Sub

Private Sub Form_Load()
Dim iTable As Integer

tblFields.Cols = 4
tblFields.Rows = 1
tblFields.FixedRows = 1
tblFields.FixedCols = 1
tblFields.TextMatrix(0, 0) = "�"
tblFields.TextMatrix(0, 1) = "�������� ����"
tblFields.TextMatrix(0, 2) = "��� ������"
tblFields.TextMatrix(0, 3) = "������"

lstTables.Clear
For iTable = 0 To SQLTablesCount - 1
    If (TablesArrey(iTable).Status = 1) Then
        lstTables.AddItem TablesArrey(iTable).TableNameRu
        lstTables.ItemData(lstTables.NewIndex) = iTable
    End If
Next

If (lstTables.ListCount > 0) Then lstTables.ListIndex = 0
ResizeColumns tblFields

LoadSuccess = True
End Sub

Private Sub lstTables_Click()
Dim iField As Integer
Dim ST As Integer

If (lstTables.ListCount = 0) Then Exit Sub
If (lstTables.ListIndex < 0) Then Exit Sub

ST = lstTables.ItemData(lstTables.ListIndex)

tblFields.Rows = 1

For iField = 0 To TablesArrey(ST).FieldsCount - 1
    If ((TablesArrey(ST).FieldsArrey(iField)(5) = 0 Or FCondition) And _
       (Not ((UCase(Left(TablesArrey(ST).FieldsArrey(iField)(1), 2)) = "ID") And ((UCase(Left(TablesArrey(ST).FieldsArrey(iField)(2), 4)) = "LONG") Or (UCase(Left(TablesArrey(ST).FieldsArrey(iField)(2), 7)) = "COUNTER"))))) Then
       
       tblFields.AddItem tblFields.Rows & vbTab & TablesArrey(ST).FieldsArrey(iField)(0) & vbTab & UCase(TablesArrey(ST).FieldsArrey(iField)(2)) & vbTab & TablesArrey(ST).FieldsArrey(iField)(3)
       tblFields.RowData(tblFields.Rows - 1) = iField
       
    End If
Next

ResizeColumns tblFields
End Sub
